<?php
/**
 * Created by PhpStorm.
 * User: jakuburbis
 * Date: 08.08.18
 * Time: 14:08
 */

?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121192568-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-121192568-2');
</script>