<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Eat Stop Eat Optimized</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=11">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/ese/web/i/fb-og-ese.png">
	<meta property="og:description" content="Eat Stop Eat: In Just One Day This Simple Strategy Frees You From Complicated Diet Rules — And Eliminates Rebound Weight Gain">
	<meta property="og:title" content="Check Out What I'm Reading...">

<script>

$(function(){

    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });

});

</script>

<?php require_once('../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="tagline">
        <div class="inner">
            The Cult-Hit Companion To The Bestselling Book, Eat Stop Eat
        </div>
    </div>

    <div id="breadcrumbs">
        Books › Weight Loss Books › Best Sellers
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <h1>ESE Optimized</h1>
                <p class="byline">By Brad Pilon</p>
                <p class="rating"><i>2017 Revised Edition</i></p>
            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img class="mobile-half-width" src="web/i/ecover.jpg?v=1" width="100%" alt="">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png?v=1" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>
                        Reduced Price For <span class="current-month-name">{%CurrentMonthName%}</span>
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$9*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$10.99</b> (50%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$9</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://67.eatstopeat.pay.clickbank.net/?cbfid=28971&vtid=clkbooks&cbskin=18285"><span class="inner">Buy Now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                    <h1>ESE Optimized</h1>
                    <p class="byline">By Brad Pilon</p>
                    <p class="rating"><i>2017 Revised Edition</i></p>
                </div>

                <p>The author of the bestselling <i>Eat Stop Eat</i> is famous for his self experimentation.</p>
                <p>And if the original <i>Eat Stop Eat</i> delivers all the <b><i>facts</i></b> about Intermittent Fasting &mdash; from labs and clinical research &mdash; then Brad’s follow up companion, <i>Eat Stop Eat <u>Optimized</u></i>, is the <u>very intimate <b>story</b></u> of Intermittent Fasting from Brad’s own personal experiences.</p>
                <p><i>Eat Stop Eat</i> is the <b>Science</b>.</p>
                <p><i>Eat Stop Eat <u>Optimized</u></i> is the <b>Art</b>.</p>
                <p>When we asked Brad how he came up with the idea for writing <i>Optimized</i> he told us:</p>

                <div class="read-more-content collapsed hidden">

                    <p><i>“I never intended to share these tricks with anyone&hellip;”</i></p>
                    <p><i>“Every time I’d post a pic of one of my meals to Twitter I’d be amazed at the flood of responses. In the end that is what convinced me to to write this unique extension of the Eat Stop Eat experience&hellip;”</i></p>
                    <p>Brad has used the <i>Eat Stop Eat</i> principles to reach many different goals. He has lost weight. He has gained muscle. He has taken care of health issues and improved markers of health. Yet he has never focused on all of these goals at once.</p>
                    <p>In <i>Optimized</i> he shows you how to apply the principles in very unique and specific ways to tailor the experience to your own personal needs.</p>
                    <p>He also shows you how he stuck with Eat Stop Eat during very time-challenged periods in his life when he was juggling two young kids, multiple businesses, family health tragedies and more.</p>
                    <p>In short, Brad has experimented with Eat Stop Eat in pretty much any way you can imagine. And he now understands that sharing all of that with you is the fastest way to make sure you find your own perfect path to reach your unique goals as efficiently and easily as possible.</p>
                    <p>In this priceless companion book to the original <i>Eat Stop Eat</i> you’ll get a peek into&hellip;</p>

                    <ul class="checkmarks checkmarks-full">
                        <li>Exactly how Brad eats</li>
                        <li>Pics of the foods he ate leading up to his now-famous photo shoot</li>
                        <li>His exact daily calories</li>
                        <li>The supplements he took to reach each goal</li>
                        <li>His mindset tricks for Intermittent Fasting</li>
                    </ul>

                    <p>And you’ll also discover why&hellip;</p>

                    <ul class="checkmarks checkmarks-full">
                        <li>He doesn’t exercise when he’s fasting</li>
                        <li>He NEVER snacks on Protein</li>
                        <li>Sometimes he cuts his fasts short to optimize the fat loss process</li>
                        <li>He never worries about eating lots of carbs or fats</li>
                        <li>And much more&hellip;</li>
                    </ul>

                    <p>If you want to discover how to get the <u><b>best results from <i>Eat Stop Eat</i> with the least possible effort</b></u>, then <i>Optimized</i> is your golden ticket.</p>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>
                        Reduced Price For <span class="current-month-name">{%CurrentMonthName%}</span>
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$9*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$10.99</b> (50%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$9</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://67.eatstopeat.pay.clickbank.net/?cbfid=28971&vtid=clkbooks&cbskin=18285"><span class="inner">Buy Now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>


</body>
</html>
