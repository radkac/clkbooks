<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Rule Breaker Diet</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="web/s/global.css?v=1" rel="stylesheet" type="text/css">
    <link href="web/s/mobile.css?v=1" rel="stylesheet" type="text/css">

    <link href="web/s/slick/slick.css" rel="stylesheet" type="text/css">
    <link href="web/s/slick/slick-theme.css" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

$(function(){

    $('.read-more A').click(function(){
        var collapsed = $('.read-more-content').hasClass('hidden');
        $(".read-more-content").toggle(400);
        $(".read-more").hide();
    });

});

</script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="headline">
    <div class="inner">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<main>
    <section>

        <h1><b>The Rule Breaker Diet</b></h1>
        <h3><b>Discover Which Misguided Diet Rules Are Actually Making You Fatter</b></h3>

        <p>The diet landscape is littered with regular folks who got fat by faithfully following the latest diet rules and eating all the so-called “healthy” foods.</p>
        <p>So if you feel like diet after diet has failed you, it’s definitely not your fault.</p>
        <p>You see, 1000s of new <i>“diet rules”</i> are born every year. How can you possibly respect them all? For that matter&hellip; how can they even be true?</p>
        <p>In fact, 99.9% of those diet “rules” are garbage. There may be a fragment of truth in each of them &mdash; some little headline from a research paper someone thought would be catchy enough to build a diet around.</p>
        <p>However, the real truth is this&hellip; If you want a sustainable way to lose weight and maintain a healthy weight for the rest of your life, you need to be willing to BREAK THE RULES&hellip;</p>
        <p>And if you get nothing else from reading this page&hellip; at least take this. The most destructive “category” of diet rules that you must break immediately is this&hellip;</p>

<div class="read-more-content hidden">

        <p>It’s the crazy notion that there are “evil” foods. Foods that you must not eat no matter what. Foods that are single handedly responsible for the obesity epidemic and to blame for all your struggles&hellip;</p>
        <p>Please&hellip;</p>
        <p>The truth is the entire notion of “evil” foods is probably responsible for more weight gain than any other misguided nutrition myths.</p>
        <p>Let me explain&hellip;</p>
        <p>Imagine this for a second. You’re sitting on the couch, and all of a sudden you get super motivated. You jump up, rush out the door, and just start running.</p>
        <p>You don’t jog. This is a full on Forest Gump run. And you just keep on going. Until&hellip;</p>
        <p>You just can’t run anymore. You’re out of fuel. Just like a car with an empty tank, you can’t go any further.</p>
        <p>This is pretty easy to understand, right? It’s something that’s easy to wrap your head around.</p>
        <p>What most people don’t realize is this: Just like your body, <u>your mind and emotions have a limited reserve of energy</u> they can rely on.</p>
        <p>The easiest way to think of this mental and emotional energy is to call it your <b>willpower</b>.</p>
        <p>And guess what&hellip; the fastest way to drain your willpower is to resist the forbidden.</p>
        <p>Let’s face it. Since we were old enough to know the difference between “mine” and “yours,” we’ve all been drawn to <b>want whatever we are told we can’t have</b>&hellip;</p>
        <p>And each time you resist temptation&hellip; each time you say no to that “evil” or “forbidden” food&hellip; you drain a little bit more willpower from your limited supply.</p>
        <p>Add to that a bad day at work, a fight with your spouse, some bad financial news, or anything else that drains your emotional or mental reserves, and you can guess what happens.</p>
        <p>Now here’s the REALLY bad part&hellip;</p>
        <p>Once your willpower breaks, it’s like opening a floodgate. There’s no closing it.</p>
        <p>Tell me if this sounds familiar&hellip;</p>
        <p>You’ve been “good” on your diet for days, maybe even weeks. And then one bite of a “forbidden” food turns into a mindblowing binge.</p>
        <p>Just that one taste flicked a switch and made you say something like&hellip;</p>
        <p><i>“Oh heck, I already screwed up, so I might as well just go for it&hellip;”</i></p>
        <p>Look: You’re not the only one, ok. This is normal. It’s even to be expected if you’re trying to eliminate a specific food from your diet.</p>
        <p>It’s just not human nature to be able to keep that up for any length of time.</p>
        <p>Which is why I keep coming back to one very simple secret to losing weight&hellip;</p>
        <p><b>You need to be a rule breaker.</b></p>
        <p>You need to be skeptical about the diet information you’re being fed. (pun intended)</p>
        <p>Did you know there are diets that actually list carrots as an evil food? Seriously?</p>
        <p>So if you aren’t sure about this whole rule breaker thing, that’s evidence enough that you should approach every diet rule you hear as suspicious.</p>
        <p>Listen: of course there are principles that make it easier to lose weight. There are lots of ways to give yourself an edge and increase your chances of success.</p>
        <p>However, none of them are diehard rules. There’s nothing on a list of things you can never eat again. And&hellip;</p>
        <p>There is no reason to EVER allow yourself to be a casualty of the “fitness freak” mentality that forces you to sacrifice your social life and alienate friends and family because you’re “on a diet.”</p>
        <p>I don’t want you to suffer anymore from the misguided and sometimes dishonest diet rules that make it harder instead of easier to lose weight.</p>
        <p>And I know you don’t want that for yourself&hellip;</p>
        <p>Which is why I wrote <b><i>The Rule Breaker Diet</i></b>. To set you free from the damaging myths and lies that sabotage you every time you finally find the motivation to get back in shape.</p>
        <p>Inside <i>The Rule Breaker Diet</i>, I cover all the things you <u>really</u> need to know including&hellip;</p>

        <ul class="check green">
            <li>Why your “metabolism” doesn’t matter and how a “fast” metabolism might be very, very bad</li>
            <li>How fruits were wrongfully convicted of fat crimes</li>
            <li>How losing weight too fast almost always backfires</li>
            <li>Why physical activity doesn’t matter for weight loss (at least not in the way you think&hellip;)</li>
            <li>What the “Weight Loss Distortion Field” is and why it makes weight loss harder</li>
            <li>Why “protein rules” may be the most destructive diet myths</li>
            <li>The exercise mistake almost everyone makes that expands your belly</li>
            <li>The lie of <i>“fat doesn’t make you fat”</i> &mdash; and how dietary fat is actually the easiest food for your body to turn into belly fat</li>
            <li>Why celebs are the WORST people to look to for diet advice</li>
            <li>The “calorie trick” that puts you in control of your weight loss</li>
            <li>Why dieting in “reverse” allows you to EAT MORE as you lose more weight</li>
            <li>Why it doesn’t matter how often you eat or at what time</li>
            <li>And much, much more&hellip;</li>
        </ul>

        <p>If you’re ready to toss out the silly rules every other diet book is trying to force down your throat&hellip; And if you can accept the idea that dieting should be a <b>positive experience</b>&hellip;</p>

        <div class="insertion right">
            <center><img src="web/i/zhtml/placeholders/gray-200x300px.jpg" alt=""></center>
        </div>

        <p>Then <b><i>The Rule Breaker Diet</i></b> is exactly what you’ve been looking for&hellip;</p>
        <p>And to make your weight loss even easier and more enjoyable, I’m also giving you FREE access today to powerful software that makes <i>The Rule Breaker Diet</i> practically DONE-FOR-YOU&hellip;</p>
        <p>It’s called the <b>RBD Calculator</b>, and thanks to the help from CLKbooks.com, I’m able to give you free lifetime access to the calculator when you purchase the book on this page.</p>
        <p>This is normally a $147 value, so I’m grateful that CLKbooks.com is allowing me to offer it to you for free today!</p>
        <p>That’s not all though. On this page only, when you invest in the book, I’m also giving you THREE more tools I created for people who are ready for simple and lifelong weight loss.</p>
        <p>I usually sell these little books for just $7 each. So this is just a little token of my appreciation for putting your trust in me.</p>

        <hr>

        <h3><b>
            Your First Free Gift Is 100% Belief In Yourself And<br>
            Your Worthiness To Have The Body You Want
        </b></h3>

        <p>Your body image and beliefs about eating have been twisted into a nightmarish monster by the media and entertainment industries.</p>
        <p>Even folks with amazing bodies are tormented by self doubt about how they look and how they should be eating.</p>
        <p>It’s so, so sad that such a natural and enjoyable part of being human has been so terribly turned against us.</p>
        <p>Which is why I wrote a short book called Thinking Thin. This quick read will reset your beliefs about food and give you the tools to rebuild a realistic and healthy image of your body and your potential.</p>

        <hr>

        <h3><b>
            Your Second Free Gift Is A Life Where Social Eating and Everyday Enjoyment Of Food Go Hand In Hand With Your Slim & Healthy Body
        </b></h3>

        <p>Listen: Life is simply too short to put it on hold while you lose weight. Food and drink are a central part of being a happy and social human being. They always have been.</p>
        <p>And with the strategies in my short book called <i>The Social Eating Survival Guide</i>, you can be a full and rich participant in your life and still lose all the weight you want.</p>
        <p>It’s not an “either / or” choice you have to make. You really can have it both ways. And I’ll give you the tools to make it easy, ok?</p>

        <hr>

        <h3><b>
            Your Third Free Gift Is A Bird’s Eye View Of Exactly Where<br>
            You Are Going And How Much Progress You’re Making
        </b></h3>

        <p>Sometimes it’s easy to lose sight of the big picture. You get caught in the daily grind, and you forget to take notice of just how great you are doing.</p>
        <p>The <i>Success Tracker Journal</i> is an easy-to-use interactive document that helps you quickly plan out your week while you enjoy seeing how far you’ve already come.</p>

        <hr>

        <h3><b>And You Get Everything Today For Just $29</b></h3>

        <p>I’m happy to announce that working with CLKbooks.com has allowed me to cut out a lot of the costs of self-publishing and programming the calculator. Which means I’m able to drop the price from the original $37 to just $29 for folks going through CLKbooks.com.</p>
        <p>Remember, you’re also getting the <i>RBD Calculator</i> worth $147 PLUS <i>Thinking Thin</i>, <i>The Social Eating Survival Guide</i>, and the <i>Success Tracker Journal</i> &mdash; a combined $21 value &mdash; as my free gifts to you today.</p>
        <p>And your entire purchase is covered by the CLKbooks.com 60-day money-back guarantee so you can get your hands on everything today and have a full 60 days to review and try it all completely risk free.</p>
        <p>To get everything right now for a single payment of just $29 use the button below:</p>

        <p class="purchase">
            <a class="button button-buy-link" href="#">ADD TO CART</a><br><br>
            <img src="web/i/payment-methods.jpg" width="260" alt="">
        </p>

</div><!-- .read-more-content -->

        <p class="read-more ta-center">[<a href="javascript:;">Read More</a>]</p>

    </section>
</main>

<footer class="cl-white bg-blue">
    <section>

        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>

    </section>
</footer>

</body>
</html>
