<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/Tracking/upsell-analytics-vtid-logic.php';

$vtid = getVtid();
$vtid = resolveVTID($vtid, 'ar', false);
// CB upsell buy links
$linkAccept = getClickbankBuyLink('200', null, 'eatstopeat', array(
    'cbur' => 'a',
    'vtid' => $vtid,
));
$linkDecline = getClickbankBuyLink('200', null, 'eatstopeat', array(
    'cbur' => 'd',
    'vtid' => $vtid
));
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Add Recipe Guide</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function resizeVideoPlayer() {
	var new_width = $('#video_player').width();
	var new_height = Math.floor(411*new_width/730);
	$('#video_player').height(new_height);
	//alert(new_width+' '+new_height);
}

function showWarning() { $('.warning').fadeTo('slow',1) }
function showContent() { $('#layout').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
	setTimeout('resizeVideoPlayer()', 3100);
});

$(window).resize(function() {
	resizeVideoPlayer();
});

</script>
<?php include $_SERVER['DOCUMENT_ROOT'].'/tracking/ga-tracking.php'; ?>
</head>
<body>

<div id="header" class="clearfix">
    <div class="inner clearfix">

        <div class="intro">
            <div class="order-steps">
                <p><img class="mobile-full-width" src="/web/i/order-steps-<?=isset($_GET['cbur'])?'stat.png':'anim.gif'?>"></p>
                <h4 class="warning hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</h4>
            </div>
        </div>

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="hidden clearfix">

    <div id="content" class="rich-text clearfix">

        <h1>
            If You Are Not A Whiz In The Kitchen&hellip;<br>
            Consider Adding This Special “Shortcut” Option<br>
            That Makes The Rule Breaker Diet Practically<br>
            “Done-For-You”
        </h1>

        <p>Hey! It’s John here&hellip;</p>
        <p>I want to congratulate you on investing in the Rule Breaker Diet today. In just a minute you’ll have your hands on everything you need to finally take control of your weight so you look and feel exactly how you want&hellip;</p>
        <p>However if you’re like me, you’re always looking for shortcuts that make life easier. Which is why I put together something very, very special for you.</p>
        <p>And on this page I’m even going to make sure you get this awesome shortcut at a terrific price.</p>
        <p>Listen: After years of working in the fitness and supplement industries, I can tell you that the #1 reason people’s diets go off the rails is a lack of variety.</p>
        <p>You see, even if you know WHAT to do and how to eat, maybe you don’t know how to make those foods tasty, affordable and easy to prepare.</p>
        <p>And that’s where the Rule Breaker Recipe Guide comes in!</p>

        <h3><b>This Is The “Done-For-You” Add-On For The Rule Breaker Diet&hellip;</b></h3>

        <p>You’ll have simple and super tasty options for breakfast, lunch, dinner and even snacks.</p>
        <p>And rest assured, I put all these recipes to the ultimate test.</p>
        <p>You see, ask anyone who knows me, and they’ll confirm&hellip; I have absolutely zero talent in the kitchen.</p>
        <p>So if I can make these amazing meals that taste great, then anyone can!</p>
        <p>And the best part is this&hellip;</p>

        <h3><b>You’re Covered By My 100% Guarantee For 60 Days</b></h3>

        <p>Which means you have the time to prepare and taste every single meal for yourself. And if you aren’t blown away by how much easier it is to lose weight using these terrific and tasty recipes then just let me know, and I’ll refund every penny of your investment.</p>
        <p>Easy decision, right?</p>
        <p>Just use the button below to add the Rule Breaker Recipe Guide to your order today and try all the great, great recipes for yourself, completely risk free, for 60 days!</p>
        <p>PLUS, when you take action today on this page only, I’m also going to knock $30 off the retail price so you can grab it for only $19.</p>
        <p>Act now to secure this low price though because it’s not available once you leave this page.</p>

        <div class="insertion center" style="margin-bottom:-1.35em !important;">
            <center><img class="mobile-full-width" src="web/i/ecover-recipe.jpg" width="500" alt=""></center>
        </div>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="<? $linkAccept ?>">Add To Order<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
            </p>

            <hr>

            <p class="discard">No thanks. <a href="<?= $linkDecline ?>">I will pass on this forever.</a>.</p>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->

</body>
</html>
