<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/Tracking/upsell-analytics-vtid-logic.php';

$vtid = getVtid();
$vtid = resolveVTID($vtid, 'aw', false);
// CB upsell buy links
$linkAccept = getClickbankBuyLink('199', null, 'eatstopeat', array(
    'cbur' => 'a',
    'vtid' => $vtid,
));
$linkDecline = getClickbankBuyLink('199', null, 'eatstopeat', array(
    'cbur' => 'd',
    'vtid' => $vtid
));
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Add Workout</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function resizeVideoPlayer() {
	var new_width = $('#video_player').width();
	var new_height = Math.floor(411*new_width/730);
	$('#video_player').height(new_height);
	//alert(new_width+' '+new_height);
}

function showWarning() { $('.warning').fadeTo('slow',1) }
function showContent() { $('#layout').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
	setTimeout('resizeVideoPlayer()', 3100);
});

$(window).resize(function() {
	resizeVideoPlayer();
});

</script>

<?php include $_SERVER['DOCUMENT_ROOT'].'/tracking/ga-tracking.php'; ?>
</head>
<body>

<div id="header" class="clearfix">
    <div class="inner clearfix">

        <div class="intro">
            <div class="order-steps">
                <p><img class="mobile-full-width" src="/web/i/order-steps-<?=isset($_GET['cbur'])?'stat.png':'anim.gif'?>"></p>
                <h4 class="warning hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</h4>
            </div>
        </div>

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="hidden clearfix">

    <div id="content" class="rich-text clearfix">

        <h1>
            Warning: Do Not Exercise While Using <i>The Rule<br>
            Breaker Diet</i> Until You Read This Entire Page
        </h1>

        <p>Hey, it’s John again with one final option to add to your Rule Breaker Diet.</p>
        <p>By now you understand that breaking the popular diet rules is the fastest way to lose weight and get the body you desire.</p>
        <p>So it may not surprise you to discover that most of the workout “rules” people stick to are actually making it harder to lose weight and in some cases are even making you fatter!</p>
        <p>Listen: There’s no question that the right exercise program is by far the best way to get faster results from <i>The Rule Breaker Diet</i>. However&hellip;</p>
        <p>There are a lot of exercise “rules” you’re going to have to break along the way if you want to get the best, fastest and easiest results.</p>

        <h3><b>You Deserve The Maximum Results From The Minimum Work</b></h3>

        <p>To be blunt, most exercise programs are barbaric. They throw the kitchen sink at you. Make you sweat. Make you suffer. Make you sore.</p>
        <p>The idea is, if you “feel” like you’re working hard, they’ve got you tricked into thinking the program must be working.</p>
        <p>Guess what&hellip;</p>
        <p>80% of the work you’re doing on those programs is useless. 20% is probably enough to get the results you need.</p>
        <p>And the other 80% is just making you miserable, gobbling up your time, and probably damaging your health and making it harder to lose weight!</p>
        <p>Which is why I created the Rule Breaker 12-Week Workout. So you ONLY have to do the most important 20% that gets you almost all your results in a fraction of the time.</p>
        <p>That means you still get to enjoy your free time. And&hellip;</p>
        <p>You’ll probably even look forward to your workouts instead of dreading them like most folks do!</p>
        <p>Yet as always… here’s the best part:</p>

        <h3><b>Just Try The Workouts For Yourself Over The Next 60 Days, Risk-Free</b></h3>

        <p>By now you know I back everything up with my 100% no-questions-asked money-back guarantee.</p>
        <p>That’s because I want you to give yourself every advantage. I want you to have the results you desire and deserve. And I don’t want any doubts getting in your way of reaching your goal.</p>
        <p>So just try the workouts. Try the first day. The first week. The first month. Or a full 60 days. If you don’t love this new way of exercising just let me know and you get every penny back. No hard feelings.</p>
        <p>And if you love it as much as most folks do, then you’ve gained a fun and terrific way to stay slim, healthy and young.</p>
        <p>PLUS, if you have any last hesitation, let me fix that for you. When you take action here on this page, I’m also going to drop the regular retail price and give you $20 off so you can have all 12 weeks of workouts for just $24.95.</p>
        <p>Grab it now and try it for yourself. There’s no risk to you. And today you’re saving $20 off the regular retail just so you can give it a try for yourself.</p>

        <div class="insertion center" style="margin-bottom:-1.35em !important;">
            <center><img class="mobile-full-width" src="web/i/ecover-workout-2.jpg" width="500" alt=""></center>
        </div>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="<?= $linkAccept ?>">Add To Order<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
            </p>

            <hr>

            <p class="discard">No thanks. <a href="<?= $linkDecline ?>">I will pass on this forever.</a>.</p>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->

</body>
</html>
