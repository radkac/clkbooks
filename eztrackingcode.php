<?php
$domain = $_SERVER['HTTP_HOST'];
$wwwpos = strpos($domain,"www");

if ($wwwpos !== FALSE){
	$domain = substr($domain, $wwwpos+4);
};

switch ($domain){
	case "bodyweightburn.com":
		$tag = "GTM-5ZJXC7";
		break;
	case "dresssizediet.com":
		$tag = "GTM-NHLJ82";
		break;
	case "halfdaydietplan.com":
		$tag = "GTM-W4L66R";
		break;
	case "halfdayfactor.com":
		$tag = "GTM-KVR3WN";
		break;
	case "jeansdiet.com":
		$tag = "GTM-WK4RSN";
		break;
	case "thebetaswitch.com":
		$tag = "GTM-WQ5WSJ";
		break;
	case "yogafitnessflow.com":
		$tag = "GTM-5TMR6R";
		break;
	case "alphanation.com":
		$tag = "GTM-N5J2BW";
		break;
	case "anabolicfinishers.com":
		$tag = "GTM-MD77JB";
		break;
	case "specforceabs.com":
		$tag = "GTM-PPMH53";
		break;
	case "specforcefactor.com":
		$tag = "GTM-3FFLTL";
		break;
	case "specforcefit.com":
		$tag = "GTM-WJVBFD";
		break;
	case "eatstopeat.com":
		$tag = "GTM-5WQZXQ";
		break;
	case "bodyweightcoach.com":
		$tag = "GTM-5RQQW6";
		break;
	case "yogafatlossflow.com":
		$tag = "GTM-WZ4HPZ";
		break;
	case "truthaboutprotein.com":
		$tag = "GTM-T39KL9";
		break;
	case "theflatbellyfix.com":
		$tag = "GTM-K8BBSS";
		break;
	case "therenewbook.com":
		$tag = "GTM-5RBJMP";
		break;
	case "tight-n-toned.com":
		$tag = "GTM-PFCSZF";
		break;
	case "fitfamilyfood.com":
		$tag = "GTM-5HG5RNV";
		break;
	case "tcyclefactor.com":
		$tag = "GTM-5HT5B7";
		break;
	case "clkbooks.com":
		$tag = "GTM-M9V5WKH";
		break;
	default:
		$tag = "null";
};

$directory = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$pii_params = array('payer_email','first_name','last_name','option_selection2','cname','cemail','czip','email','login','from','token','param','emailaddress','creditcards_name','address_street','street_number','phone','ipaddress','name','email','st_addr1','st_addr2','st_city','st_zip','st_province','st_country','ccountry');
$path_pii_removed = $directory;
if (count($_GET)!==0){
	foreach ($_GET as $key=>$value){
		if(NULL == (array_search($key,$pii_params))){
			$removed_pii_params[$key]=$value;
		}
	}
	if (count($removed_pii_params)>=1){
		$qs_pii_removed = http_build_query($removed_pii_params);
		$path_pii_removed = $directory.'?'.$qs_pii_removed;
	}	
} else {
	$path_pii_removed = $directory;
}

$included_files = get_included_files();

foreach ($included_files as $filename) {
	$includefetchdata = strpos($filename,"fetchdata");
    if ($includefetchdata !== FALSE){
		$fetchdataexists = 1;
		echo "<script>
		dataLayer = [{
		'transactionId':'$receipt',
		'transactionAffiliation':'$affiliate',
		'transactionTotal':$amount ,
		'transactionProducts': [{
			'sku':'$item',
			'name':'$code',
			'price':$amount,
			'quantity': 1
		}],
		'Page Path - PII Removed':'$path_pii_removed'
		}];

		</script>";
	}
};
if ($fetchdataexists !== 1){
	echo "<script> dataLayer = [{'Page Path - PII Removed':'$path_pii_removed'}];</script>";
}

echo <<<GTMTAG
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','$tag');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=$tag"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
GTMTAG;
?>