<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Discover The Secret Of The Lucky Few Who Eat Whatever They Want And Never Get Fat</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="../web/s/zhtml.css?v=4">
    <link rel="stylesheet" type="text/css" href="../web/s/global.css?v=4">
    <link rel="stylesheet" type="text/css" href="../web/s/mobile.css?v=4">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>

$(function(){
    $('.sci-refs-switch').click(function(){
        $('.sci-refs').show(1000);
    });
    $('.sci-ref').click(function(){
        $('.sci-refs').show();
    });
});

</script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>


<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="../web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="../web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>


<div class="section default first">
    <div class="content rich-text">

        <h1>
            Discover The Secret Of The <u>Lucky Few</u> Who Eat<br>
            Whatever They Want And <u>Never Get Fat</u>
        </h1>

        <div class="hint"><span class="bg-blue"><b>Hint:</b> It Has Nothing To Do With Their Genetics&hellip;</span></div>

        <div class="insertion right" style="margin-left:20px;">
            <center><img class="mobile-3of4-width" src="../web/i/goodbellybadbelly/st027.png" width="270" alt=""></center>
        </div>

        <p>I just wanted to just slap her skinny face with my slice of pizza!&hellip;</p>
        <p>Listen, I love my best friend Angie, but sometimes her and her stupid “skinny” genes made me feel&hellip;</p>
        <p>&hellip;I don’t know&hellip; <b>Cursed</b> I guess!</p>
        <p>And it brought out the worst in me. Sometimes I’d even catch myself looking at her tight little figure and praying she would start packing on weight &mdash; the way I did a few years back.</p>
        <p>You see&hellip; I didn’t always have trouble with my weight.</p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-white bg-green">
        It Seemed Like Overnight,<br>
        Diet &amp; Exercise Just STOPPED Working
    </div>
    <div class="content rich-text">

        <p>Truth is, my guilty fantasies about Angie getting fat were really because I used to be slim like her too!&hellip;</p>
        <p>Like Angie, I could polish off a whole pizza by myself. I used to enjoy a small bowl of my favorite Cherry Garcia ice cream whenever the heck I wanted. I could have a glass of wine every night with dinner&hellip;</p>
        <p>And I could eat normal meal portions and never have to feel hungry. All while staying slim and feeling great&hellip;</p>
        <p>Then something changed&hellip; and it felt like it slapped me in the face literally overnight.</p>

        <div class="insertion right">
            <center><img class="mobile-3of4-width" src="../web/i/goodbellybadbelly/st382.png" width="270" alt=""></center>
        </div>

        <p>All of a sudden even a TINY “cheat” on the weekend started leaving me feeling bloated, lethargic and HELPLESS on Monday morning&hellip;</p>
        <p>And just a single spoon of ice cream would send me into a tailspin of cravings and binging that would compel me to polish off the entire tub!&hellip;</p>
        <p>Worst of all, unless I forced myself to eat tiny portions and suffer constant hunger, the scale would keep creeping up and I would put on a bit more fat every single week&hellip;</p>
        <p>I was desperate to end the pain and the tears. And I tried pretty much every diet you can think of:</p>

        <div class="columns think-of">
            <div class="column cl-1-4 mobile-hidden">&nbsp;</div>
            <div class="column cl-1-4">

                <ul class="cross red">
                    <li>Low Carb</li>
                    <li>Low Fat</li>
                    <li>The Paleo Diet</li>
                </ul>

            </div>
            <div class="column cl-1-4">

                <ul class="cross red">
                    <li>South Beach</li>
                    <li>Weight Watchers</li>
                </ul>

            </div>
            <div class="column cl-1-4 mobile-hidden">&nbsp;</div>
        </div>

        <p>You name it&hellip;</p>
        <p>Yet I always felt like my own body was sabotaging everything I tried. And the weight would just pile back on &mdash; and then some &mdash; every time I made the smallest progress&hellip;</p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-black bg-gray">
        It’s Just Not <span class="cl-red">FAIR:</span> Why Can Some Folks<br>
        Eat Whatever They Want  And Never Get Fat??
    </div>
    <div class="content rich-text">

        <p>If you told me 3 months ago that I could once again be one of those annoying people &mdash; you know&hellip; the folks who seem to get away with eating whatever they want and still look <b>amazing</b> &mdash; let’s just say I would have barked a bitter laugh and called you crazy&hellip; or a liar&hellip;</p>
        <p>Yet that’s before I discovered that folks like Angie are NOT slim because of their genetics&hellip;</p>
        <p>I discovered there’s <u><b>nothing</b> “special”</u> about folks who can eat whatever they want and stay slim&hellip;</p>
        <p>Yet <u>they DO have an unfair advantage</u> over the rest of us.</p>
        <p><b>Truth is:</b> I was about to discover there was something BROKEN inside me. And my body was BETRAYING me with every bite of food I took&hellip;</p>
        <p><b>It was making me sick and fat!</b></p>
        <p>So pay close attention because if you are having ANY trouble with your weight, there’s a good chance <u>this is the key to turning things around</u> for you.</p>
        <p>Do you relate to any of this?&hellip;</p>
        <p>I would eat like a bird all week&hellip; and then only a couple slices of pizza on the weekend would send the scale creeping up bit by bit every month!&hellip;</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/st782.png" width="100%" alt=""></center>
        </div>

        <p>Do you feel like you put on a few pounds just by walking past the ice cream freezer at the grocery store&hellip;</p>
        <p>Then you’re probably suffering from the same sneaky condition I was&hellip;</p>
        <p>Which is GOOD news, because just <u>some <b>subtle shifts</b> in the foods you eat</u> will <b>quickly</b> and <b>naturally</b> give you back your ability to <b>lose fat and stay slim</b> again&hellip;</p>
        <p>The simple strategy I’ll give you on this page is the exact secret I used to COMPLETELY reprogram my metabolism so I now <u>stay slim no matter what I eat</u>&hellip;</p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-black bg-gray">
        Diets Don’t Work<br>
        <i>[...Unless You <u class="cl-red">Fix</u> This First...]</i>
    </div>
    <div class="content rich-text">

        <p>Listen: In case you’re thinking this is all about another diet&hellip;</p>
        <p>Let me tell you that I’ve tried them all and diets <u>never worked</u> for me. (At least not for long&hellip;)</p>
        <p>And I was about to find out exactly why&hellip;</p>
        <p>You see, everything exploded in my face that dreadful night, hanging out with Angie and eating pizza&hellip;</p>
        <p>I watched her pound back her fourth slice while I secretly sulked about her slim waist and toned physique&hellip;</p>
        <p>And it just slipped out!&hellip;</p>

        <h2><i>“<span class="cl-red">Bitch</span>”</i></h2>

        <p>At first I hoped I hadn’t actually said it out loud&hellip;</p>

        <div class="insertion left" style="margin:.5em 1.5em 1.5em 0;">
            <center><img class="mobile-3of4-width" src="../web/i/goodbellybadbelly/st974.png" width="270" alt=""></center>
        </div>

        <p>Then the hurt look on Angie’s face told me I had, and I just started sobbing!&hellip;</p>
        <p>The look on Angie’s face went from hurt, to confused, to concerned as she slid over to me, gave me a hug and asked me what was wrong&hellip;</p>
        <p>I confessed everything&hellip; how frustrated I was with my weight. That I used to be able to stay skinny like her no matter what I ate. That no matter what I tried now I just kept getting fatter&hellip;</p>
        <p>And that although I didn’t want to&hellip; I constantly felt jealous of her metabolism and her “skinny” genetics&hellip;</p>
        <p>What she said next totally shocked me!&hellip;</p>
        <p><b><i>“You know I used to be really chubby before you met me right?”&hellip;</i></b></p>
        <p>Turns out Angie used to battle with her weight too&hellip;</p>
        <p>She used to weigh almost 30 lbs MORE than the slim-figured woman I had come to know&hellip;</p>
        <p>Then she told me about the crazy diets she tried all through college&hellip;</p>
        <p>And as we swapped stories and shared tears I asked her&hellip;</p>
        <p><big><big><big><b><i>“So how the heck do you manage to look so great even though I see you eating whatever you want!”&hellip;</i></b></big></big></big></p>
        <p><i>“Tell you what”</i> she said&hellip; <i>“Meet me at our usual coffee shop on Saturday at 9:00. There’s someone I want you to meet&hellip;”</i></p>
        <p>That night as I drove home to my family after our “girl’s night” I was full of mixed emotions&hellip;</p>


        <p>I wanted to have <b>HOPE</b> that something would finally work for me. I desperately clung to the idea that I could look and feel slim again, and still enjoy eating freely the way Angie did&hellip;</p>
        <p>Yet at the same time part of me was too skeptical to hope. Part of my mind kept running through all the times I had been excited at the prospect of a new diet that ended up crushing my hope and leaving me convinced I’d be fat and unhappy forever. And I was scared to open myself up to that again&hellip;</p>
        <p>I found myself standing at the front door of my house. And I knew in that moment I had three choices&hellip;</p>

        <div class="insertion right">
            <center><img class="mobile-3of4-width" src="../web/i/goodbellybadbelly/st491.png" width="250" alt=""></center>
        </div>

        <ul class="choices">
            <li>
                <h6>1</h6>
                <p><big><b>One</b></big> &mdash; I could continue down the path I was on&hellip;</p>
                <p>And pretend that someday I’d find a diet that worked&hellip;</p>
            </li>
            <li>
                <h6>2</h6>
                <p><big><b>Two</b></big> &mdash; I could just give up and embrace being overweight and unhappy&hellip;</p>
                <p>&nbsp;</p>
            </li>
            <li class="final">
                <h6>3</h6>
                <p class="accent"><big><b>Or <span class="cl-yellow">Three</span> &mdash; I could decide to take charge of my own body&hellip;</b></big></p>
                <p>And figure out WHY I could no longer control my weight, despite dieting ALL the time&hellip;</p>
            </li>
        </ul>

        <p>As I turned the key and walked through the door I knew there was really only one option for me&hellip;</p>
        <p>When Saturday morning rolled around, I walked into the coffee shop with grim determination&hellip;</p>
        <p>I looked around and found Angie. Sitting beside her was a guy who looked like a really fit and muscular nerd&hellip;</p>
        <p>Angie introduced us&hellip; <i>“Emma, I’d like you to meet my friend Brad”</i>&hellip;</p>
        <p>Brad didn’t waste any time&hellip;</p>
        <p><i>“So, Angie tells me you’re having trouble controlling your weight”</i>&hellip;</p>
        <p>After turning beet red with embarrassment I filled him in on my story&hellip;</p>
        <p>&hellip;how I did my best to eat responsibly and exercise, but just wasn’t getting the results I desired.</p>
        <p>Brad listened careful then asked&hellip; <i>“Have you ever heard the saying:“</i></p>
        <p><b><i>“<u>First You Feed The Machines That Then Feed You</u>”&hellip;</i></b></p>
        <p><i>Huh??</i></p>
        <p>I guess he could read the look of totally <u><b>confusion</b></u> and <u>skepticism</u> on my face&hellip;</p>
        <p>Because he quickly continued with&hellip; <i>“Sorry, let me explain&hellip;”</i></p>
        <p>And&hellip;</p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-white bg-green">
        What he revealed next was shocking&hellip;
    </div>
    <div class="content rich-text">

        <p>Brad started by explaining that your digestive system &mdash; or as he calls it your <i>“gut”</i> &mdash; is a lot more than just digestive proteins and stomach acid. In fact, most of the digestive process is actually done by bacteria in your digestive tract.</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/animation.gif" width="100%" alt=""></center>
        </div>

        <p>Yeah, sounds gross right?&hellip;</p>
        <p>But as Brad filled me in I was shocked to learn that those little critters &mdash; that together make up something they call your “gut flora” &mdash; are more important than anyone imagined up until the last few years.</p>
        <p>Brad told me that recent breakthroughs in scientific research prove that your gut flora is directly linked to&hellip;</p>

        <div class="columns">
            <div class="column cl-1-2">

                <ul class="check blue">
                    <li><b>Weight gain</b> and weight loss</li>
                    <li>Inflammatory Bowel Disorder (IBD)</li>
                    <li>Chronic <b>Fatigue</b> Syndrome</li>
                    <li>Multiple Sclerosis</li>
                    <li>Autism</li>
                </ul>

            </div>
            <div class="column cl-1-2">

                <ul class="check blue">
                    <li><b>Obesity</b></li>
                    <li>Insulin resistance leading to <b>Diabetes</b></li>
                    <li>Certain cancers&hellip;</li>
                    <li>And a bunch more devastating health conditions&hellip;</li>
                </ul>

            </div>
        </div>

        <p>Everything he was saying was kinda scary. But I couldn’t help asking the only question I really cared about at that moment&hellip;</p>

        <h2><i class="bg-yellow">“Brad, how does this help me lose weight?&hellip;”</i></h2>

        <p><i>“Great question”</i> he said&hellip; <i>“Now this is an oversimplification&hellip; However the simple answer is you are suffering from something I call&hellip;”</i></p>
        <p><b><i>“Bad Belly”</i></b></p>
        <p><i>“And that is why <b>none of your diets can work</b>&hellip;”</i></p>

        <h3>
            <i>“It’s not your fault. No diet works in the long run when<br>
            you’re suffering from <u>Bad Belly</u>&hellip;”</i>
        </h3>

        <p>He went on to explain that although there are many stages in between&hellip;</p>
        <p>In terms of how your gut is affecting your health and metabolism, you can be in one of two states&hellip;</p>
        <p>Most folks nowadays suffer from <b>Bad Belly</b>&hellip;</p>
        <p>Which sucks away at good health and causes weight gain&hellip;</p>
        <p>While the opposite state is what Brad calls&hellip;</p>

        <div class="insertion right">
            <center><img class="mobile-3of4-width" src="../web/i/goodbellybadbelly/st045.png" width="270" alt=""></center>
        </div>

        <p><b>Good Belly</b>&hellip;</p>
        <p>Here’s how he explained it to me&hellip;</p>
        <p>Everyone gains weight, and everyone loses weight. The puzzling question is why is there such a wide range in how much weight some people can gain or lose?</p>
        <p>The LAZY answer is that it’s a lack of self-control or that people are simply lying about how much they eat. Yet, this same variability happens in well-controlled scientific studies &mdash; studies that not only control how much people eat, but also how much exercise they do.</p>

        <div class="insertion left">
            <center><img class="mobile-3of4-width" src="../web/i/goodbellybadbelly/st926.png" width="270" alt=""></center>
        </div>

        <p>And get this!&hellip;</p>
        <p>Even identical twins eating the same diet and doing the same level of activity can experience different levels of weight gain or weight loss!</p>
        <p>The truth is, if weight gain was simply a perfect mathematical equation of calories in and calories out, we would be able to estimate weight gain with amazing accuracy &mdash; but we can’t&hellip;</p>
        <p>Which leads to the question&hellip;</p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-white bg-green">
        What Is The <i>X-Factor</i> That Causes Some Folks To<br>
        Lose Weight Easily While It Makes It Impossible For Others?
    </div>
    <div class="content rich-text">

        <p>The answer is shocking&hellip;</p>
        <p>In fact it turns out that Bad Belly could actually be <b>POISONING</b> you from the inside out!</p>
        <p>And that these poisons &mdash; they are called <b>“Endotoxins”</b> &mdash; are what determines whether you can lose or maintain your weight.<sup><a class="sci-ref" href="#fn-102">102</a></sup></p>
        <p>When your gut flora shifts towards Bad Belly, it starts producing these toxins.</p>
        <p>There are always some of these endotoxins in your gut. The nightmare only starts when Bad Belly gets the upper hand and these toxins start leaking from your gut into the rest of your body.</p>
        <p>When that happens, it triggers inflammation. Which in turn leads to metabolic problems, diabetes, obesity, heart trouble, certain cancers and even problems thinking and concentrating!</p>
        <p>And THAT is the <b>X-FACTOR</b> that determines whether you can lose weight&hellip;</p>

        <div class="insertion center" style="margin:3em 0;">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/schema-xfactor.png" alt=""></center>
        </div>

        <p>The long and short of it is&hellip;</p>
        <ol>
            <li><p>You eat</p></li>
            <li><p>The food you eat is processed by your digestive system <b>AND</b> by the bacteria that live inside your gut.</p></li>
            <li><p>Depending on the health of your <i>Good Belly or Bad Belly</i> status&hellip; your gut flora either produces nutrients that contribute to your health and weight loss&hellip; <u>OR it produces toxins that increase inflammation, tear down your health and make it feel impossible to lose weight</u>.</p></li>
        </ol>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/schema-bb-gb-eating.png" alt=""></center>
        </div>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-black bg-gray">
        Reclaim Your Body’s <span class="cl-red">Natural Fat-Burning</span> Ability
    </div>
    <div class="content rich-text">

        <p>When you help your gut repair itself and return to a healthy state, amazing transformations take place in your body!&hellip;</p>
        <p>You balance all the <u><b>hormones</b></u> required for a strong <b>fat-burning</b> metabolism&hellip;</p>
        <p>Having a healthy <i>Good Belly</i> reduces your Chronic Inflammation. And modern medicine has shown that living with Chronic Inflammation blocks fat loss and causes multiple deadly diseases&hellip;</p>
        <p>So by simply repairing your gut you allow your body to return naturally and effortlessly to a fat burning state. Your metabolism starts to burn hot and you’re able to burn off excess calories instead of storing them as fat.</p>
        <p>And because your hunger hormones are now balanced, you don’t go off the deep end with binges and cravings.</p>
        <p>Which means it is even easier to eat normally, never be hungry, and enjoy regularly indulging in your favorite treat foods guilt-free&hellip;</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/schema-meal.png" alt=""></center>
        </div>

        <hr>

        <h2>Brad’s Little Blue Binder Resets<br> Your Fat Burning Metabolism</h2>

        <p>At this point I guess Brad could tell that I was still having trouble absorbing all this, because he grabbed a pen from his pocket and wrote down his number on a napkin&hellip;</p>
        <p>I thanked him and wandered out to my car in a daze. For the rest of the day I obsessed over everything I had learned&hellip;</p>
        <p>And that night I started researching Brad’s background. [&hellip;<i>I have a bit of a skeptical nature you see</i>&hellip;]</p>
        <p>And I was blown away! On top of his impressive post-graduate credentials in nutrition, it turns out he’s also a best-selling author published in several languages&hellip;</p>
        <p>And he used to head up the R&amp;D department for one of the largest nutritional supplement companies in the world.</p>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="../web/i/goodbellybadbelly/st070.png" width="220" alt=""></center>
        </div>

        <p>So I gave Brad a call&hellip;</p>
        <p>He laughed about me checking up on his background. And he told me that although he was headed out of town for few days to speak at a health conference, <u><b>he would leave a little blue binder</b></u> at his office for me.</p>
        <p>He told me the binder contained everything I needed to get started on repairing my Good Belly and <u>restart my fat-burning metabolism</u>.</p>
        <p>That afternoon I picked it up and read through the whole thing in a couple hours.</p>
        <p>It was so simple that I actually started the very next morning&hellip;</p>
        <p>I was super motivated too. Because at the back of that 3-ring binder were pages and pages of body measurements and blood work documenting the success of folks who had already been using Brad’s Good Belly, Bad Belly protocol&hellip;</p>
        <p>As I flipped through the binder I finally started to feel like&hellip;</p>
        <p><big><b>YEAH&hellip; This makes sense!&hellip;</b></big></p>
        <p>And after just a couple weeks following Brad’s advice I could already tell I had stumbled onto something more people needed to hear about!</p>
        <p>I’d already lost 7 lbs&hellip;</p>
        <p>However the best part was that I FELT so much better&hellip;</p>
        <p>And thanks to all the research summaries in Brad’s little blue binder I understood exactly why&hellip;</p>
        <p>It turns out that your “gut” controls a lot more than just how much weight you gain or lose. Reclaiming your Good Belly will also&hellip;</p>

        <ul class="check green">
            <li>Help eliminate Chronic Fatigue&hellip;</li>
            <li>Boost Your Mood and Energy&hellip;</li>
            <li>Increase Concentration &amp; Focus</li>
            <li>Decrease inflammation so your joints feel better</li>
            <li>Reduce or eliminate digestive discomfort</li>
            <li>And MUCH more&hellip;</li>
        </ul>

        <p>No wonder I had more energy and better focus. I even noticed some aches and pains just disappeared in only days!&hellip;</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/schema-health.png" alt=""></center>
        </div>

        <br>

        <h2>Listen: <i>Bad Belly</i> Is NOT Your Fault</h2>

        <p>Believe me, Bad Belly is NOT your fault&hellip;</p>
        <p>Did you know the government itself practically CONSPIRES to give you Bad Belly?&hellip;</p>
        <p>And that the medical community carelessly makes the problem worse&hellip;</p>
        <p>In fact the over prescription of antibiotics is probably the number one factor in Bad Belly&hellip;</p>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="../web/i/goodbellybadbelly/st014.png" width="250" alt=""></center>
        </div>

        <p>Yet even the antibiotics added to your hand soap and toothpaste can push you over the edge&hellip;</p>
        <p>Not to mention all the preservatives and other chemicals the government FORCES food companies to add to the foods you eat — even the ones you think are fresh and healthy&hellip;</p>
        <p>Did you know there are even antibiotics in your toothpaste, soaps, skin care products and the cleaning supplies you use around your house?&hellip;</p>
        <p>The government calls them “preservatives”&hellip; But really they are antibiotics&hellip;</p>
        <p>And there is clear research linking high antibiotic use with Bad Belly and weight gain&hellip;</p>
        <p>In fact one research study showed that when 48 subjects were given antibiotics, there was a definite increase in their weight DURING their treatment&hellip;</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/schema-anabolics.png" alt=""></center>
        </div>

        <p>However it doesn’t stop there&hellip;</p>
        <p>A 2013 a New York University study that found that antibiotic use in your first six months is linked with obesity throughout your life&hellip;</p>
        <p>In other words once antibiotics, processed foods and environmental chemicals conspire to give you Bad Belly, you’re stuck with it. And&hellip;</p>
        <p><u>You suffer weight problems <b>until you fix it</b></u>&hellip;</p>
        <p>Which is why Brad’s methods are so exciting!&hellip;</p>
        <p>I had tried and failed to lose weight on countless diets. Yet after just a few weeks using the easy tips in Brad’s <i>Good Belly</i>, <i>Bad Belly</i> binder the scale was down a few pounds already, my clothes were fitting better and I was even getting some compliments at work!&hellip;</p>
        <p>However that’s not even the <b>best part</b>&hellip;</p>

        <h2><i>“I Can Eat Like A Normal Person Again!”</i></h2>

        <p>Sure, it’s great to be back to my “skinny” weight, yet the real joy is that I reclaimed the ability to <b>eat like a normal person again</b>!&hellip;</p>

        <div class="insertion left">
            <center><img class="mobile-3of4-width" src="../web/i/goodbellybadbelly/st795.png" width="250" alt=""></center>
        </div>

        <p>I can pretty much eat any food I want, although I still have to be reasonable of course. I can eat until I’m satisfied and never be hungry. And I effortlessly maintain my ideal weight&hellip;</p>
        <p>Finally&hellip; I <u>don’t have to be jealous</u> of folks like Angie anymore&hellip;</p>
        <p>Which is why you need to ask yourself right now&hellip;</p>
        <p><b>What is it worth to you to become one of “those people?”&hellip;</b></p>
        <p>To be able to eat what you want and still look amazing&hellip;</p>
        <p>To finally lose the weight after years of trying and failing on every diet&hellip;</p>
        <p>To feel confident and happy in your swimsuit or form fitting clothes&hellip;</p>
        <p>To never feel hungry or suffer cravings and binges&hellip;</p>
        <p>&hellip;While still achieving and maintaining your ideal bodyweight and shape&hellip;</p>
        <p>Because that’s the choice that is in front of you right now&hellip;</p>
        <p>You face the same decision I did that night at my front door&hellip;</p>
        <p>Remember, until now your own body has been working against you&hellip;</p>
        <p>And if you continue on this path you will suffer the same yo-yo diet results&hellip;</p>
        <p>Or be forced to simply give up and accept fatigue, declining health and creeping weight gain&hellip;</p>
        <p>That’s not what I want for you and I know it’s not what you desire for yourself&hellip;</p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-white bg-green">
        My Success Can Be Your Success&hellip;
    </div>
    <div class="content rich-text">

        <p>Once my husband and I started enjoying the rewards of the “Good Belly” habits, I hounded Brad for months to organize everything into one easy to use manual that we could share with you and so many other people suffering in silence&hellip;</p>
        <p>Listen: Brad is a bit of a “research geek”&hellip;</p>
        <p>So up until now he was happy to just keep his findings and his life-changing guidelines sitting in a simple 3-ring binder that he loaned out every now and then to friends and family&hellip;</p>
        <p>And he wasn’t really interested in committing the time, money and effort to publishing it&hellip;</p>
        <p>However when I explained to him that it simply wasn’t fair to withhold this information, and that millions of people were needlessly suffering from unexplained weight problems due to <i>Bad Belly</i>&hellip;</p>
        <p>He agreed to accept my help in getting this out to as many folks as possible&hellip;</p>
        <p>So without further ado, I’d like to introduce you to the official <b>Good Belly, Bad Belly</b> method&hellip;</p>
        <p>Listen: There are hundreds of books out there on “healing your gut.” What’s different about <i>Good Belly, Bad Belly</i> is that you don’t have to wait months to see results.</p>
        <p>Brad’s methods focus on helping you <b>feel better NOW</b>. You’ll enjoy near-instant improvements in your energy, moods and sleep. And you’ll almost immediately notice that it’s <u>easier to lose weight</u> even while you enjoy foods you previously wrote off as “forbidden”&hellip;</p>

        <div class="insertion center">
            <center><img class="mobile-half-width" src="../web/i/goodbellybadbelly/gbbb-ecover.jpg" alt=""></center>
        </div>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-black bg-gray cond">
        How The <span class="cl-red">Science</span> Of <u><i>Good Belly, Bad Belly</i></u> Is Now Outsmarting The<br>
        Dangerous Toxins That Cause Belly Fat
    </div>
    <div class="content rich-text">

        <p>By now you understand the terrible damage caused by the toxins pumped out by <i>Bad Belly</i> bacteria. The GOOD news is&hellip;</p>
        <p>Scientific research has already given us a ton of tricks you can use daily to outsmart even the worst case of <i>Bad Belly</i>. Which means you can <u>start feeling better and losing weight right away</u> even before you are finished reclaiming your Good Belly health&hellip;</p>
        <p>Here are just a few of the tricks you’ll discover inside the pages of <i>Good Belly, Bad Belly</i>:</p>

        <ul class="check green">
            <li>How a simple beverage choice can reverse damage from high-fat meals</li>
            <li>The simple trick that turns baked potatoes into a powerful fat burning food</li>
            <li>The one trick that makes it easy to eat any dessert guilt-free</li>
            <li>Which beer can be your weekend “superfood”</li>
            <li>And MUCH more&hellip;</li>
        </ul>

        <p>Every tip in the book is backed by solid scientific research. Better yet&hellip; it’s working everyday in the real world for all the folks using Brad’s simple methods. In fact&hellip;</p>
        <p>Because Brad’s methods are working so well, I’m often asked&hellip;</p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-white bg-green">
        “Why Is This Book So Cheap?”
    </div>
    <div class="content rich-text">

        <p>Listen: you may think a solution like this is going to cost you a pretty penny&hellip;</p>
        <p>Especially considering the results so many others are getting with this groundbreaking new approach.</p>
        <p>After all, what you’re doing is completely re-programming your fat burning metabolism for life, <u>so you actually become <b>one of those people who can eat what they want and never gain weight</b></u>&hellip;</p>
        <p>Compare that with crash diet programs and pills that cost twice as much and don’t last&hellip;</p>
        <p>And I think you’ll agree that anything less than $150 would be a huge bargain for that kind of freedom from restrictive diets and frustrating weight gain!</p>
        <p>However Brad insists that you deserve every chance possible to get your hands on this solution.</p>
        <p>The problem is&hellip; <b>Brad has no idea</b> how much it actually costs to arrange all the logistics of getting this into your hands. So it took some arguing but I eventually convinced him we need to set a regular retail price of at least <strike>$19.97</strike>. But&hellip;</p>
        <p>To get him to agree to that he made me promise that we’d try to make a go of it first at an introductory price of only <b>$10</b>!&hellip;</p>
        <p>However I made it clear to Brad that if he started losing money I’d make him raise the price to $19.97 asap. So make sure you act now to secure Good Belly, Bad Belly for <b class="bg-yellow">just $10</b>.</p>
        <p>If you come back and see the price has increased I will not be able to make an exception for you. Just use the button below to secure the discounted $10 price now&hellip;</p>

        <div class="columns pricing">
            <div class="column cl-1-6 mobile-hidden">&nbsp;</div>
            <div class="column cl-1-3 mobile-hidden">
                <p class="ta-center">Retail Price</p>
                <p class="ta-center"><strike>$19.97</strike></p>
            </div>
            <div class="column cl-1-3 mobile-hidden">
                <p class="ta-center"><b>Limited-Time Discount</b></p>
                <p class="ta-center"><big class="cl-green">$10</big></p>
            </div>
            <div class="column cl-1-6 mobile-hidden">&nbsp;</div>
        </div>

        <p class="ta-center">
            <a class="purchase-button" href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&cbskin=18285&vtid=clk31279lf">Yes! Give Me The Good Belly, Bad Belly Book<br> At The Reduced Price Of Just $10!&hellip;</a>
        </p>

        <br>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-black bg-gray">
        PLUS You’re Getting <span class="cl-red">FREE <u>VIP Inside Access</u></span>
    </div>
    <div class="content rich-text">

        <p>Now if for some reason you are still on the fence, I’m about to make this a complete no-brainer for you&hellip;</p>
        <p>Brad has agreed to give you 3 weeks of FREE access to his VIP insider newsletter called the Advocate.</p>
        <p>This is where Brad shares the mind blowing insights, tips and tricks that come from his constant scouring of the latest scientific research papers on nutrition, weight loss, exercise and health.</p>
        <p>He sifts through it all for you, chooses the most relevant and useful information, and translates it into easy to understand advice you can put into action right away.</p>
        <p>Right after checkout, you’ll see a page asking if you want to activate your free access. Just use the button on that page and with one click you’ll start receiving Brad’s Advocate newsletter right in your inbox.</p>

        <h3>
            AND&hellip; I need to let you know about a couple MORE <u>FREE</u> gifts that<br>
            I convinced Brad to include for you&hellip;
        </h3>

        <p>However these are <b>only available</b> with this special <u>introductory discounted offer</u>. Once we take this page down and return to the regular retail price these will no longer be available for FREE&hellip;</p>

        <hr>

        <p><big>FREE Gift #1:</big></p>

        <div class="insertion right bonus">
            <p class="ta-right">
                <img class="mobile-half-width" src="../web/i/goodbellybadbelly/bonus-1-ecover.jpg" width="240" alt=""><br>
                [$19.50 Value] &nbsp; &nbsp; &nbsp;
            </p>
        </div>

        <h3 class="ta-left">The 3-Minute Lower Belly Flatteners</h3>

        <p>This is a simple 3-Minute sequence of exercises to tighten up the core muscles in your belly and quickly get rid of your lower belly pooch&hellip;</p>
        <p>This is the exact sequence Brad gave me after I lost all the weight and complained that my lower belly still sagged&hellip;</p>
        <p>Within days I started to notice my belly looked flatter and tighter in the bathroom mirror&hellip;</p>
        <p>And you’re getting the 3-Minute Lower Belly Flatteners completely <b>free</b> today&hellip;</p>

        <hr>

        <p><big>FREE Gift #2:</big></p>

        <div class="insertion right bonus">
            <p class="ta-right">
                <img class="mobile-half-width" src="../web/i/goodbellybadbelly/bonus-2-ecover.jpg" width="240" alt=""><br>
                [$19.50 Value] &nbsp; &nbsp; &nbsp;
            </p>
        </div>

        <h3 class="ta-left">The 7-Minute Fat Burning Cardio Collection</h3>

        <p>Look, you don’t have to exercise to lose weight with Good Belly, Bad Belly&hellip;</p>
        <p>However, I was eager to slim down as <b>fast</b> as possible&hellip;</p>
        <p>And when I asked Brad what I could do to speed up the process&hellip;</p>
        <p>&hellip;WITHOUT spending hours exercising&hellip;</p>
        <p>He gave me these simple 7-Minute cardio routines I could do whenever I had the time&hellip;</p>
        <p>And they worked so well for me I wanted to make sure you get access too&hellip;</p>

        <hr>

        <p>So let’s add all this up to see the amazing deal you are getting when you take action today.</p>
        <p>You’re getting BOTH the 7-Minute Fat Burning Cardio Collection and the 3-Minute Lower Belly Flatteners &mdash; a combined value of $39 &mdash; absolutely <b>free</b> with your investment in Good Belly, Bad Belly&hellip;</p>
        <p>PLUS you’re getting your copy of Good Belly, Bad Belly for just $10 &mdash; more than <b>HALF OFF</b> the regular retail price of $19.97. AND you’re getting 3 weeks of FREE access to Brad’s VIP newsletter, The Advocate.</p>
        <p>Which means you’re getting a combined value today of <strike>$65.97</strike> for just $10!</p>

        <div class="insertion center" style="margin:2em 0 0;">
            <center><img class="mobile-full-width" src="../web/i/goodbellybadbelly/bundle.jpg?v=2" width="500" alt=""></center>
        </div>

        <div class="columns pricing">
            <div class="column cl-1-6 mobile-hidden">&nbsp;</div>
            <div class="column cl-1-3 mobile-hidden">
                <p class="ta-center">Combined<br> Package Price</p>
                <p class="ta-center"><strike>$65.97</strike></p>
            </div>
            <div class="column cl-1-3 mobile-hidden">
                <p class="ta-center"><b>Limited-Time<br> Experimental Price</b></p>
                <p class="ta-center"><big class="cl-green">$10</big></p>
            </div>
            <div class="column cl-1-6 mobile-hidden">&nbsp;</div>
        </div>

        <p class="ta-center">
            <a class="purchase-button" href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&cbskin=18285&vtid=clk31279lf">Yes! Give Me The Entire <i>Good Belly, Bad Belly</i><br> Package (A <strike>$65.97</strike> Value) For Just $10!&hellip;</a>
        </p>

        <br>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-black bg-gray">
        REMEMBER:<br>
        There’s <u>Zero Risk</u> For You Today
    </div>
    <div class="content rich-text">

        <p>Now if you’re already shocked to be paying LESS for the entire package than you would pay for just ONE of the bonuses separately, let me tell you what else Brad has agreed to offer you today&hellip;</p>
        <p>Because we want to make it easy for you to make the right decision today &mdash; and take back control of your weight and your health for good &mdash; you can try Good Belly, Bad Belly risk-free for 60 days&hellip;</p>
        <p>Experience the results for yourself. And if for any reason you are not 100% satisfied with your transformation, simply let us know and we’ll make sure you receive a 100% no-questions asked money-back refund the very same day&hellip;</p>
        <p>Just use the button below the video to secure your risk-free access right away&hellip;</p>

        <p class="ta-center">
            <a class="purchase-button" href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&cbskin=18285&vtid=clk31279lf">Yes! I Want To Try <i>Good Belly, Bad Belly</i><br> <u>Risk-Free</u> For 60 Days!&hellip;</a>
        </p>

        <br>
        <hr>

        <p>As I mentioned, I’m not sure how long I can allow Brad to make this low price available. So once you leave this page I can’t guarantee that you’ll have access to it in the future&hellip;</p>
        <p>Remember this is your chance to take back control of your health and your weight&hellip;</p>
        <p>And I fought to get you this low price because I want you to be able to take advantage of it&hellip;</p>

        <br>
        <h3>Imagine how it’s going to feel in just a few weeks from now&hellip;</h3>

        <p>Picture going out to your favorite restaurant and eating a rich, decadent meal&hellip;</p>
        <p>Then waking up the next morning and feeling satisfied and fresh instead of heavy and bloated&hellip;</p>
        <p>And as you get out of bed you catch a glimpse of yourself in the mirror&hellip;</p>
        <p>Your breath catches in your throat because you’re still shocked every time you see your slim and firm reflection&hellip;</p>
        <p>A smile spreads over your face because you remember that when you get dressed, you’ll be putting on your favorite form-fitting clothes that have been hiding for years at the back of your closet&hellip;</p>
        <p>That feeling of freedom is what I want for you. And I know it’s what you want for yourself&hellip;</p>
        <p>The freedom of transforming yourself into one of those people who just seems to be able to eat whatever they want and never gain an ounce&hellip;</p>
        <p>You CAN reclaim your own natural <i>Good Belly</i> right now. And the plan is so easy to follow that you can get started as early as tonight&hellip;</p>
        <p>Just don’t put it off another second ok&hellip;</p>
        <p>Because ignoring your gut balance and health can only lead to more frustration&hellip;</p>
        <p>And months from now as you look back at the continued wreckage of failed diets and rebound weight gain, you’ll wonder if you can ever dig yourself out of the pit of despair and frustration that comes from creeping weight gain that feels totally out of your control.</p>
        <p>Listen: That’s not what I want for you. And I know it’s not what you desire for yourself&hellip;</p>
        <p>Which means the only smart decision you have in front of you today&hellip;</p>
        <p>&hellip;Is to turn the key and walk through the door with me&hellip;</p>
        <p>Reclaim control of your weight and your health&hellip;</p>
        <p>And in just 4-6 weeks from now you’ll be enjoying the rewards of a naturally balanced <i>Good Belly</i> and a renewed fat-burning metabolism&hellip;</p>
        <p>Just remember&hellip;</p>

        <br>
        <h3>This is NOT another weight loss diet program&hellip;</h3>

        <p>Yes, you will lose all the weight you want. However&hellip;</p>
        <p><i>Good Belly</i>, Bad Belly is not another plan that will trick you with massive weight loss, only to leave you cursed with rebound weight gain as soon as you finish the diet&hellip;</p>
        <p>Because Good Belly, Bad Belly is designed to <u><b>completely reset and rebalance your fat burning systems</b></u>, you finish the plan ready to enjoy life&hellip; Eat tasty foods&hellip; Go out for dinners and drinks&hellip; And never have to worry about going back to that horrible place where every treat left you feeling fat and guilty&hellip;</p>
        <p>You will never use the words “cheat meal” or “cheat food” again, because nothing will be off limits. So you can enjoy your favorite foods without guilt&hellip;</p>
        <p>Remember today you’re also getting the <i>3-Minute Lower Belly Flatteners</i> and the <i>7-Minute Fat Burning Cardio Collection</i> totally FREE&hellip;</p>
        <p>And you’re also covered by our 100% no-questions-asked, iron-clad money back guarantee&hellip;</p>
        <p>So you can actually go through the entire <i>Good Belly, Bad Belly</i> system for a full 60 days and experience the results for yourself&hellip;</p>
        <p>And if for any reason you aren’t completely satisfied with every aspect of the program, just let me know and I’ll make sure you get a full refund the very same day&hellip;</p>
        <p>So you see there’s absolutely no reason to put off trying the <i>Good Belly, Bad Belly</i> system right away&hellip;</p>
        <p>Just use the button below this video to get started completely risk-free today&hellip;</p>

        <p class="ta-center">
            <a class="purchase-button" href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&cbskin=18285&vtid=clk31279lf">Yes! Get Me Started Today&hellip;</a>
        </p>

        <br>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="sheader cl-black bg-gray">
        Note From The Author<br>
        <small>By Brad Pilon</small>
    </div>
    <div class="content rich-text">

        <p>I wanted to personally thank you for reading this page.</p>
        <p>You see I’m a science geek and an Author. However I’m terrible at selling (and spelling for that matter). So I’m grateful to have help reaching out to folks like you &mdash; savvy, sophisticated, successful and clever people who want to understand how the body REALLY loses weight and returns to health&hellip;</p>
        <p>As you read in the second half of this page, it really isn’t your fault if you can no longer lose weight using conventional diet programs.</p>
        <p>We’ve seen that the <i>X-Factor</i> between the food you eat and how it affects your health and your weight is the real cause of your weight problems.</p>
        <p>Because the diet industry is so massive &mdash; at last count 108 million Americans are dieting and spending over <u><b>$20 Billion a year</b></u> on diet books, diet drugs and weight-loss surgeries &mdash; it’s almost impossible to get out the message that NONE of it works if you don’t first take care of what’s happening to your gut health&hellip;</p>
        <p>In fact the diet industry knows the truth about how gut health sabotages your weight loss. The science is very black and white on the matter. Yet they’d rather focus on selling you the next miracle weight loss cure than give you the simple tricks you can start using today to lose weight naturally and near-effortlessly.</p>
        <p>Which is why I’m excited that you’re still with me. It’s only open minded trendsetters like you who can spread the message&hellip;</p>
        <p>So I’m excited to hear your story. I hope you’ll reach out to me and let me know about your <i>Good Belly, Bad Belly</i> experience.</p>
        <p>I’m not a typical author who hides out behind my PR team and agent. You’ll find me on Twitter every day interacting with all the Good Belly, Bad Belly early adopters. And I hope you’ll join us today!</p>
        <p>Tweet me, ok&hellip; (@BradPilon)</p>

        <div class="insertion left">
            <center><img src="../web/i/brad-photo.jpg" width="150" alt=""></center>
        </div>

        <p>
            Your friend,<br>
            <img src="../web/i/brad-sign.png" width="180" alt=""><br>
            Brad
        </p>

    </div><!-- .content -->
</div><!-- .section -->


<div class="section default">
    <div class="content rich-text" style="font-size: 14px;">

        <p>&raquo; <a class="sci-refs-switch" href="javascript:;">Scientific References (click to show)</a></p>

        <div class="sci-refs hidden">

        <p>1. Scientific American Article by Rachel Feltmen on December 14, 2013</p>
        <p>2. Candela, M. et al. Interaction of probiotic Lactobacillus and Bifidobacterium strains with human intestinal epithelial cells: adhesion properties, competition against enteropathogens and modulation of IL-8 production. Int. J. Food Microbiol. 125, 286–292 (2008)</p>
        <p>3. Fukuda, S. et al. Bifidobacteria can protect from enteropathogenic infection through production of acetate. Nature 469, 543–547 (2011).</p>
        <p>4. Devillard, E., McIntosh, F. M., Duncan, S. H. &amp; Wallace, R. J. Metabolism of linoleic acid by human gut bacteria: different routes for biosynthesis of conjugated linoleic acid. J. Bacteriol. 189, 2566–2570 (2007)</p>
        <p>5. Bäckhed, F.et al.The gut microbiota as an environmental factor that regulates fat storage. Proc. Natl Acad. Sci. USA 101, 15718–15723 (2004)</p>
        <p>6. Olszak, T. et al. Microbial exposure during early life has persistent effects on natural killer T cell function. Science 336, 489–493 (2012)</p>
        <p>7. Patel PN, Shah RY, Ferguson JF, Reilly MP. Human experimental endotoxemia in modeling the pathophysiology, genomics, and therapeutics of innate immunity in complex cardiometabolic diseases. Arterioscler Thromb Vasc Biol. 2015 Mar;35(3):525-34</p>
        <p>8. Boroni Moreira AP, de Cássia Gonçalves Alfenas R. The influence of endotoxemia on the molecular mechanisms of insulin resistance. Nutr Hosp. 2012 Mar-Apr;27(2):382-90.</p>
        <p>9. Vila IK, Badin PM, Marques MA, Monbrun L, Lefort C, Mir L, Louche K, Bourlier V, Roussel B, Gui P, Grober J, Štich V, Rossmeislová L, Zakaroff-Girard A, Bouloumié A, Viguerie N, Moro C, Tavernier G, Langin D. Immune cell Toll-like receptor 4 mediates the development of obesity- and endotoxemia-associated adipose tissue fibrosis. Cell Rep. 2014 May 22;7(4):1116-29.</p>
        <p>10. National Institutes of Health, U.S. Department of Health and Human Services. Opportunities and Challenges in Digestive Diseases Research: Recommendations of the National Commission on Digestive Diseases. Bethesda, MD: National Institutes of Health; 2009. NIH Publication 08–6514.</p>
        <p>11. Forsythe P, Kunze WA, Bienenstock J. On communication between gut microbes and the brain. Curr Opin Gastroenterol. 2012;28:557-562.</p>
        <p>12. Kershaw EE, Flier JS. Adipose tissue as an endocrine organ. Journal of Clinical Endocrinology and Metabolism. 2004; 89:2548–2556.</p>
        <p>13. Carvalho BM, Saad MJ. Influence of gut microbiota on subclinical inflammation and insulin resistance. Mediators Inflamm. 2013:986734.</p>
        <p>14. Parnell JA, Reimer RA Weight loss during oligofructose supplementation is associated with decreased ghrelin and increased peptide YY in overweight and obese adults. Am J Clin Nutr 2009;89:1751–9.</p>
        <p>15. Patrice D. Cani, Melania Osto, Lucie Geurts, and Amandine Everard Involvement of gut microbiota in the development of low-grade inflammation and type 2 diabetes associated with obesity. Gut Microbes. 2012 July 1; 3(4): 279–288.</p>
        <p>16. Murphy EF, Cotter PD, Hogan A, et al. Divergent metabolic outcomes arising from targeted manipulation of the gut microbiota in diet-induced obesity. Gut. 2013;62:220- 226.</p>
        <p>17. Qin, J. et al. A human gut microbial gene catalogue established by metagenomic sequencing. Nature 464, 59–65 (2010).</p>
        <p>18. The Human Microbiome Project Consortium. Structure, function and diversity of the healthy human microbiome. Nature 486, 207–214 (2012).</p>
        <p>19. Jalanka-Tuovinen J, Salonen A, Nikkilä J, et al. Intestinal microbiota in healthy adults: temporal analysis reveals individual and common core and relation to intestinal symptoms. PLoS One. 2011;6:e23035</p>
        <p>20. Lozupone CA, Stombaugh JI, Gordon JI, Jansson JK, Knight R. Diversity, stability and resilience of the human gut microbiota. Nature. 2012 Sep 13;489(7415):220-30</p>
        <p>21. Yatsunenko, T. et al. Human gut microbiome viewed across age and geography. Nature 486, 222–227 (2012)</p>
        <p>22. De Filippo, C. et al. Impact of diet in shaping gut microbiota revealed by a comparative study in children from Europe and rural Africa. Proc. Natl Acad. Sci. USA 107, 14691–14696 (2010)</p>
        <p>23. Qin, J. et al. A human gut microbial gene catalogue established by metagenomic sequencing. Nature 464, 59–65 (2010)</p>
        <p>24. Manichanh, C.et al.Reduced diversity of faecal microbiota in Crohn’s disease revealed by a metagenomic approach. Gut 55, 205–211 (2006)</p>
        <p>25. Lepage, P.et al.Twin study indicates loss of interaction between microbiota and mucosa of patients with ulcerative colitis. Gastroenterology 141, 227–236 (2011)</p>
        <p>26. Logan AC, Rao, AV, Irani D. Chronic fatigue syndrome: lactic acid bacteria may be of therapeutic value.  Medical hypotheses (2003) 60(6); 915-923</p>
        <p>27. Claesson, M. J. et al. Gut microbiota composition correlates with diet and health in the elderly. Nature 488, 178–184 (2012)</p>
        <p>28. Le Chatelier E, et al. Richness of human gut microbiome correlates with metabolic markers. Nature. 2013 Aug 29;500(7464):541-6.</p>
        <p>29. Lee YK, Menezes JS, Umesaki Y, Mazmanian SK. Proinflammatory T-cell responses to gut microbiota promote experimental autoimmune encephalomyelitis. Proc Natl Acad Sci U S A. 2011;108 Suppl 1:4615-4622</p>
        <p>30. Wang Y, Kasper LH.The role of microbiome in central nervous system disorders. Brain Behav Immun. 2013 Dec 25.</p>
        <p>31. Vaarala O, Atkinson MA, Neu J. The "perfect storm" for type 1 diabetes: the complex interplay between intestinal microbiota, gut permeability, and mucosal immunity. Diabetes. 2008;57:2555-2562</p>
        <p>32. Hara N, Alkanani AK, Ir D, et al. The role of the intestinal microbiota in type 1 diabetes. Clin Immunol. 2013;146:112-119</p>
        <p>33. Dicksved, J. et al. Molecular analysis of the gut microbiota of identical twins with Crohn’s disease. ISME J. 2, 716–727 (2008)</p>
        <p>34. Lupton, J. R. Microbial degradation products influence colon cancer risk: the butyrate controversy. J. Nutr. 134, 479–482 (2004)</p>
        <p>35. Xiao S, Zhao L.Gut microbiota-based translational biomarkers to prevent metabolic syndrome via nutritional modulation. FEMS Microbiol Ecol. 2013 Nov 12</p>
        <p>36. Kelly CJ, Colgan SP, Frank DN. Of microbes and meals: the health consequences of dietary endotoxemia. Nutr Clin Pract. 2012 Apr;27(2):215-25.</p>
        <p>37. Tang WH, Wang Z, Levison BS, et al. Intestinal microbial metabolism of phosphatidylcholine and cardiovascular risk. N Engl J Med. 2013;368:1575-1584</p>
        <p>38. Scher JU, Sczesnak A, Longman RS, et al. Expansion of intestinal Prevotella copri correlates with enhanced susceptibility to arthritis. eLife. 2013;2:e01202</p>
        <p>39. Maes M, Kubera M, Leunis JC. The gut-brain barrier in major depression: intestinal mucosal dysfunction with an increased translocation of LPS from gram negative enterobacteria (leaky gut) plays a role in the inflammatory pathophysiology of depression. Neuro Endocrinol Lett. 2008;29(1):117-24.</p>
        <p>40. Opal SM. Endotoxins and other sepsis triggers Contrib Nephrol. 2010;167:14-24</p>
        <p>41. Cani, PD; Amar J, Iglesias MA, Poggi M, Knauf C, Bastelica D et al. (2007). "Metabolic endotoxemia initiates obesity and insulin resistance.". Diabetes 56: 1761–1772</p>
        <p>42. Boutagy NE, McMillan RP, Frisard MI, Hulver MW. Metabolic endotoxemia with obesity: Is it real and is it relevant? Biochimie. 2016 May;124:11-20.</p>
        <p>43. Deopurkar R, Ghanim H, Friedman J, et al. Differential effects of cream, glucose, and orange juice on inflammation, endotoxin, and the expression of Toll-like receptor-4 and suppressor of cytokine signaling-3. Diabetes Care 2010;33:991–997</p>
        <p>44. Amar J, Burcelin R, Ruidavets JB, Cani PD, Fauvel J, Alessi MC, Chamontin B, Ferriéres J.Energy intake is associated with endotoxemia in apparently healthy men. American Journal of Clinical Nutrition 87(5):1219-23</p>
        <p>45. Luı´sa Neves A, Coelho J, Couto L, Leite-Moreira A and Roncon-Albuquerque Jr R. Metabolic endotoxemia: a molecular link between obesity and cardiovascular risk. Journal of Molecular Endocrinology (2013) 51, R51–R64</p>
        <p>46. Kallus SJ, Brandt LJ. The intestinal microbiota and obesity. J Clin Gastroenterol. 2012;46:16-24</p>
        <p>47. Ramachanrappa S and Farooqi I.S. Genetics approaches to understanding human obesity. Journal of Clinical investigation. 2011 121:2080-2086</p>
        <p>48. Tremblay A, Despres JP, Therault G, Fournier G, bouchard C. Overfeeding and energy expenditure in humans. Am J Clin Nutr 1993:56:857-62</p>
        <p>49. Bouchard C, Tremblay A, Després JP, Nadeau A, Lupien PJ, Thériault G, Dussault J, Moorjani S, Pinault S, Fournier G.The response to long-term overfeeding in identical twins. N Engl J Med. 1990 May 24;322(21):1477-82</p>
        <p>50. Pontzer H, Raichlen DA, Wood BM, Mabulla AZ, Racette SB, Marlowe FW.Hunter-gatherer energetics and human obesity. PLoS One. 2012;7(7):e40503.</p>
        <p>51. Prentice AM, Black AE, Coward WA, Cole TJ. Energy expenditure in overweight and obese adults in affluent societies: an analysis of 319 doubly-labelled water measurements.Eur J Clin Nutr. 1996 Feb;50(2):93-7</p>
        <p>52. Swinburn BA, Sacks G, Lo SK, Westerterp KR, Rush EC, Rosenbaum M, Luke A, Schoeller DA, DeLany JP, Butte NF, Ravussin E.Estimating the changes in energy flux that characterize the rise in obesity prevalence.Am J Clin Nutr. 2009 Jun;89(6):1723-8</p>
        <p>53. Bäckhed, F.et al.The gut microbiota as an environmental factor that regulates fat storage. Proc. Natl Acad. Sci. USA 101, 15718–15723 (2004)</p>
        <p>54. Devillard, E., McIntosh, F. M., Duncan, S. H. &amp; Wallace, R. J. Metabolism of linoleic acid by human gut bacteria: different routes for biosynthesis of conjugated linoleic acid. J. Bacteriol. 189, 2566–2570 (2007)</p>
        <p>55. Rossi M, Amaretti A, Raimondi S.Folate production by probiotic bacteria.Nutrients. 2011 Jan;3(1):118-34.</p>
        <p>56. Metges CC et al. Availability of intestinal microbial lysine for whole body lysine homeostasis in human subjects. American Journal of Physiology, 1999, 277:E597–E607.</p>
        <p>57. Metges CC et al. Incorporation of urea and ammonia nitrogen into ileal and fecal microbial proteins and plasma free amino acids in normal men and ileostomates. American Journal of Clinical Nutrition, 1999, 70:1046–1058.</p>
        <p>58. Turnbaugh, P. J. et al. An obesity-associated gut microbiome with increased capacity for energy harvest. Nature 444, 1027–1031 (2006).</p>
        <p>59. Le Chatelier E, et al. Richness of human gut microbiome correlates with metabolic markers.Nature. 2013 Aug 29;500(7464):541-6.</p>
        <p>60. Kohl, KD, Amaya J, Passement A, Dearing MD and McCue MD. Unique and shared responses of the gut microbiota to prolonged fasting: a comparative study across five classes of vertebrate hosts. FEMS Microbiol Ecol 90 (2014) 883–894</p>
        <p>61. Turnbaugh, P. J. et al. An obesity-associated gut microbiome with increased capacity for energy harvest. Nature 444, 1027–1031 (2006)</p>
        <p>62. Dethlefsen, L., S. Huse, M. L. Sogin, and D. A. Relman. 2008. The pervasive effects of an antibiotic on the human gut microbiota, as revealed by deep 16S rRNA sequencing. PLoS Biol. 6:e280.</p>
        <p>63. Jakobsson HE et al Short-term antibiotic treatement ha differing long-term impacts on the human throat and gut microbiome. PLoS ONE 5s, e9836 (2010).</p>
        <p>64. Jernberg C, Lofmark S, Edlund C, Jansson JK. Long-term ecological impacts of antibiotic administration on the human intestinal microbiota. ISME J. 1 56-66 (2007)</p>
        <p>65. Ajslev, T. A., Andersen, C. S., Gamborg, M., Sorensen, T. I. &amp; Jess, T. Childhood overweight after establishment of the gut microbiota: the role of delivery mode, pre- pregnancy weight and early administration of antibiotics. Int. J. Obes. 35, 522–529 (2011)</p>
        <p>66. Office-related antibiotic prescribing for persons aged ≤14 years -- United States, 1993- 1994 to 2007-2008. MMWR Morb Mortal Wkly Rep 2011;60:1153-1156</p>
        <p>67. Hicks LA, Bartoces MG, Roberts RM, Suda KJ, Hunkler RJ, Taylor TH Jr, Schrag SJ. US outpatient antibiotic prescribing variation according to geography, patient population, and provider specialty in 2011. Clin Infect Dis. 2015 May 1;60(9):1308-16.</p>
        <p>68. Dethlefsen L, Relman DA. Incomplete recovery and individualized responses of the human distal gut microbiota to repeated antibiotic perturbation. Proc Natl Acad Sci U S A (2011) 108(Suppl 1):4554–61.</p>
        <p>69. Trasande L, Blustein J, Liu M, Corwin E, Cox LM, Blaser MJ. Infant antibiotic exposures and early-life body mass.Int J Obes (Lond). 2013 Jan;37(1):16-23.</p>
        <p>70. Thuny F, Richet H, Casalta JP, Angelakis E, Habib G, Raoult D. Vancomycin treatment of infective endocarditis is linked with recently acquired obesity. PLoS One (2010) 5(2):e9074.10.1371/journal.pone.0009074</p>
        <p>71. Duran GM, Marshall DL. Ready-to-eat shrimp as an international vehicle of antibiotic- resistant bacteria. J Food Prot. 2005 Nov;68(11):2395-401.</p>
        <p>72. Campos J, Mourão J, Pestana N, Peixe L, Novais C, Antunes P. Microbiological quality of ready-to-eat salads: an underestimated vehicle of bacteria and clinically relevant antibiotic resistance genes.Int J Food Microbiol. 2013 Sep 16;166(3):464-70.</p>
        <p>73. Brown JC, Jiang X. Prevalence of antibiotic-resistant bacteria in herbal products. Food Prot. 2008 Jul;71(7):1486-90.</p>
        <p>74. http://www.takepart.com/article/2013/04/06/antibiotic-use-organic-apples-pears</p>
        <p>75. http://www.tufts.edu/med/apua/about_issue/agents.shtml</p>
        <p>76. FSNET. 2000. Survey of U.S. Stores reveals widespread availability of soaps containing potentially harmful antibacterial agents. Centre for Safe Food, University of Guelph. September 10.</p>
        <p>77. Halden RU, Paull DH. Co-occurrence of triclocarban and triclosan in U.S. water resources.Environ Sci Technol. 2005 Mar 15;39(6):1420-6.</p>
        <p>78. Möller W, Häussinger K, Winkler-Heil R, Stahlhofen W, Meyer T, Hofmann W, Heyder J. Mucociliary and long-term particle clearance in the airways of healthy nonsmoker subjects. J Appl Physiol (1985) 2004;97:2200–6.</p>
        <p>79. Kaplan GG, Hubbard J, Korzenik J, Sands BE, Panaccione R, Ghosh S, Wheeler AJ, Villeneuve PJ. The inflammatory bowel diseases and ambient air pollution: a novel association. Am J Gastroenterol. 2010 Nov; 105(11):2412-9.</p>
        <p>80. Ananthakrishnan AN, McGinley EL, Binion DG, Saeian K. Ambient air pollution correlates with hospitalizations for inflammatory bowel disease: an ecologic analysis. Inflamm Bowel Dis. 2011 May; 17(5):1138-45.</p>
        <p>81. Kaplan GG, Szyszkowicz M, Fichna J, Rowe BH, Porada E, Vincent R, Madsen K, Ghosh S, Storr M. Non-specific abdominal pain and air pollution: a novel association. PLoS One. 2012; 7(10):e47669.</p>
        <p>82. Möller W, Häussinger K, Winkler-Heil R, Stahlhofen W, Meyer T, Hofmann W, Heyder J. Mucociliary and long-term particle clearance in the airways of healthy nonsmoker subjects.. J Appl Physiol (1985). 2004 Dec; 97(6):2200-6.</p>
        <p>83. Beamish LA, Osornio-Vargas AR, Wine E. Air pollution: An environmental factor contributing to intestinal disease. J Crohns Colitis. 2011 Aug; 5(4):279-86.</p>
        <p>84. Calkins BM.A meta-analysis of the role of smoking in inflammatory bowel disease. Dig Dis Sci. 1989 Dec; 34(12):1841-54.</p>
        <p>85. Chen H, Burnett RT, Kwong JC, Villeneuve PJ, Goldberg MS, Brook RD, van Donkelaar A, Jerrett M, Martin RV, Brook JR, Copes R. Risk of incident diabetes in relation to longterm exposure to fine particulate matter in Ontario, Canada. Environ Health Perspect. 2013 Jul;121(7):80410.</p>
        <p>86. Tang M, Chen K, Yang F, Liu W. Exposure to organochlorine pollutants and type 2 diabetes: a systematic review and metaanalysis. PLoS One. 2014 Oct 15;9(10):e85556.</p>
        <p>87. Chuang KJ, Yan YH, Chiu SY, Cheng TJ. Longterm air pollution exposure and risk factors for cardiovascular diseases among the elderly in Taiwan. Occup Environ Med. 2011 Jan;68(1):648.</p>
        <p>88. Madsen K, Cornish A, Soper P, McKaigney C, Jijon H, et al. (2001) Probiotic bacteria enhance murine and human intestinal epithelial barrier function. Gastroenterology 121: 580–591</p>
        <p>89. Kish L, Hotte N, Kaplan GG, et al. Environmental Particulate Matter Induces Murine Intestinal Inflammatory Responses and Alters the Gut Microbiome. Bereswill S, ed. PLoS ONE. 2013;8(4):e62220.</p>
        <p>90. Van de Wiele T, Vanhaecke L, Boeckaert C, Peru K, Headley J, Verstraete W, Siciliano S. Human colon microbiota transform polycyclic aromatic hydrocarbons to estrogenic metabolites. Environ Health Perspect. 2005;113:6–10.</p>
        <p>91. Byeong-Kyu Lee (2010). Sources, Distribution and Toxicity of Polyaromatic Hydrocarbons (PAHs) in Particulate Matter, Air Pollution, Vanda Villanyi (Ed.), InTech, DOI: 10.5772/10045.</p>
        <p>92. Wu GD, Chen J, Hoffmann C, Bittinger K, Chen YY, Keilbaugh SA, Bewtra M, Knights D, Walters WA, Knight R, Sinha R, Gilroy E, Gupta K, Baldassano R, Nessel L, Li H, Bushman FD, Lewis JD. Linking long-term dietary patterns with gut microbial enterotypes.Science. 2011 Oct 7;334(6052):105-8.</p>
        <p>93. Cotillard A, et al. Dietary intervention impact on gut microbial gene richness. Nature 500, 585–588 (29 August 2013)</p>
        <p>94. Ershler WB, Keller ET. Age-associated increased interleukin-6 gene expression, late-life diseases, and frailty. Annu Rev Med. 2000;51:245-70. Review.</p>
        <p>95. Ershler WB, Sun WH, Binkley N. The role of interleukin-6 in certain age-related diseases. Drugs Aging. 1994 Nov;5(5):358-65. Review.</p>
        <p>96. Bharat B. Aggarwal, R.V. Vijayalekshmi, and Bokyung Sung. Targeting Inflammatory Pathways for Prevention and Therapy of Cancer: Short-Term Friend, Long-Term Foe. Clinical Cancer Research 2009; 15(2): 425-430.</p>
        <p>97. Esposito K, Nappo F, Marfella R, Giugliano G, Giugliano F, Ciotola M, Quagliaro L, Ceriello A, Giugliano D. Inflammatory cytokine concentrations are acutely increased by hyperglycemia in humans: role of oxidative stress. Circulation. 2002 Oct 15;106(16):2067- 72.</p>
        <p>98. Loffreda S, Yang SQ, Lin HZ, Karp CL, Brengman ML, Wang DJ, Klein AS, Bulkley GB, Bao C, Noble PW, Lane MD, Diehl AM. Leptin regulates proinflammatory immune responses. Federation of American Societies for Experimental Biology Journal. 1998 Jan;12(1):57-65.</p>
        <p>99. Eng RH, Smith SM, Fan-Havard P, Ogbara T. Effect of antibiotics on endotoxin release from gram-negative bacteria. Diagn Microbiol Infect Dis. 1993 Mar-Apr;16(3):185-9.</p>
        <p>100. Patel C, Ghanim H, Ravishankar S, Sia CL, Viswanathan P, Mohanty P, Dandona P.Prolonged reactive oxygen species generation and nuclear factor-kappaB activation after a high-fat, high-carbohydrate meal in the obese. J Clin Endocrinol Metab. 2007 Nov;92(11):4476-9.</p>
        <p>101. Laugerette F, Alligier M, Bastard JP, Drai J, Chanséaume E, Lambert-Porcheron S, Laville M, Morio B, Vidal H, Michalski MC. Overfeeding increases postprandial endotoxemia in men: Inflammatory outcome may depend on LPS transporters LBP and sCD14. Mol Nutr Food Res. 2014 Jul;58(7):1513-8.</p>
        <p><a name="fn-102">102</a>. Trøseid M, Nestvold TK, Rudi K, Thoresen H, Nielsen EW, Lappegård KT.Plasma lipopolysaccharide is closely associated with glycemic control and abdominal obesity: evidence from bariatric surgery. Diabetes Care. 2013 Nov;36(11):3627-32.</p>
        <p>103. Ghanim H, Abuaysheh S, Sia CL, et al. Increase in plasma endotoxin concentrations and the expression of Toll-like receptors and suppressor of cytokine signaling-3 in mononuclear cells after a high-fat, high-carbohydrate meal: implications for insulin resistance. Diabetes Care 2009;32:2281–2287</p>
        <p>104. Ghanim H, Sia CL, Korzeniewski K, et al. A resveratrol and polyphenol preparation suppresses oxidative and inflammatory stress response to a high-fat, high-carbohydrate meal. J Clin Endocrinol Metab 2011;96:1409–1414</p>
        <p>105. Scalbert A, Manach C, Morand C, Rémésy C, Jiménez L. Dietary polyphenols and the prevention of diseases. Crit Rev Food Sci Nutr (in press).</p>
        <p>106. Kaulmann A, Bohn T. Bioactivity of Polyphenols: Preventive and Adjuvant Strategies toward Reducing Inflammatory Bowel Diseases-Promises, Perspectives, and Pitfalls. Oxid Med Cell Longev. 2016;2016:9346470.</p>
        <p>107. Ghanim H, Sia CL, Upadhyay M, Korzeniewski K, Viswanathan P, Abuaysheh S, Mohanty P, Dandona P. Orange juice neutralizes the proinflammatory effect of a high- fat, high-carbohydrate meal and prevents endotoxin increase and Toll-like receptor expression.Am J Clin Nutr. 2010 Apr;91(4):940-9.</p>
        <p>108. Clemente-Postigo M, Queipo-Ortuño MI, Boto-Ordoñez M, Coin-Aragüez L, Roca- Rodriguez MM, Delgado-Lista J, Cardona F, Andres-Lacueva C, Tinahones FJ. Effect of acute and chronic red wine consumption on lipopolysaccharide concentrations. Am J Clin Nutr. 2013 May;97(5):1053-61.</p>
        <p>109. Ford CT, Richardson S, McArdle F, Lotito SB, Crozier A, McArdle A, Jackson MJ. Identification of (poly)phenol treatments that modulate the release of pro-inflammatory cytokines by human lymphocytes. Br J Nutr. 2016 May 28;115(10):1699-710</p>
        <p>110. Vendrame S, Guglielmetti S, Riso P, Arioli S, Klimis-Zacas D, Porrini M. Six-week consumption of a wild blueberry powder drink increases bifidobacteria in the human gut. J Agric Food Chem. 2011 Dec 28;59(24):12815-20.</p>
        <p>111. Hokayem M, Blond E, Vidal H, Lambert K, Meugnier E, Feillet- Coudray C, Coudray C, Pesenti S, Luyton C, Lambert-Porcheron S, Sauvinet V, Fedou C, Brun JF, Rieusset J, Bisbal C, Sultan A, Mercier J, Goudable J, Dupuy AM, Cristol JP, Laville M, Avignon A (2013) Grape polyphenols prevent fructose-induced oxidative stress and insulin resistance in first- degree relatives of type 2 diabetic patients. Diabetes Care 36:1454–1461</p>
        <p>112. Puupponen-Pimiä R, Nohynek L, Hartmann-Schmidlin S, Kähkönen M, Heinonen M, Määttä-Riihinen K, Oksman-Caldentey KM.Berry phenolics selectively inhibit the growth of intestinal pathogens. J Appl Microbiol. 2005;98(4):991-1000.</p>
        <p>113. Bertoia ML, Mukamal KJ, Cahill LE, Hou T, Ludwig DS, Mozaffarian D, Willett WC, Hu FB, Rimm EB.Changes in Intake of Fruits and Vegetables and Weight Change in United States Men and Women Followed for Up to 24 Years: Analysis from Three Prospective Cohort Studies. PLoS Med. 2015 Sep 22;12(9):e1001878.</p>
        <p>114. J Pérez-Jiménez, V Neveu, F Vos and A Scalbert. Identification of the 100 richest dietary sources of polyphenols: an application of the Phenol-Explorer database. European Journal of Clinical Nutrition (2010) 64, S112–S120</p>
        <p>115. Senn JJ, Klover PJ, Nowak IA, and Moony RA. IL-6 Induces Cellular Insulin Resistance in Hepatocytes. Diabetes. 2002; 51(12):3391-9.</p>
        <p>116. Chung HY, Kim HJ, Kim JW, Yu BP. The inflammation hypothesis of aging: molecular modulation by calorie restriction. Annals of the New York Academy of Sciences. 2001; 928:327-35.</p>
        <p>117. Boutagy NE, McMillan RP, Frisard MI, Hulver MW. Metabolic endotoxemia with obesity: Is it real and is it relevant? Biochimie. 2016 May;124:11-20.</p>
        <p>118. Patel C, Ghanim H, Ravishankar S, Sia CL, Viswanathan P, Mohanty P, Dandona P.Prolonged reactive oxygen species generation and nuclear factor-kappaB activation after a high-fat, high-carbohydrate meal in the obese. J Clin Endocrinol Metab. 2007 Nov;92(11):4476-9.</p>
        <p>119. Esposito K, Nappo F, Marfella R, Giugliano G, Giugliano F, Ciotola M, Quagliaro L, Ceriello A, Giugliano D. Inflammatory cytokine concentrations are acutely increased by hyperglycemia in humans: role of oxidative stress. Circulation. 2002 Oct 15;106(16):2067- 72.</p>
        <p>120. Dandona P, Aljada A, Bandyopadhyay A.: Inflammation: the link between insulin resistance, obesity and diabetes. Trends Immunol 2004; 25: 4–7</p>
        <p>121. Fishebin L, Biological effects of Dietary Restriction. Springer-Verlag, New York.1991.</p>
        <p>122. Dixit VD. Adipose-immune interactions during obesity and caloric restriction: reciprocal mechanisms regulating immunity and health span. Journal of Leukocyte Biology. 2008: 84:882-892.</p>
        <p>123. Pilon, B. Eat Stop Eat. 2013</p>
        <p>124. Cani PD, Amar J, Iglesias MA, Poggi M, Knauf C, Bastelica D, Neyrinck AM, Fava F, Tuohy KM, Chabo C, Waget A, Delmée E, Cousin B, Sulpice T, Chamontin B, Ferrières J, Tanti JF, Gibson GR, Casteilla L, Delzenne NM, Alessi MC, Burcelin R. Metabolic endotoxemia initiates obesity and insulin resistance. Diabetes. 2007 Jul;56(7):1761-72.</p>
        <p>125. Johnston KL, Thomas EL, Bell JD, Frost GS, Robertson MD. Resistant starch improves insulin sensitivity in metabolic syndrome. Diabet Med. 2010 Apr;27(4):391-7.</p>
        <p>126. Murphy MM et al. Resistant Starch Intakes in the United States J Am Diet Assoc. 2008;108:67-78</p>
        <p>127. Zhou J, Hegsted M, McCutcheon KL, Keenan MJ, Xi X, Raggio AM, Martin RJ. Peptide YY and proglucagon mRNA expression patterns and regulation in the gut. Obesity 2006;14:683-689.</p>
        <p>128. Englyst HN, Kingman SM, Hudson GJ, Cummings JH. Measurement of resistant starch in vitro and in vivo. Br J Nutr. 1996 May;75(5):749-55.</p>
        <p>129. Brinkworth GD, Noakes M, Clifton PM, Bird AR. Comparative effects of very low- carbohydrate, high-fat and high-carbohydrate, low-fat weight-loss diets on bowel habit and faecal short-chain fatty acids and bacterial populations. British Journal of Nutrition 2009;101:1493–502.</p>
        <p>130. Walker AW, Ince J, Duncan SH, Webster LM, Holtrop G, Ze X, et al. Dominant and diet- responsive groups of bacteria within the human colonic microbiota. ISME Journal 2011;5:220–30.</p>
        <p>131. Higgins JA, Higbee DR, Donahoo WT, Brown IL, Bell ML, Bessesen DH. Resistant starch consumption promotes lipid oxidation. Nutr Metab . 2004;1:8-18</p>
        <p>132. P Burton and HJ Lightowler. The impact of freezing and toasting on the glycaemic response of white bread. European Journal of Clinical Nutrition (2008) 62, 594–599</p>
        <p>133. Bird AR, Brown IL, Topping DL. Starch, resistant starch, the gut microflora and human health. Current Issues Int Micro. 2000;1:25-37.</p>
        <p>134. Pendyala S, Walker JM, and Holt PR. A High-Fat Diet Is Associated With Endotoxemia That Originates From the Gut. Gastroenterology 2012;142:1100–1101</p>
        <p>135. Bala S, Marcos M, Gattu A, Catalano D, Szabo G.Acute binge drinking increases serum endotoxin and bacterial DNA levels in healthy individuals. PLoS One. 2014 May 14;9(5):e96864.</p>
        <p>136. A. Piendl and M. Biendl, Freising-WeihenstephanPhysiological significance of polyphenols and hop bitters in beer. Brauwelt International Technical Feature</p>
        <p>137. Yeh CC, Kao SJ, Lin CC, Wang SD, Liu CJ, Kao ST. The immunomodulation of endotoxin-induced acute lung injury by hesperidin in vivo and in vitro. Life Sci. 2007 Apr 24;80(20):1821-31.</p>
        <p>138. Bosenberg, A.T., Brock-Utne, J.G., GaYn, S.L., Wells, M.T., Blake, G.T., Strenuous exercise causes systemic endotoxemia. Journal of Applied Physiology 1988; 65, 106–108.</p>
        <p>139. Brock-Utne, J.G., GaYn, S.L., Wells, M.T., Gathiram, P., Sohar, E., James, M.F., et al. Endotoxemia in exhausted runners after a long-distance race. South African Medical Journal 1988; 73, 533–536.</p>
        <p>140. Camus, G., Poortmans, J., Nys, M., Deby-Dupont, G., Duchateau, J., Deby, C., et al., 1997. Mild endotoxemia and the inflammatory response induced by a marathon race. Clinical Science (London) 92, 415–422.</p>
        <p>141. Claus SP et al. The gut microbiotia: a major player in the toxicity of environmental pollutants. Biofilms and Microbiomes 2016 2 16003</p>

        </div><!-- .references.hidden -->

    </div><!-- .content -->
</div><!-- .section -->


<? /*
<div class="section footer">
    <div class="content rich-text">

        <!-- Footer -->

    </div><!-- .content -->
</div><!-- .section -->
*/ ?>


<br><br>


</body>
</html>
