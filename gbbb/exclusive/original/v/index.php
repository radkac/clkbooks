<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Good Belly, Bad Belly / Digital Edition Pre-Release</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="../web/s/zhtml.css?v=3">
    <link rel="stylesheet" type="text/css" href="../web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="../web/s/mobile.css?v=3">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

	<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

	<script>
	function preloadImages(arrayOfImages) {
		$(arrayOfImages).each(function(){ $('<img/>')[0].src = this; });
	}
	preloadImages([
		'web/i/bg-3.jpg',
		'web/i/check-icon.png'
	]);
	</script>
</head>
<body>

<div class="bglayer"></div>

<div id="bg-placeholder">
<div id="top-band"></div>
<div id="layout" class="clearfix">

    <div id="header" class="clearfix">
        <h1>Good Belly, Bad Belly</h1>
        <div class="video-script">
            <p>By Brad Pilon</p>

            <h2>Are The Foods You Eat Triggering Health Harming “Obesity Toxins” In Your Gut?</h2>

            <p>Did you know certain foods trigger the release of toxins right inside your digestive system? And that these toxins can actually cause weight gain, diabetes, heart disease and more?</p>
            <p>And did you know just subtle shifts in what you eat, or even how you combine certain foods, can eliminate a huge amount of these “Obesity Toxins” from ever being released and absorbed in your body?</p>
            <p>I’ll tell you how in a second. And I'll shock you by revealing the one food many "experts" are now telling you to avoid that can protect you even when you make bad diet decisions. First I need to come clean&hellip;</p>

            <p><i>Confession:</i> For years I was one of the health authors insisting that <b><i>‘calories in, calories out’</i></b> is all that really matters when it comes to losing weight and maintaining health.</p>
            <p>And because of that, I could never think of a good reason to tell folks like you to <i>“<u>eat healthy</u>.”</i></p>
            <p>After all, the scientific data seemed to say that <b>eating ice cream or broccoli didn’t really matter</b> as long as you got the calories right.</p>
            <p>Listen: I’ve never doubted the fact that a healthy diet is beneficial. It’s just I could never find the <b><u>scientific</u> explanation</b> good enough for me to feel confident telling YOU that it’s important&hellip;</p>
            <p>However NOW I’ve finally found <u>the missing link</u>&hellip;</p>
            <p>And I can tell you exactly why &mdash; from a scientific standpoint &mdash; eating certain <u><i>healthy</i></u> foods, <u>combined</u> with a responsible calorie intake is the optimal way to lose weight and stay healthy. And that includes a totally natural food that some "experts" are now telling you is unhealthy! (total bunk)</p>
            <p>Truth: you WILL lose weight and improve health by simply eating less. However you’ll lose it faster and get more striking health benefits in less time when you combine eating less with eating the right kinds of healthy foods in the right combinations.</p>
            <p>And you don’t need to turn your diet upside down. In fact just a few simple and subtle shifts in what you eat can make immediate improvements in your health and help you lose weight.</p>

            <h2>Obesity Toxins: The REAL Reason Food Makes You Fat &amp; Sick</h2>

            <p>Common wisdom boils digestion down to a simple equation &mdash; eat food + digest food + absorb calories and nutrients.</p>
            <p>The truth is, the latest science demonstrates that the process is MUCH more complex. And that calories and nutrients are just a small part of what’s happening when you eat.</p>
            <p>You’ve probably heard that your gut is full of bacteria, right? And that “good” bacteria is important for your health. And that’s why probiotics have become so popular all of a sudden.</p>
            <p>However… Although it’s important to improve the bacterial balance in your gut, there’s a much more immediate danger to your health that you <u>need to take care of right now</u>&hellip;</p>
            <p>It’s the TOXINS lurking inside your digestive system as we speak.</p>
            <p>These are referred to as “Endotoxins” because they are created right inside your own body.</p>
            <p>And seemingly harmless foods you swallow right now can cause the Obesity Toxins living in your own gut to enter your body causing everything from obesity to diabetes, cancer, heart disease, autoimmune disease and more&hellip;</p>

            <h2>Get Instant Relief So You Lose Weight Faster &amp; Rebuild Your Health Now</h2>

            <div class="insertion right">
                <center><img class="mobile-half-width" src="../web/i/ecover.jpg" alt=""></center>
            </div>

            <p>Look: There are hundreds books on the shelves nowadays dealing with “gut health.” Yet the problem with ALL of them is they only focus on the very <b>long</b> term process of improving the balance of your gut bacteria.</p>
            <p>Yes, I do believe there is benefit in gradually rebuilding what these books call your “microbiome” and I’ll show you how to do that too. However&hellip;</p>
            <p>My main goal with <b><i>Good Belly, Bad Belly</i></b> is to provide you with simple things you can start doing today to get immediate relief from the damage Endotoxins are doing to your body and your health right now.</p>
            <p><u><b>Even if you keep eating 90% of the foods you are eating already</b></u>, there are subtle shifts you can make in what you are eating WITH those foods that will make a huge reduction in the formation and absorption of endotoxins in your system.</p>
            <p>Here's a great example of something you can start doing today. <b>Just add a glass or two of orange juice to your diet.</b> Contrary to what many experts are now recommending, this will protect you against the toxins created by bad fats and sugars in your diet. And I explain exactly why in the book.</p>
            <p>Listen: you can read through the entire <i>Good Belly, Bad Belly</i> book in only a couple hours and start applying these life changing tricks right away.</p>
            <p>This isn’t stuff you have to wait months to benefit from. It’s action steps you can take today to prevent endotoxins from chipping away at your health and adding inches to your waistline. And it doesn’t require you to make painful changes to your entire diet.</p>
            <p>Good Belly, Bad Belly is currently ONLY available as a pre-release limited-time special offer here on CLK*Books. To get it now at this discounted pre-sale price just use the link on this page to secure the special deal for my current customers only.</p>

            <table class="pricing">
                <thead>
                    <tr>
                        <td colspan="2">Special Pre-Release Price</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="k">List Price:</td>
                        <td class="v">$19.99</td>
                    </tr>
                    <tr>
                        <td class="k">CLK*Books Price:</td>
                        <td class="v red"><b>$10.00*</b></td>
                    </tr>
                    <tr>
                        <td class="k">You Save:</td>
                        <td class="v red"><b>$9.99</b> (50%)</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">*Includes free international wireless delivery</td>
                    </tr>
                </tfoot>
            </table>

            <p class="purchase first">
                <a class="button" href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&vtid=clk31279og&cbskin=18285">Get Access Now!<i></i></a>
                <span class="link"><a href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&vtid=clk31279og&cbskin=18285">Get the <i class="cl-red">Good Belly, Bad Belly</i> digital book for Just $10</a></span>
                <span class="accepted"><img class="mobile-full-width" src="../web/i/purchase-accepted-cards.png" alt=""></span>
            </p>
            <br><br>
        </div>
    </div>

    <div id="footer" class="clearfix">
        <p class="cb-info">ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products. Your credit card statement will show a charge from CLKBANK* All testimonials are true. Some identities and photos have been altered for the privacy and protection of requested individuals. Customer will pay shipping and handling fee. Questions or concerns? Contact us at support@specforcealpha.com.</p>
        <p>
            For support please contact us: help [at] clkbooks.com<br>
            Copyright &copy; clkbooks.com. All Rights Reserved.<br>
        </p>
    </div><!-- #footer -->

</div><!-- #layout -->
</div><!-- #bg-placeholder -->

<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
