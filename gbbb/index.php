<?php require_once('../prices.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Good Belly Bad Belly</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=10">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=10">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=10">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="/web/js/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/gbbb/web/i/fb-og-gbbb.png">
	<meta property="og:description" content="Good Belly, Bad Belly: Are The Foods You Eat Triggering Health-Harming “Obesity Toxins” In Your Gut?">
	<meta property="og:title" content="Check Out What I'm Reading...">

<script>

$(function(){

    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });

});

</script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="tagline">
        <div class="inner">
            Discover the latest research on the surprising link between gut-bacteria, endotoxins and weight gain
        </div>
    </div>

    <div id="breadcrumbs">
        Books › Weight Loss Books › New Releases
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Book</p>
                <h1>Good Belly, Bad Belly</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating">Digital Edition Pre-Release</p>
            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img class="main-ecover mobile-half-width" src="web/i/eCover-New.png?v=1" width="100%" alt="">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>
                        Special Pre-Release Price
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$<?php echo $gbbb; ?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$9.99</b> (50%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?php echo $gbbb; ?></td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&cbskin=18285&vtid=clk31279"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Book</p>
                <h1>Good Belly, Bad Belly</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating">Digital Edition Pre-Release</p>
                </div>

                <h2>Are The Foods You Eat Triggering Health-Harming “Obesity Toxins” In Your Gut?</h2>

                <p>Did you know certain foods trigger the release of toxins right inside your digestive system? And that these toxins can actually cause weight gain, diabetes, heart disease and more?</p>
                <p>And did you know just subtle shifts in what you eat, or even how you combine certain foods, can eliminate a huge amount of these “Obesity Toxins” from ever being released and absorbed in your body?</p>
                <p>I’ll tell you how in a second. And I'll shock you by revealing the one food many "experts" are now telling you to avoid that can protect you even when you make bad diet decisions. First I need to come clean&hellip;</p>

                <div class="read-more-content collapsed hidden">

                <p><i>Confession:</i> For years I was one of the health authors insisting that <b><i>‘calories in, calories out’</i></b> is all that really matters when it comes to losing weight and maintaining health.</p>
                <p>And because of that, I could never think of a good reason to tell folks like you to <i>“<u>eat healthy</u>.”</i></p>
                <p>After all, the scientific data seemed to say that <b>eating ice cream or broccoli didn’t really matter</b> as long as you got the calories right.</p>
                <p>Listen: I’ve never doubted the fact that a healthy diet is beneficial. It’s just I could never find the <b><u>scientific</u> explanation</b> good enough for me to feel confident telling YOU that it’s important&hellip;</p>
                <p>However NOW I’ve finally found <u>the missing link</u>&hellip;</p>
                <p>And I can tell you exactly why &mdash; from a scientific standpoint &mdash; eating certain <u><i>healthy</i></u> foods, <u>combined</u> with a responsible calorie intake is the optimal way to lose weight and stay healthy. And that includes a totally natural food that some "experts" are now telling you is unhealthy! (total bunk)</p>
                <p>Truth: you WILL lose weight and improve health by simply eating less. However you’ll lose it faster and get more striking health benefits in less time when you combine eating less with eating the right kinds of healthy foods in the right combinations.</p>
                <p>And you don’t need to turn your diet upside down. In fact just a few simple and subtle shifts in what you eat can make immediate improvements in your health and help you lose weight.</p>

                <h2>Obesity Toxins: The REAL Reason Food Makes You Fat &amp; Sick</h2>

                <p>Common wisdom boils digestion down to a simple equation &mdash; eat food + digest food + absorb calories and nutrients.</p>
                <p>The truth is, the latest science demonstrates that the process is MUCH more complex. And that calories and nutrients are just a small part of what’s happening when you eat.</p>
                <p>You’ve probably heard that your gut is full of bacteria, right? And that “good” bacteria is important for your health. And that’s why probiotics have become so popular all of a sudden.</p>
                <p>However&hellip; Although it’s important to improve the bacterial balance in your gut, there’s a much more immediate danger to your health that you <u>need to take care of right now</u>&hellip;</p>
                <p>It’s the TOXINS lurking inside your digestive system as we speak.</p>
                <p>These are referred to as “Endotoxins” because they are created right inside your own body.</p>
                <p>And seemingly harmless foods you swallow right now can cause the Obesity Toxins living in your own gut to enter your body causing everything from obesity to diabetes, cancer, heart disease, autoimmune disease and more&hellip;</p>

                <h2>Get Instant Relief So You Lose Weight Faster &amp; Rebuild Your Health Now</h2>

                <p>Look: There are hundreds books on the shelves nowadays dealing with “gut health.” Yet the problem with ALL of them is they only focus on the very <b>long</b> term process of improving the balance of your gut bacteria.</p>
                <p>Yes, I do believe there is benefit in gradually rebuilding what these books call your “microbiome” and I’ll show you how to do that too. However&hellip;</p>
                <p>My main goal with <b><i>Good Belly, Bad Belly</i></b> is to provide you with simple things you can start doing today to get immediate relief from the damage Endotoxins are doing to your body and your health right now.</p>
                <p><u><b>Even if you keep eating 90% of the foods you are eating already</b></u>, there are subtle shifts you can make in what you are eating WITH those foods that will make a huge reduction in the formation and absorption of endotoxins in your system.</p>
                <p>Here's a great example of something you can start doing today. <b>Just add a glass or two of orange juice to your diet.</b> Contrary to what many experts are now recommending, this will protect you against the toxins created by bad fats and sugars in your diet. And I explain exactly why in the book.</p>
                <p>Listen: you can read through the entire <i>Good Belly, Bad Belly</i> book in only a couple hours and start applying these life changing tricks right away.</p>
                <p>This isn’t stuff you have to wait months to benefit from. It’s action steps you can take today to prevent endotoxins from chipping away at your health and adding inches to your waistline. And it doesn’t require you to make painful changes to your entire diet.</p>
                <p>Good Belly, Bad Belly is currently ONLY available as a pre-release limited-time special offer here on CLK*Books. To get it now at this discounted pre-sale price just use the link on this page to secure the special deal for my current customers only.</p>

                <ul class="checkmarks">
                    <li>Thought-Provoking Books</li>
                    <li>New Year's Resolutions</li>
                    <li>Healthy Choices</li>
                </ul>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>
                        Special Pre-Release Price
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$<?php echo $gbbb; ?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$9.99</b> (50%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?php echo $gbbb; ?></td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://115.eatstopeat.pay.clickbank.net/?cbfid=31279&cbskin=18285&vtid=clk31279"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>


        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
