<?php
//LOAD COOKIE SCRIPT
require($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
?>
<html><head>

    <title>Add Red Juice </title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<style>

@import url('https://fonts.googleapis.com/css?family=Roboto');

/* Forced responsive code */

.lp-pom-body, .lp-positioned-content, #lp-pom-root, #lp-pom-block-8, #lp-code-10 {  width:100% !important; display:table !important; float:left !important; left:0px; top:0px; margin: 0 !important; height: 0; overflow: visible; }


	#fullwidth { width: 100%; background-color: #fffff;  }
	#afloatingmastercontainer { width:100%; max-width: 920px !important; margin-left: auto; margin-right: auto; background-color:#FFFFFF;  padding: 10px 0 10px 0; /*box-shadow: 0px 5px 16px 1px #d5d5d5;*/     border: 1px solid #d0d0d0; margin-top: 15px;}
	#fullpad { width:100%; display: table !important; }


/* Start page styles */
body{ position: relative; margin-left: 0; margin-right: 0; margin-top: 0; background: #EEEEEE;}
/*#fullwidth {
	background: url("http://clkbooks.com/web/i/home-promo-bg.jpg");
	background-repeat: no-repeat;
	background-size: cover;
	background-attachment: fixed;

}*/
.background {
	background: #51c13e;
	width: 100%;
/*	position: absolute;
	top: 75px;
	z-index: -1;*/
}
.logo {
	max-width: 220px;
	display: block;
	margin: 0 auto;
	padding: 10px 0;
}

.cong {
	background: #0282C7;
	width: 94%;
	margin: 0 auto;
	padding: 8px 0;
	box-sizing: border-box;
	text-align: center;
	font-size: 24px;
	color: #fff;
	margin-top: 35px;
}


/* Edge Padding */
	h1, h2, h3, h4, p, ul  { padding: 0 4% 0 4%; }


/* Set body and heading fonts */
	h1, h2, h3, h4  { font-family: 'Roboto', sans-serif; }
	*, p, ul, li  { font-family: 'Open sans', sans-serif; }

/* Set heading and body specifics, size, colors */
	h1, h2, h3, h4 {
	margin: 40px 0 19px 0;
	text-align: center;
	color: #000000;
}
	h1 {
	font-size: 40px;
	line-height: 1.2em;
	font-weight: normal;
	text-align: center;
	color: #333;
	clear: both;
	border-top: 0;
}
	h2 {
	font-size: 30px;
	line-height: 1.1em;
	font-weight: bold;
	color: #dd0000;
	display: inline-block;
}
	h3 {
	font-size: 24px;
	line-height: 1.4em;
	color: #1E71C7;
}
	p , ul {
	font-size: 20px;
	text-align: left;
	margin: 0 0 20px 0;
	line-height: 1.4em;
}

	sub { font-size: 12px; }
/* Floats and Layouts */
	.right { float: right; }
	.left { float: left; }
	.center { text-align: center; }

	.block { display: block; }

	.full { width: 100%; }
	.half { width: 50%; }
	.third { width: 33.32%;  }
	.fourth { width: 25%; }
	.onefifth { width: 20%;  }
	.twofifth { width: 40%;  }
	.threefifth { width: 60%;  }
	.fourfifth { width: 80%;  }
	.ffifth { width: 80%;  }
	.onesixth { width: 16.6%;  }
	.fivesixth { width: 83.3%;  }

	.clear { clear: both; }

	.footer { margin-left: auto; margin-right: auto; width: 100%; max-width: 900px; text-align: center; }
	.footer p, .footer a { font-size: 16px; color: #404040; text-align: center; }


/* Padding and margins in pixels - even numbers only, up to 30px */
	.pad2px { padding: 2px; } .mar2px { margin: 2px; }
	.pad4px { padding: 4px; } .mar4px { margin: 4px; }
	.pad6px { padding: 6px; } .mar6px { margin: 6px; }
	.pad8px { padding: 8px; } .mar8px { margin: 8px; }
	.pad10px { padding: 10px; }  .mar10px { margin: 10px; }
	.pad12px { padding: 12px; }  .mar12px { margin: 12px; }
	.pad14px { padding: 14px; }  .mar14px { margin: 14px; }
	.pad16px { padding: 16px; }  .mar16px { margin: 16px; }
	.pad18px { padding: 18px; }  .mar18px { margin: 18px; }
	.pad20px { padding: 20px; }  .mar20px { margin: 20px; }
	.pad22px { padding: 22px; }  .mar22px { margin: 22px; }
	.pad24px { padding: 24px; }  .mar24px { margin: 24px; }
	.pad26px { padding: 26px; }  .mar26px { margin: 26px; }
	.pad28px { padding: 28px; }  .mar28px { margin: 28px; }
	.pad30px { padding: 30px; }  .mar30px { margin: 30px; }


/* Bigger Fonts for P tags */
	p.fontsizeBIGGER1 { font-size: 20px; }
	p.fontsizeBIGGER2 { font-size: 26px; }
	p.fontsizeBIGGER3 { font-size: 30px; }

/* Text colors */
	.color-pink { color: #ef1571; }
	.color-pinklight { color:#FF9C9D; }
	.color-cyan { color:#0BACFF; }
	.color-red { color:#dd0000; }
	.color-green { color:#10C400; }
	.color-graydark { color:#4D4D4D; }
	.color-graymedium { color:#A1A1A1; }
	.color-graylight { color:#CCCCCC; }
	.highlight-blue { background-color: #B5DBEE; }
	.highlight-yellowLIGHT { background-color:#FDFFA3; }
	.highlight-yellow { background-color:#F7FF0C; }


/* Video Embedding  */
	.video-holder {  margin: 0 auto 20px; max-width: 900px; }
	.videoWrapper { height: 0; padding-bottom: 56.25%; position: relative; }
	.videoWrapper iframe { height: 100%; left: 0; position: absolute; top: 0; width: 100%;}


/* Fancy Bullet Lists  */
	ul.xmarks { background-color:#F1F1F1; border-left: 2px solid #CB1D20; margin: 0 0 30px 0; padding-top: 18px; padding-bottom: 9px;  }
	ul.xmarks li { list-style: none; padding: 0 0 18px 50px; margin: 8px 38px 0px 0px; line-height: 1.3em; background-image: url(https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/x-mark.png); background-size: 31px; background-repeat: no-repeat; background-position: 0 0;}

	ul.checkmarks { background-color:#edf9ff; border-left: 2px solid #CB1D20; margin: 0 0 30px 0; padding-top: 18px; padding-bottom: 28px;  }
	ul.checkmarks li { list-style: none; padding: 16px 0 16px 50px; margin: 0px 38 0px 0px; line-height: 1.3em; background-image: url(https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/check-mark.png); background-size: 31px; background-repeat: no-repeat; background-position: 0 17px; border-bottom: 1px solid #BABABA}

	ul.checkmarks-reversed { background-color:#FFEFEF; margin: 0 0 30px 0; padding-top: 18px; padding-bottom: 28px;  }
	ul.checkmarks-reversed li { list-style: none; color: #000; padding: 16px 0 16px 50px; margin: 0px 38 0px 0px; line-height: 1.3em; background-image: url(https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/check-mark.png); background-size: 31px; background-repeat: no-repeat; background-position: 0 17px; border-bottom: 1px solid #000; }





	.shadow-lightgray { box-shadow: 0px 0px 16px 4px #d5d5d5; }
	.shadow-yellow { box-shadow: 0px 0px 16px 1px #F5FF1C; }






/* Certification Seal Box  */
.certbox h2 {
	/*background: #c4d6d6;*/
	border-bottom: 2px dashed #5e7979;
    width: 100%;
    margin-top: 0px;
    padding: 25px;
    box-sizing: border-box;}

.certbox p {  font-size: 20px; line-height: 30px; max-width: 100%; text-align: center; padding-left: 10px; padding-right: 10px;  }
.certbox p.headlinemoneyback {  font-size: 37px; font-weight: bold; color: #01ae15; border-bottom: 1px solid #BD9A54; padding-top: 30px; padding-bottom: 30px; display: block; }


/*.certbox { float: left; width: 90%; margin-left: 5%;  }*/

.certbox  .certbox-toprow { float: left; width: 100%; }
.certbox  .certbox-toprow .certbox-col2 { background-image: url(https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2756assets/certborder-edge-top-8345298203475.jpg); }

.certbox  .certbox-centerrow { float: left; width: 100%; }
.certbox  .certbox-centerrow .certbox-col2 { float: left; width: 90%; padding: 5%; background: #ffffe5; background-size: 100%; clear: both; }
.certbox  .certbox-centerrow .certbox-col2 .certbox-pad {
	float: left;
    background: #d4e6e6;
    border: #5e7979 2px dashed;
    width: 100%;
    padding: 10px;
    box-sizing: border-box; }

.certbox  .certbox-bottomrowr { float: left; width: 100%; margin-bottom: 15px; }
.certbox  .certbox-bottomrowr .certbox-col2 { background-image: url(https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2756assets/certborder-edge-bottom-8345298203475.jpg); }

.certbox  .certbox-col2 { width: 100%; }

/* Cert Border Size -- keep both numbers below the same  */
.certbox  .certbox-toprow .certbox-col2,
.certbox  .certbox-toprow .certbox-col2 .certbox-col1,
.certbox  .certbox-toprow .certbox-col2 .certbox-col3,
.certbox  .certbox-bottomrowr .certbox-col2,
.certbox  .certbox-bottomrowr .certbox-col2 .certbox-col1,
.certbox  .certbox-bottomrowr .certbox-col2 .certbox-col3 { height: 90px; }

/* CTA   */
	.products-section .full { background: #f6f6fa; border: 1px solid #e5e5e5; mpadding: 20px 10px; }
	.ctatopbar {
	text-align: center;
	min-height: 0px;
}
	.cartbutton { width: 100%; max-width: 270px; }
	.sixtydayseal { max-width: 240px; }
	.products-section p { text-align: center; padding: 0px; margin: 0; }
	.products-section .long-description { font-size: 16px; margin: 0 10% 0 10%; line-height: 1.4em; }
	.products-section h2 {     text-align: center; padding: 0px; font-size: 27px; margin: 20px 10% 15px 10% !important; width: 80%; }
	.products-section h2 { margin-bottom: 10px; }
	.products-section .most-popular p { font-size: 23px; padding: 5px; }
	.products-section .most-popular { border: 1px solid #ef1571; background-color: #ef1571; color: #fff; font-weight: bold; margin: 0px auto 0px 1px; width: 310px; text-align: center; }
	.products-section .border-div-pro { background: #f6f6fa; border: 1px solid #e5e5e5; padding: 20px 10px; /* margin: 0 3px; */ box-shadow: 0px 0px 1px 1px #d5d5d5; }
	.border-div-pro h2 span { font-size: 21px; }
	.products-section p.save-percent {
	font-weight: bold;
	font-size: 33px;
	text-transform: uppercase;
	letter-spacing: -1px;
}
	.products-section p.price { font-size: 38px; padding: 0px; max-width: 100%; }
	.products-section p.price span { font-size: 20px; }
	.products-section p.loyalty,
	.products-section .type { font-size: 20px; }
	.products-section .type { max-width: 100%; padding: 0px; margin-top: 20px; margin-bottom: -98px; }
	.products-section .img-products { width: 90%; margin: 0px auto;text-align: center; }
	.products-section div.most { width: 67%; margin: 0px 31px 0px 318px; }
	.products-section .most-popular2 p { color: #fff; }
	.products-section .most-popular2 p span { color: #cc0000; }
	.products-section .most-popular2 { border: 1px solid #ffda01; background-color: #ffda01; color: #fff; font-weight: bold; margin: 0 auto; width: 310px; text-align: center; }
	.products-section .most-popular2 { border: 1px solid #f04900; background-color: #f04900; margin: 0 auto 0px 0px; width: 310px;}
	.products-section a { color: #000; text-decoration: none; display: block }
	.products-section .third .full { margin-bottom: 120px; }
	.products-section .description { font-size: 26px; }
	.products-section .each {
	font-size: 16px;
}

	.sixtydayseal {
	max-width: 184px;
}

	.secure-icons { width: 100%; text-align: center; }
	.secure-icons img {
	width: 100%;
	max-width: 177px;
}

	.img-products { width: 100%; text-align: center; }
	.img-products img { width: 100%; }
	.add-to-cart { text-align: center;margin-top:14px;margin-bottom:7px; }
	.add-to-cart p { font-weight: 700; }

@media screen and (max-width: 390px) {
     p { font-size: 17px; }
    h1 { font-size: 29px; }
    h2 { font-size: 23px; }
	.half { width: 100%; }
	.third { width: 100%; }
	blockquote { margin: 0; }
	.certbox p { font-size: 21px; line-height: 1.3em; }

}

@media screen and (max-width: 590px) {
	.products-section .third { width: 100%; }

}

	.testimonial {
	float: left;
	width: 100%;
	display: block;
}
	.testimonial p {
	font-family: Baskerville, "Palatino Linotype", Palatino, "Century Schoolbook L", "Times New Roman", "serif";
	display: block;
	margin-left: 15%;
	width: 70%;
}
	.testimonial strong { font-family: Baskerville, "Palatino Linotype", Palatino, "Century Schoolbook L", "Times New Roman", "serif"; }

	.buynow-section { padding: 20px; }
	.buynow-section a {
		display: block;
		margin: 0 auto;
		text-decoration: none;
	    width: 100%;
	    max-width: 367px;
	    background: rgba(169,31,35,1);
	    text-shadow: 1px 1px #0f0000;
	    color: #fff;
	    border: 2px solid black;
	    font-size: 55px;
	    line-height: 78px;
	    font-weight: bold;
	    font-family: Arial,Helvetica,sans-serif;
	    text-align: center;
	}
	.buynow-section a:hover {
	    background: #000;
	    color: #fff;
	    border: 2px solid rgba(169,31,35,1);
	}

	.img-top { width: 100%; max-width: 402px; }
	.img-btm { width: 100%; max-width: 366px; }

	.lock {
		position: absolute;
	    margin-top: -100px;
	    margin-left: 16px;
	}



</style>


	  	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
	  	<style type="text/css">
	  	.half1 {width: 50%; }
.half1 {width: 100%; }
        .third1 {width: 33%;  }
.third1 {width: 100%; }
        .img-products1 { width: 100%; text-align: center; }
        .img-products1 img { width: 70%; margin-top: 22px; }
        .third2 {width: 33.32%;  }
.third2 {width: 100%; }
        </style>

	<?php require($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

  <div class="background">
  	<img class="logo" src="http://clkbooks.com/web/i/head-logo.png" alt="" ">
  </div>


<div id="fullwidth">


<div id="afloatingmastercontainer" >
	<p class="center" style="font-size: 16px; font-weight: bold; color: #dd0000; margin-bottom: 0; margin-top: 15px;"><strong>Do Not Hit Your “Back” Button</strong></p>
	<p class="center" style="font-size: 16px;">Pressing your “back” button can cause errors in your order</p>

 <div style="margin: 0; padding: 0; border: 0; text-align: center;"><!-- <img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/logo.jpg" width="186" height="41" class="center" style=" max-width:233px;" /> -->

<p class="cong"><strong>Congratulations On Your Order!</strong></p>

<h1 style="font-size: 2.75em; margin: 15px 20px 13px; padding: 15px 20px 0;">
<i>Revealed:</i> The <u><b>Shortcut</b></u> to all the benefits of Good Belly, Bad Belly in a single glass of delicious juice!&hellip;
</h1>
<h1 style="font-size: 1.95em; margin: .5em 0 .75em; padding: 10px 20px 15px; ">
Now How Would You Like To <strong style="color: #dd0000;">Add The Exact 11 Super Foods</strong> That Fight <strong style="color: #dd0000;">Sagging Skin, Help Bring Back Your Prime, Protect you from Endotoxins, and ‘Wake Up’ Your Brain&hellip;</strong> In One Easy-to-Mix Delicious Drink?</h1>

<img src="https://user-assets-unbounce-com.s3.amazonaws.com/29fbc796-d33c-11e3-93ce-22000ac464fa/819ce60c-9f66-4f46-9f85-cab086a965c4/redjuicebottlewithmodel3-jpg.original.png" class="right half" style="margin: -4em 0 0 20px; width: 27%;">

<p>When you invested in <i>Good Belly, Bad Belly</i> today, you made the decision to invest in yourself and your health. And to make it even easier to get the results you deserve, I have teamed up with the folks at Organifi to offer you an easy and affordable way to get a healthy.</p>
<p>Just one serving a day will boost all the benefits you get from the <i>Good Belly, Bad Belly</i> lifestyle. You’ll give your body, skin, and BRAIN an instant rush of:</p>

<div style="border-top:1px solid #ccc;border-bottom:1px solid #ccc;background:#fefee5; width:100%; overflow:hidden;">

<br>

<h2 style="font-size:2.5em;margin:.35em 0;">Remember: Every One Of These Powerful Ingredients Boosts The <u><i>Good Belly</i></u> Effect&hellip;</h2>
<h2 style="font-size:2.5em;margin:.35em 0;">But Discovering These 11 Super Foods Was Only Half the Battle&hellip;</h2>

<p>They had the ingredients&hellip; but now they had to figure out how to pack the perfect blend into a simple scoop that tasted great and helped your skin, energy, and mental clarity.</p>
<p>Eventually they nailed it. The result?</p>

<br>

</div>

<h2 style="font-size:2.5em;margin:.75em 0 0;">Because You Invested In <i>Good Belly, Bad Belly Today</i>, You Get All These Benefits for
Less Than $2 A Day</h2>
<h3 style="margin: .65em 0;">(But Only While You’re On This Page)</h3>

<p>Just getting your hands on many of these ingredients that are only found in the world’s most remote areas would be next to impossible… and you darn sure wouldn’t be able to get them for anywhere near $2!</p>
<p>And that’s only the financial cost. Think about the enormous cost in time and effort doing shopping, chopping, and juicing as well.</p>
<p>So why go through all that trouble when you can get all the benefits for a fraction of the cost? And in just mere seconds a day, by simply mixing and drinking? And in a ready-made recipe that tastes absolutely delicious?</p>
<p>Here are your options to order:</p>


<!-- Purchase section (begin) -->
<div class="products-section full left" id="products">
  <div class="third left">
    <div class="ctatopbar" style="background-color:#FFF; color: #FFF; font-weight: bold; padding: 6px;">–</div>
    <div class="full left">
      <h2 class="color-graydark"> <span>One Bottle</span></h2>
      <p class="description" style="margin-top: 24px;"><strong><s>$79.95</s></strong></p>
      <p class="each">$69.00</p>
      <p class="price"><strong>Save $10</strong></p>
      <p class="loyalty" style="font-size:13px;">+Shipping </p>

      <div class="add-to-cart">
        <!--a href="http://organifijv.pay.clickbank.net/?cbur=a&cbrblaccpt=true&cbitems=orj1sub.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-ordersubscripotion.png" width="716">
        </a-->
        <a href="http://organifijv.pay.clickbank.net/?cbur=a&cbitems=orj1otjv.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-buynow.png" width="716">
        </a>
      </div>

      <div class="secure-icons"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/security-icons1.png"></div>
      <div class="img-products1"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/tub-1.png"></div>
      <p class="type"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/30-money.png" alt="" class="sixtydayseal"></p>
    </div>
  </div>
  <div class="third left">
    <div class="ctatopbar" style="background-color:#6BCE75; color: #FFF; font-weight: bold; padding: 6px;">MOST POPULAR</div>
    <div class="full left">
      <h2 class="color-graydark"> <span>Three Bottles</span></h2>
      <p class="description" style="margin-top: 24px;"><strong><s>$239.85</s></strong></p>
      <p class="each">$149.95</p>
      <p class="price color-green"><strong class="">Save $89</strong></p>
      <p class="loyalty" style="font-size:13px;">+Shipping</p>

      <div class="add-to-cart">
        <!--a href="http://organifijv.pay.clickbank.net/?cbur=a&cbrblaccpt=true&cbitems=orj3sub.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-ordersubscripotion.png" width="716">
        </a-->
        <a href="http://organifijv.pay.clickbank.net/?cbur=a&cbitems=orj3otjv.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-buynow.png" width="716">
        </a>
      </div>

      <div class="secure-icons"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/security-icons1.png"></div>
      <div class="img-products1"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/tub-3.png"></div>
      <p class="type"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/30-money.png" alt="" class="sixtydayseal"></p>
    </div>
  </div>
  <div class="third left">
    <div class="ctatopbar" style="background-color: #AF2326; color: #FFF; font-weight: bold; padding: 6px;">BEST VALUE!</div>
    <div class="full left">
      <h2 class="color-graydark"> <span>Six Bottles</span></h2>
      <p class="description" style="margin-top: 24px;"><strong><s>$479.70</s></strong></p>
      <p class="each">$249.95</p>
      <p class="price color-red"><strong>Save $229</strong></p>
      <p class="loyalty" style="font-size:13px;">+Shipping</p>

      <div class="add-to-cart">
        <!--a href="http://organifijv.pay.clickbank.net/?cbur=a&cbrblaccpt=true&cbitems=orj6sub.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-ordersubscripotion.png" width="716">
        </a-->
        <a href="http://organifijv.pay.clickbank.net/?cbur=a&cbitems=orj6otjv.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-buynow.png" width="716">
        </a>
      </div>

      <div class="secure-icons"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/security-icons1.png"></div>
      <div class="img-products1"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/tub-4.png"></div>
      <p class="type"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/30-money.png" alt="" class="sixtydayseal"></p>
    </div>
  </div>
</div>
<!-- Purchase section (end) -->


<p>Now, you may be thinking&hellip;</p>
<p><em>&ldquo;Brad, this sounds good and all, but what happens if I put down my hard-earned money and decide that Red Juice isn’t right for me?&rdquo;</em></p>
<p>Great question, and here&rsquo;s my answer&hellip;</p>

<!-- Monye back section -->
<div class="certbox">
<div class="certbox-toprow">
<div class="certbox-col2">&nbsp;</div>
</div>

<div class="certbox-centerrow">
<div class="certbox-col2">
<div class="certbox-pad">
	<h2><strong>Organifi Red Juice Comes With A<br /> 60 Day, No-Questions-Asked<br /> 100% Money-Back Guarantee&hellip; <br>
	<em><u>Even If You Empty The Container</u></em></strong><u><strong><em>!</em></strong></u></h2>
	<p>Here’s the deal…</p>
	<p>Try Organifi Red Juice for a full 60 days.</p>
	<p>If you’re not more than satisfied with the results, Drew DEMANDS that you ask for a refund. </p>
	<p>If you’re not happy, he’s not happy.</p>
	<p>So, what happens if you try a scoop and decide you hate the taste, or you don’t feel the BEST you’ve felt in years?</p>
	<p><strong>You get your money back.</strong></p>
	<p>What if you go through the entire package and THEN decide you don’t like it?</p>
	<p><strong>You get your money back. </strong></p>
	<p>So if you’re not satisfied for any reason, or no reason at all, just get in touch for a prompt refund. That sounds fair to me. How about you?</p>
	<p>To get Red Juice risk-free…  </p>
	<p><strong>All you have to do is click the ‘Buy Now’ button, put in your credit card info on the next page, and get your container of Organifi Red Juice delivered right to your doorstep.</strong></p>
	<p>All you do is mix just 1 scoop of Red Juice into water and enjoy the yummy taste as it enters your mouth. </p>
	<p>You’ll feel the surge of energy that lasts until bedtime.</p>
	<p>You’ll notice the brain fog start to disappear, as you gain clarity and razor focus. </p>
	<p>And over time, you’ll start hearing people mentioning how different you’re looking for your age! </p>
	<p>It&rsquo;s simple as that. </p>
</div>
<!-- <div class="certbox-col1"></div>
								<div class="certbox-col3"> </div> --></div>
</div>

<div class="certbox-bottomrowr">
<div class="certbox-col2">&nbsp;</div>
</div>
</div>
<div class="money-back"><img alt="" src="http://www.eatdrinkshrinkplan.com/assets/30dcfunnels/organify-protein/money-back-8345298203475.png" style="margin-top: -140px;"></div>
<!-- End Money Back -->
<h2>Why Such A Great Deal… And Why <u>Today Only?</u></h2>
<p>Drew and his team made a deal with us. He owed me a favor, so this is a hidden deal for MY customers only. The general public spends $11 more per jar! (And they still buy like crazy even at that price because Red Juice is that good. Many buy SIX at a time.) </p>
<p><strong>So you’re <u> <span class="highlight-yellowLIGHT">saving 20% more than the masses are</span></u> just for being one of my customers. </strong></p>
<p>That’s why we were able to secure such an awesome discount for you. As our customer, we want to reward you. (And we love rewarding our customers to inspire them to rave about us even more.)</p>
<p>However, since we’re aiming to give away so many initial containers for Drew, this amazing discount offer is only good while you’re on this page.</p>
<p><strong>Once you exit this page, this special promotion will no longer be valid.</strong> After that, you’ll only see Red Juice available at the ‘general public price’ of $69.95.  </p>
<p>So make the decision NOW by choosing your order option below!</p>
<h3>So, You Have a Decision to Make. What Will You Do?</h3>
<p>Look, you don’t want to be sitting there years later, wondering what happened… </p>
<p>…because you no longer look the same in the mirror, and you’ve noticeably slowed down, both mentally and physically.</p>
<p>ESPECIALLY when you could’ve seized the opportunity TODAY to look and feel like you traveled back in time, get a shot of youthful energy, and get laser-sharp focus by trying something different…</p>
<p>…something supremely healthy… something age defying… and something for less than $2.00 a day!</p>
<p>That’s less than a cup of coffee, less than a couple of candy bars at the vending machine, and less than most energy drinks… and a hell of a lot healthier. </p>
<p>Remember, all the risk is on us. You can ask for a refund for any reason, or no reason at all.<strong> Even if you use the entire container!</strong></p>
<p>So why not try it now and decide later? Let the folks at Organifi PROVE to you that Red Juice does all I say it will. <strong>I know for a fact that it does.</strong></p>
<p>You can watch your skin improve, feel that boost of energy, and get the mental focus to be at your best all day.</p>
<p>But you must take action. With the <span class="highlight-yellowLIGHT">60-day money-back guarantee,</span> you have absolutely nothing to lose, and so much to gain.</p>
<h3>So go ahead and click the Buy Now button right now. You’ll be so glad you did. </h3>

<p>&nbsp;</p>

<!-- Purchase section (begin) -->
<div class="products-section full left" id="products">
  <div class="third left">
    <div class="ctatopbar" style="background-color:#FFF; color: #FFF; font-weight: bold; padding: 6px;">–</div>
    <div class="full left">
      <h2 class="color-graydark"> <span>One Bottle</span></h2>
      <p class="description" style="margin-top: 24px;"><strong><s>$79.95</s></strong></p>
      <p class="each">$69.00</p>
      <p class="price"><strong>Save $10</strong></p>
      <p class="loyalty" style="font-size:13px;">+Shipping </p>

      <div class="add-to-cart">
        <!--a href="http://organifijv.pay.clickbank.net/?cbur=a&cbrblaccpt=true&cbitems=orj1sub.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-ordersubscripotion.png" width="716">
        </a-->
        <a href="http://organifijv.pay.clickbank.net/?cbur=a&cbitems=orj1otjv.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-buynow.png" width="716">
        </a>
      </div>

      <div class="secure-icons"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/security-icons1.png"></div>
      <div class="img-products1"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/tub-1.png"></div>
      <p class="type"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/30-money.png" alt="" class="sixtydayseal"></p>
    </div>
  </div>
  <div class="third left">
    <div class="ctatopbar" style="background-color:#6BCE75; color: #FFF; font-weight: bold; padding: 6px;">MOST POPULAR</div>
    <div class="full left">
      <h2 class="color-graydark"> <span>Three Bottles</span></h2>
      <p class="description" style="margin-top: 24px;"><strong><s>$239.85</s></strong></p>
      <p class="each">$149.95</p>
      <p class="price color-green"><strong class="">Save $89</strong></p>
      <p class="loyalty" style="font-size:13px;">+Shipping</p>

      <div class="add-to-cart">
        <!--a href="http://organifijv.pay.clickbank.net/?cbur=a&cbrblaccpt=true&cbitems=orj3sub.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-ordersubscripotion.png" width="716">
        </a-->
        <a href="http://organifijv.pay.clickbank.net/?cbur=a&cbitems=orj3otjv.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-buynow.png" width="716">
        </a>
      </div>

      <div class="secure-icons"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/security-icons1.png"></div>
      <div class="img-products1"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/tub-3.png"></div>
      <p class="type"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/30-money.png" alt="" class="sixtydayseal"></p>
    </div>
  </div>
  <div class="third left">
    <div class="ctatopbar" style="background-color: #AF2326; color: #FFF; font-weight: bold; padding: 6px;">BEST VALUE!</div>
    <div class="full left">
      <h2 class="color-graydark"> <span>Six Bottles</span></h2>
      <p class="description" style="margin-top: 24px;"><strong><s>$479.70</s></strong></p>
      <p class="each">$249.95</p>
      <p class="price color-red"><strong>Save $229</strong></p>
      <p class="loyalty" style="font-size:13px;">+Shipping</p>

      <div class="add-to-cart">
        <!--a href="http://organifijv.pay.clickbank.net/?cbur=a&cbrblaccpt=true&cbitems=orj6sub.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-ordersubscripotion.png" width="716">
        </a-->
        <a href="http://organifijv.pay.clickbank.net/?cbur=a&cbitems=orj6otjv.1">
            <img class="cartbutton" src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/button-buynow.png" width="716">
        </a>
      </div>

      <div class="secure-icons"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/security-icons1.png"></div>
      <div class="img-products1"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/tub-4.png"></div>
      <p class="type"><img src="https://s3-us-west-2.amazonaws.com/landingpage-assets-101/fl2792assets/30-money.png" alt="" class="sixtydayseal"></p>
    </div>
  </div>
</div>
<!-- Purchase section (end) -->


</div>

<br style="clear:both;width:100%;overflow:hidden;">
<br>
<p style="font-size:80%;text-align:center;">
    <a href="http://organifijv.pay.clickbank.net/?cbur=d&cbitems=orj1otjv.1">No thanks!</a> I do not want the Shortcut to all the benefits of Good Belly, Bad Belly in a single glass of<br> delicious juice and I understand I will never see this special pricing again.
</p>


<div class="footer">

	  <p class="fon-siz-15">&nbsp;</p>
	  <p> <a href="https://www.organifishop.com/pages/terms-conditions" target="_blank"> <strong>Terms Of Use</strong></a> | <a href="https://www.organifishop.com/pages/health-disclaimer" target="_blank"><strong>Disclaimer</strong></a> | <a href="https://www.organifishop.com/pages/privacy-policy" target="_blank"><strong>Privacy Policy</strong></a></p>
	  <p>ClickBank is the retailer of products on this site. CLICKBANK@ is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products. </p>
	  <p class="font-wei-bol font-siz-14 copy-foot-m">&nbsp;</p>
    </div>

    </div>


	<!-- Smooth Scroll -->
    <script>
    	$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
    </script>

	</body></html>
