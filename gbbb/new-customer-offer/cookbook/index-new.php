<?php
require($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>GB Cookbook</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=3">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->

<?php require($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="layout" class="clearfix">
    <div id="content" class="rich-text clearfix">

        <h1><b>
            Quickly Put Your Fat-Burning <i>Good Belly</i> On Auto-Pilot With Easy, Tasty &amp; Affordable Meals You Can Make In Minutes
        </b></h1>

        <p>Now that you’re equipped with the knowledge to quickly convert your digestive system into a fat-burning <b>Good Belly</b>, I’m excited to offer you the exclusive add on that everyone agrees is making the entire Good Belly system practically done-for-you.</p>
        <p>Listen: We all know the worst part of going on any kind of weight loss program is having to read labels, figure out what you’re “allowed” to eat every day, and grind through preparing the right meals&hellip;</p>
        <p>That’s why I’ve created a special book of quick, easy, delicious and almost-done-for-you recipes&hellip;</p>
        <p>All you have to do is print up the special shopping lists I’ve created for you and head to the market (don’t worry, I’ve made a point of making sure everything you need to buy is cheap and affordable)&hellip;</p>
        <p>Then whenever you are short on time, just follow along with the incredibly-simple instructions, and you’ll be eating healthy and feeling satisfied in shockingly little time, all without having to think about what you’re eating at all.</p>
        <p>Introducing the <b><i>Good Belly Cookbook</i></b>&hellip;</p>
        <p>The <i>Good Belly Cookbook</i> is designed to save you time, energy and money around the kitchen while making your fat loss super fun and delicious.</p>
        <p>Look, your daily meals are absolutely the most important part of your amazing transformation and we want to ensure you love every fat burning meal you make and enjoy every part of your journey.</p>
        <p>So, that’s why inside The <i>Good Belly Cookbook</i> we’ve taken the liberty of putting together a ton of super easy to make, mouth-watering recipes to match accelerate your Good Belly transformation.</p>
        <p>The fat burning <i>Good Belly Cookbook</i> is jammed packed with amazing tasting, metabolism boosting recipes that use smart, simple and cash sparing ingredients to accelerate your fat loss.</p>
        <p>Now, you can make money-saving, gourmet meals the whole family will love and thank you for while you’re training your digestive system to burn more fat with every bite.</p>
        <p>If you want us to do all of the recipe work for you and make your fat loss experience so sinfully delicious and enjoyable it feels like you’re indulging, click the add to order button below and you’ll be cooking up a fat burning storm in no time. :-)</p>

        <div class="insertion center" style="margin-bottom:-1em;">
            <center><img class="mobile-full-width" src="web/i/products/cookbook-ecover.jpg" alt=""></center>
        </div>

        <table class="pricing">
            <tr>
                <td class="reg">
                    <center><strike>&nbsp;$79&nbsp;</strike></center>
                </td>
                <td>&nbsp;</td>
                <td class="inv">
                    <center><em>$37 USD</em></center>
                </td>
            </tr>
        </table>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="http://144.eatstopeat.pay.clickbank.net/?cbur=a">Add To Order<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="http://144.eatstopeat.pay.clickbank.net/?cbur=a">Click To Add The <i>Good Belly Cookbook</i> To Your Order</a></span>
            </p>
        </div>



    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->

<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
