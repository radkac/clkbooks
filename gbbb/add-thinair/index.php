<?php
//LOAD COOKIE SCRIPT
require_once('../../checkreturning.php');
require_once('../../fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Discover The Surprising Connection Between Exhaustion, Fatigue, Weight Gain and The Air You Breathe</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=5">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=5">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=5">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function resizeVideoPlayer() {
	var new_width = $('#video_player').width();
	var new_height = Math.floor(411*new_width/730);
	$('#video_player').height(new_height);
	//alert(new_width+' '+new_height);
}

function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
	setTimeout('resizeVideoPlayer()', 3100);
});

$(window).resize(function() {
	resizeVideoPlayer();
});

</script>

<?php require_once('../../eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>
            Discover The Surprising Connection Between Exhaustion, Fatigue, Weight Gain and The Air You Breathe
        </h1>

        <h2 class="cl-red"><i>And How A Simple 10 Second Trick Can Double Your Energy In 20 Minutes</i></h2>

        <div class="topbox">
            <p>On this page only you have access to an exclusive ClickBooks price negotiated directly with the author of Good Belly, Bad Belly. Review the details below before leaving this page.</p>
        </div>

        <p>Hey, Brad here. And I have another exciting opportunity for you because you invested in <i>Good Belly, Bad Belly</i> today&hellip;</p>
        <p>You’ve demonstrated that you are someone who cares about your health. Which is why I want you to know about a very serious problem that no one is talking about.</p>
        <p>First, let me ask if you’ve ever suffered from nagging and unexplained fatigue, headaches, lethargy, mysterious weight problems or headaches and even migraines?</p>
        <p>If yes, have you tried everything &mdash; including diets, exercise, countless doctors, acupuncture, alternative medicine, and the list goes on &mdash; to get relief?</p>
        <p>Then I’ve got fantastic news for you. Your nagging challenges may have <u>nothing to do with your diet or your exercise</u>. And it will take you all of 10 seconds to implement a simple trick that <b>could start reversing ALL your symptoms in less than 20 minutes</b>. Let me explain&hellip;</p>
        <p>Everything I described earlier is identical to the symptoms outlined in a groundbreaking report released by the Air Force. And as strange as this sounds, it turns out there’s a strange link between these mysterious symptoms and research into a certain Greenhouse Gas.</p>

        <h2>In other words. The air you are breathing right now could be making you fat and sick!</h2>

        <p>The document reported that the majority of workplace buildings in the US had levels of this Greenhouse Gas high enough to affect up to 80% of workers with these same symptoms.</p>
        <p>Have you guessed the Greenhouse Gas I’ve been talking about? You’ll be shocked&hellip;</p>
        <p>It’s nothing more complicated than everyday Carbon Dioxide or Co2&hellip;</p>
        <p>However when you look into what’s happening it’s terribly frightening. If you consider our human evolution you’ll see we evolved to breathe an atmosphere with Co2 concentrations of about 300 parts per million (or ppm)&hellip;</p>
        <p>Yet thanks to Greenhouse Gas emissions, even average OUTDOOR levels of Co2 are now over 400 ppm. But that’s just OUTDOOR levels. Get this&hellip;</p>
        <p>I discovered “good” office building levels are in the range of 600-800 ppm if you’re lucky. And that it’s not uncommon for many buildings to be twice or three times those levels!&hellip;</p>
        <p>While a recent study by KPMG and Middlesex Universities found that even levels of Co2 in the 600 ppm range could reduce concentration by as much as 30 percent. At levels above 1,500 ppm 79% of people reported feeling lethargic.</p>
        <p>Think about that number for a second&hellip; That’s almost 8 out of every 10 people feeling exhausted because of the air they’re breathing!</p>
        <p>The Air Force report warned that even at 600 ppm many people will suffer sleepiness, fatigue, poor concentration, and a sensation of stuffiness&hellip;</p>
        <p><b>Yet as I continued my research I discovered something even more shocking&hellip;</b></p>
        <p>Danish researchers are now discovering how <u><b>rising Co2 concentrations are making humans <span class="bg-yellow">hungrier and fatter</span></b></u>&hellip;</p>
        <p>The researchers were surprised to see that both fat and thin people taking part in their studies over a 22-year period put on weight &mdash; and the increase was proportionately the same. The only consistent factor they could find across the entire group was rising Co2 levels&hellip;</p>
        <p>Looking for a deeper explanation they stumbled on something called Orexins. These are a type of hormone in the brain that affect wakefulness and energy expenditure and they can be disrupted by high Co2 in your blood, and this can cause you to go to bed later, affecting your metabolism so it is easier for you to put on weight&hellip;</p>
        <p>Worse&hellip; Orexins are also DIRECTLY involved in the stimulation of your hunger and food intake.<br>
        Yep! It looks like science is now proving that Co2 makes you eat more&hellip;</p>
        <p>And it doesn’t stop there. A Swiss study demonstrated signs of increased insulin resistance and waist-circumference in people exposed to poor air quality. Which means these folks had trouble storing nutrients in their muscles and other lean tissue. The science tells us that since the body needs to get rid of the sugar in the blood that results from eating, it quickly stores it as fat!&hellip;</p>
        <p>I explain all this research and much more in a very easy to understand and quick to read book called <b><i>Thin Air</i></b>.</p>
        <p>In the book I also lay out a <b>simple plan</b> you can start implementing <u><b>today</b></u> that will help you <u>reclaim your energy and reboot your fat burning metabolism</u>.</p>
        <p>And as I mentioned, the first tip takes only <u>10 seconds or less and you’ll begin noticing the effects in under 20 minutes</u>!</p>
        <p>Now, because you are a brand new customer, I came to an exclusive agreement with ClickBooks to let you have the complete book for only $9.99 today! Just make sure you take advantage of this one-time special offer because it is only available here at ClickBooks.</p>

        <div class="purchase-holder">
            <div class="insertion center purchase">

                <img class="ecover mobile-2of3-width" src="web/i/products/eCover.png" alt="">

                <table align="center" class="costs" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>List Price:</td>
                        <td>$ 49.00</td>
                    </tr>
                    <tr>
                        <td><b>CLK*Book Price:</b></td>
                        <td><b>$ 7*</b></td>
                    </tr>
                    <tr>
                        <td>You Save</td>
                        <td>$ 42.00 (85% OFF)</td>
                    </tr>
                    <tr>
                        <td colspan="2">*Includes free international wireless delivery</td>
                    </tr>
                </table>

                <a class="button" href="http://152.eatstopeat.pay.clickbank.net/?cbur=a">Add <em>Thin Air</em> To My Order</a>

            </div>
        </div>

        <hr>

        <p class="discard">No thanks. I understand the information in this book is cutting edge stuff that will help me reclaim my energy, reboot my metabolism and achieve my ideal weight &mdash; and that this ClickBooks exclusive opportunity is the only way to get this vital information for only $7 &mdash; however I will try to figure this out on my own. <a href="http://152.eatstopeat.pay.clickbank.net/?cbur=d">I will take a pass on this exclusive one-time-offer</a></p>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.</p>

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
