<? require($_SERVER['DOCUMENT_ROOT'].'/prices.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Advocate</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=3">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/jquery.elevateZoom-3.0.8.min.js" type="text/javascript"></script>

	<meta property="og:image" content="http://clkbooks.com/advocate/web/i/fb-og-ta.png">
	<meta property="og:description" content="Advocate: Make Sense Of Confusing Nutrition and Exercise News &mdash; Health Author Brad Pilon Simplifies and Interprets The Research So You Don’t Have To">
	<meta property="og:title" content="Check Out What I'm Reading&hellip;">

<script>
$(function(){
    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        $(".read-more").toggleClass("collapsed");
        $(".read-more-content").toggle(400);
    });
    $("#mainEcoverId").elevateZoom({
        zoomType: "inner",
        cursor: "zoom-in"
    });
});
</script>

<?php require($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="breadcrumbs">&nbsp;</div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Offer</p>
                <h1>The Advocate Newsletter</h1>
            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img id="mainEcoverId" class="main-ecover mobile-full-width" src="web/i/sbs-brad-imac-table.jpg" width="100%" alt="" data-zoom-image="web/i/sbs-brad-imac-table.jpg">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>
                        Limited Time Pricing
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$7.00</td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>FREE TRIAL</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$7.00</b> (100%)</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">FREE</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://197.eatstopeat.pay.clickbank.net/?cbskin=18285&vtid=clkbooks"><span class="inner">15 Days For FREE!</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Offer</p>
                <h1>The Advocate Newsletter</h1>
                </div>

                <h2>Make Sense Of Confusing Nutrition and Exercise News &mdash; Health Author Brad Pilon Simplifies and Interprets The Research So You Don’t Have To</h2>

                <p class="byline">From The Desk Of Brad Pilon</p>
                <p class="rating">Ontario, Canada</p>

                <p>Dear friend,</p>
                <p>I feel very fortunate. Since 1998 I’ve been earning a living by writing about health, fitness and weight control.</p>
                <p>And what I’ve discovered about myself is that I really enjoy writing emails about my daily, weekly and monthly deep-dives into the science behind eating and exercise for health, longevity, strength, and of course looking good.</p>
                <p>In fact I love writing these emails so much that I want to devote as much time to them as possible, so I can bring you the very best, no-nonsense, science-based information in a way that is easy to understand.</p>
                <p>My mission is to make sure these emails have a direct and practical effect on helping you achieve your health and body composition goals as simply as possible.</p>
                <p>So in order to devote myself to these emails and continue to earn a living with my writing, I went ahead and created an exciting service called The Advocate&hellip;</p>

                <div class="read-more-content collapsed hidden">

                <h2>Let Me Be Your Health and Weight Loss <span class="cl-red">Advocate</span> So You Can Cut Through The Clutter And Get Directly To The Truth</h2>

                <p>Reading research papers is both an art and a science &mdash; one I’ve devoted most of my adult life to. In fact I’ve been subscribed to prestigious scientific journals since my early teens.</p>
                <p>My passion for the science of nutrition and exercise inevitably led me to post-graduate studies in nutrition and then to a job as head of R&amp;D for one of the biggest supplement companies in the world.</p>
                <p>However my calling has always been my work as an author. I just love getting excited about a topic, devouring all the available research on it, and then interpreting it for you in a way that is easy to understand and take action on.</p>
                <p>And that’s exactly what I do with every edition of The Advocate&hellip;</p>

                <h2>2-3 times a week &mdash; every week &mdash; I want to share with you my most exciting new insights&hellip;</h2>

                <p>The exciting thing about The Advocate is that I get to go crazy on all the science relating to the “hot topics” you’re already hearing about in the news. Which means I get to do what I like &mdash; spending hours and hours pouring through research papers &mdash; and you get to read my simple breakdown of the facts and my recommendations based on those facts.</p>
                <p>You get to peel back the curtain and see the real facts behind all the sensationalized news reports and Facebook posts&hellip;</p>
                <p>From the subscriber feedback I get, some of the things you’ll enjoy most from your subscription are:</p>

                <ul class="check green">
                    <li>Understanding which nutrition “secrets” actually make sense&hellip;</li>
                    <li>How to steer clear of the diet fads that are doomed to fail&hellip;</li>
                    <li>Recognizing which nutrition strategies are best for YOUR body and preferences&hellip;</li>
                    <li>How to lose weight as quickly and simply as possible</li>
                    <li>Keeping the weight off for life&hellip;</li>
                    <li>And just plain cutting through the crap so you know exactly how the latest diet and exercise trends affect you, your health and your weight&hellip;</li>
                </ul>

                <p>Please remember EVERY edition of the Advocate is <u><b>backed by hard-science</b></u> that I distill into easy-to-understand and <u><b>actionable advice</b></u>.</p>
                <p>I know you’re smart. You have the motivation. What you haven’t had until now is a <b>trusted guide</b> who actually LIKES devoting hours to reading the raw research papers for you.</p>
                <p>I want to provide you with the untarnished truth that’s at the base of all the exaggerated media headlines.</p>
                <p>Every day your inbox is packed full of newsletters from well meaning fitness pros recommending the latest diet or exercise trend. Heck, sometimes you even get the same email from multiple sources in the same day!&hellip;</p>
                <p>I can’t fault their desire to bring you new ideas and recommend what they think will work for you. However they just don’t provide you with the <u><b>filter</b></u> you need. I would bet that the majority of them don’t even know how to interpret a research paper, if they bother to look at one at all.</p>
                <p>Which is why I want to be your advocate&hellip;</p>

                <h2>I want to send you 2-3 carefully crafted email newsletters each week. And they will all be 100% ad free and promotion free&hellip;</h2>

                <ul class="check blue">
                    <li>You’ll receive my unbiased and unapologetic interpretation of the latest scientific research on weight loss and optimal health&hellip;</li>
                    <li>You’ll benefit from my analysis of the latest trendy topics to hit the media and the daytime talk shows&hellip;</li>
                    <li>And you’ll even be able to send me your questions so I can clear up any confusion caused by the self-proclaimed diet experts you have to put up with every day&hellip;</li>
                </ul>

                <p>This is what makes me jump out of bed every morning. I’m passionate about digging up only the <b>best</b> new tips and insights to bring you in my premium newsletters&hellip;</p>
                <p>I call this service <b>The Advocate</b> because I promise to be there for you, to represent your interests, and to protect you from all the misinformation&hellip;</p>
                <p>When you join me it means we’re in it together&hellip; And I’ve got your back. Each issue is like having your own personal champion who’s there to fight for your rights and results.</p>

                <h2>Today, Get Started For FREE</h2>

                <p>The world of exercise and nutrition is more and more cluttered every day. And like you I’m also increasingly choosy about where I spend my hard earned dollars.</p>
                <p>So today I want it to be easy for you to give The Advocate a try, because I know how much you’ll enjoy knowing the truth behind the headlines.</p>
                <p>Not only will you be able to apply the information in The Advocate to accelerate you towards your own goals, you’ll also be able to help set your friends and family straight when they start repeating the misinformation and half-truths they hear in the media.</p>
                <p>I’ve already made sure The Advocate is amazingly affordable. In fact if you buy 2 lattes a month at Starbucks, that’s already more than your monthly price for The Advocate!</p>
                <p>However today you’ll get 15 days of The Advocate for just $0 because I want to make it super easy for you to see how useful it is to you.</p>
                <p>Just use the button below to get your first 15 days for $0, then you’ll only pay your easy monthly price of just $7 if you decide to keep your subscription. If not, just let me know and we’ll cancel your access and we part as friends.</p>
                <p>Heck, I’ll even guarantee your investment today. If you don’t feel The Advocate delivers on its promise just let me know and I’ll return your investment right away.</p>
                <p>Get started today for FREE. Click the button below.</p>

                <p align="center"><a class="pbutton" href="http://197.eatstopeat.pay.clickbank.net/?cbskin=18285&vtid=clkbooks">Special Offer:<br> 15 Days Subscription To<br> The Advocate For FREE</a></p>

                <ul class="checkmarks">
                    <li>Thought-Provoking Content</li>
                    <li>Smart Weight Management</li>
                    <li>Energizing Strategies</li>
                </ul>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>
                        Limited Time Pricing
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$7.00</td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>FREE TRIAL</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$7.00</b> (100%)</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">FREE</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://197.eatstopeat.pay.clickbank.net/?cbskin=18285&vtid=clkbooks"><span class="inner">15 Days For FREE!</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>


        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
