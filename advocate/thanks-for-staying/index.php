<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Advocate</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="../web/s/zhtml.css?v=3">
    <link rel="stylesheet" type="text/css" href="../web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="../web/s/mobile.css?v=3">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="../web/s/jquery.elevateZoom-3.0.8.min.js" type="text/javascript"></script>

<?php require($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="../web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="../web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="breadcrumbs">&nbsp;</div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent">
				<center>
				<h1> Thanks for keeping the Advocate. </h1>
				<br>
                <h2>You have been re-added to the Advocate newsletter.</h2>
				<br>
                <p>The Advocate newsletter is delivered ENTIRELY via email, so make sure you are receiving our emails.</p>
                <p>If you don't receive our emails, or encounter any issues, please contact help@clkbooks.com and we'll get you sorted right away.</p>
				</center>
            </div>

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="../web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
