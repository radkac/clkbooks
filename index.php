<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CLK*Books | Health &amp; Lifestyle Books</title>
    <meta name="description" content="CLK*Books. Your online source for health books and ebooks.">
    <meta name="keywords" content="Health, Books, eBooks, Health Books, Healthy Life, Brad Pilon, Brad Pilon Books, Brad Pilon Health Books, Clickbank Books, Clickbank Health Books, Lifestyle Books, Life Books">
    <meta name="author" content="CLK*Books">

    <link rel="apple-touch-icon" href="images/touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="167x167" href="images/touch-icon-ipad-retina.png">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=12">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=12">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=12">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>
$(function(){
    $(document).scroll(function(){
        var sYPos = $(document).scrollTop();
        var scale = (sYPos < 200) ? (150 + sYPos / 20) : 160;
        $('#promo').css('background-size', scale+'%');
    });
});
</script>

<?php require_once('eztrackingcode.php'); ?>

</head>
<body class="home">

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="promo">
    <div class="inner" class="clearfix">
        <img class="logo" src="web/i/head-logo-alt.png" alt="">
        <div class="tag">This week's Best Reads</div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

        <div class="container products">
            <a class="division dv-1-3" href="http://clkbooks.com/thinair/">
                <img class="ecover" src="web/i/home-book1.jpg" alt="">
                <span class="title">Thin Air</span>
                <span class="descr">The surprising connection between exhaustion, lethargy and the air you breath.</span>
                <span class="button">Buy Now</span>
            </a>
            <a class="division dv-1-3" href="http://clkbooks.com/hmp/">
                <img class="ecover" src="web/i/home-book2.jpg" alt="">
                <span class="title">How Much Protein</span>
                <span class="descr">How much protein do you need to build muscle, lose fat and be healthy.</span>
                <span class="button">Buy Now</span>
            </a>
            <a class="division dv-1-3" href="http://clkbooks.com/ese/">
                <img class="ecover" src="web/i/home-book3.jpg?v=2" alt="">
                <span class="title">Eat Stop Eat</span>
                <span class="descr">The original "2 day diet." Learn why dieting every day will ruin your fat loss and how a simple nutritional strategy with only ONE rule is the key to rapid fat loss and lifelong weight management.</span>
                <span class="button">Buy Now</span>
            </a>
            <a class="division dv-1-3" href="http://clkbooks.com/gbbb/">
                <img class="ecover" src="web/i/home-book4.jpg" alt="">
                <span class="title">Good Belly Bad Belly</span>
                <span class="descr">How to protect yourself from the risk of obesity-promoting toxins created by the foods you eat.</span>
                <span class="button">Buy Now</span>
            </a>
            <a class="division dv-1-3" href="http://clkbooks.com/progressions/">
                <img class="ecover" src="web/i/home-book5.jpg" alt="">
                <span class="title">Progressions</span>
                <span class="descr">The official ESE workout. A NATURAL Way To Get The Same Results As Pros (or Cheaters)!</span>
                <span class="button">Buy Now</span>
            </a>
            <a class="division dv-1-3" href="http://clkbooks.com/fat-cell-killer/">
                <img class="ecover" src="web/i/home-book6.jpg?v=2" alt="">
                <span class="title">Fat Cell Killer</span>
                <span class="descr">The ONLY Known Protocol To Permanently KILL And Flush Away Your Fat Cells &mdash; AND Guarantee Permanent &amp; Irreversible Fat Loss.</span>
                <span class="button">Buy Now</span>
            </a>
        </div>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
