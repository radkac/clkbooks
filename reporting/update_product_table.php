<?php

function create_product_table($cbaccount){

	switch ($cbaccount){
		case 'eatstopeat':
			define('devkey','DEV-HL08P3C252BITMENETH11I5R9A1M9UGR');
			break;
	}

	//setup and start cURL session
	$url = "https://api.clickbank.com/rest/1.3/products/list/?site=$cbaccount";
	$ch = curl_init();
	
	//set cURL options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true); 
	curl_setopt($ch, CURLOPT_HTTPGET, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //return transfer instead of printing it automatically
	curl_setopt($ch, CURLOPT_HEADER, false); //exclude header from response so that json decode can give a proper array instead of returning null
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json", "Authorization:".devkey.":API-B43R0HEKUQGK6NRR5O52DBJLEUHJUQM3"));
	$result = curl_exec($ch);

	$result = json_decode($result, true); //decode json response to php array

	$product_count = $result['total_record_count'];
	
	if (curl_errno($ch)) { 
	   print curl_error($ch); 
	} 

	//count number of pages in API response
	if ($product_count % 100 > 0){
		$total_pages = ceil($product_count / 100);
	} else {
		$total_pages = 1;
	};
	
	while ($page_count <= $total_pages){
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json", "Authorization:".devkey.":API-B43R0HEKUQGK6NRR5O52DBJLEUHJUQM3","page:$page_count"));
		$result = curl_exec($ch);
		
		$result = json_decode($result, true);
		$int_products_array = $result['products']['product'];//discard unnecessary array levels
		
		foreach ($int_products_array as $int_product){
			$int_product_id = $int_product['@sku'];
			$products_array[$int_product_id] = $int_product;	
			$i++;
		}
		$page_count++;
	}
	
	$connect = mysqli_connect('localhost', 'eatstope_c1kbks', 'qu7MTK?ZbJI$', 'eatstope_clkbooks');

	$success=false;
	
	$query = "DELETE FROM $cbaccount;";
	mysqli_query($connect,$query);
	
	foreach($products_array as $product){
		$sku = $product['@sku'];
		$name = $product['title'];
		$name = mysqli_real_escape_string($connect, $name);
		$initial_price = $product['pricings']['pricing']['standard']['price']['native_price'];
		
		if($product['digitalRecurring']==='true'){
			$rebill_price = $product['pricings']['pricing']['recurring']['price']['native_price'];
			$query = "INSERT INTO $cbaccount (sku, name, initial_price, rebill_price) VALUES ('$sku','$name','$initial_price','$rebill_price');";
		} else {
			$query = "INSERT INTO $cbaccount (sku, name, initial_price) VALUES ('$sku','$name','$initial_price');";
		}
		
		if (mysqli_query($connect, $query)) {
		echo 'Table updated successfully </br>';
		} else {
			echo "Error updating table: " . mysqli_error($connect);
		}
		
	}

	mysqli_close($connect);
	
	curl_close($ch);
};

create_product_table('eatstopeat');

?>