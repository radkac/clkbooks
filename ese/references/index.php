<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Eat Stop Eat</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="../web/s/zhtml.css?v=9">
    <link rel="stylesheet" type="text/css" href="../web/s/global.css?v=9">
    <link rel="stylesheet" type="text/css" href="../web/s/mobile.css?v=9">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="/web/js/month-days-left.js"></script>

<script>

$(function(){

    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });

});

</script>

<?php require_once('../../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="../web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="../web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent">
                <h5>
                    <strong>
                        Eat Stop Eat Scientific References:
                    </strong>
                </h5>
                <p>Swinburn B, Sacks G, Ravussin E. Increased food energy supply is more than sufficient to explains the US epidemic of obesity. Am J Clin Nutr 2009; 90:1453-6</p>
                <p>Li C, Ford ES, Zhao G, Balluz LS, Giles WH. Estimates of body composition with dual-energy X-ray absorptiometry in adults. Am J Clin Nutr. 2009; 90(6):1457-65</p>
                <p>Adams KM, Kohlmeier M, Zeisel SH. Nutrition Education in U.S. Medical Schools: LatestUpdate of a National Survey. Academic Medicine. 2010; 85(9): 1537-1542</p>
                <p>Cuneen SA. Review of meta-analytic comparisons of bariatric surgery with a focus on laparoscopic adjustable gastric banding. Surgery for Obesity and Related Diseases. 2008;4: S47-S55.</p>
                <p>Buchwald H, Avidor Y, Braunwald E, et al. Bariatric surgery a systematic review and meta-analysis. Journal of the American Medical Association. 2004; 292:1724-37.</p>
                <p>Webber J, Macdonald IA. The cardiovascular, metabolic and hormonal changes accompanying acute starvation in men and women. British Journal of Nutrition. 1994; 71:437-447.</p>
                <p>Heilbronn LK, et al. Alternate-day fasting in non-obese subjects: effects on body weight, body composition, and energy metabolism. American Journal of Clinical Nutrition. 2005; 81:69-73</p>
                <p>Keim NL, Horn WF. Restrained eating behavior and the metabolic response to dietary energy restriction in women. Obesity Research. 2004; 12:141-149.</p>
                <p>Verboeket-Van De Venne WPHG, et al. Effect of the pattern of food intake on human energy metabolism. British Journal of Nutrition. 1993; 70:103-115</p>
                <p>Bellisle F, et al. Meal Frequency and energy balance. British Journal of Nutrition. 1997; 77: (Suppl. 1) s57-s70</p>
                <p>Gjedsted J, et al. Effects of a 3-day fast on regional lipid and glucose metabolism in human skeletal muscle and adipose tissue. Acta Physiologica Scandinavia 207; 191:205-216</p>
                <p>Gardner CD, et al. Comparison of the Atkins, Zone, Ornish, and LEARN diets on change in weight and related risk factors among overweight premenopausal women. The A to Z weight loss study: A randomized trial. Journal of the American Medical Association. 2007; 297(9): 969-998</p>
                <p>Hultman E. Physiological role of muscle glycogen in man, with special reference to exercise. Circ Res 1967;20(suppl 1):199-114</p>
                <p>Hultman E. Physiological role of muscle glycogen in man, with special reference to exercise. Circ Res 1967;20(suppl 1):199-114</p>
                <p>Knapik JJ, Jones BH, Meredith C, Evans WJ. Influence of a 3.5 day fast on physical performance. European Journal of Applied Physiology and Occupational Physiology 1987; 56(4):428-32</p>
                <p>Schisler JA, Ianuzzo CD. Running to maintain cardiovascular fitness is not limited by short-term fasting or enhanced by carbohydrate supplementation. Journal of Physical Activity and Health. 2007 Jan;4(1):101-12.</p>
                <p>Knapik JJ, Meredith CN, Jones LS, Young VR, Evans WJ. Influence of fasting on carbohydrate and fat metabolism during rest and exercise in men. Journal of Applied Physiology 1998; 64(5): 1923-1929</p>
                <p>Nieman DC, et al. Running endurance in 27-h-fasted humans. Journal of Applied Physiology 1987; 63(6):2502-2509</p>
                <p>Zinker BA, Britz K, Brooks GA. Effects of a 36-hour fast on human endurance and substrate utilization. Journal Applied Physiology 1990; 69(5): 1849-1855</p>
                <p>Aragon-Vargas LF. Effects of fasting on endurance exercise. Sports Med 1993; 16:255-65 26 Gleeseon M, Greenhaff PL, Maughan RJ. Influence of a 24 h fast on a high intensity cycle exercise performance in man. Eur J Appl Physiol Occup Physiol 1988;46:211-19</p>
                <p>Dohm, GL, Beeker RT, Isreal RG, Tapscott EB. Metabolic responses to exercise after fasting. Journal of Applied Physiology 61(4): 1363-1368,1986.</p>
                <p>Hermansen L, Vaage O. Lactate disappearance and glycogen synthesis in human muscle after maximal exercise. Am J Phsiol 1977; 233:E422-9.</p>
                <p>Muthayya S, Thomas T, Srinivasan K, Rao K, Kurpad AV, van Klinken JW, Owen G, de Bruin EA. Consumption of a mid-morning snack improves memory but not attention in school children. Physiology &amp; Behavior. 2007 Jan 30;90(1):142-50.</p>
                <p>Green MW, Elliman NA, Rogers, PJ. Lack of effect of short-term fasting on cognitive function. Journal of Psychiatric Research 1995; 29(3), 245-253.</p>
                <p>Leiberman HR, Caruso CM, Niro PJ, Adam GE, Kellogg MD, Nindl B, Kramer FM. A double-blind, placebo-controlled test of 2 d of calorie deprivation: effects on cognition, activity, sleep, and interstitial glucose concentrations. American Journal of Clinical Nutrition 2008;88:667–76.</p>
                <p>Green MW, Rogers PJ, Elliman NA, Gatenby SJ. Impairment of cognitive performance associated with dieting and high levels of dietary restraint. Physiology and Behavior. 1994;55(3):447-52.</p>
                <p>Green MW, Rogers PJ. Impaired cognitive functioning during spontaneous dieting. Psychological Medicine. 1995;25(5):1003-10.</p>
                <p>Witte AV, Fobker M, Gellner R, Knecht S, Flöel A. Caloric restriction improves memory in elderly humans. The Proceedings of the National Academy of Sciences. 2009 Jan 27;106(4):1255-60</p>
                <p>Bryner RW. Effects of resistance training vs. Aerobic training combined with an 800 calorie liquid diet on lean body mass and resting metabolic rate. Journal of the American College of Nutrition 1999; 18(1): 115-121</p>
                <p>Rice B, Janssen I, Hudson, R, Ross R. Effects of aerobic or resistance exercise and/or diet on glucose tolerance and plasma insulin levels in obese men. Diabetes Care 1999; 22: 684-691</p>
                <p>Janssen I, et al. Effects of an energy-restrictive diet with or without exercise on abdominal fat, intermuscular fat, and metabolic risk factors in obese women. Diabetes Care 2002; 25:431-438</p>
                <p>Chomentowski P, et al. Moderate Exercise Attenuates the Loss of Skeletal Muscle Mass That Occurs With Intentional Caloric Restriction – Induced Weight Loss in Older, Overweight to Obese Adults. Journal of Gerontology: MEDICAL SCIENCES. 2009. Vol. 64A, No. 5, 575–580</p>
                <p>Marks BL, Ward A, Morris DH, Castellani J, and Rippe RM. Fat-free mass is maintained in women following a moderate diet and exercise program. Medicine and Science in Sports and Exercise. 1995; 27(9): 1243-51</p>
                <p>Gjedsted J, Gormsen L, Buhl M, Norrelund H, Schmitz, Keiding S, Tonnesen E, Moller N. Forearm and leg amino acids metabolism in the basal state and during combined insulin and amino acid stimulation after a 3-day fast. Acta Physiologica. 2009; (6): 1-9.</p>
                <p>Gibala MJ, Interisano SA, Tarnopolsky MA et al. (2000) Myofibrillar disruption following acute concentric and eccentric resistance exercise in strength-trained men. Can J Physiol Pharmacol 78, 656–661</p>
                <p>Bray GA, Smith SR, De Jonge L, Xie H, Rood J, Martin CK, Most M, Brock C, Manscuso S, Redman LM. Effect of Dietary Protein Content on Weight Gain, Energy Expenditure, and Body Composition During Overeating. JAMA. 2012;307(1):47-55</p>
                <p>Deldicque L, De Bock K, Maris M, Ramaekers M, Nielens H, Francaux M, Hespel P. Increased p70s6k phosphorylation during intake of a protein-carbohydrate drink following resistance exercise in the fasted state. Eur J Appl Physiol. 2010 Mar;108(4):791-800.</p>
                <p>Samer W. El-Kadi, Agus Suryawan, Maria C. Gazzaneo, Neeraj Srivastava, Renán A. Orellana, Hanh V. Nguyen, Gerald E. Lobley, and Teresa A. Davis. Anabolic signaling and protein deposition are enhanced by intermittent compared with continuous feeding in skeletal muscle of neonates. Am J Physiol Endocrinol Metab 2012;302 E674-E686</p>
                <p>Van Proeyen K, De Bock K, Hespel P. Training in the fasted state facilitates re-activation of eEF2 activity during recovery from endurance exercise. Eur J Appl Physiol. 2011 Jul;111(7):1297-305.</p>
                <p>Phillips SM, Tipton KD, Aarsland A et al. (1997) Mixed muscle protein synthesis and breakdown after resistance exercise in humans. Am J Physiol 273(1 Pt 1), E99–107.</p>
                <p>Rasmussen BB, Tipton KD, Miller SL, Wolf SE, Wolfe RR. An oral essential amino acid-carbohydrate supplement enhances muscle protein anabolism after resistance exercise. J Appl Physiol. 2000:88;386-392.</p>
                <p>Tipton KD, Rasmussen BB, Miller SL, Wolf SE, Owens-Stovall SK, Petrini BE, Wolfe RR. Timing of amino acid-carbohydrate ingestion alters anabolic response of muscle to resistance exercise. Am J PhysiolEndocrinol Metab. 2001 Aug;281(2):E197-206.</p>
                <p>Burd NA, West DW, Moore DR, Atherton PJ, Staples AW, Prior T, Tang JE, Rennie MJ, Baker SK, Phillips SM. Enhanced amino acid sensitivity of myofibrillar protein synthesis persists for up to 24 h after resistance exercise in young men. J Nutr. 2011 Apr 1;141(4):568-73.</p>
                <p>Rogers PJ, Smith HJ. Food cravings and food “addiction”: a critical review of the evidence from a biopsychosocial perspective. Pharmacology biochemistry and Behavior 2000;66(1): 3-14</p>
                <p>Lowe MR, Butryn ML. Hedonic hunger: a new dimension of appetite? Physiol Behav 2007; 91: 432–439.</p>
                <p>Honma KL, Honma S, Hiroshige T. Critical role of food amount for prefeeding cortcosterone peak in rats.American Journal of Physiology. 1983; 245: R339-R344.</p>
                <p>Comperatore CA, Stephan FK. Entrainment of duodenal activity to periodic feeding. Journal of Biological Rhythms. 1987; 2:227-242.</p>
                <p>Stephan FK. The “other” circadian system” food as a Zeitgeber Journal of Biological Rhythms. 2002; 17:284-292.</p>
                <p>Steffens AB, 1976 Influence of the oral cavity on insulin release in the rat AM J PHysiol 230:1411-1415</p>
                <p>Johnstone AM, Faber P, Gibney ER, Elia M, Horgan G, Golden BE, Stubbs RJ. Effect of an acute fast on energy compensation and feeding behaviour in lean men and women. Int J Obes Relat Metab Disord. 2002 Dec;26(12):1623-8.</p>
                <p>Guettier JM, Gorden P. Hypoglycemia. Endocrinology Clinics of North America. 2006; 35:753–766</p>
                <p>Wiesli P, Schwegler B, Schmid B, Spinas GA, Schmid C. Mini-mental state examination is superior to plasma glucose concentrations in monitoring patients with suspected hypoglycemic disorders during the 72-hour fast. European Journal of Endocrinololgy 2005;152: 605–610.</p>
                <p>Alken J, et al. Effect of fasting on young adults who have symptoms of hypoglycemia in the absence of frequent meals. European Journal of Clinical Nutrition 2008; 62: 721–726</p>
                <p>Halaas J, Gajiwala K, Maffei M, Cohen S, Chait B, et al. Weight-reducing effects of the plasma protein encoded by the obese gene. Science 1995; 269:543–46</p>
                <p>Chan JL, et al. Short-term fasting-induced autonomic activation and changes in catecholamine levels are not mediated by changes in leptin levels in healthy humans. Clinical Endocrinology 2007; 66: 49–57</p>
                <p>Rosenbaum M, et al. Effects of Weight Change on Plasma Leptin Concentrations and Energy Expenditure. Journal of Clinical Endocrinology and Metabolism 197; 82: 3647–3654</p>
                <p>Rosenbaum M, et al. Low dose leptin administration reverses effects of sustained weight reduction on energy expenditure and circulating concentrations of thyroid hormones. The Journal of Clinical Endocrinology &amp; Metabolism 87(5):2391–2394</p>
                <p>Ahima RS, Flier JS. Leptin. Annual Review of Physiology. 2000; 62:413-37.</p>
                <p>Kolaczynski JW, Considine RV, Ohannesian J, Marco C, Opentanova I, Nyce MR, Myint M, Caro JF. Responses of leptin to short-term fasting and refeeding in humans: a link with ketogenesis but not ketones themselves. Diabetes. 1996; 45(11):1511-5.</p>
                <p>Brennan AM, Mantzoros CS. Drug insight: the role of leptin in human physiology and pathophysiology: emerging clinical applications in leptin deficient states. Nature Clinical Practice Endocrinology &amp; Metabolism. 2006; 2:318–27.</p>
                <p>Hislop MS, Ratanjee BD, Soule SG, Marais AD. Effects of anabolic–androgenic steroid use or gonadal testosterone suppression on serum leptin concentration in men. European Journal of Endocrinology 1999: 141; 40–46</p>
                <p>Harle P, Straub RH. Leptin is a link between adipose tissue and inflammation. Annals of the New York Academy of Sciences 2006; 1069: 454-462</p>
                <p>Horio N et al. New frontiers in gut nutrient sensor research: nutrient sensors in the gastrointestinal tract: modulation of sweet taste sensitivity by leptin. J Pharmacol Sci. 112(1):8-12. 2010</p>
                <p>Baker HWG, Santen RJ, Burger HG, De Krester DM, Hudson B, Pepperell RJ, Bardin CW. Rhythms in the secretion of gonadotropins and gonadal steroids. Journal of Steroids Biochemistry, 1975; 6:793-801.</p>
                <p>Habito RC, Ball MJ (2001) Postprandial changes in sex hormones after meals of different composition. Metabolism 50:505–511</p>
                <p>Habito RC, Montalto J, Leslie E, Ball MJ (2000) Effects of replacing meat with soyabean in the diet on sex hormone concentrations in healthy adult males. Br J Nutr 84:557–563.</p>
                <p>Meikle AW, Stringham JD, Woodward MG, Mcmurry MP (1990) Effects of a fat-containing meal on sex hormones in men. Metabolism 39:943–946.</p>
                <p>Volek JS, Gomez AL, Love DM, Avery NG, Sharman MJ, Kraemer WJ (2001) Effects of a high-fat diet on postabsorptive and postprandial testosterone responses to a fat-rich meal. Metabolism 50:1351–1355.</p>
                <p>Garrel DR, Todd KS, Pugeat MM, Calloway DH. Hormonal changes in normal men under marginally negative energy balance. Am J Clin Nutr 1984;39:930-936.</p>
                <p>Mohr BA, Bhasin S. Link CL, O’Donnell AB and McKinlay JB. The effect of changes in adiposity on testosterone levels in older men: longitudinal results from the Massachusetts Male Aging Study. European Journal of Endocrinology. 2006; 155:443-452.</p>
                <p>Derby CA, Zilber S, Brambilla D, Morales KH, McKinlay JB. Body mass index, waist circumference and waist to hip ratio and change in sex steroid hormones: the Massachusetts Male Ageing Study. Clin Endocrinol (Oxf). 2006 Jul;65(1):125-31.</p>
                <p>Strain GW, Zumoff B, Miller LK, Rosner W, Levit C, Kalin M, Hershcopf RJ, Rosenfeld RS. Effect of massive weight loss on hypothalamic-pituitary-gonadal function in obese men. J Clin Endocrinol Metab. 1988 May;66(5):1019-23.</p>
                <p>Pritchard J, Després JP, Gagnon J, Tchernof A, Nadeau A, Tremblay A, Bouchard C. Plasma adrenal, gonadal, and conjugated steroids following long-term exercise-induced negative energy balance in identical twins. Metabolism. 1999 Sep;48(9):1120-7.</p>
                <p>Khoo J, Piantadosi C, Worthley S, Wittert GA. Effects of a low-energy diet on sexual function and lower urinary tract symptoms in obese men. Int J Obes (Lond) 2010;34: 1396–403.</p>
                <p>Cangemi R, Friedmann AJ, Holloszy JO, Fontana L, Long term effects of calorie restriction on serum sex hormone concentrations in men. Aging Cell (2010) 9, 236-242</p>
                <p>Friedl KE, Moore RJ, Hoyt RW, Marchitelli LJ, Martinez-Lopez LE, Askew EW. Endocrine markers of semistarvation in healthy lean men in a multistressor environment. J Appl Physiol. 2000 May;88(5):1820- 30</p>
                <p>Röjdmark S. Influence of short-term fasting on the pituitary-testicular axis in normal men. Hormone Research. 1987; 25(3):140-6.</p>
                <p>Bergendahl M, Aloi JA, Iranmanesh A, Mulligan TM, Veldhuis JD. Fasting suppresses pulsatile luteinizing hormone (LH) secretion and enhances orderliness of LH release in young but not older men. J Clin Endocrinol Metab. 1998 Jun;83(6):1967-75.</p>
                <p>Klibanski A, Beitins IZ, Badger T, Little R, McArthur JW. Reproductive function during fasting in men. Journal of Clinical Endocrinology and Metabolism. 1981; 53(2):258-63.</p>
                <p>Chennaoui M, Desgorces F, Drogou C, Boudjemaa B, Tomaszewski A, Depiesse F, Burnat P, Chalabi H, Gomez-Merino D. Effects of Ramadan fasting on physical performance and metabolic, hormonal, and inflammatory parameters in middle-distance runners. Applied Physiology Nutrition and Metabolism. 2009; 34(4):587-94.</p>
                <p>Röjdmark S, Asplund A, Rössner S. Pituitary-testicular axis in obese men during short-term fasting. Acta Endocrinol (Copenh). 1989 Nov;121(5):727-32.</p>
                <p>Klibanski A, Beitins IZ, Badger T, Little R, McArthur JW. Reproductive function during fasting in men. J Clin Endocrinol Metab. 1981 Aug;53(2):258-63.</p>
                <p>Roemmich JN and Sinning WE. Weight loss and wrestling training: effects on growth-related hormones. J Appl Physiol 82: 1760–1764, 1997.</p>
                <p>Friedl KE, Moore RJ, Hoyt RW, Marchitelli LJ, Martinez-Lopez LE, Askew EW. Endocrine markers of semistarvation in healthy lean men in a multistressor environment. J Appl Physiol. 2000; 88(5): 1820–1830.</p>
                <p>Bergendahl M, Vance ML, Iranmanesh A, Thorner MO, Veldhuis JD.Fasting as a metabolic stress paradigm selectively amplifies cortisol secretory burst mass and delays the time of maximal nyctohemeral cortisol concentrations in healthy men. J Clin Endocrinol Metab. 1996 Feb;81(2):692-9.</p>
                <p>Soeters MR. Intermittent fasting does not affect whole-body glucose, lipid, or protein metabolism. American Journal of Clinical Nutrition. 2009; 90:1244–51.</p>
                <p>Schteingart DE, Gregerman RI, Conn JW. A comparison of the characteristics of increased adrenocortical function in obesity and Cushing's Syndrome. Metabolism 1963; 1:261-85.</p>
                <p>Morton NM. Obesity and corticosteroids: 11beta-hydroxysteroid type 1 as a cause and therapeutic target in metabolic disease. Mol Cell Endocrinol 2010; 316: 154-164</p>
                <p>Jacoangeli F, Zoli A, Taranto A, et al, 2002 Osteoporosis and anorexia nervosa: relative role of endocrine alterations and malnutrition. Eat Weight Disord 7: 190-195.</p>
                <p>Song WO, Chun OK, Obayashi S, Cho S, Chung CE. Is consumption of breakfast associated with body mass index in US adults? J Am Diet Assoc 2005 105(9): 1373-82</p>
                <p>Gibson SA, O’Sullivan KR: Breakfast cereal consumption patterns and nutrient intakes of British school children. J R Soc Health 115:336–370, 1995</p>
                <p>Shlundt DG, Hill JO, Sbrocco T, Pope-Cordle J, Sharp T. The role of breakfast in the treatment of obesity: A randomized clinical trial. Am J Clin Nutr 1992;55:645-51</p>
                <p>Cotton JR, Burley VJ, Blundell JE. Fat and satiety: No additional intensification of satiety following a fat supplement breakfast. Int J Obes, 1992 16(suppl 1): 11</p>
                <p>Morgan KJ, Zabik ME, Stampley GL. The role of breakfast in the diet adequacy of the U.S. adult population. J Am Coil Nutr l986;5: 551-63.</p>
                <p>Martin A, Normand S, Sothier M, Peyrat J, Louche-Pelissier C, Laville M. Is advice for breakfast consumption justified? Results from a short-term dietary and metabolic experiment in young healthy men. British Journal of Nutrition (2000) 84;337-344</p>
                <p>Sarri KO, et al. Greek orthodox fasting rituals: a hidden characteristic of the Mediterranean diet of Crete. British Journal of Nutrition. 2004; 92: 277-284</p>
                <p>Sarri KO, et al. Effects of Greek Orthodox Christian church fasting on serum lipids and obesity. BMC Public Health. 2003; 3: 3-16</p>
                <p>Neel JV. Diabetes Mellitus: A “thrifty” genotype rendered detrimental by progress”? the American Journal of Human Genetics. 1962; 14:353-362.</p>
                <p>Randle PJ, Garland PB, Hales CN, Newsholme EA, The glucose fatty-acid cycle. Its role in insulin sensitivity and the metabolic disturbances of diabetes mellitus. Lancet 1963:1;785-789.</p>
                <p>Halberg N, et al. Effect of intermittent fasting and refeeding on insulin action in healthy men. Journal of Applied Physiology 2005; 99:2128-2136</p>
                <p>Klein S, et al. Progressive Alterations in lipid and glucose metabolism during short-term fasting in young adult men. American Journal of Physiology 1993; 265 (Endocrinology and metabolism 28):E801-E806</p>
                <p>Soules MR, Merriggiola MC, Steiner RA, Clifton DK, Toivola B, Bremner WJ. Short-Term fasting in normal women: absence of effects on gonadotrophin secretion and the menstrual cycle. Clinical Endocrinology 1994; 40:725-731.</p>
                <p>Hosker J, Matthews D, Rudenski A, Burnett M, Darling P, Bown E, Turner R: Continuous infusion of glucose with model assessment: measurement of insulin resistance and b-cell function in man. Diabetologia 28:401–411, 1985</p>
                <p>Turner R, Holman R, Matthews D, Hockaday T, Peto J: Insulin deficiency and insulin resistance interaction in diabetes: estimation of their relative contribution by feedback analysis from basal plasma insulin and glucose concentrations. Metabolism 2 8 : 1086–1096, 1979</p>
                <p>Matthews D, Hosker J, Rudenski A, Naylor B, Treacher D, Tu rner R: Homeostasis model assessment: insulin resistance and b-cell function from fasting plasma glucose and insulin concentrations in man. Diabetologia28:412–419, 1985</p>
                <p>Wong MH, Holst C, Astrup A, Handjieva-Darlenska T, Jebb SA, Kafatos A, Kunesova M, Larsen TM, Martinez JA, Pfeiffer AF, van Baak MA, Saris WH, McNicholas PD, Mutch DM; DiOGenes. Caloric restriction induces changes in insulin and body weight measurements that are inversely associated with subsequent weight regain. PLoS One. 2012;7(8):e42858.</p>
                <p>Svendsen PF, Jensen FK, Holst JJ, Haugaard SB, Nilas L, Madsbad S. The effect of a very low calorie diet on insulin sensitivity, beta cell function, insulin clearance, incretin hormone secretion, androgen levels and body composition in obese young women.Scand J Clin Lab Invest. 2012 Sep;72(5):410-9.</p>
                <p>Mason C, Foster-Schubert KE, Imayama I, Kong A, Xiao L, Bain C, Campbell KL, Wang CY, Duggan CR, Ulrich CM, Alfano CM, Blackburn GL, McTiernan A. Dietary weight loss and exercise effects on insulin resistance in postmenopausal women.Am J Prev Med. 2011 Oct;41(4):366-75.</p>
                <p>Kassi E, Papavassiliou AG. Could glucose be a proaging factor? Journal of Cellular and Molecular medicine. 2008; 12(4):1194-8</p>
                <p>Ling PR, Smith RJ, Bistrian BR. Acute effects of hyperglycemia and hyperinsulinemia on hepatic oxidative stress and the systemic inflammatory response in rats. Critical Care Medicine 2007; 35: 555-560.</p>
                <p>Zechner R, Kienseberger PC, Hammerle G, Zimmermann R, Lass A. Adipose triglyceride lipase and the lipolytic catabolism of cellular fat stores. Journal of Lipid Research 2009;50:3-21.</p>
                <p>Nielsen TS, Vandelbo MH, Jessen N, Pedersen SB, Jorgensen JO, Lund S, Moller N. Fasting, but not exercise, increases adipose triglyceride lipase (ATGL) protein and reduces G(0)/G(1) switch gene 2 (G0S2) protein and mRNA content in human adipose tissue. J Clin Endocrin Metab. 2011;96:E0000-E0000.</p>
                <p>Tunstall RJ, et al. Fasting activates the gene expression of UCP3 independent of genes necessary for lipid transport and oxidation in skeletal muscle. Biochemical and Biophysical Research Communications 2002; 294:301-308</p>
                <p>Eakman GD, Dallas JS, Ponder SW, Keenan BS. The effects of testosterone and dihydrotestosterone on hypothalamic regulation of growth hormone secretion. J Clin Endocrinol Metab 81: 1217–1223, 1996.</p>
                <p>Lang I, Schernthaner G, Pietschmann P, Kurz R, Stephenson JM, Templ H. Effects of sex and age on growth hormone response to growth hormone-releasing hormone in healthy individuals. J Clin Endocrinol Metab 65: 535–540, 1987.</p>
                <p>Hartman ML, et al. Augmented growth hormone (GH) secretory burst frequency and amplitude mediate enhanced CH secretion during a two-day fast in normal men. Journal of Clinical Endocrinology and Metabolism 1992; 74(4):757-765</p>
                <p>Vendelbo MH, Jorgensen JO, Pedersen SB, Gormsen LC, Lund S, Schmitz O, Jessen N, and Moller N. Exercise and fasting activate growth hormone-dependent myocellular signal transducer and Activator of transcription-5b phosphorylation and Insulin-like growth factor-1 messenger ribonucleic acid expression in humans. Journal of Clinical Endocrinology and Metabolism. 2010; 95(9): 1-5</p>
                <p>Rizza RA, Mandarino LJ &amp; Gerich JE. Effects of growth hormone on insulin action in man. Mechanism of insulin resistance, impaired suppression of glucose production, and impaired stimulation of glucose utilization. Diabetes 1982 31 663–669</p>
                <p>Norrelund H. Modulation of basal glucose metabolism and insulin sensitivity by growth hormone and free fatty acids during short-term fasting. European Journal of Endocrinology 2004; 150: 779-787</p>
                <p>Hansen M, et al. Effects of 2 wk of GH administration on 24-h indirect calorimetry in young, healthy, lean men. American Journal of Physiology Endocrinology and Metabolism 2005; 289: E1030-E1038</p>
                <p>Moller L, Dalman L, Norrelund H, Billestrup N, Frystyk J, Moller N, and Jorgensen JOL. Impact of fasting on growth hormone signaling and action in muscle and fat. Journal of Clinical Endocrinology and Metabolism. 2009;4: 965-972.</p>
                <p>Szego CM, White A. The influence of purified growth hormone on fasting metabolism. J Clin Endocrinol Metab 8;1948:594.</p>
                <p>Norrelund H. The protein-retaining effects of growth hormone during fasting involve inhibition of muscle protein breakdown. Diabetes 2001;50:96-104</p>
                <p>Norrelund H, Rils AL, Moller N. Effects of GH on protein metabolism during dietary restriction in man. Growth hormone &amp; IGF Research 2002; 12: 198-207</p>
                <p>Moller N, Jorgensen JO. Effects of growth hormone on glucose, lipid and protein metabolism in human subjects. Endocrine Reviews. 2009; 30:152-177</p>
                <p>Norrelund H. Abstracts of Ph.D. Dissertations – Effects of growth hormone on protein metabolism during dietary restriction. Studies in Normal, GH-Deficient and Obese Subjects. Danish Medical Bulletin 2001; 47 (5): 370</p>
                <p>Norrelund H. The metabolic role of growth hormone in humans with particular reference to fasting. Growth Hormone and IGF research. 2005;15:95-122.</p>
                <p>Oscarsson J, Ottosson M, Eden S. Effects of growth hormone on lipoprotein lipase and hepatic lipase. J Endocrinol Invest 22; 1999: 2-9</p>
                <p>Oscarsson J, Ottosson M, Vikman-adolfsson K, et al. GH but not IFG-1 or insulin increases lipprotein lipase activity in muscle tissues of hpophysectomizes rats. J Endocrinol. 160;1999:247-255.</p>
                <p>Veldhuis JD, Iranmanesh A, Ho KK, Waters MJ, Johnson ML, Lizarralde G. Dual defects in pulsatile growth hormone secretion and clearance subserve the hyposomatotropism of obesity in man. J Clin Endocrinol Metab. 1991 Jan;72(1):51-9.</p>
                <p>Cornford AS, Barkan AL, Horowitz JF. Rapid suppression of Growth Hormone concentration by overeating: Potential mediation by hyperinsulinemia. J Clin Endocrinol Metab 96: 824-830, 2011.</p>
                <p>Rabinowitz D, Zierler KL. A metabolic regulating device based on the actions of growth hormone and of insulin singly and together in the human forearm 1963. Nature; 199: 913-915.</p>
                <p>Veldhuis JD, Roemmich JN, Richmond EJ, Bowers CY. Somatotropic and gonadotropic axes linkages in infancy, childhood, and the pubertyadult transition. Endocr Rev 27: 101–140, 2006.</p>
                <p>Veldhuis JD. Aging and hormones of the hypothalamo-pituitary axis: gonadotropic axis in men and somatotropic axes in men and women. Ageing Research Reviews. 2008; 7: 189–208.</p>
                <p>Finkelstein JW, Roffwarg HP, Boyar RM, Kream J, Hellman L. Age-related change in the twenty-four hour spontaneous secretion of growth hormone. Journal of Clinical Endocrinolology and Metabolism. 1972 Nov;35(5):665-70</p>
                <p>Corpas E, Harman SM, Blackman MR. Human growth hormone and human aging. Endocrine Reviews. 1993; 14: 20–39.</p>
                <p>Frayne, K.N. 1993. Insulin resistance and lipid metabolism. Curr. Opin. Lipidol. 4:197–204 156 Boden, G., Chen, X., Ruiz, J., White, J.V., and Rosetti, L. 1994. Mechanism of fatty acid induced inhibition of glucose uptake. J. Clin. Invest. 93:2438–2446.</p>
                <p>Kanaley JA, Weatherup-Dentes MM, Jaynes EB, Hartman ML. Obesity attenuates the growth hormone response to exercise. Journal of Clinical Endocrinology Metabolism. 1999;84:3156-3161.</p>
                <p>Redman LM, Veldhuis JD, Rood J, Smith SR, Williamson D, Ravussin E; Pennington CALERIE Team. The effect of caloric restriction interventions on growth hormone secretion in nonobese men and women. Aging Cell. 2010 Feb;9(1):32-9.</p>
                <p>Rasmussen MH, Hvidberg A, Juul A, et al. Massive weight loss restores 24-hour growth hormone release profiles and serum insulin-like growth factor-I levels in obese subjects. Journal of Clinical Endocrinology Metabolism. 1999; 80:1407-1415</p>
                <p>Mauras N, O’brien KO, Welch S, et al. Insulin-like growth factor 1 and growth hormone (GH) treatment in GH-Deficient humans: differential effects on protein, glucose, lipid and calcium metabolism. J Clin Endocrinol Metab 85;2000:1686-1694.</p>
                <p>Rennie MJ. Claims for the anabolic effects of growth hormone: a case of the Emperor’s new clothes? British Journal of Sports Medicine 2003; 37:100–105</p>
                <p>Duncan GG, Cristofori FC, Yue JK, Murthy MSJ: Control of obesity by intermittent fasts. Med Clin N Amer 48: 1359, 1964.</p>
                <p>Johnstone, AM. Fasting – the ultimate diet? Obesity Reviews 2007; 8(3): 211-222</p>
                <p>Lionetti L, Mollica MP, Lombardi A, Cavaliere G, Gifuni G, Barletta A. From chronic overnutrition to insulin resistance: the role of fat-storing capacity and inflammation. Nutriton, Metabolism &amp; Cardiovascular disease 2009;19:146-152.</p>
                <p>Chung HY, Kim HJ, Kim JW, Yu BP. The inflammation hypothesis of aging: molecular modulation by calorie restriction. Annals of the New York Academy of Sciences. 2001; 928:327-35.</p>
                <p>Senn JJ, Klover PJ, Nowak IA, and Moony RA. IL-6 Induces Cellular Insulin Resistance in Hepatocytes. Diabetes. 2002; 51(12):3391-9.</p>
                <p>Bharat B. Aggarwal, R.V. Vijayalekshmi, and Bokyung Sung. Targeting Inflammatory Pathways for Prevention and Therapy of Cancer: Short-Term Friend, Long-Term Foe. Clinical Cancer Research 2009; 15(2): 425-430.</p>
                <p>Kershaw EE, Flier JS. Adipose tissue as an endocrine organ. Journal of Clinical Endocrinology and Metabolism. 2004; 89:2548–2556.</p>
                <p>Loffreda S, Yang SQ, Lin HZ, Karp CL, Brengman ML, Wang DJ, Klein AS, Bulkley GB, Bao C, Noble PW, Lane MD, Diehl AM. Leptin regulates proinflammatory immune responses. Federation of American Societies for Experimental Biology Journal. 1998 Jan;12(1):57-65.</p>
                <p>Esposito K, Nappo F, Marfella R, Giugliano G, Giugliano F, Ciotola M, Quagliaro L, Ceriello A, Giugliano D. Inflammatory cytokine concentrations are acutely increased by hyperglycemia in humans: role of oxidative stress. Circulation. 2002 Oct 15;106(16):2067-72.</p>
                <p>Dixit VD. Adipose-immune interactions during obesity and caloric restriction: reciprocal mechanisms regulating immunity and health span. Journal of Leukocyte Biology. 2008: 84:882-892.</p>
                <p>Morgan TE, Wong AM, and Finch CE. Anti-inflammatory mechanisms of dietary restriction in slowing aging processes. Interdisciplinary Topics in Gerontology. 2007; 35:83-97.</p>
                <p>Fontana L. Neuroendocrine factors in the regulations of inflammation: Excessive adiposity and calorie restriction. Experimental Gerontology. 2009; 44:41-45.</p>
                <p>Prestes J, Shiguemoto G, Botero JP, Frollini A, Dias R, Leite R, et al. Effects of resistance training on resistin, leptin, cytokines, and muscle force in elderly post-menopausal women. Journal of Sports Science. 2009 27(14):1607-1615.</p>
                <p>Bruun JM, Helge JW, Richelsen B, Stallknecht B. Diet and exercise reduce low-grade inflammation and macrophage infiltration in adipose tissue but not in skeletal muscle in severely obese subjects. American Journal of Physiology Endocrinology and Metabolism.</p>
                <p>2006 May;290(5):E961-7. 176 Schapp LA, Plijm SMF, Deeg DJh and Visser M. Inflammatory markers and loss of muscle mass (Sarcopenia) and strength. 2006. American Journal of Medicine; 199: U82-U90.</p>
                <p>Toth MH, Mattews DE, Tracy RP and Previs MJ. Age-related differences in skeletal muscle protein synthesis: relation to markers of immune activation. American Journal of Pysiology Endocrinology and Metabolism 2005; 288:E883-E891.</p>
                <p>Schoeller, D. A., L. K. Cella, M. K. Sinha, and J. F. Caro. Entrainment of the diurnal rhythm of plasma leptin to meal timing. J. Clin. Invest. 100: 1882–1887, 1997.</p>
                <p>Laughlin, G. A., and S. S. C. Yen. Hypoleptinemia in women athletes: absence of a diurnal rhythm with amenorrhea. J. Clin. Endocrinol. Metab. 82: 318–321, 1997</p>
                <p>L. K. HILTON AND A. B. LOUCKS Low energy availability, not exercise stress, suppresses the diurnal rhythm of leptin in healthy young women. Am. J. Physiol. Endocrinol. Metab. 278: E43–E49, 2000.</p>
                <p>Bourdon, L., A. Buguet, M. Cucherat, and M.W. Radomski. Use of a spreadsheet program for circadian analysis of biological/ physiological data. Aviat. Space Environ. Med. 66: 787–791, 1995.</p>
                <p>Grinspoon, S. K., H. Askari, M. L. Landt, D. M. Nathan, D. A. Schoenfeld, D. L. Hayden, M. Laposata, J. Hubbard, and A. Klibanski. Effects of fasting and glucose infusion on basal and overnight leptin concentrations in normal-weight women. Am. J. Clin. Nutr. 66: 1352–1356, 1997.</p>
                <p>Visser M, Pahor M, Taaffe DR, Goodpaster BH, Simonsick EM, Newman AB et al. Relationship of interluekin-6 and tumor necrosis factor-a with muscle mass and muscle strength in elderly men ad women” The health ABC study. Journal of Gerontology Series A: Biological Science and Medical Science 2002; 57: M326-M332.</p>
                <p>Deter RL, De Duve C. Influence of glucagon, an inducer of cellular autophagy, on some physical properties of rat liver lysosomes. J Cell Biol 1967;33:437–449</p>
                <p>A.M. Cuervo, E. Bergamini, U.T. Brunk, W. Droge, M. Ffrench, A. Terman, Autophagy and aging: the importance of maintaining ‘‘clean’’ cells, Autophagy 1 (2005) 131e140.</p>
                <p>T Kanazawa, Ikue Taneike, Ryuichiro Akaishi, Fumiaki Yoshizawa, Norihiko Furuya, Shinobu Fujimura, and Motoni Kadowaki. Amino Acids and Insulin Control Autophagic Proteolysis through Different Signaling Pathways in Relation to mTOR in Isolated Rat Hepatocytes. THE JOURNAL OF BIOLOGICAL CHEMISTRY Vol. 279, No. 9, Issue of February 27, pp. 8452–8459, 2004</p>
                <p>Spaulding, S. W., I. J. Chopra, R. S. Sherwin, and S. S. Lyall. Effect of caloric restriction and dietary composition on serum T3 and reverse T3 in man. J. Clin. Endocrinol. Metab. 42: 197-200,1976.</p>
                <p>Glynn EL, Fry CS, Drummond MJ, Timmerman KL, Dhanani S, Volpi E, Rasmussen BB. Excess leucine intake enhances muscle anabolic signaling but not net protein anabolism in young men and women. J Nutr. 2010 Nov;140(11):1970-6.</p>
                <p>Joon-Ho Sheen, Roberto Zoncu, Dohoon Kim, David M. Sabatini Defective Regulation of Autophagy upon Leucine Deprivation Reveals a Targetable Liability of Human Melanoma Cells In Vitro and In Vivo. Cancer Cell, Volume 19, Issue 5, 613-628, 17 May 2011</p>
                <p>Browning, J. D., J. Baxter, S. Satapati, and S. C. Burgess. The effect of short-term fasting on liver and skeletal muscle lipid, glucose, and energy metabolism in healthy women and men. <i>J. Lipid Res.</i> 2012. 53: 577–586.</p>
                <p>Ding, WX. The emerging role of autophagy in alcoholic liver disease Exp Biol Med 1 May 2011: 546-556.</p>
                <p>Hara T, et al. Suppression of basal autophagy in neural cells causes neurodegenerative disease in mice. Nature 2006; 441:885-9</p>
                <p>Komatsu M, et al. Loss of autophagy in the central nervous system causes neurodegeneration in mice. Nature 2006; 441:880-4</p>
                <p>Mizushima N, Levine B, Cuervo AM, Klionsky DJ. Autophagy fights disease through cellular self digestion. Nature 2008; 451:1069-75</p>
                <p>Alirezaei M, Kiosses WB, Flynn CT, Brady NR, Fox HS. Disruption of neuronal autophagy by infected microglia results in neurodegeneration. PLoS ONE 2008; 3:2906</p>
                <p>Wijngaarden MA, Bakker LE, van der Zon GC, 't Hoen PA, Willems van Dijk K, Jazet IM, Pijl H, Guigas B. Regulation of skeletal muscle energy/nutrient-sensing pathways during metabolic adaptation to fasting in healthy humans. Am J Physiol Endocrinol Metab. 2014 Sep 23.</p>
                <p>Orvedahl A, Levine B. Eating the enemy within: autophagy in infectious diseases. Cell Death Differ 2009; 16:57-69</p>
                <p>Alirezaei M, Kemball CC, Flynn CT, Wood MR, Whitton JL, Kiosses WB. Short-term fasting induces profound neuronal autophagy. Autophagy. 2010 Aug;6(6):702-10.</p>
                <p>Hara, N., K. Nakamura, M. Matsui, A. Yamamato, Y. Nakahara, R. Suzuki-Migishima, M. Yokoyama, K. Mishima, I. Saito, H. Okana, and N. Mizushima. Suppression of basal autophagy in neural cells causes neurodegenerative disease in mice. Nature. In press</p>
                <p>Komatsu M, et al. Loss of autophagy in the central nervous system causes neurodegeneration in mice. Nature 2006; 441:880-4</p>
                <p>Jaeger PA, Wyss-Coray T. All-you-can-eat: autophagy in neurodegeneration and neuroprotection. Mol Neurodegener 2009; 4:16</p>
                <p>Harris RBS, Ramsay TG, Smith SR, Bruch RC. Early and late stimulation of ob mRNA expression in meal-fed and overfed rats. J Clin Invest 97: 2020–2026, 1996.</p>
                <p>Saladin R, Devos P, Guerremillo M, Leturque A, Girard J, Staels B, Auwerx J. Transient increase in obese gene-expression after food-intake or insulin administration. Nature 377: 527–529, 1995.</p>
                <p>Hung SY, Huang WP, Liou HC, Fu WM. Autophagy protects neuron from Aß-induced cytotoxicity. Autophagy 2009; 5:502-10.</p>
                <p>Donati A, Cavallini G., Paradiso C., Vittorini S., Pollera M., Gori Z. and E. B. Age-related changes in the autophagic proteolysis of rat isolated liver cells: effects of antiaging dietary restrictions. J Gerontol A Biol Sci Med Sci. 2001; 56: B375-383.</p>
                <p>Rubinsztein DC. The roles of intracellular protein-degradation pathways in neurodegeneration. Nature. 2006; 443: 780-786</p>
                <p>K. Kirkegaard, M.P. Taylor, W.T. Jackson, Cellular autophagy: surrender, avoidance and subversion by microorganisms, Nat. Rev. Microbiol. 2 (2004) 301e314</p>
                <p>B. Levine, Eating oneself and uninvited guests: autophagy-related pathways in cellular defense, Cell 120 (2005) 159e162</p>
                <p>Lee MJ, Yang RZ, Gong DW, Fried SK. Feeding and insulin increase leptin translation. Importance of the leptin mRNA untranslated regions. <i>J Biol Chem</i> 282: 72–80, 2007.</p>
                <p>Russell CD, Ricci MR, Brolin RE, Magill E, Fried SK. Regulation of the leptin content of obese human adipose tissue. <i>Am J Physiol Endocrinol Metab</i> 280: E399–E404, 2001.</p>
                <p>M. Ogawa, C. Sasakawa, Bacterial evasion of the autophagic defense system, Curr. Opin. Microbiol. 9 (2006) 62e68</p>
                <p>M.S. Swanson, Autophagy: eating for good health, J. Immunol. 177 (2006) 4945e4951.</p>
                <p>Anson RM, et al. Intermittent fasting dissociates beneficial effects of dietary restriction on glucose metabolism and neuronal resistance to injury from calorie intake. Proc Natl Acad Sci USA 2003; 100:6216-20</p>
                <p>Duan W, et al. Dietary restriction normalizes glucose metabolism and BDNF levels, slows disease progression, and increases survival in huntingting mutant mice. Proc Natl Acad Sci USA 2003; 100:2911-6</p>
                <p>Tohyama D, Yamaguchi A and Yamashita T. Inhibition of a eukaryotic initiation factor (eIF2Bdelta/F11A3.2) during adulthood extends lifespan in Caenorhabditis elegans. FASEB J. 2008; 22: 4327-4337</p>
                <p>Nair U, Klionsky DJ. Activation of autophagy is required for muscle homeostasis during physical exercise. Autophagy. 2011 Dec 1;7(12).</p>
                <p>Sandri M. Autophagy in health and disease. 3. Involvement of autophagy in muscle atrophy. Am J Physiol Cell Physiol 2010; 298:C1291-7</p>
                <p>Drummond DA. Mistranslation-induced protein misfolding as a dominant constraint on coding-sequence evolution. Cell. 2008; 134: 341-352</p>
                <p>Fishebin L, Biological effects of Dietary Restriction. Springer-Verlag, New York.1991.</p>
                <p>Lane MA, Ingram DK, Roth GS. Caloric Restriction in nonhuman primates: Effects on Diabetes and cardiovascular disease risk. Toxilogical Sciences 1999; 52s: 41-48.</p>
                <p>Varaday KA, Bhutani S, Church EC, Klempel EC, Short-term modified alternate-day fasting: a novel dietary strategy for weight loss and cardioprotection in obese adults. American Journal of Clinical Nutrition 2009; 90:1138–43.</p>
            </div>

            <div class="columnContent">

                <div class="footer">
                    <hr>
                    <p>Contact us: <a href="mailto:help@eatstopeat.com">help@eatstopeat.com</a></p>
                    <p>ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.</p>
                </div>

            </div>

        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
