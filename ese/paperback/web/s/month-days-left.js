

// helpers
var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

// basics
var today = new Date();
var now = today.getDate();
var year = today.getFullYear();
var month = today.getMonth();
var monthName = monthNames[month];

// check for leap year
if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) monthDays[1] = "29";

// how much days left
var daysLeftInThisMonth = monthDays[month] - now;

// just fix to never get zero :)
if(daysLeftInThisMonth == 0) daysLeftInThisMonth++;

// Plural interpretation of days
var daysLeftInThisMonthText = daysLeftInThisMonth + (daysLeftInThisMonth>1?' Days':' Day');

/* initialize and update UI */
$(function(){

    //alert(monthName);
    //alert(daysLeftInThisMonthText);

    $(".monthly_diet."+monthName).show();
    $(".current-month-name").html(monthName);
    $(".month-days-left").html(daysLeftInThisMonthText);

});





