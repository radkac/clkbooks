<?php
require($_SERVER['DOCUMENT_ROOT'].'/prices.php');
$servername = "localhost";
$username = "pricewin_c1kbks";
$password = "qu7MTK?ZbJI$";
$dbname = "pricewin_clkbooks";
$initial_count = 800;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: ".$conn->connect_error);
}

// let's count the row
$count = "SELECT COUNT(*) FROM purchases";
$count = $conn->query($count);
$count = $count->fetch_row();

// Now return the count, oh, before that I'll close MySQLi connection to let me MySQL server free
$conn->close();
$so_count_down = $initial_count - $count[0];
$sold_count_up = $count[0] -300;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Eat Stop Eat</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=2">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=2">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=2">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

    <meta property="og:image" content="http://clkbooks.com/ese/web/i/fb-og-ese.png">
	<meta property="og:description" content="Eat Stop Eat: In Just One Day This Simple Strategy Frees You From Complicated Diet Rules — And Eliminates Rebound Weight Gain">
	<meta property="og:title" content="Check Out What I'm Reading. Finally available in print format!">

    <script>

        $(function () {

            $(".read-more").click(function () {
                var collapsed = $(".read-more").html() == "Read more" ? true : false;
                var caption = collapsed ? "Read less" : "Read more";
                $(".read-more").html(caption);
                if (collapsed) {
                    $(".read-more").addClass("collapsed");
                } else {
                    $(".read-more").removeClass("collapsed");
                }
                $(".read-more-content").toggle(400);
            });

        });

    </script>

<?php require($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="tagline">
        <div class="inner">
            <h2>Claim One Of <strike class="claim-left">500<i><?= $so_count_down ?></i></strike> FREE Copies Of The Bestselling <i>Eat
                    Stop Eat</i></h2>
        </div>
    </div>

    <div id="breadcrumbs">
        Books › Weight Loss Books › New Releases
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <div class="intro-outlined">
                    <p>We've received so many requests to offer a print edition of Brad's bestseller that we decided to
                        have 500 copies printed for you. And to thank you for patience, we're giving away this first
                        batch for FREE. However, as an experiment, we also decided to offer the book on Amazon. And
                        after only a few days, <?= $sold_count_up ?> copies have already been claimed at full price. So if you want to
                        claim a FREE copy, act now&hellip;</p>
                </div>

                <h1>Eat Stop Eat</h1>
                <p class="byline">By Brad Pilon
                    <small>(Author)</small>
                </p>
                <p class="rating"><img src="web/i/4-of-5-stars-rating.gif" alt="">
                    <small>(based on 574 reviews from Goodreads)</small>
                </p>

            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img class="main-ecover mobile-2of3-width" src="web/i/brad-with-ese-new.jpg?v=3" width="100%" alt="">
                    <br><br>
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>
                        FREE Print Edition
                    </h4>
                    <p><i>(<?= $so_count_down ?> Copies Remaining)*</i></p>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v"><strike>&nbsp;$19.99&nbsp;</strike></td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b class="bg-yellow">FREE</b>*</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*We just ask you to cover Shipping &amp; Handling</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://147.eatstopeat.pay.clickbank.net/?cbfid=31409&cbskin=18285&vtid=clk31409"><span
                                    class="inner">Enter Your Shipping Details</span></a>
                    <p class="purchase-hint"><i>*inventory updates in real time</i></p>
                    </p>
                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                    <div class="intro-outlined">
                        <p>We've received so many requests to offer a print edition of Brad's bestseller that we decided
                            to have 500 copies printed for you. And to thank you for patience, we're giving away this
                            first batch for FREE. However, as an experiment, we also decided to offer the book on
                            Amazon. And after only a few days, <?= $sold_count_up ?> copies have already been claimed at full price. So if
                            you want to claim a FREE copy, act now&hellip;</p>
                    </div>

                    <h1>Eat Stop Eat</h1>
                    <p class="byline">By Brad Pilon
                        <small>(Author)</small>
                    </p>
                    <p class="rating"><img src="web/i/4-of-5-stars-rating.gif" alt="">
                        <small>(based on 574 reviews from Goodreads)</small>
                    </p>

                </div>

                <p>Here’s a real head-scratcher for you &mdash; it’s something everyone seems to believe about weight
                    loss. And it may be the reason you struggle with your weight&hellip;</p>
                <p>Think about this&hellip; Almost every diet, weight loss pill, supplement or program is telling you to
                    consume MORE of something.</p>
                <p>Weird right?</p>
                <p>They quietly convince you that eating less is hard, scary, bad for you, and that it’s just not right&hellip;
                    and they leave out the fact that it’s the only real solution to permanent weight control.</p>
                <p>Instead, why aren’t they telling you about the ONE thing that actually <u>makes it <b>easier</b></u>
                    to eat less?</p>
                <p>It’s a simple way of eating that mimics the way your ancestors ate. It’s what kept them slim and
                    healthy. And it’s what your body still craves today&hellip;</p>

                <div class="read-more-content collapsed hidden">

                    <p>The simple and unpopular truth is: you need to limit how much you eat in order to lose weight. So
                        why isn’t that what you are hearing? Simple&hellip;</p>
                    <p>Scientific studies that tell you to “consume” less are not sexy &mdash; and they don’t get the
                        same backing and marketing dollars as studies that hint you can buy a pill, eat a certain food,
                        or purchase a certain supplement and still lose weight&hellip;</p>
                    <p>Take heart though. Because even though those things don’t work, there’s still good
                        news&hellip;</p>
                    <p>What if you only had to “diet” one or two days a week to lose weight? Here’s what I
                        mean&hellip;</p>
                    <p>When you don’t eat you get hungry, right?&hellip;</p>
                    <p>And when you’re trying to follow a diet, <u>doesn’t it feel like your tummy is rumbling ALL the
                            time</u>?</p>
                    <p>It’s like you’re running a marathon and the finish line keeps getting pushed away. There’s never
                        a day or a time when you are allowed to just NOT feel hungry. However&hellip;</p>
                    <p>Imagine feeling hunger ONLY once or twice a week for a specific amount of time &mdash; and still
                        getting the same or BETTER weight loss results as when you have to diet every single
                        day&hellip;</p>
                    <p>And the rest of the time your belly feels full and happy&hellip; And you are never even thinking
                        about your next meal.</p>
                    <p>Wouldn’t that lift a weight off your shoulders?</p>
                    <p>The method I’m talking about is a specific way to do something nutritionists call Intermittent
                        Fasting (IF). However the method you’re about to discover is very different to what you may have
                        heard about&hellip;</p>
                    <p>It’s a simple method of Intermittent Fasting called Eat Stop Eat &mdash; developed by
                        international author, speaker and nutrition expert Brad Pilon.</p>
                    <p>You’ll only ever use this method once or twice a week, depending on your goal.</p>
                    <p>And get this&hellip; you never go a single day without enjoying a completely normal meal. Now&hellip;</p>
                    <p>Think about your ancestors. They naturally had cycles of high and low calories. Really that’s all
                        you’re doing when you practice Eat Stop Eat.</p>
                    <p>Listen: if you’re like most folks new to this simple yet powerful weight loss and health
                        enhancing strategy, you may think these brief periods without eating could feel limiting. But
                        it’s the exact opposite.</p>
                    <p><b>Remember:</b> when you’re on a diet you feel hungry all the time, right?</p>
                    <p>Yet when you’re following the Eat Stop Eat method you’re hungry just <u>once or twice a week at
                            most</u>.</p>
                    <p>That’s it! And you still get all the weight loss benefits of constant dieting. In fact most folks
                        get BETTER results.</p>
                    <p>And guess what&hellip; the hunger never gets any worse than your normal hunger between meals that
                        you CONSTANTLY suffer from when you’re on a diet.</p>
                    <p>Wait! What?</p>
                    <p><i>“Instead of being hungry ALL THE TIME &mdash; like when you’re on a diet &mdash; you can limit
                            your hunger to only one or two days a week?&hellip;”</i></p>
                    <p><b>Yes!</b> And you won’t be any more or less hungry than on a diet. But you’ll be FREE from
                        hunger the rest of the week. And you’ll be free to eat whatever you want.</p>
                    <p>FREEDOM is the reward you get from Eat Stop Eat.</p>
                    <p>Now, the author of Eat Stop Eat &mdash; Brad Pilon &mdash; has studied over 317 peer reviewed
                        scientific research papers. He continues to devour every new piece of scientific evidence linked
                        to Intermittent Fasting. And&hellip;</p>
                    <p>He discovered the benefits of the simple strategy he created are shocking, and go way beyond
                        simple weight loss. Here’s what you can expect&hellip;</p>

                    <p><b><i>In the pages of Eat Stop Eat, the author explains the research* behind what happens in your
                                body when you use this surprising strategy:</i></b></p>

                    <ul class="check green">
                        <li>An Increase In Your #1 <b>Fat Burning Hormone</b> By 700%</li>
                        <li>Control of Your “Hunger Hormone” and an End to Cravings</li>
                        <li>A Decrease In Your Stress Hormone So You <b>Burn More Belly Fat</b></li>
                        <li>Increase in Your Brain Function For Better <b>Memory</b> and <b>Concentration</b></li>
                        <li>And a Boost In Your <b>Metabolism &amp; Energy</b></li>
                        <li><b>Reduced Risk of Diabetes</b> &amp; an Easing of Symptoms</li>
                        <li>Increased <b>Testosterone</b> If You’re a Man</li>
                        <li>Increased Insulin Sensitivity So You Can <b>Eat More &amp; Stay Slim</b></li>
                        <li>Faster <b>Weight Loss</b></li>
                        <li>Decreased Inflammation So Your <b>Joints Heal &amp; Feel Better</b></li>
                        <li>Rapid <b>Cleansing &amp; Renewal</b> of Your Body At a Cellular Level</li>
                        <li>And much more&hellip;</li>
                    </ul>

                    <p>Brad’s Eat Stop Eat uses no-nonsense language. It explains the science of exactly WHY
                        Intermittent Fasting &mdash; done right &mdash; delivers all these powerful and lasting benefits&hellip;</p>
                    <p>Listen: when Brad started his research into Intermittent Fasting he was actually 100% opposed to
                        it. He thought you had to eat every 2-3 hours to reach your weight loss goals. But he had to eat
                        his words when the research became clear.</p>
                    <p>The science* is clearly showing that:</p>

                    <ul class="check blue">
                        <li>Your metabolism will never be damaged by the Eat Stop Eat method of fasting, in fact the
                            research shows your fat burning metabolism will actually increase
                        </li>
                        <li>Even the more exaggerated methods of Intermittent Fasting will not reduce your lean muscle
                            mass
                        </li>
                        <li>You will not feel a decrease in energy while intermittent fasting, in fact it has been shown
                            to increase alertness and concentration
                        </li>
                    </ul>

                    <p>Plus, you don’t have to take our word for any of this. All this and more is referenced by Brad in
                        the book. And you’ll see and feel the results for yourself after even the first day of using the
                        protocol.</p>

                    <div class="insertion right">
                        <center><img class="mobile-half-width" src="web/i/ecover-quick-start-guide.jpg" width="200"
                                     alt=""></center>
                    </div>

                    <p>And to help you start feeling the power of Eat Stop Eat even faster, we asked Brad to create a
                        <u><b>CLK*books exclusive bonus</b></u> for you. It’s called the ESE Quick Start Guide and we
                        are including it as a <b>FREE</b> digital download right now with Eat Stop Eat.</p>
                    <p>Here’s how Brad describes this handy manual:</p>
                    <p><i>“Have you ever skipped the complicated instructions when setting up some new software on your
                            computer, and then regretted it? That's me… I always want to skip ahead and just get
                            started. That’s why I created the <b>Quick Start Guide</b> for you. You’ll get a running
                            start so you can quickly enjoy the benefits of Eat Stop Eat within minutes of receiving the
                            book.”</i></p>
                    <p>PLUS, today you are also getting FREE VIP Inside Access to Brad’s exclusive member-only
                        newsletter. He has agreed to give you 9 Issues of his Advocate newsletter for FREE. This is
                        where Brad shares the mind blowing insights, tips and tricks that come from his constant
                        scouring of the latest scientific research papers on nutrition, weight loss, exercise and
                        health.</p>
                    <p>He sifts through it all for you, chooses the most relevant and useful information, and translates
                        it into easy to understand advice you can put into action right away.</p>
                    <p>Because so many people thank Brad for the tips and tricks they get inside the Advocate, he wanted
                        to make sure you get access to the same helpful advice for yourself. Which is why you are
                        getting 3 weeks, usually about 9 Issues of the newsletter, totally free.</p>
                    <p>Remember we have only printed 500 copies of <i>Eat Stop Eat</i>. And <?= $sold_count_up ?> of them have already
                        been claimed at full price on Amazon! To secure this exclusive FREE offer for CLK*books
                        customers only, secure your copy right away with the button below.</p>
                    <br>
                    <p align="center">Remaining Inventory*: <?= $so_count_down ?></p>
                    <p align="center">
                        <a class="button" href="http://147.eatstopeat.pay.clickbank.net/?cbfid=31409&cbskin=18285&vtid=clk31409">
                            Claim Your FREE Copy Now<br>
                            Enter Your Shipping Details
                        </a>
                    </p>
                    <p align="center"><i>*Inventory updates in real time</i></p>

                    <br>
                    <hr>
                    <p>*<a href="http://clkbooks.com/ese/references/" target="_blank">Over 317 Research Papers
                            Supporting the Eat Stop Eat Method</a></p>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>
                        FREE Print Edition
                    </h4>
                    <p><i>(375 Copies Remaining)*</i></p>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v"><strike>&nbsp;$19.99&nbsp;</strike></td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b class="bg-yellow">FREE</b>*</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*We just ask you to cover Shipping &amp; Handling</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://147.eatstopeat.pay.clickbank.net/?cbfid=31409&cbskin=18285&vtid=clk31409"><span
                                    class="inner">Enter Your Shipping Details</span></a>
                    <p class="purchase-hint"><i>*inventory updates in real time</i></p>
                    </p>
                </center>
            </div>


        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark
            of Click Sales Inc., a Delaware corporation located at 1444 S. Entertainment Ave., Suite
            410 Boise, ID 83709, USA and used by permission. ClickBank's role as retailer does not
            constitute an endorsement, approval or review of these products or any claim, statement or
            opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
