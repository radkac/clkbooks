<?php
//LOAD COOKIE SCRIPT
require_once($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Eat Stop Eat</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=3">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
});
</script>


<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h3 class="ta-center">Eat Stop Eat &mdash; Order Step 5</h3>

        <h1>EXPEDITED SHIPPING &mdash; <em>Why Wait?</em></h1>

        <div class="insertion left" style="margin:1em 1.5em 0 0;">
            <center><img class="mobile-half-width" src="web/i/usps.png" width="120" alt=""></center>
        </div>

        <p class="delivery">Our standard delivery time is 2‑3 weeks. Normally warehouse processing can take 1 to 2 weeks so take advantage of our <b>Expedited Shipping</b> service for only $2.97 and your <b>order will be placed at a priority level</b>, which means it will be at the top of the shipping list for the warehouse to ship within 2 to 3 business days!</p>
        <p class="action">
            <u class="cl-blue">So click the blue button below</u> to take advantage of Expedited Shipping and <b>receive your order even faster!</b>
        </p>

        <div class="columns purchased">
            <div class="column cl-1-2">
                <p><big><b>Here's what you've purchased:</b></big></p>
                <ul class="check green">
                    <li>
                        Eat Stop Eat Paperback<br>
                        <p><img src="web/i/EatStopEat-book.jpg" alt=""></p>
                    </li>
                </ul>
            </div>
            <div class="column cl-1-2">
                <center>
                    <a class="button animated" href="http://exs-001.eatstopeat.pay.clickbank.net/?cbur=a">Yes! Move My Shipment to the Front Of The Line for Just $2.97!</a>
                    <br><br>
                    <a class="nothnx animated" href="http://exs-001.eatstopeat.pay.clickbank.net/?cbur=d">No Thanks</a>
                </center>
            </div>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2017 &copy; Clkbooks.com<br>
        For support please contact support [at] clkbooks.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
