<?php
//LOAD COOKIE SCRIPT
require_once($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Save over 50% On The Amazon.com Price For The Audiobook Version / The Kindle Version Of Eat Stop Eat</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css?v=7">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=7">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=7">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=7">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
});
</script>


<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1><span class="cl-red">Save Over 50%</span> On The<br> 
            Amazon.com Price<br> 
            <small>Add The Audiobook And / Or The Kindle<br> 
            Version Of Eat Stop Eat To Your Order<br> 
            <u>On This Page Only</u></small>
        </h1>

        <p>Want to listen to Eat Stop Eat while you’re on the go? Grab the audiobook version and give your eyes a break. Want to make sure you can refer back to something in Eat Stop Eat when you only have your Kindle or your phone on you? Just add the Kindle book to your library and you’ll have access at the swipe of a finger.</p>
        <p>You can grab both on Amazon.com anytime you want but they’ll cost you $15 each. However&hellip;</p>
        <p>On THIS page only you can get either — or both — for as much as <span class="bg-yellow">HALF OFF</span>.</p>
        <p>Remember, once you leave this page you can only get these via Amazon.com for full price. So make sure you take action now!</p>
        <p>Just choose your option below to add it to your order&hellip;</p>

        <div class="container mpurchbox">
            <div class="division dv-1-3 ta-center">
                <h2 class="cl-blue"><br>Kindle<br><br></h2>
                <h2 class="cl-green">$9</h2>
                <div class="insertion center">
                    <center><img class="ecover mobile-half-width hauto" src="web/i/ese-ebook-kindle-new.jpg" alt="">
                </div>
                <p><a href="http://eatstopeat.pay.clickbank.net/?cbur=a&cbitems=162.1"><img class="mobile-3of4-width" src="web/i/button-add-to-cart.png" width="220" alt=""></a></p>
            </div>
            <div class="division dv-1-3 ta-center">
                <h2 class="cl-blue"><br>Audiobook<br><br></h2>
                <h2 class="cl-green">$9</h2>
                <div class="insertion center">
                    <center><img class="ecover mobile-half-width hauto" src="web/i/ese-ebook-audio-new.jpg" alt="">
                </div>
                <p><a href="http://eatstopeat.pay.clickbank.net/?cbur=a&cbitems=163.1"><img class="mobile-3of4-width" src="web/i/button-add-to-cart.png" width="220" alt=""></a></p>
            </div>
            <div class="division dv-1-3 ta-center bundle">
                <h2 class="cl-red">Kindle and<br style="display:block;"> Audiobook<br style="display:block;"> Bundle</h2>
                <h2 class="cl-red">$14</h2>
                <div class="insertion center">
                    <center><img class="ecover mobile-half-width hauto" src="web/i/ese-ebook-bundle-new.jpg" alt="">
                </div>
                <p><a href="http://eatstopeat.pay.clickbank.net/?cbur=a&cbitems=164.1"><img class="mobile-3of4-width" src="web/i/button-add-to-cart.png" width="220" alt=""></a></p>
                <div class="seal"></div>
            </div>
        </div>

        <p class="ta-center"><a class="nothnx animated" href="http://164.eatstopeat.pay.clickbank.net/?cbur=d">No Thanks</a></p>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2017 &copy; Clkbooks.com<br>
        For support please contact support [at] clkbooks.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
