<?php
//LOAD COOKIE SCRIPT
require_once($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Access My Eat Stop Eat Insider Tricks</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

	<script>
	function showHideHelp() {
		e = document.getElementById('help-opts');
		if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
	}

	function resizeVideoPlayer() {
		var new_width = $('#video_player').width();
		var new_height = Math.floor(411*new_width/730);
		$('#video_player').height(new_height);
		//alert(new_width+' '+new_height);
	}

	function showWarning() { $('.warning').fadeTo('slow',1) }
	function showContent() { $('#layout').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
	$(function(){
		// show warning message
		setTimeout('showWarning()', 1500); // 1500
		// show the rest of the content
		setTimeout('showContent()', 3000); // 3000
        // show all hidden items fast in case of "cbur" existence
        <?if(isset($_GET['cbur'])):?>
            $('.hidden').fadeTo('fast',1);
        <?endif;?>
		setTimeout('resizeVideoPlayer()', 3100);
	});

	$(window).resize(function() {
		resizeVideoPlayer();
	});

	</script>


<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="clearfix">
    <div class="inner clearfix">

        <div class="intro">
            <div class="order-steps">
                <p><img class="mobile-full-width" src="/web/i/order-steps-<?=isset($_GET['cbur'])?'stat.png':'anim.gif'?>"></p>
                <h4 class="warning hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</h4>
            </div>
        </div>

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="hidden clearfix">

    <div id="content" class="rich-text clearfix">

        <h1><b>Access My <i>Eat Stop Eat</i> <u class="cl-red">Insider</u> Tricks</b></h1>
        <h2>Reach Your Goal Weight 2X <span class="cl-red">Faster &amp; Easier</span>&hellip;</h2>
        <h2 style="font-size:30px;"><i class="cl-blue">“I <u>Never Intended To Share These</u> Tricks With Anyone&hellip;”</i></h2>

<!--
        <br>
        <div class="video" style="margin:0 auto; width:730px;">
            <iframe id="video_player" src="//player.vimeo.com/video/107802393?autoplay=1&title=0&byline=0" width="730" height="411" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>

        <table class="pricing">
            <tr>
                <td class="reg">
                    Your Regular Price<br>
                    (for Insiders only)<br>
                    <center><strike>&nbsp;$37.95&nbsp;</strike></center>
                </td>
                <td class="inv">
                    Your Exclusive <i>ESE Optimized</i><br>
                    Price On This Page Only<br>
                    <center><em>$7</em></center>
                </td>
            </tr>
        </table>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="http://7.eatstopeat.pay.clickbank.net/?cbur=a">Yes Brad! I want an intimate insider<br> view of your Eat Stop Eat lifestyle.<br> Add ESE Optimized To My Order!<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="http://7.eatstopeat.pay.clickbank.net/?cbur=a">Yes Brad! I want an intimate insider view of your Eat Stop Eat lifestyle.<br> Add ESE Optimized To My Order!</a></span>
            </p>
        </div>
-->

        <div class="quotation">
            Every time I’d post a pic of one of my meals to Twitter I’d be amazed at the flood of responses. Which is what convinced me to offer you this unique extension to your Eat Stop Eat experience&hellip;
        </div>

        <p>Remember I’m a science geek at heart&hellip;</p>
        <p>As you discovered on the previous page I’m not very good at selling myself. Which is why I used to always be shocked at the huge response I’d get from readers and fans when I would post pics of my meals, or videos of my workouts, to Twitter or Facebook&hellip;</p>
        <p>I don’t do that anymore which I’ll explain in a second. Yet the point is this&hellip;</p>
        <p>I learned that giving you an intimate look into exactly how I am implementing Eat Stop Eat into my own life is an incredibly valuable way for you to apply it to your own goals&hellip;</p>
        <p>You see I have used ESE to achieve very different objectives. I have lost weight. I’ve gained muscle. I’ve addressed health issues. Yet almost never all three at once&hellip;</p>
        <p>I’ve also stuck with Eat Stop Eat during periods in my life when I had nothing to do but study and write, and other periods when I was juggling two young kids, multiple businesses, family health tragedies and more&hellip;</p>
        <p>Listen, I’ve experimented with Eat Stop Eat in pretty much any way you can imagine. And I now understand that sharing all of that with you is the fastest way to make sure you find your own perfect path to reach your personal goals as efficiently as possible.</p>
        <p>Unfortunately I just don’t have the time or the desire to share that kind of intimate look into my life anymore via Social Media. Yet I understand how powerful it can be to share it with you. Which is why I want to share something brand new and exciting with you&hellip;</p>
        <p>It’s called <b><i>Eat Stop Eat <u>Optimized</u></i></b>&hellip;</p>

        <h2 class="inverted blue">ESE Optimized</h2>

        <div class="insertion right">
            <img src="web/i/products/ecover-01.jpg" alt="">
        </div>

        <p>Over the last decade I have developed my own unique way of using Eat Stop Eat that gives me the <u>best possible results with the least possible work</u> and hassle.</p>
        <p>In short&hellip; if Eat Stop Eat is the <b><i>facts</i></b> on Intermittent Fasting &mdash; from labs and clinical research &mdash; then Eat Stop Eat <u>Optimized</u> is the <u>very intimate <b>story</b></u> of Intermittent Fasting from my personal experiences.</p>
        <p>Eat Stop Eat is the Science.</p>
        <p>Eat Stop Eat Optimized is the Art.</p>
        <p>This is my own personal 'Optimization' of the principles within Eat Stop Eat. It’s how I have used them for the last ten years and how my approach changed with my goals and my lifestyle over this time.</p>

        <p>You’ll get a peek into&hellip;</p>
        <ul class="check green">
            <li>Exactly how I eat</li>
            <li>Pics of the foods I ate leading up to the photo below</li>
            <li>My exact calories</li>
            <li>The supplements I took to reach each goal</li>
            <li>My mindset tricks for Intermittent Fasting</li>
        </ul>
        <p>And you’ll also discover why&hellip;</p>
        <ul class="check green">
            <li>I don't exercise when I'm fasting</li>
            <li>I NEVER snack on Protein</li>
            <li>Sometimes I cut my fasts in length to optimize the fat loss process</li>
            <li>I never worry about eating lots of carbs or fats</li>
            <li>And much more&hellip;</li>
        </ul>

        <p>And listen&hellip; I admit I’m lazy&hellip; Yet I’m also vain and like to look and feel great&hellip;</p>

        <div class="insertion left">
            <img src="web/i/visual-01.jpg" alt="">
        </div>

        <p>So you’re going to learn how to get the <u><b>best results with the least possible effort</b></u>&hellip;</p>

        <hr>

        <p>Yet that’s not all&hellip;</p>
        <p>I also want to offer you an extra FREE bonus. And it’s one of the most REQUESTED resources that Eat Stop Eat customers have been asking for since the books was first released over 10 years ago&hellip;</p>
        <p>Because it makes your time between Eat Stop Eat protocols practically <b>done-for-you</b>&hellip;</p>
        <p>It’s the exact nutritional strategy to lose weight as quickly, safely and easily as possible if you want to accelerate your Eat Stop Eat results. It’s called&hellip;</p>

        <h2 class="inverted green">The 5-Day Diet <span style="font-weight:normal;letter-spacing:1px;">[Limited Time FREE BONUS]</span></h2>

        <div class="insertion right">
            <img src="web/i/products/ecover-5day.jpg" width="150" alt="">
        </div>

        <p>So many folks ask me for a foolproof formula on exactly how to eat on the days between your Eat Stop Eat protocols.</p>
        <p>And this is it. It’s my EXACT blueprint for losing weight and toning your body in HALF the time&hellip;</p>
        <p>This book is easy to read and the diet is even easier to follow. Heck, even if you just use the 5-Day Diet plan half the time, you’ll still get results twice as fast as all the folks around you that are using regular and boring diet methods!&hellip;</p>

        <hr>

        <p>As you can see the regular member price for Eat Stop Eat <u>Optimized</u> is $39.95.</p>
        <p>However on this page only I’m able to do you one better.</p>
        <p>In fact, because I’ve seen first hand how folks are using ESE Optimized and 5-Day Diet to 2x the speed and ease of their results, I’m willing to let you have BOTH for just a single payment of just <b>$15</b> if you’re willing to commit to living the Eat Stop Eat lifestyle and sharing it with people you care about&hellip;</p>
        <p>That’s less than HALF what you would usually pay for Optimized alone&hellip;</p>
        <p>Just don’t wait because once you leave this page I can’t offer this special deal anywhere else. This is your one chance to grab ESE Optimized at this steep discount.</p>
        <p>And remember you’re also covered by my 100% money back satisfaction guarantee. So you can easily check it out risk free and make sure it’s for you&hellip;</p>
        <p>Just use the button below to secure access now.</p>

        <div class="insertion left">
            <img class="mobile-half-width" src="web/i/brad-photo.jpg" alt="">
        </div>

        <p>
            <br>
            Yours in youthful health and fitness<br>
            <br>
            <img src="web/i/brad-sign.png" alt=""><br>
            <br>
            Brad Pilon
        </p>
        <br class="clearfix">

        <div class="insertion center">
            <center><img src="web/i/products/bundle.jpg" alt=""></center>
        </div>

        <table class="pricing">
            <tr>
                <td class="reg">
                    Your Regular Price<br>
                    (for Insiders only)<br>
                    <center><strike>&nbsp;$39.95&nbsp;</strike></center>
                </td>
                <td class="inv">
                    Your Exclusive ESE Optimized<br>
                    Price On This Page Only<br>
                    <center><em>$15</em></center>
                </td>
            </tr>
        </table>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="http://95.eatstopeat.pay.clickbank.net/?cbur=a">Yes Brad! Add ESE Optimized and The 5-Day<br> Diet To My Order!<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="http://95.eatstopeat.pay.clickbank.net/?cbur=a">Yes Brad! I want an intimate inside view of your Eat Stop Eat lifestyle PLUS a done-for<br> you diet for the rest of the time. Add ESE Optimized and The 5-Day Diet To My Order!</a></span>
            </p>

            <hr>

            <p class="discard">No thanks Brad. I understand that I’ll never see this massive discount again. I’ll take my chances of finding my own path through Eat Stop Eat. Thanks for the offer but I don’t want to see how you have experimented and applied Eat Stop Eat for multiple goals and periods in your life. And I don’t need a done-for-you diet plan for the rest of the time.  I know that once I leave this page I’ll lose out on this deal. I’m ok with that and I’d rather take my chances. <a href="http://95.eatstopeat.pay.clickbank.net/?cbur=d">I'll pass</a>.</p>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
