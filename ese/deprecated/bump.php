﻿<?php require "../checkforCBhopinfo.php"; ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="robots" content="noarchive" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Eat - STOP - Eat</title>

    <!-- Styles -->
    <link rel="stylesheet" href="web/css/foundation.min.css" />
    <link rel="stylesheet" href="web/css/style.css" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,500,800,900|Open+Sans:300italic,400italic,300,400,700,800,900|Open+Sans+Condensed:300,700|Roboto+Condensed:400,700'"rel="stylesheet" type="text/css">

    <!-- jQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- Plugins -->
<script src="web/js/jquery.fullscreen-popup.js"></script>
<!--<link rel="stylesheet" type="text/css" media="all" href="web/css/styles.css">-->

<link rel="stylesheet" type="text/css" media="all" href="web/css/magnific-popup.css">
<script type="text/javascript" src="web/js/jquery.magnific-popup.min.js"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

<script src="web/js/month-days-left.js"></script>

    <!-- Analytics -->



<script src="google2_files/analytics.js" async=""></script>

<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '855724374478558']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=855724374478558&amp;ev=PixelInitialized" /></noscript>

<!-- Google Analytics ESE-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1008025-7', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
<header>
  <div class="row">
    <div class="medium-6 small-text-center medium-text-left columns"><img src="web/img/logo.jpg"></div>
    <div class="medium-6 small-text-center medium-text-right  columns text-right">
    <div class="addthis_sharing_toolbox"></div>
      <!--<ul class="soc">
        <li><a class="soc-twitter" href="#"></a></li>
        <li><a class="soc-facebook" href="#"></a></li>
        <li><a class="soc-google" href="#"></a></li>
        <li><a class="soc-pinterest" href="#"></a></li>
        <li><a class="soc-linkedin" href="#"></a></li>
        <li><a class="soc-rss soc-icon-last" href="#"></a></li>
      </ul>-->
    </div>
  </div>
</header>
<div class="banner">
  <div class="row banner_row">
    <div class="medium-6 right columns">
      <h1 class="whitetext small-text-center medium-text-left">In Just One Day This Simple Strategy Frees You From Complicated Diet Rules - And Eliminates Rebound Weight Gain Permanently</h1>

      <!--      <h1 class="whitetext hmartop30">Do This Once or Twice a Week to Experience The Weight, Health & Energy You Desire and Deserve</h1>-->

      <!--      <h1 class="whitetext hmartop30">Do This Once or Twice a Week to Experience Your Ideal Weight, Health, and Energy</h1>-->
    </div>
    <div class="medium-6 columns text-right"> <img src="web/img/family.png"> </div>
  </div>
</div>
<div class="main_container grn_border" style="margin-top:-20px; position:relative;">
  <div class="row pad20">
    <div class="row minus_margin">
      <div class="column medium-2 small-text-center medium-text-right"><img src="web/img/adam_propic.png"></div>
      <div class="column medium-10 small-text-center medium-text-left blu_i">
        <p><script src="web/js/date.js" type="text/javascript"></script><br>
          By Adam Steer, NSCA-CPT<br>
          Epilogue by Brad Pilon, MSc</p>
      </div>
    </div>
    <div class="columns medium-7">
      <p>If you’re ready to finally lose all the weight you want then you’ll love this story...</p>
      <p>I used to follow the diet gurus like a lost sheep…</p>
      <p>That all ended over a juicy hamburger in 2009 across from a fellow named Brad Pilon. He helped me dig out of a deep pit most chronic “dieters” feel trapped in...</p>
    </div>
    <div class="columns medium-5"> <img src="web/img/hamburger.jpg"> </div>
    <div class="column">
      <p>Maybe you’ve felt this too...</p>
      <p>You start out strong. You’re confident “this time” you’re going to lose the weight and keep it off.</p>
      <p>You pick a “diet” and dig in. At first it seems doable. Then the rules get confusing.</p>
      <p>You get busy. All of a sudden fighting the endless food restrictions, calorie counts, ounces and portions is overwhelming.</p>
      <p>Every meal is a mental battle. What foods to eat? How many calories? It’s exhausting!...</p>
      <p>I would go out with friends. I’d either watch them enjoy their meals while I picked at a salad... Or I’d break down... Eat twice as much as I usually would… And end up feeling terrible about myself...</p>
      <p>Whatever it is, something knocks you off the plan... </p>
      <p>You tell yourself you’ll get back to it tomorrow. But tomorrow never comes. And…</p>
      <p>Before you know it you’ve gained all the weight back — <strong>and more</strong>!...</p>
      <p>When I look back at those times it’s like I was being herded. Like I was a lost sheep blindly following wherever the diet gurus and their fancy plans drove me — even though time and time again I ended up right back where I started. I was trapped in a pit, depressed and a little bit fatter than when I started...</p>
      <p></p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>“How Can Losing Weight Really Be This Complicated?!?”</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column medium-7">
      <p>In every other corner of my life I was in charge. And if you’re like me, you probably wonder how a free-thinking, sophisticated, clever and successful person like <strong>you</strong> can find losing weight so complicated and impossible...</p>
      <p>Which is why I’m writing this page for you. Because I want to share the story of how blind luck lead me to Brad. And how he showed me the one simple weight loss strategy that replaced all the complicated rules and <span class="underline">gave me back control</span>.</p>
      <p>Not only that, using this one strategy is also proven to <span class="highlighter_u">rejuvenate your cells from the inside out</span>. So you enjoy a <span class="underline">more youthful appearance and energy</span> while you brush away aches, pains and health troubles you used to accept as “normal”...</p>
      <p>And I want the same freedom for you. However before you keep reading I should warn you…</p>
      <p>The simple strategy you’re about to discover is not something the “gurus”, the big food companies, or the diet industry want you to know. In fact they’ve done their best to attack it with false labels like “controversial” or “dangerous.”</p>
      <p>Which is why this information has been so hard to find until now. Just keep reading to find out what they don’t want you to know, and why...</p>
    </div>
    <div class="column medium-4 text-center"> <img src="web/img/bussinesswomen.jpg"> </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>Weight Loss 101 <br>
        <span style="font-size:24px">(or Why Your Diets Have Failed)</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column medium-5 text-center"><img src="web/img/empty_full.jpg"></div>
    <div class="column medium-7">
      <p>Did you know less that 2% of &ldquo;dieters&rdquo; manage to keep off the weight?...</p>
      <p>No wonder we all lose hope that we'll ever get the body we strive for.</p>
      <p>What I learned though — and what I'll share with you today — is that the endless cycle of rebound weight gain was never my fault. Just like it's not your fault. You see…</p>
      <p>Diets don't work… for one very simple reason...</p>
      <p>When you are told to skip entire food groups. When you are forced to limit the amount of calories you eat. Your only option is to use <span class="underline">discipline</span> to stick to the plan.</p>
    </div>
    <div class="column">
      <p>Yet behavioural science clearly proves you only have a small supply of discipline. And when you run out… well, you know what happens right?...</p>
      <p>And the reason you need discipline to follow a diet is because diets are NOT NATURAL.</p>
      <p>When you follow a diet day in and day out you are fighting against everything your body thinks is best for it&rsquo;s own survival.</p>
      <p>Which is why you end up sick, suffering from accelerated aging, and fighting once again with rebound weight gain...</p>
      <p>Yet some diets do get ONE thing right. It's just not what you think...</p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>Calories = The Biggest Lie In Weight Loss</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>In the last few years a lot of diet gurus have tried to argue that calories don’t count… And to be honest I understand why. It’s what we all want to hear!...</p>
      <p>But to be blunt — It’s also the biggest lie in weight loss...</p>
      <p>Ignore for a moment all the fancy weight loss theories you’ve heard. Some of them may be true. Some completely bogus. And others so weak they don’t make a difference in your real results.</p>
      <p>However there is one undeniable truth about weight loss...</p>
      <p>This one thing may not be popular. But it MUST happen, no matter what the diet gurus pretend. Here it is…</p>
      <p><strong>To lose weight you must eat below a certain <span class="highlighter_u">threshold</span> of calories.</strong></p>
      <p>Now, stick with me for a second, ok? I know you’re probably thinking…</p>
      <p>“How is this any different from what I’ve already heard?...”</p>
      <p>You’ll see in a moment why this is totally different. First...</p>
      <div class="grybox">
        <p>Let’s define a calorie...</p>
        <p>Basically it’s a way of talking about the “energy” stored in the food you eat. It’s also used to talk about the “energy” your body burns to live — and to do whatever activities you do throughout your day.</p>
      </div>
      <p>Now I know this probably isn’t going to be “popular” advice. However...</p>
      <p>Even though <span class="underline"><strong>you NEVER have to “count” a single calorie</strong></span>…  I just don’t have a “magic formula” that allows you to eat as many calories as you want and still lose weight. And…</p>
      <p>If someone claims you can do that… run as fast as you can in the opposite direction!</p>
      <p>And second… I said you need to eat below a certain<strong class="underline"> threshold </strong>of food. </p>
      <p>The trick is knowing what your level of calories is and getting below it. However...</p>
      <p><strong class="underline">You don’t have to do it every day</strong>. There’s a simple strategy that allows you to stay below your personal threshold WITHOUT daily discipline, precise calorie counting, or giving up any of your favorite foods.</p>
      <p>And using this simple strategy is what helps clean out your body — even at the deepest level of your cells — so that you can actually turn back the clock, look younger and feel more energy than folks half your age…<br>
      </p>
    </div>
    <div class="row">
      <div class="columns pad0 medium-12 text-center"><img src="web/img/75yrsold_body.jpg" class="bigimg"></div>
      <div class="columns text-center">
        <h2 class="hmartop30" style="font-size:22px;">"Both These Bodies Are 75 Years Old - The Choice is Yours..."</h2>
        <p>We’ll get into exactly how to do it in a minute. First, we need to uncover...</p>
      </div>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>The Secret Kept By Your Great, Great Grandparents That Unlocks Your Lean & Youthful Body</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column medium-7">
      <p>Chances are your Great Grandparents, and certainly your Great Great Grandparents happily ate fewer calories than you do.</p>
      <p>However I’d be willing to bet they didn’t discipline themselves or give up specific foods. In fact...</p>
      <p>If you could hop in a time machine and go back thousands of years to visit your ancestors, you’d see that for most of human history there was a very natural <span class="underline"><strong>cycle of consumption</strong></span>. This kept our bodies lean, healthy and in balance.</p>
      <p>You see, calories came in waves. Sometimes high and abundant. Sometimes low and hard to find. </p>
      <p>And even today human communities in remote places still follow this <span class="underline">natural cycle of high and low calories</span>. And researchers who have visited these communities discover people who are lean, strong, youthful and shockingly healthy compared to the constantly overfed people in the modern world.</p>
      <p>Listen: your biology is pretty much identical to your ancestors...<br>
      </p>
    </div>
    <div class="column medium-5 text-center"><img src="web/img/yinyang_food.jpg"></div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>The Genetic Code That Unlocks Permanent Weight Loss</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column medium-5 text-center"><img src="web/img/football_ground.jpg"></div>
    <div class="column medium-7">
      <p>Picture a football field…</p>
      <p>Imagine it’s 3rd Down and the ball is less than an inch from the goal line…</p>
      <p>Now imagine the entire length of the field as the 300,000 years of human genetic evolution. Well…</p>
      <p>The inch that’s left between the ball and the goal line would be like the time since humans discovered agriculture. And…</p>
      <p><u>One blade of grass</u> right before the goal line would be like the time since we started making modern industrial food.</p>
    </div>
    <div class="column">
      <p>So you can see that your genes haven’t had time to adapt to the embarrassing over-abundance of food that you have today.</p>
      <p>Your body still <strong>craves</strong> the <span class="underline">natural waves of higher and lower calories</span> that it evolved to expect.</p>
      <p>And we already saw that discipline is not the answer. And that it goes against every survival instincts your body is wired for. After all…</p>
      <p>Your ancestors survived by <strong>eating their fill</strong> when food was plenty so they could cruise through times when it was harder to find. And that’s why you struggle now…</p>
      <p><strong>You were never meant to avoid certain types of foods.</strong> You are not made to endure daily discipline and deprivation. <strong>And you certainly weren’t meant to spend every day counting calories</strong>. Heck, we didn’t even discover the calorie until the early 1800s!</p>
    </div>
    <div class="columns medium-6">
      <p>However your genetic code <strong>IS</strong> hardwired to go without food for short periods between times of plenty. </p>
      <p>And that’s what’s missing in the modern diet. These days it’s like you eat “<em>one long meal per day</em>” — it starts at breakfast and doesn’t end until you go to bed...</p>
      <p>That’s why we’re sick. </p>
      <p>That’s why we’re fat.</p>
      <p>That’s why losing weight seems so complicated.</p>
      <p>That’s why you end up gaining it all back.</p>
      <p>That’s why you need to shift from “Food Obsession” to “Food Freedom”... and give your body the break it deserves...</p>
    </div>
    <div class="columns medium-6"> <img src="web/img/onelongmeal.jpg"> </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>Stop Obsessing About Food <br>
        <span style="font-size:24px">(if you want to finally keep the weight off)</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>When you don’t eat you get hungry, right?...</p>
      <p>And when you’re trying to follow a diet, <span class="highlighter">doesn’t it feel like you’re hungry all the time</span>?</p>
      <p>It’s like you’re running a marathon and the finish line keeps getting pushed away. There’s never a day or a time when you are allowed to just NOT be hungry. However...</p>
      <p><span class="underline">Imagine being hungry only once or twice a week</span> for a specific amount of time…</p>
      <p>And the rest of the time you’re full and satisfied… Never even thinking about your next meal.</p>
      <p>Wouldn’t that be<strong> freeing</strong>?</p>
      <p>When I started using the method I’m about to show you it was like a weight being lifted.</p>
      <p>And the crazy thing is… I discovered something about hunger too.
      <p> </p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>Hunger Loses All Its Power!</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="columns medium-6">
      <p>All of a sudden hunger did not control me. I wasn’t scared of it anymore...</p>
      <p>Because just a little bit of hunger… the tiniest amount of self control… opened up a world of <strong>freedom</strong> for me.</p>
      <p>The method I’ve been talking about is a specific way to do something nutritionists call Intermittent Fasting (IF). However my way of doing it is very different to what you may have heard about. So keep reading...</p>
      <p>Because this is the system that <span class="underline">freed me from the endless cycle of rebound weight gain</span>. It allowed me to ditch all the crazy diet rules. <span class="underline">And now I finally enjoy a stable weight and a lean body I’m proud of</span>.</p>
      <p>And it will do the same for you too, like it has for over 54,000 other folks who are using the simple nutritional shifts I learned from a guy named Brad Pilon </p>
    </div>
    <div class="columns medium-6"><img src="web/img/adam_before_after.jpg"></div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2> <span class="rwd-line">This Is The Key To <span class="underline">Rewarding</span> Yourself </span> <span class="rwd-line"> Without <span class="underline">Obsessive</span> “Food-Guilt”</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>I get a lot of strange looks when I first talk to folks about Intermittent Fasting…</p>
      <p>It’s something they find either confusing, extreme or scary. And they should because you CAN do it the wrong way. So…</p>
      <p>Let’s start with a no-nonsense explanation of how to do it right…</p>
      <p>I follow a simple method of Intermittent Fasting called Eat Stop Eat — developed by international author and nutrition expert Brad Pilon who you’ll meet in a minute. Here’s how it works...</p>
      <p>Once or twice a week I stop eating for a specific pre-set amount of time depending on my goal and my amount of bodyfat. Simple as that…  and it's only EVER once or twice a week, never more!</p>
      <p>And get this… you <span class="underline"><strong>never go a single day without enjoying a completely normal meal</strong></span>. Now...</p>
    </div>
    <div class="columns medium-6"><img src="web/img/rich_meal.jpg"></div>
    <div class="columns medium-6">
      <p>Think back to your ancestors and their <span class="underline">natural cycles of high and low calories</span>. Really that’s all you’re doing when you practice Eat Stop Eat.</p>
      <p>Listen: if you’re like most folks new to this simple yet powerful weight loss and health enhancing strategy, you may think these brief periods without eating could feel limiting. Yet it’s quite the opposite.</p>
      <p>Remember: when you’re on a diet you feel hungry all the time, right?</p>
      <p>Yet when you’re following the Eat Stop Eat method you’re hungry once or twice a week <span class="underline">at most</span>. </p>
    </div>
    <div class="column">
      <p>That’s it!...</p>
      <p>And guess what… the hunger never gets any worse than your normal hunger between meals that you CONSTANTLY suffer from when you’re on a diet.</p>
      <p><strong>Wait! What?</strong></p>
      <p>“Instead of being hungry ALL THE TIME — like when you’re on a diet — you can limit your hunger to only one or two days a week?...”</p>
      <p><strong>Yes! </strong>And you won’t be any more or less hungry than on a diet. But you’ll be <span class="underline">FREE from hunger</span> the rest of the week. And you’ll be free to eat whatever you want.</p>
      <p>FREEDOM is the reward you get from Eat Stop Eat.</p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row greenbg">
    <div class="column">
      <h2>If You Can Stay Busy For 7 to 8 Hours, Then You Can Lose All The Weight You Want</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="columns medium-8 ">
      <p>Here’s what changed my life and my relationship to food...</p>
      <p class="highlighter">Simply extending your normal overnight fast once or twice a week <span class="underline">eliminates the need to diet</span>. And it allows you to lose all the weight you want while you improve the markers associated with good health and longevity.</p>
      <p>Now if you’re as busy as I am, those hours will fly by and you’ll barely notice them. In fact most folks report feeling more clear headed, alert and productive on those one to two days per week when they simply hold off on their first meal!...<br>
      </p>
    </div>
      <div class="columns medium-4 text-center"><img src="web/img/stopwatch.jpg"></div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>Remember You Don’t JUST Lose Weight <br>
        <span style="font-size:24px"> (Over 317 peer-reviewed studies prove the surprising health benefits)</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>Listen, I first started using Eat Stop Eat because I wanted a permanent solution to controlling my weight. You see…</p>
      <p>Even though I’m a fitness professional, I used to constantly fluctuate between looking great and hiding my body in embarrassment. Eat Stop Eat gave me freedom from that struggle. Yet I soon discovered that I was doing a lot more good for my body than simply weight control…</p>
      <p>The author of Eat Stop Eat — Brad Pilon — has studied over 317 peer reviewed scientific research papers. He continues to devour every new piece of scientific evidence linked to Intermittent Fasting. And…</p>
      <p>The benefits of the simple strategy he created are shocking. Here’s what you can expect...</p>
    </div>
    <div class="column">
      <h2><strong>“Give Me 24 Hours and I’ll Give You…”</strong></h2>
      <ul class="tick_list">
        <li>An Increase In Your #1 <strong>Fat Burning Hormone</strong> By 700%</li>
        <li>Control of Your “Hunger Hormone” and an End to Cravings</li>
        <li>A Decrease In Your Stress Hormone So You <strong>Burn More Belly Fat</strong></li>
        <li>Increase in Your Brain Function For Better <strong>Memory</strong> and <strong>Concentration</strong></li>
        <li>And a Boost In Your <strong>Metabolism</strong> &amp; <strong>Energy</strong></li>
        <li><strong>Reduced Risk of Diabetes</strong> &amp; an Easing of Symptoms</li>
        <li>Increased <strong>Testosterone</strong> If You’re a Man</li>
        <li>Increased Insulin Sensitivity So You Can <strong>Eat More</strong> &amp; <strong>Stay Slim</strong></li>
        <li>Faster <strong>Weight Loss</strong></li>
        <li>Decreased Inflammation So Your <strong>Joints Heal</strong> &amp; <strong>Feel Better</strong></li>
        <li>Rapid <strong>Cleansing</strong> &amp; <strong>Renewal</strong> of Your Body At a Cellular Level</li>
        <li>And much more...</li>
      </ul>
      <p>Brad’s Eat Stop Eat uses no-nonsense language. It explains the science of exactly WHY the Intermittent Fasting — done right — delivers such powerful and lasting benefits…</p>
      <p>In fact, after reading Eat Stop Eat I knew more about nutrition and how it affects my body than 99% of the professional nutritionists I work with in the industry...</p>
      <p></p>
    </div>
  </div>
  <div class="" id="popup" style="display: none; max-width:900px; width:100%;"> <a href="#_" class="lightbox" id="img1"> <img src="web/img/bookindex1.jpg"> </a> </div>
  <div class="row">
    <div class="columns pad0 medium-6 medium-centered">
      <div class="text-center">
        <h2>Take a Second Now To Review The BIG Health Benefits Covered In Brad’s Eat Stop Eat</h2>
      </div>
      <a class="open-popup" href="#popup" href="#img1"> <img src="web/img/bookindex1.jpg"> </a> </div>
    <div class="columns pad0 medium-3 hidden-for-medium-down"> <img src="web/img/enlarge_text.png" style="margin-top:-300px;"> </div>
  </div>
</div>
<div class="main_container">
  <div class="row bluebg">
    <div class="column">
      <h2>The Pioneer Who Defied The Diet Industry & Revealed The Health Healing and Weight Loss Power of “I.F.”</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column medium-8">
      <p>Meet Brad Pilon...</p>
      <p>In the Summer of 2009 I joined Brad for lunch at a little burger joint in his hometown of Guelph, Canada. I’d only just started using his <em>Eat Stop Eat</em> method and I was excited to discuss my new passion with the author of the book.</p>
      <p>He didn’t disappoint. Brad talks about Eat Stop Eat with all the passion of an artist. Yet he backs it up with a depth of knowledge I’ve rarely seen in any fitness professional. And I’ve met my fair share!...</p>
      <p>Brad actually stumbled on the Eat Stop Eat protocol in a strange way. You see…</p>
      <p>In his post-graduate studies he set out to prove Intermittent Fasting was wrong.</p>
      <p>Yet what he discovered shocked him and forced him to write his thesis in defence of a specific type of Intermittent Fasting. And it’s his post-grad thesis that became the foundation for Eat Stop Eat.<br>
      </p>
    </div>
    <div class="column medium-4"> <img src="web/img/brad_pilon.jpg"> </div>
    <div class="column">
      <p>Brad went on to head up the R&amp;D department for one of the biggest supplement companies in the world. He describes his time there as being like a kid in a candy store…</p>
      <p>He had access to all the latest gadgets and scientific labs a nutrition geek could ever hope for. And he was able to poke, prod and measure countless test subjects — including fitness models and athletes. And…</p>
      <p>Although it’s the complete opposite of what the supplement company was trying to sell… Brad continually saw that everything pointed back to what he’d discovered during his post-grad research — <span class="underline">Intermittent Fasting when done according to the findings of his thesis is the fastest way to lose weight</span>. It’s the most consistent way to control it for life. And the most natural way to help your body achieve optimal health...<br>
      </p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2><strong>Introducing The Eat Stop Eat Method: </strong><br>
        Now Science, Evolution and Tradition All Agree On The Simplest, Most Efficient and Permanent Way To Lose Weight</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column medium-5"><img src="web/img/ecover.jpg"></div>
    <div class="column medium-7">
      <p>There’s a strange contradiction in the diet world. Everyone seems to accept it without question…</p>
      <p>Think about it… Almost every diet, weight loss pill, supplement or program is encouraging you to consume MORE of something.</p>
      <p>Weird right? </p>
      <p class="underline">No one is talking about<strong class="highlighter"> how to make it easier</strong> to eat less.</p>
      <p>They try and convince you that eating less is hard, scary, bad for you, and that it’s just not appropriate… and they leave out the fact that it’s the <span class="underline">only sustainable solution</span> to permanent weight control.</p>
      <p>Now, Brad’s permanent solution to weight loss is not new. As far back as ancient Greece the famous philosophers of the day were promoting the same principles found in Eat Stop Eat.</p>
    </div>
    <div class="column hmartop20">
      <p>And as we’ve seen… it’s really just a way for you to mimic the way your ancestors ate. It’s what kept them lean and healthy. And it’s what your body still craves today...</p>
      <p>So it may seem strange that “science” has taken so long to catch up. Well…</p>
      <p>That may not be true. You see the scientific research has been there for years. And it continues to build up...</p>
      <p>However studies that tell you to “consume” less don’t get the same backing and marketing dollars as studies that hint you can buy a pill, eat a certain food, or purchase a certain supplement and lose weight…</p>
      <p>It took a pioneer like Brad to gather all the research. He’s had to fight every step of the way to get the word out. </p>
      <p>He battled against the status quo — with people trying to claim silly things like fasting would slow your metabolism. Or you’d lose muscle. Or that it wouldn’t work for women. Yet the evidence is overwhelming...</p>
      <p>Using Brad’s specific method of IF actually improves your metabolism, preserves your lean muscle tone, and can be used by ANYONE to improve organ health, reverse signs of aging and achieve your lifelong ideal weight.<br>
      </p>
    </div>
  </div>
</div>
<div class="row text-center redbg">
  <h1><strong>Here’s a quick tour of what we now scientifically KNOW is happening in your body each time you practice Brad’s Eat Stop Eat protocol...</strong></h1>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>How Eat Stop Eat Boosts Your <span class="underline">#1 Fat Burning Hormone</span> 15x</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>What if there was a near &quot;magical&quot; fat burning hormone?...</p>
      <p>It may sound too good to be true… but we know that Human Growth Hormone (GH) is such a powerful fat burner that models and athletes actually inject themselves with it...</p>
      <p>It <span class="underline">acts directly on your fat cells</span>, telling them to release stored fat for energy. </p>
      <p>Unfortunately your GH can plummet as you get older <span class="underline">if you don’t take measures to prevent it</span>.</p>
      <p>However you don’t need expensive injections to enjoy the fat burning effects of GH at any age. In fact...</p>
      <p>Research clearly shows Brad’s Eat Stop Eat protocol increases your body’s own GH production by 15x... (Some studies have even shown a boost as high as <span class="underline">2000%</span>!)</p>
      <div class="text-center hmarbot30"><img src="web/img/hgh_graphs.jpg"> <img src="web/img/growth_hormone.jpg"> </div>
      <p>Which is what makes Eat Stop Eat work so quickly compared to traditional "dieting."</p>
      <p>In fact Brad receives hundreds of notes like this one from his customer Scott Smith:</p>
      <div class="row testimonials">
        <div class="columns pad0 medium-4"><img src="web/img/scott_smith.jpg"></div>
        <div class="columns medium-8 txt">
          <h2 class="">“I purchased Eat Stop Eat last April and I saw incredible results and I was close to my goal of a 6 pack by mid May.”<strong> — Scott Smith</strong></h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>Eat Stop Eat Is The Original <span class="underline">Cleanse & Detox Healing Protocol</span> <br>
        <span style="font-size:24px">(Or Why Detox Potions Are a Rip Off…)</span> </h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>Your body is toxic. There’s no getting around it.</p>
      <p>You’re exposed each and every day to more than 70,000 industrial chemicals created in the past 100 years.</p>
      <p>No matter how “clean” you try to live you can’t avoid toxic exposure. Most of it comes from your food. Plus there’s the toxins you breathe in and that touch your skin…</p>
      <p>Together all these chemicals make up what’s called your “toxic load.”</p>
      <p>So you can see why expensive detox potions and formulas have become all the rage. We need to do something to unclog our cells of this toxic goop right? </p>
      </div>
      <div class="columns medium-7">
      <p>I mean… you may not even realize it anymore — it’s been going on so long — yet these toxins are making you feel sick, ruining your sleep, draining your energy and…</p>
      <p>They even increase your weight gain because they clog up your mitochondria — the energy generators that burn calories deep within every cell of your body. So...</p>
      </div>
      <div class="columns medium-5"><img src="web/img/water_splash.jpg"></div>
      <div class="column">
                  <p>Of course we want to clean up the mess. Except the folks selling the detox potions don’t tell you...</p>
            <p>Your body <span class="underline">already has everything it needs</span> to completely detoxify and cleanse without ANY outside help from fancy herbal formulas, superfood smoothies or cleansing flushes...</p>
      <p>In fact your body has a specific process called <strong><em>Autophagy</em></strong>. And it is responsible for repairing damaged and defective organelles, cell membranes and cellular proteins... </p>
      <p>Basically it’s your body’s internal <em><strong>‘cleanup crew</strong></em>’ — it identifies and discards damaged or malfunctioning parts. However...</p>
      <p>Constant eating — which is how mainstream dieting recommends you eat — shuts down Autophagy. Especially meals that are high in sugar and protein. The good news is…</p>
      <p>The research clearly shows that practicing <span class="underline">Eat Stop Eat kicks Autophagy into overdrive</span> so you naturally turn on your cleansing and detoxifying pathways.</p>
      <p>And this is the #1 factor in generating a healthy and happy body now and in your future.</p>
      <p>Listen: Brad’s not a doctor and neither am I. You need a doc you can trust. And you need to listen to your health care pros. However you can’t deny the potential of Eat Stop Eat to clear out nagging health issues and possibly prevent future problems…</p>
      <p>Check out what popular blogger Richard Nikoley experienced:</p>
      <p></p>
      <div class="row testimonials">
        <div class="columns pad0 medium-4"><img src="web/img/Richard.jpg"></div>
        <div class="columns medium-8 txt">
          <h2 class="">“I’ve <span class="underline">gotten off a couple of medications</span>, my <span class="underline">BP has come way down</span>, and most particularly, the Eat Stop Eat has radically altered my appetite... I don’t seem to crave food when I’m not particularly hungry. And, a bit of hunger has actually become a pleasant feeling rather than a gnawing feeling.”<strong> — Richard Nikoley</strong></h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>The Shocking Truth About Eat Stop Eat and Your <span class="underline">Muscle Quality</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>The fear-mongers try to scare you away from Eat Stop Eat with the silly idea that you’ll somehow lose muscle mass and quality. However that could not be further from the truth. In fact…</p>
      <p>As you’ll see in a minute, not only does Eat Stop Eat protect muscle mass when you do it properly… there’s good evidence that it may actually help you build NEW muscle more quickly if that’s your goal. You see...</p>
      <p>Your body goes through a very specific process to build muscle. Bear with me because this gets a bit geeky…</p>
      <ol class="orderlist">
        <li>You create local inflammation with exercise</li>
        <li>Something called satellite cells react to the inflammation</li>
        <li>The satellite cells seep into the damaged muscle cells</li>
        <li>Amino acids (protein) gets pulled into the muscle cells</li>
        <li>Muscle repair and growth occurs</li>
      </ol>
      <p>Now for all this to happen you need to release a precise cocktail of hormones and other biochemical signals. Except…</p>
      <p>It’s easy for your body to override this entire muscle growth process. Things like systemic inflammation (throughout your whole body) and hormonal imbalance can make it impossible to grow muscle…</p>
      <p>Now here’s the really cool part… Eat Stop Eat can reset the growth process in your body and eliminate the factors that are messing up the conditions you need to grow muscle. Here’s how:<br>
      </p>
      <ol class="orderlist">
        <li>Eat Stop Eat lowers chronic inflammation</li>
        <li>It encourages cell health and detox to prepare for growth</li>
        <li>It enhances the growth effects of insulin on muscle cells</li>
        <li>Eat Stop Eat preserves your testosterone levels</li>
        <li>It massively increases your Growth Hormone</li>
        <li>And that’s just the most obvious effects...</li>
      </ol>
      <p>Listen, Brad used to be a die-hard believer in the “eat-all-the-time” approach to building muscle. Yet his story proves quite the opposite...</p>
      <div class="row testimonials">
        <div class="columns medium-6">
          <p><strong>More Muscle. Less Hassle</strong></p>
          <p>To the right are 3 pics of Brad. The first was taken after 5 months of dieting for his first bodybuilding show. He planned his meals, ate well over 200 grams of protein a day, and did cardio in the morning, weights at lunch, then cardio again at night.</p>
          <p>The second picture was taken three years later — after three years of practicing Eat Stop Eat. No more suffering and sacrifice. Eating whatever he wanted most days. And working out just 3 to 4 times per week.</p>
          <p>The third picture is 8 years later — March of 2014 — after 5 more years of Eat Stop Eat. By now you can see Brad still maintains impressive muscle and is leaner than ever...<br>
          </p>
        </div>
        <div class="columns medium-6"> <img src="web/img/brad_8yrslater.jpg" class="hmartop30"> </div>
      </div>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>How Eat Stop Eat <span class="underline">Boosts Your Metabolism</span> So You Can Eat More and Still Lose Weight</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>By the time you finish your first Eat Stop Eat protocol — as soon as tomorrow — you’re going to FEEL the difference in the way your clothes fit. You will SEE that your face looks leaner. And you’ll even notice that you feel lighter and more energetic…</p>
      <p>You, your friends and your family will notice. Yet here’s what’s actually going on inside your body…</p>
      <div class="row peach_bx">
        <div class="columns medium-7">
          <h2>Fat Burning Bio-Factor #1: Insulin Sensitivity</h2>
          <p>Your body can’t burn fat when your insulin is high. It’s a hormone that signals your fat cells to store more fat. And it locks existing fat in your cells.</p>
          <p><span class="underline">During the Eat Stop Eat protocol your Insulin Levels will drop to one third of regular levels</span> allowing you to burn fat at an extreme pace.</p>
          <p>And practicing one to two ESE protocols per week will increase something called Insulin Sensitivity — allowing you to store more calories in lean tissue and less in fat, even when your insulin levels are higher.</p>
        </div>
        <div class="columns medium-5"> <img src="web/img/Insulin_sensitivity.jpg" class="hmartop20"> </div>
      </div>
      <div class="row yellow_bx">
        <div class="columns medium-7">
          <h2>Fat Burning Bio-Factor #2: Fat Oxidation</h2>
          <p>If you’ve ever fanned the flames of a fire you know that air makes it burn hotter. So Oxidation is the fancy word for actually burning up your fat for fuel.</p>
          <p>In the minute before you start your first Eat Stop Eat protocol you’ll be burning fat at about 0.5 umol/kgFFM/min.</p>
          <p>By the end you’ll be operating at a whopping 4.0 umol/kgFFM/min. <strong>That’s an 800% increase</strong>!</p>
        </div>
        <div class="columns medium-5"> <img src="web/img/Fat_oxidation.jpg" class="hmartop20"> </div>
      </div>
      <div class="row aqua_bx">
        <div class="columns medium-7">
          <h2>Fat Burning Bio-Factor #3: Metabolic Rate</h2>
          <p>How would you like to burn an extra 8840 calories per year — no strings attached?</p>
          <p>That’s what would happen if you practice Eat Stop Eat twice per week for 1 year. And that’s simply from the boost in metabolism you get from the protocol.</p>
          <p>To put that in perspective — on top of your regular Eat Stop Eat fat burning you’ll lose an extra 3 lbs without any extra effort. OR…</p>
          <p>You could eat an extra 55 chocolate chip cookies instead with ZERO weight gain.</p>
        </div>
        <div class="columns medium-5"> <img src="web/img/Metabolic_rate.jpg" class="hmartop20"> </div>
      </div>
      <div class="row testimonials">
        <div class="columns pad0 medium-5"><img src="web/img/robertra.jpg"></div>
        <div class="columns medium-7 txt">
          <p>Roberta hit rock bottom at 48 years old. She weighed 171 lbs at 5’1”. She suffered from back pain, thyroid problems and adrenal issues. Yet as you can see it’s all behind her now. And she feels and looks like a woman half her age!...</p>
          <p>The Eat Stop Eat protocol gave Roberta the freedom she needed. It allowed her body to naturally find its own weight loss rhythm. And the results are stunning…</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>How Eat Stop Eat Reverses The #1 Cause Of Accelerated Aging</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>If you think you look older than you should… then you probably suffer from something called chronic inflammation…</p>
      <p>The research now shows that inflammation plays a critical role in determining your biological age. And we know <span class="underline">it is triggered from overeating and being overweight. </span></p>
      <p>Chronic inflammation is also linked to most of the diseases associated with aging including: arthritis, hypertension, atherosclerosis, fatty liver, asthma, cardiovascular disease, diabetes and many more... </p>
      <p>Yes. Inflammation is making you old before your time.</p>
      <p>Don’t worry though. Here’s the good news...</p>
      <p>Practicing Eat Stop Eat has been shown to cut markers of chronic inflammation in half.<br>
      </p>
      <div class="row smtxt_block valign_middle">
        <div class="columns pad0 medium-5"><img src="web/img/inflammation_graph.jpg"></div>
        <div class="columns medium-7 txt">
          <h2 class="hmartop30 text-center">“Practicing Eat Stop Eat has been
            shown to cut markers of chronic inflammation in half.”</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2><span class="rwd-line">What Happens In Your Body During 24 Hours and </span> <span class="rwd-line">7 Days Using Eat Stop Eat</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>Remember how your ancestors and even your Great, Great Grandparents naturally experienced <span class="underline">cycles of consumption</span> that kept their overall calories under a certain <span class="underline">threshold</span>.</p>
      <p>As a result their bodies stayed lean. And they avoided the accelerated aging and organ damage suffered by folks following the Standard American Diet (SAD).</p>
      <p>Yet it happened in a way that didn’t cause the mental stress, cravings and inevitable bingeing caused by modern diets.</p>
      <p>You see, everyone has a line in the sand — a certain calorie threshold. Above that line you gain weight. <span class="underline">Below that line you lose weight</span>.</p>
      <div class="row smtxt_block valign_middle">

        <div class="columns medium-7 txt">
          <p class="hmartop30">“Everyone has a line in the sand — a certain calorie threshold. Above that line you gain weight. <span class="underline">Below that line you lose weight</span>.</p>
          <p>Eat Stop Eat is the only weight loss protocol that allows you to stay below your line in the sand WITHOUT the stress and anxiety caused by diets, food restriction and calorie counting.”</p>
        </div>
                <div class="columns pad0 medium-5"><img src="web/img/energy_expenditure.jpg"></div>
      </div>
      <p>Your line is determined mostly by your height.</p>
      <p>Yes it’s true… tall people get to eat more. At 5’6” that’s a sad fact I’ve had to accept...</p>
      <p>Which means for me… if I want to stay below my threshold I’d have to eat like a bird <span class="underline">if I followed a typical diet
        every day</span>.</p>
      <p>Yet with Eat Stop Eat most days you can <strong>eat MORE calories than someone else your height</strong>, and still <span class="underline">stay below the line of your weekly weight loss calories</span>.</p>
      <p>That’s the magic of Eat Stop Eat. That’s why you never have to count another calorie. And it’s something you can very quickly test out for yourself...</p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row greenbg">
    <div class="column text-center">
      <h2><span class="rwd-line">You’ll Know By Tomorrow Night That</span> <span class="rwd-line"> Eat Stop Eat Is Working For You</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>I want to prove to you how quickly you’ll experience noticeable results from Eat Stop Eat. Give this a try...</p>
      <p>Simply eat your normal evening meal tonight…</p>
      <p>Then right after dinner start applying the simple instructions you’ll find in Eat Stop Eat on page 116. Then…</p>
      <p>Tomorrow night after your evening meal I want you to answer these questions…</p>

      <ol class="orderlist">
          <li>Do you feel lighter and have more bounce in your step?</li>
          <li>Does your digestive system somehow feel cleaner and more healthy?</li>
          <li>Do you feel more clear headed and alert than usual?</li>
      </ol>

      <p>Then — the very next morning — I want you to weigh yourself. Do you weigh 1-3 lbs lighter?</p>
      <p>Wait one more week and weigh yourself on the same scale, at the same time of day. Do you see that your weight has dropped another 3-5 lbs?</p>
      <p>By now you should also be feeling that each time you use the Eat Stop Eat protocol it gets easier and you feel less and less hungry. In fact hunger will practically lose it’s grip on you entirely, while cravings all but disappear.</p>
      <p>And finally — 30 days from now — put on the same clothes you’re wearing today and step in front of the mirror. Do you feel that your clothes are hanging off you? Do you see that your face looks lean and even seems more youthful and brighter?</p>
      <p>And do you notice how much more energy you have throughout your entire day? And that you no longer suffer from the “brain fog” you didn’t even realize was holding you down?</p>
      <p>Are you excited that there are no forbidden foods. That you can reward yourself with your favorites. And that social eating is a celebration again instead of a burden?</p>
      <p>If you answer<strong> yes </strong>to most of these questions, then Eat Stop Eat is working for you. And you can look forward to a long love affair with this simple yet elegant strategy that is already helping over 54,000 people around the world to live a life free from weight worries and frustrating diets.<br>
      </p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column text-center">
      <h2><span class="rwd-line">This works for anyone, man or woman,</span><span class="rwd-line"> no matter how young or how old you are</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>I’ve heard a lot of excuses from folks before they tried Eat Stop Eat. Yet once you try it they melt away just as quickly as your extra body fat. Here are some of the biggest excuses I’ve heard… and why they just aren’t true...</p>
      <ul class="but_i_list">
        <li class="row">
          <div class="columns medium-2"><img src="web/img/sandwich.jpg"></div>
          <div class="columns medium-10">
            <h2>“But I like food too much...”</h2>
            <p>Perfect… Eat Stop Eat is the only system that will work for you. Because you get to enjoy all your favorite foods while you limit your hunger to only one or two brief periods a week — never more.</p>
          </div>
        </li>
        <li class="row">
          <div class="columns medium-2"><img src="web/img/japan.jpg"></div>
          <div class="columns medium-10">
            <h2>“But I’m too fat and sick…”</h2>
            <p>By now you’ve seen that Eat Stop Eat is the perfect solution to reset your health while you lose weight. Over 317 peer-reviewed studies prove it is safe, effective and fast.</p>
          </div>
        </li>
        <li class="row">
          <div class="columns medium-2"><img src="web/img/aged.jpg"></div>
          <div class="columns medium-10">
            <h2>“But I’m too old…”</h2>
            <p>Not for long… as you’re beginning to see Eat Stop Eat actually turns back the hands of time by decreasing inflammation and increasing youth-enhancing hormones like HGH. There’s no such thing as the fountain of youth. However practicing Eat Stop Eat comes close!...</p>
          </div>
        </li>
        <li class="row">
          <div class="columns medium-2"><img src="web/img/calendar.jpg"></div>
          <div class="columns medium-10">
            <h2>“But there’s no way I could practice IF every day...”</h2>
            <p>That’s the last thing Brad wants you to do. In fact Eat Stop Eat shows you exactly why you need to wait 48 to 72 hours between Eat Stop Eat protocols (and sometimes more).</p>
          </div>
        </li>
        <li class="row">
          <div class="columns medium-2"><img src="web/img/hospital70.jpg"></div>
          <div class="columns medium-10">
            <h2>“But I’m only in my 20s…”</h2>
            <p>I wish I had discovered Brad’s work in my 20s! It would have saved me over a decade of frustration. And I would have avoided a pile of health issues and joint injuries associated with overeating and inflammation. So yes! The younger you start the better.</p>
          </div>
        </li>
        <li class="row">
          <div class="columns medium-2"><img src="web/img/sandwhichico.jpg"></div>
          <div class="columns medium-10">
            <h2>“But I love Breakfast too much to skip it...”</h2>
            <p>Perfect, because I love breakfast too. And with Eat Stop Eat I still eat breakfast almost every single day. Pancakes, waffles, or a bowl of yogurt, it doesn’t  matter… I get to eat the foods I love because I create that freedom for myself.</p>
          </div>
        </li>
        <li class="row">
          <div class="columns medium-2"><img src="web/img/weight.jpg"></div>
          <div class="columns medium-10">
            <h2>“But I have a slow metabolism…”</h2>
            <p>Not really… You may have temporarily sabotaged your ability to burn off your food as fuel. Yet as you saw earlier, simply practicing Eat Stop Eat once or twice a week restores your metabolism and improves your ability to burn fat by 800% or more!...</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="main_container" style="background: #fffbe8;">
  <div class="certficate clearfix">
  <div class="columns medium-3 right">
      <p class="text-center" ><img src="web/img/certificate_seal.png"></p>
    </div>
    <div class="columns medium-9">
      <h2><strong>Look: You Don’t Have To Decide Today</strong></h2>
      <p> Because you need the time to experiment with Eat Stop Eat for yourself — and to go through each question above so you can prove to yourself it works — Brad has agreed to give you a crazy guarantee. He will accept all the risk on his own shoulders. You see…</p>
    </div>
    <div class="column medium-12">
      <p>You can try Eat Stop Eat today — and continue using it for the next 60 days — before you even decide if it’s really for you.</p>
      <p>And Brad expects that before the end of those 60 days you must agree that Eat Stop Eat is a sustainable lifestyle. And that it will give you a lifetime of peace and freedom from your struggles with weight and diets. If not he actually expects you to ask for a refund of your purchase. You see...</p>
      <p>Brad’s passion is to make health and weight control simple again. He wants you to enjoy your life, your food, your family and your friends to the fullest. The last thing he wants is to bring more stress into your life. So if you decide Eat Stop Eat is not for you then he’ll happily give you a no-hassles, no-questions-asked, 100% refund.<br>
      </p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2><span class="rwd-line">Save Time & Money — Use This Checklist To See</span><span class="rwd-line"> If You Qualify For Eat Stop Eat</span></h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>I need to be totally upfront with you. Even Brad admits that Eat Stop Eat is not for everyone. In fact he has identified a few common traits shared by the rare folks who did not thrive on his system. So take a second to review the list just in case you don’t qualify… </p>
      <ul class="cross_list">
        <li>If you have too much idle time and you’re not busy enough to keep your mind off food for a few hours, Eat Stop Eat may not be ideal for you…</li>
        <li>If you prefer the daily discipline and structure of traditional diets, you would not appreciate the freedom of Eat Stop Eat…</li>
        <li>If you’re able to control your appetite and cravings even during social meals then you likely don’t need the flexibility of the Eat Stop Eat lifestyle…</li>
        <li>If you enjoy the challenge of conquering diet rules, then the simplicity of Eat Stop Eat’s single strategy will probably not satisfy you.</li>
        <li>If losing weight quickly is your only motivation — and you aren’t worried about the health consequences of crash dieting — then the Eat Stop Eat health benefits won’t matter to you…</li>
      </ul>
      <p>On the other hand, Eat Stop Eat “<strong>power users</strong>” — those folks who’ve gotten the best results and continue to experience vibrant health and optimal weight after years using the system — all share these common qualities…</p>
      <ul class="tick_list">
        <li>You want a <strong>simple</strong> strategy you can follow for a lifetime</li>
        <li>You care as much about your <strong>health</strong> as about <strong>looking great</strong></li>
        <li>You want <strong>control</strong> over when to be disciplined and when to indulge</li>
        <li>You <strong>enjoy food</strong> with friends and family</li>
        <li>You want <strong>freedom</strong> from hunger and food-obsession</li>
        <li>You want to understand the <strong>scientific proof</strong> behind the method</li>
        <li>You want results <strong>quickly</strong>, and you want to keep them <strong>for life</strong>!...</li>
      </ul>
      <p>If that sounds like you, then Eat Stop Eat is not just “a” great solution for you, it’s the ONLY system that will give you everything you are looking for...</p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2><span class="rwd-line">Brad’s Mission (why we have <u>lowered the price</u></span> for the month of <span class="current-month-name">{%CurrentMonthName%}</span>)</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>Remember, Brad’s main motivation is to spread the word about this one simple strategy that will free you from all the diet misinformation and finally deliver the body you deserve. In fact…</p>
      <p>Brad firmly believes the information in Eat Stop Eat is the solution to our modern crisis of obesity and poor health. Because until now… no one had figured out how to make it easier to eat less!...</p>
      <p>Which is why you won’t pay the regular price of $49.95 for Eat Stop Eat during the month of <span class="current-month-name">{%CurrentMonthName%}</span>. In fact you won’t even pay half that. Because…</p>
      <p>Brad understands that this time of year can be challenging...</p>
    </div>

    <!--Monthly Deit Section Start From Here-->

    <div class="monthly_diet December"><!--Dec-->
      <div class="column medium-6">
        <p>The thermometer is dropping and you naturally long for warm and satisfying comfort foods. Plus…</p>
        <p>The days are short. The weather is cool.</p>
        <p>The season of never ending string of Christmas and New Years parties all tax your willpower and endanger your waistline.</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all those joyful treats while still losing weight and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_01.jpg"></div>
    </div>
    <div class="monthly_diet January"><!--Jan-->
      <div class="column medium-6">
        <p>Winter has you in it’s grip and you naturally long for warm and satisfying comfort foods. Plus…</p>
        <p>All your holiday eating has expanded your waistline and made you wonder if you’ll ever manage to get rid of the extra flab.</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in your favorite winter comfort foods while still losing all the weight you want, reaching your New Year’s Resolutions and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_04.jpg"></div>
    </div>
    <div class="monthly_diet Febuary"><!--Feb-->
      <div class="column medium-6">
        <p>The power of New Year’s Resolutions are long gone. Yet winter still holds you in it’s grip and you naturally long for warm and satisfying comfort foods. Plus…</p>
        <p>Super Bowl parties and Valentine’s Day chocolates may be a welcome treat. Yet they can be a big danger to your waistline.</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all the fun and treats while still losing weight and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_04.jpg"></div>
    </div>
    <div class="monthly_diet March"><!--Mar-->
      <div class="column medium-6">
        <p>You’re starting to think about the approach of Summer. And maybe you wonder if you’ll be ready to strut your stuff at the beach or beside the pool…</p>
        <p>The dark days of Winter may be coming to a close. Yet the effects of cold weather comfort foods and hiding behind bulky clothes are harder to get rid of.</p>
        <p>Thankfully Eat Stop Eat quickly strips off your “winter tire” without forcing you to give up your favorite foods or go hungry all the time.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_04.jpg"></div>
    </div>
    <div class="monthly_diet April"><!--Apr-->
      <div class="column medium-6">
        <p>It’s almost Summer. And maybe you wonder if you’ll be ready to strut your stuff at the beach or beside the pool…</p>
        <p>The dark days of Winter may be gone. Yet the effects of cold weather comfort foods and hiding behind bulky clothes are harder to get rid of.</p>
        <p>Thankfully Eat Stop Eat quickly strips off your “winter tire” without forcing you to give up your favorite foods or go hungry all the time.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_04.jpg"></div>
    </div>
    <div class="monthly_diet May"><!--May-->
      <div class="column medium-6">
        <p>It’s almost Summer! In fact warm and sunny days might already be cause to break out your shorts or even your bathing suit. And you’re excited to experience the sun on your skin. However…</p>
        <p>Summer treats like ice cream, hamburgers and hotdogs are half the fun on those warm Spring days. And you want to lose weight without depriving yourself…</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all those summer treats and more while still losing weight and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_05.jpg"></div>
    </div>
    <div class="monthly_diet June"><!--Jun-->
      <div class="column medium-6">
        <p>Summer weather is finally here! And you’re excited to experience the sun on your skin. However…</p>
        <p>Summer treats like ice cream, hamburgers and hotdogs are half the fun and you want to keep losing weight without depriving yourself…</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all those summer treats and more while still losing weight and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_05.jpg"></div>
    </div>
    <div class="monthly_diet July"><!--Jul-->
      <div class="column medium-6">
        <p>It’s Summer! It’s time to get outside, be active and enjoy the weather. However…</p>
        <p>Summer treats like ice cream, hamburgers and hotdogs are half the fun and you want to keep losing weight without depriving yourself…</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all those summer treats and more while still losing weight and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_05.jpg"></div>
    </div>
    <div class="monthly_diet August"><!--Aug-->
      <div class="column medium-6">
        <p>It’s Summer! It’s time to get outside, be active and enjoy the weather. However…</p>
        <p>Summer treats like ice cream, hamburgers and hotdogs are half the fun and you want to keep losing weight without depriving yourself…</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all those summer treats and more while still losing weight and experiencing growing energy and health. </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_05.jpg"></div>
    </div>
    <div class="monthly_diet September"><!--Sept-->
      <div class="column medium-6">
        <p>The thermometer is starting to drop and you naturally start longing for warm and satisfying comfort foods. Plus…</p>
        <p>Labor Day parties may have given you a head start on building up your “winter insulation.”</p>
        <p>Thankfully Eat Stop Eat gives you the solution to stripping off fat while you still enjoy your favorite end-of-summer treats and indulge in the comfort foods you love on cool Autumn nights. </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_03.jpg"></div>
    </div>
    <div class="monthly_diet October"><!--Oct-->
      <div class="column medium-6">
        <p>The thermometer is dropping and you naturally long for warm and satisfying comfort foods. Plus…</p>
        <p>The season of never ending special holidays like Halloween, Thanksgiving, and Christmas all tax your willpower and endanger your waistline.</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all those joyful treats while still losing weight and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_06.jpg"></div>
    </div>
    <div class="monthly_diet November"><!--Nov-->
      <div class="column medium-6">
        <p>The thermometer is dropping and you naturally long for warm and satisfying comfort foods. Plus…</p>
        <p>The season of never ending special holidays like Thanksgiving, Christmas and New Years all tax your willpower and endanger your waistline.</p>
        <p>Thankfully Eat Stop Eat actually gives you the freedom to indulge in all those joyful treats while still losing weight and experiencing growing energy and health.<br>
        </p>
      </div>
      <div class="column medium-6 text-center"><img src="web/img/eatstopeat_bowls_02.jpg"></div>
    </div>

    <!--Monthly Deit Section Start From Here-->

    <div class="column testimonials" style="padding:20px;">
      <h2>“I saved more than the price of Brad’s book on my first week’s grocery bill.”<br>
        <strong> — Marie, Illinois</strong></h2>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row bluebg">
    <div class="column">
      <h2>PLUS: Accept This Extra Gift From Brad To Make Your Decision Even Easier Today</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="column">
      <p>After interviewing hundreds of successful Eat Stop Eat users, I discovered a simple extra to make your experience even easier and faster. So…</p>
      <p>I asked Brad to develop a new manual and to include it for free with Eat Stop Eat. And he agreed to test the idea.</p>
      <p>Just make sure you take advantage of this while it’s still free, ok…<br>
      </p>
    </div>
  </div>
  <div class="row johnson_box" style="margin-top:-20px;">
    <div class="column medium-9 hmartop30">
      <div class="columns medium-3"> <img src="web/img/bonus_badge.png" style="margin-top:-20px;"> </div>
      <div class="columns medium-9">
        <h2 style="font-size:27px;"><strong> Eat Stop Eat Quick Start Guide <br>
          — Free Bonus</strong></h2>
      </div>
      <div class="column clearfix">
        <p><strong>Message From Brad:</strong></p>
        <p>Have you ever skipped the complicated instructions when setting up some new software on your computer, and then regretted it? That's me… I always want to skip ahead and just get started. So I created the Quick Start Guide for you. You’ll get a running start while so you can quickly enjoy the benefits of Eat Stop Eat within minutes of receiving the book.</p>
      </div>
    </div>
    <div class="column medium-3 text-left"> <img src="web/img/bonus_guide.png"> </div>
  </div>
</div>
<div class="main_container">
  <div class="row drkgrybg">
    <div class="column">
      <h2>Now It’s Time To Ask Yourself The Question That Really Matters… How Do You Feel About Your Body and Your Health Today?</h2>
    </div>
  </div>
  <div class="row pad20">
    <div class="columns medium-7 hmartop30">
      <p>Close your eyes and think about your body for a second. Are you happy with how it feels?</p>
      <p>That’s really what it all comes down to. Are you happy with the results of everything you’ve done to get you to where you are today?...</p>
      <p>Because if you’re not... the only way to guarantee you won’t feel just as frustrated next week, or 6 weeks from now, is by making a change. And that’s what I want for you…</p>
      <p>That’s why I wrote this page for you. So you can see how different Brad’s Eat Stop Eat is compared to all the other so-called “solutions” you’ve tried before…</p>
      <p>Whereas so many diets force you restrict certain foods like carbs or fats, Eat Stop Eat breaks those chains and unlocks the door to enjoying food again on your terms…</p>
      <p>Whereas dangerous diet pills, fancy supplements and pre-packaged weight loss meals cost you a bundle, Eat Stop Eat actually puts grocery money back in your pocket so you can spend it enjoying meals out with friends and family…</p>
    </div>
    <div class="columns medium-5"><img src="web/img/girl_with_icream.jpg"  style="margin-left:36px;"></div>
    <div class="columns">
      <p>Whereas popular weight loss advice like “watch what you eat” or “exercise more” requires constant discipline, with Eat Stop Eat you simply apply a small amount of restraint once or twice a week while you enjoy a flood of food-freedom the rest of the time…</p>
      <p>Whereas we’ve all experienced the awkward social moments of “being on a diet” and defending it to friends and family, no one need ever know that you practice Eat Stop Eat because you can simply schedule it around your social life…</p>
      <p>Whereas exercise takes a huge effort for a small result, <strong>just</strong> 24 hours delivers noticeable changes using Eat Stop Eat…</p>
      <p>And finally, whereas virtually every other weight loss method works in the short term but ultimately ends in gaining all the weight back once you fall off the wagon, Eat Stop Eat is truly a lean-body lifestyle that you’ll enjoy forever.</p>
      <p>Just don’t put it off a second longer. Because I’ve seen too many people put their health and happiness on the back burner… insisting they’ll get to it “tomorrow”... yet looking back with regret and self-accusation after weeks or even months of not taking action...</p>
      <p>That’s not what I want for you, and I know it’s not what you desire for yourself. That’s why you can get started risk-free today and prove to yourself why Eat Stop Eat is so very different…</p>
      <p>Remember, this month you’re also getting Brad’s limited-time discounted price on Eat Stop Eat. Instead of paying the regular $49.95 retail price you’re getting the <strong>unabridged 5th Edition</strong> of Eat Stop Eat <strong class="underline">PLUS</strong> both bonuses — worth over $50 on their own — for just one, single, secure and guaranteed payment of $19…<br>
      </p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row greenbg">
    <div class="column text-center">
      <h1>Limited-Time Reduced Price For <br>
        <strong><span class="current-month-name">{%CurrentMonthName%}</span> Ends In <span class="month-days-left">Days Left In This Month</span></strong></h1>
    </div>
  </div>
  <div class="row pad20">
    <div class="column text-center"> <img src="web/img/mainbook.jpg"> </div>
    <div class="column medium-8 medium-centered">
      <div class="column medium-5 text-center">
        <p>Regular Retail Price</p>
        <h1 class="regular_price"><strong>$49.95</strong></h1>
      </div>
      <div class="column medium-5 text-center">
        <p>Your Price (Until the End of <span class="current-month-name">{%CurrentMonthName%}</span> Only)</p>
        <h1 class="offer_price"><strong>$10</strong></h1>
      </div>
    </div>
    <!--<div class="text-center"> <a href="#url"><img src="web/img/button.jpg"></a> </div>-->
    <div class="text-center"> <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="button yellow">
      <div class="btn_seal"><img src="web/img/60day_seal.png"></div>
      Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On<br class="hide-for-medium-down">
      The System That Makes Weight Loss Simple Again!...</a>
      <div class="text-center"><img src="web/img/payemnt_icons.png"></div>
      <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="btn_lnk">Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On <br class="hide-for-medium-down">
      The System That Makes Weight Loss Simple Again!...</a> </div>
    <div class="column medium-3 text-center"><img src="web/img/adamnbrad.jpg"></div>
    <div class="column medium-9">
      <p>Yours in youthful health and fitness</p>
      <span><img src="web/img/signatures.jpg"></span>
      <p><strong>Adam Steer </strong>with Brad Pilon</p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row pad20">
    <div class="column">
      <h2><strong>Epilogue</strong></h2>
      <p><strong>By Brad Pilon</strong></p>
      <p>I wanted to personally thank you for reading this page.</p>
      <p>You see I’m a science geek and an Author. However I’m terrible at selling. So I’m grateful that Adam is helping me reach out to folks like you — savvy, sophisticated, successful and clever people who want to understand how the body REALLY loses weight and returns to health…</p>
      <p>It’s my mission to bust the myth that you have to consume MORE of some expensive pill, potion or gizmo to lose weight. It doesn’t make sense and it just is not supported by the research.</p>
      <p>What we really need is a simple way to make it <span class="underline">EASY to eat less</span>. </p>
      <p>And after pouring through 317+ scientific research papers and interacting with over 54,000 Eat Stop Eat early adopters, I’m more convinced than ever that the specific Eat Stop Eat protocol is the answer you’ve been looking for. After all...</p>
      <p>As Adam mentioned in the 5th section of this letter, humans evolved to eat according to these natural cycles. So it makes sense that going back to your roots allows your body to naturally release unneeded fat and repair creeping health issues right at a cellular level.</p>
      <p>You and I still have a long uphill climb though. The diet industry and big food companies would still rather keep you spending big bucks on their latest product rollouts. And believe it or not…</p>
      <p>Some folks are so stuck in their ways that they’d prefer to suffer through the constant, everyday hunger of traditional “dieting” rather than <span class="underline">simply limit their hunger to just one or two times a week</span> so they can enjoy food completely the rest of the time…</p>
      <p>Which is why I’m excited that you’re still with me. It’s only open minded trendsetters like you who can spread the message...</p>
      <p>So I’m excited to hear your story. I hope you’ll reach out to me and let me know about your Eat Stop Eat experience. </p>
      <p>I’m not a typical author who hides out behind my PR team and agent. You’ll find me on Twitter every day interacting with all the Eat Stop Eat early adopters. And I hope you’ll join us today! </p>
      <p>Tweet me, ok… (@BradPilon)</p>
      <p>Your friend,<br>
        Brad<br>
      </p>
      <div class="column medium-8 medium-centered">
        <div class="column medium-5 text-center">
          <p>Regular Retail Price</p>
          <h1 class="regular_price"><strong>$49.95</strong></h1>
        </div>
        <div class="column medium-5 text-center">
          <p>Your Price (Until the End of <span class="current-month-name">{%CurrentMonthName%}</span> Only)</p>
          <h1 class="offer_price"><strong>$10</strong></h1>
        </div>
      </div>
      <!--<div class="text-center"> <a href="#url"><img src="web/img/button.jpg"></a> </div>-->
      <div class="text-center"> <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="button yellow">
        <div class="btn_seal"><img src="web/img/60day_seal.png"></div>
        Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On<br class="hide-for-medium-down">
        The System That Makes Weight Loss Simple Again!...</a>
        <div class="text-center"><img src="web/img/payemnt_icons.png"></div>
        <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="btn_lnk">Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On <br class="hide-for-medium-down">
        The System That Makes Weight Loss Simple Again!...</a> </div>
      <p><strong>-ps-</strong>Remember you’ve got Brad’s 100% no-questions-asked guarantee on Eat Stop Eat so that you can try it yourself for 60 days. If you decide it’s not for you just let us know via email. We’ll make sure you get a 100% refund the very same day and we part as friends…</p>
      <p><strong>-pps-</strong>If you have questions about Eat Stop Eat Brad is happy for you to hit him up on Twitter. However we’ve put together some of the most common questions and answers for you right here...<br>
      </p>
    </div>
  </div>
</div>
<div class="main_container">
  <div class="row greenbg">
    <div class="column text-center">
      <h1>Eat Stop Eat Frequently Asked Questions</h1>
    </div>
  </div>
  <div class="row">
    <div class="column">
      <ul class="faq portfolio">
        <li>
          <h3>Q: I want to lose fat and gain muscle. Can I do both with Eat Stop Eat?</h3>
          <p> Absolutely. In Eat Stop Eat I outline exactly how you can build muscle while also losing body fat,
            just like <a href="web/img/faq/1.jpg" title=""> Doctor Kevin.</a> You can check out the chapter ‘Fasting and your muscle mass’ for more information. </p>
        </li>
        <li>
          <h3>Q: I'm really enjoying the weight loss results I’m getting from Eat Stop Eat, but I'd still like to clean up the way I eat even more, any tips?</h3>
          <p>A: With Eat Stop Eat you can incorporate any diet style you like. My personal opinion is that the general guideline of eating 'lean and green' with lots of fruits, vegetables, herbs and spices is an ideal complement to the Eat Stop Eat lifestyle, but you can incorporate any diet style you wish and still see fantastic results just like <a href="web/img/faq/2.jpg" > Meagan did</a>, You can check out the chapter ‘How to eat, Eat Stop Eat style’ for more information.</p>
        </li>
        <li>
          <h3>Q: I’ve heard that women shouldn’t follow diets that involve missing meals, is this true?</h3>
          <p>A: The Eat Stop Eat style of eating has helped thousands of women lose weight - Just look at the results of <a href="web/img/faq/6.jpg">Benita</a>, <a href="web/img/faq/5.jpg" >Tracey</a> and<a href="web/img/faq/8.jpg" > Roberta</a>!  In fact, there is an entire chapter in Eat Stop Eat devoted to helping women get the absolute best results possible from Eat Stop Eat, you can check out the chapter ‘Fasting for women’ for more information.</p>
        </li>
        <li>
          <h3>Q: Do you think the Eat Stop Eat lifestyle would be beneficial to someone who is simply trying to maintain his or her current weight?</h3>
          <p>A: Yes. Eat Stop Eat provides a simple way to lose weight, and to also maintain your weight. The trick is in the timing. If you want to be like <a href="web/img/faq/10.jpg" >Officer Mike</a> and not only lose weight, but keep it off, You can check out the chapter ‘How to keep it off’ for more information on maintaining your weight with Eat Stop Eat.</p>
        </li>
        <li>
          <h3>Q: Can you guarantee that I will lose fat if I buy your book?</h3>
          <p>A: No. Eat Stop Eat does not have a weight loss guarantee. Any diet that does is scamming you. Any effective Weight loss program requires you to eat less and exercise more. when you purchase Eat Stop Eat what you get is a book outlining all the principles you need to be very successful at losing weight and keeping it off, but it's up to YOU to actually put these principles into action. If you want to see the same kind of amazing results as <a href="web/img/faq/9.jpg" > Robb </a> Eat Stop Eat has the tools to make that happen.</p>
        </li>
      </ul>

          <div class="text-center" style="text-align:center !important;"> <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="button yellow">
            <div class="btn_seal"><img src="web/img/60day_seal.png"></div>
            Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On<br class="hide-for-medium-down">
            The System That Makes Weight Loss Simple Again!...</a>
            <div class="text-center"><img src="web/img/payemnt_icons.png"></div>
            <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="btn_lnk">Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On <br class="hide-for-medium-down">
            The System That Makes Weight Loss Simple Again!...</a> </div>

      <ul class="faq portfolio">
        <li>
          <h3>Q: Is this type of diet healthy in the long run?</h3>
          <p>A: According to the research, <strong>yes.</strong> Fasting initiates many health promoting pathways inside your body, including decreasing inflammation, activating autophagic pathways, and decreasing your body’s total toxin load. For more information you can check out the chapters in <em>‘The Health Benefits of Fasting’</em> section of Eat Stop Eat.</p>
        </li>
        <li>
          <h3>Q: I really want to try this diet, but I’m worried that I’m too old. Is this program appropriate for someone my age (I’m 67 years young)?</h3>
          <p>A: Absolutely. Many of our best Eat Stop Eat transformations have come from people over the age of 50. You don’t have to be in your 20’s to see fantastic results - Just look at<a href="web/img/faq/3.jpg" > Stephen .</a>and <a href="web/img/faq/7.jpg" >William</a> As long as you have medical clearance to diet and exercise this program will work for you if followed correctly. When in doubt you can always check with your healthcare professional.</p>
        </li>
        <li>
          <h3>Q: I've read that high protein diets can help with weight loss. Can I eat a high protein diet while doing Eat Stop Eat?</h3>
          <p>A: Yes. With Eat Stop Eat HOW you eat is completely your choice. There are several published research studies suggesting that a higher amount of dietary protein might be associated with an increased rate of weight loss (As long as the diet is calorie reduced). Most of the research I've reviewed have had people eat between 70 and 150 grams of protein per day (not the crazy 250 grams of protein per day suggestions that you find in fitness magazines). If you like, you can definitely try eating a higher protein diet while using Eat Stop Eat.</p>
        </li>
        <li>
          <h3>Q: I’d like to get to single digit body fat, is this the right program for me?</h3>
          <p>Whether your goal is to lose the last 10 stubborn pounds, or if you want to get absolutely shredded <a href="web/img/faq/11.jpg" >like Michael</a>, Eat Stop Eat can and will help you. You can read the chapter ‘The Eat Stop Eat lifestyle’ for more information on reaching extremely low levels of body fat.</p>
        </li>
        <li>
          <h3>Q: I want to get started right away; do I have to wait for you to deliver my book physically before I get started?</h3>
          <p>Don’t worry, I know your excited to start seeing results like <a href="web/img/faq/4.jpg" >Allan did</a>, so Eat Stop Eat will be instantly available for you to download right after your purchase. No shipping fees, no delays, no waiting to get started.</p>
        </li>
      </ul>
    </div>
  </div>
  <div class="text-center"> <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="button yellow">
    <div class="btn_seal"><img src="web/img/60day_seal.png"></div>
    Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On<br class="hide-for-medium-down">
    The System That Makes Weight Loss Simple Again!...</a>
    <div class="text-center"><img src="web/img/payemnt_icons.png"></div>
    <a href="http://80.esenews_eatstopeat.pay.clickbank.net/?cbfid=23347&vtid=ohbump" class="btn_lnk">Yes! I Want The <span class="current-month-name">{%CurrentMonthName%}</span> Discount On <br class="hide-for-medium-down">
    The System That Makes Weight Loss Simple Again!...</a> </div>
</div>
<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-566fe03a065b0a87" async></script>
<script>
$(function() {
  $(".open-popup").fullScreenPopup({
    	bgColor: "#000",
		inlineStyles: true,
		animationSpeed: 200
  });
});

$(function(){
  $('.portfolio').magnificPopup({
    delegate: 'a',
    type: 'image',
    image: {
      cursor: null,
      titleSrc: 'title'
    },
    gallery: {
      enabled: true,
      preload: [0,1], // Will preload 0 - before current, and 1 after the current image
      navigateByImgClick: true
		}
  });
});

</script>

<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>


<!--script src="google2_files/jquery.js"></script-->
<script src="google2_files/all.js"></script>

<img src="http://ESENEWS.eatstopeat.hop.clickbank.net" width="1px" height="1px" />


</body>
</html>
