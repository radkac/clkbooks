<?php
//LOAD COOKIE SCRIPT
require_once('../../checkreturning.php');
require_once('../../fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Add Academy</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=3">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function resizeVideoPlayer() {
	var new_width = $('#video_player').width();
	var new_height = Math.floor(411*new_width/730);
	$('#video_player').height(new_height);
	//alert(new_width+' '+new_height);
}

function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
	setTimeout('resizeVideoPlayer()', 3100);
});

$(window).resize(function() {
	resizeVideoPlayer();
});

</script>

	<?php require_once('../../eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>
            <b>
            On This Page Only You Are Eligible For Lifetime Online
            Coaching From Eat Stop Eat Author Brad Pilon<br>
            </b>
        </h1>

        <h2>(WITHOUT EVER Paying The $79/Mo Coaching Fee!)</h2>

        <div class="columns intro">
            <div class="column cl-1-4 ta-center">

                <img class="doctors-photo mobile-half-width" src="web/i/doctors-photo.jpg" alt="">

            </div>
            <div class="column cl-3-4">

                <div class="quotation">
                    If you’re an intelligent and motivated man or woman and you’ve been trying to lose weight or improve your health for at least a year without seeing the success you deserve&hellip; then the <b>online coaching</b> opportunity below will literally change your life&hellip;
                </div>

                <p>Having a coach is the fastest way to accelerate your results and achieve so much more than you could ever do on your own&hellip;</p>
                <p>And now, with the help of my super smart team of techies, I created the ultimate place where I can welcome you, give you access to ALL my work, and that allows me to come as close as possible to being your own private mentor and coach.</p>

            </div>
        </div>

        <hr>

        <h2>I want to bring you into my living room</h2>

        <p>I’m inviting you to become a charter member of a special place I call <u><b>The Academy</b></u>&hellip;</p>

        <ul class="check green">
            <li>Where I <u>answer all your questions</u> and ensure you get the fastest and easiest results possible&hellip;</li>
            <li>Where you can easily get the solutions you need <u>24 hours a day</u>&hellip;</li>
            <li>Where you’ll quickly find everything you need to <u>optimize your health</u> and achieve the <u>body you desire</u>&hellip;</li>
            <li>It’s a place where <u>you’ll often find me hanging out</u> for a quick chat where I can answer your question on the spot&hellip; or you can leave me a quick message and I’ll have your question answered in hours&hellip;</li>
            <li>It’s like <u>hanging out at home or at the gym with me</u> and being able to pick my brain 24/7&hellip;</li>
        </ul>

        <hr>

        <h2>Here are some of the powerful tools you’ll enjoy&hellip;</h2>

        <ul class="check green">
            <li>You’ll have access to my <u>Facts In Five videos</u> &mdash; where I bring you into my living room and share my no-holds-barred opinion on anything and everything Academy members are curious about&hellip;</li>
            <li class="insertion right mobile-full-width" style="padding:0px;width:35%;background:none;">
                <img class="mobile-full-width" src="web/i/portal-gif-animation-2.gif" width="100%" alt=""><br>
                <p align="center"><i>Searchable Facts In Five Videos Provide Easy Answers In Seconds</i></p>
            </li>
            <li>You’ll be automatically enrolled in the <u>university-level nutrition course</u> that I’ve previously charged more than $700 for &mdash; absolutely free&hellip;</li>
            <li>You’ll get access to my growing library of <u>exercise videos</u> in case you want to enhance your results with a workout program&hellip;</li>
            <li>You’ll have all <u>my best podcasts and premium interviews</u> collected in one place&hellip;</li>
            <li>You’ll have coaching calls where you’ll actually get on the phone with me to ensure you take your results to the next level&hellip;</li>
        </ul>

        <p>And when you secure your access via this private page, you will NEVER pay the monthly dues &mdash; ever &mdash; which I’ll explain in a second&hellip;</p>

        <hr>

        <h2>ZERO Monthly Dues and NO Hidden Fees (<i>Ever</i>&hellip;)</h2>

        <p>Now when you add it all up&hellip; the $700 university-level nutrition course&hellip; the monthly coaching calls&hellip; my Facts In Five videos&hellip; premium podcasts and interviews&hellip; the exercise video library&hellip; live-chat access to me&hellip; and all the bonus videos and goodies you’ll be getting every month&hellip;</p>
        <p>I could easily be charging the steep $147 per MONTH price tag that many of my colleagues are getting for similar services. However&hellip;</p>
        <p>I told you that as a brand new customer today there will be <b>NO monthly membership fees</b> for you&hellip;</p>
        <p>In fact I’m so convinced that The Academy is the <u><b>key to your guaranteed success</b></u> that I want to make it easy for you to join.</p>
        <p>Which is why&hellip; for a FRACTION of what my colleagues charge for a single month YOU will receive <b>LIFETIME access</b>&hellip;</p>
        <p>And <u><b>you’ll <span class="bg-yellow">never be charged again</span> for your Lifetime access to The Academy!</b></u>&hellip;</p>
        <p>NO monthly fees.</p>
        <p><b class="bg-yellow">NO rebills</b>&hellip; EVER&hellip;</p>
        <p><b>Look: there are absolutely NO hidden fees.</b> You will not pay another dime for The Academy EVER.</p>
        <p>Just use the button below to secure this once-in-a-lifetime opportunity to guarantee your results&hellip;</p>
        <p>You’ll immediately get FREE access to everything:</p>

        <ul class="check green">
            <li>the $700 university-level nutrition course&hellip;</li>
            <li>the monthly coaching calls&hellip;</li>
            <li>my Facts In Five videos&hellip;</li>
            <li>premium podcasts and interviews&hellip;</li>
            <li>the exercise video library&hellip;</li>
            <li>live-chat access to me&hellip;</li>
            <li>and all the bonus videos and goodies you’ll be getting every month.</li>
        </ul>

        <p>Experience the results for yourself. NEVER pay the regular $79 a month coaching fee. And receive lifetime access for just <b>ONE</b> single investment of only $37&hellip;</p>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="http://94.eatstopeat.pay.clickbank.net/?cbur=a">I Want Lifetime Access<br> To Your Online Coaching!<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="http://94.eatstopeat.pay.clickbank.net/?cbur=a">I Want Lifetime Access<br> To Your Online Coaching!</a></span>
            </p>
        </div>

        <p><b>Remember there’s no risk.</b> As always, your full investment is covered by my 100% iron-clad money-back satisfaction <b>guarantee</b>&hellip;</p>
        <p>I want you to feel absolutely confident in trying out The Academy so you can see the results and the value for yourself before you decide.</p>
        <p>So you have a full 60 days to test drive the Academy. And if you decide it’s not for you just let me know&hellip; I’ll give you a full refund and we part as friends no matter what, ok?&hellip;</p>
        <p>Just use the button below to secure your risk-free access right now though. Because once you leave this page you just won’t see this extraordinary special offer anywhere else&hellip;</p>
        <p>And I don’t want you to have to pay the $79 a <b>month</b> regular price for The Academy if you decide later that you want to gain access&hellip;</p>
        <p>So take action right now. And as soon as you log into The Academy in the next few minutes make sure you hit me up with your most burning questions, ok?&hellip;</p>
        <p>See you on the inside&hellip;</p>

        <div class="insertion left">
            <img class="mobile-half-width" src="web/i/brad-photo.jpg" alt="">
        </div>

        <p>
            <br>
            Yours in youthful health and fitness<br>
            <br>
            <img src="web/i/brad-sign.png" alt=""><br>
            <br>
            Brad Pilon
        </p>
        <br class="clearfix">

        <table class="pricing">
            <tr>
                <td class="reg">
                    At The Regular<br>
                    MONTHLY Price<br>
                    <center><strike>&nbsp;$948/year&nbsp;</strike></center>
                </td>
                <td class="inv">
                    <b class="bg-yellow">One</b> <u>LIFETIME</u><br>
                    Investment<br>
                    <center><em>$37</em></center>
                </td>
            </tr>
        </table>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="http://94.eatstopeat.pay.clickbank.net/?cbur=a">I Want Lifetime Access<br> To Your Online Coaching!<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="http://94.eatstopeat.pay.clickbank.net/?cbur=a">I Want Lifetime Access<br> To Your Online Coaching!</a></span>
            </p>

            <hr>

            <p class="discard">I understand that this is my only chance to get your online coaching and to secure access to The Academy at the steeply discounted price for new customers only. I know that once I leave this page I’ll lose out on this deal. I’m ok with that and I’d rather go it alone. <a href="http://94.eatstopeat.pay.clickbank.net/?cbur=d">I'll pass</a>.</p>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2017 &copy; Clkbooks.com<br>
        For support please contact support [at] clkbooks.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
