<?php
//LOAD COOKIE SCRIPT
require_once($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>On This Page Only You Are Eligible For Lifetime Online Coaching From Eat Stop Eat Author Brad Pilon (WITHOUT EVER Paying The $79/Mo Coaching Fee!)</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=2">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=2">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=2">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
});
</script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>The Brad Pilon Premium Health &amp; Fitness Treasury</h1>

        <p><i><b class="bg-yellow">On This Page Only</b>: Receive access to Brad’s complete Health &amp; Fitness solutions &mdash; in one place &mdash; without ever paying the yearly subscription fee.</i></p>
        <p>Which is why we created The Brad Pilon <u><b><i>Premium</i></b></u> Health &amp; Fitness Treasury so you <b class="cl-red">save</b> hundreds of dollars while shaping the body of your dreams and enjoying health and energy you never believed could be yours. Quite simply, you’re receiving access to ALL Brad’s books &mdash; past, present and future &mdash; PLUS a whole lot more insider-only goodies!</p>
        <p><b class="cl-red">Yes</b>, we’ll send you digital access to all CLKBooks titles Brad publishes in the future <u>in addition to</u> giving you <b>immediate access</b> to all his current ebook titles! This alone is already worth more than $193 on it’s own&hellip;</p>
        <p>PLUS, you’re getting a bunch of extra <b>Inside Access</b> goodies that <b>no one</b> else can get their hands on&hellip;</p>
        <p>Here’s everything you receive today:</p>

        <div class="product">
            <div class="insertion left">
                <center>
                    <img class="mobile-full-width" src="web/i/products/visual-01.png?v=2" width="250" alt=""><br><br>
                    <b>$97 Value</b>
                </center>
            </div>
            <div class="descr">
                <h4><u><b>Premium</b></u> Video Coaching</h4>
                <p>You’ll have access to Brad’s Facts In Five Video Coaching &mdash; where he brings you into his living room and shares his no-holds-barred opinion on anything and everything you are curious about. Just ask and he’ll give you the no-nonsense answer you are looking for. No topic is off limits. You can ask Brad how to put the finishing touches on your beach ready body. Or you can ask how to double your energy so you can sail through your day. And anything in between!</p>

            </div>
        </div>

        <div class="product">
            <div class="insertion left">
                <center>
                    <img class="mobile-full-width" src="web/i/products/visual-02.png?v=2" width="250" alt=""><br><br>
                    <b>$193 Current Value</b>
                </center>
            </div>
            <div class="descr">
                <h4><u><b>Premium</b></u> All-Access To Brad Pilon’s Books</h4>
                <p>YES&hellip; you get ALL Brad’s ebooks. And you get any ebook Brad publishes with us in the future. It’s as simple as that. Brad covers a wide range of topics that cover all your most important health and physique challenges. Every ebook is firmly rooted in scientific research and presented in a no-nonsense and easy to understand format. Check out a list of all the titles you’re getting today at the bottom of this page&hellip;</p>
            </div>
        </div>

        <div class="product">
            <div class="insertion left" style="min-width:250px;">
                <center>
                    <img class="mobile-1of3-width" src="web/i/products/visual-03.png" width="80" alt=""><br><br>
                    <b>$149 Value</b>
                </center>
            </div>
            <div class="descr">
                <h4><u><b>Premium</b></u> Exercise Instructional Video Library</h4>
                <p>You don’t need to start working out to live a healthy life and have a slim body. However, because the energy and momentum you gain from using Brad’s methods is going to get you motivated, you’ll get access to a complete library of exercise videos in case you want to refine your evolving physique with a workout program&hellip;</p>
            </div>
        </div>

        <div class="product">
            <div class="insertion left">
                <center>
                    <img class="mobile-full-width" src="web/i/products/visual-04.png" width="250" alt=""><br><br>
                    <b>$700 Value</b>
                </center>
            </div>
            <div class="descr">
                <h4>BONUS: Free Access To Brad’s $700 Advanced Nutrition Course</h4>
                <p>We call it “The U” &mdash; a university level nutrition course delivered in terms <u><b>anyone can understand</b></u>. Once you’re done you’ll understand nutrition better than 99% of all diet professionals and even registered nutritionists. You’ll blow your friend’s minds as you pick apart the flimsy theories and silly diet trends on daytime TV shows like Dr Oz.</p>
                <p>And you’ll finally be able to understand exactly how to create the body and health you desire through nutrition choices that make sense for YOU. And you’ll be automatically enrolled in this university-level nutrition course that Brad usually charges more than $700 for &mdash; absolutely FREE&hellip;</p>
            </div>
        </div>

        <h2><b>REMEMBER:</b> ZERO Monthly Dues and NO Hidden Fees (Ever&hellip;)</h2>

        <p>Now when you add it all up… the $700 university-level nutrition course&hellip; the monthly member-only private podcast&hellip; the Facts In Five Video Coaching&hellip; the exercise video library&hellip; direct access to Brad&hellip; all the bonus videos and goodies you’ll be getting every month&hellip; and of course access to every single one of Brad’s bestselling ebooks&hellip; your shopping cart would come out to over $1156!&hellip;</p>
        <p>And almost everyone agrees that the regular $147 yearly fee for access is a steal. However&hellip;</p>
        <p>As a brand new customer today there will be <u><b>NO membership fees</b></u> for <b class="cl-red">you</b>&hellip;</p>
        <p>In fact, because we want to make it soooo easy for you to say yes, today we are giving you the option to eliminate the yearly fee and get lifetime access for one easy payment of just $147.</p>
        <p>And you’ll never be charged again for your Lifetime <u><b><i>Premium</i></b></u> access!&hellip;</p>
        <p>NO yearly fees.</p>
        <p>NO rebills&hellip; EVER&hellip;</p>
        <p>However this amazing opportunity is available ONLY on this page. Once you leave you will not be able to access this one-time price again.</p>
        <p>Just use the button below to secure this once-in-a-lifetime opportunity to guarantee your results&hellip;</p>
        <p>You’ll immediately get TOTAL access to everything:</p>

        <ul class="check green">
            <li>All Brad’s books!</li>
            <li>The Facts In Five Video Coaching&hellip;</li>
            <li>the exercise video library&hellip;</li>
            <li>Direct access to Brad&hellip;</li>
            <li>the $700 university-level nutrition course&hellip;</li>
            <li>and all the EXTRA bonus videos and goodies you’ll be getting every month.</li>
        </ul>

        <br class="mobile-hidden">
        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/products/total-bundle.jpg?v=4" width="800" alt=""></center>
        </div>
        <br class="mobile-hidden">

        <div class="purchase clearfix">
            <p class="price"><b>$99</b> <span class="cl-red">One-Time</span> Payment*</p>
            <p class="note">*Zero Rebills or Extra Fees</p>
            <p class="action">
                <a class="button" href="http://145.eatstopeat.pay.clickbank.net/?cbur=a">Add Lifetime Access To The Premium Treasury<br> To My Order For One Easy Payment</a><br><br>
                <img src="web/i/purchase-accepted-cards.png" width="220" alt="">
            </p>
        </div>

        <hr>

        <h2><b>Brad Pilon Bestsellers FREE With The Premium Treasury:</b></h2>

        <div class="product">
            <div class="insertion right">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-ese.png?v=2" width="140" alt=""></center>
            </div>
            <h4>Eat Stop Eat</h4>
            <p>Brad’s original bestseller, this book is the only resource that reveals the full story behind the science and benefits of intermittent fasting. Discover how following Brad’s <i>Eat Stop Eat</i> method once or twice a week will allow you to effortlessly control your weight and enhance your health.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-ese-optimized.png" width="140" alt=""></center>
            </div>
            <h4>ESE Optimized</h4>
            <p>If the original Eat Stop Eat delivers all the facts about Intermittent Fasting — from labs and clinical research — then Brad’s follow up companion, Eat Stop Eat Optimized, is the very intimate story of Intermittent Fasting from Brad’s own personal experiences. Eat Stop Eat is the Science. Eat Stop Eat Optimized is the Art. Discover how Brad lives and breaths the <i>Eat Stop Eat</i> lifestyle every day.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-ese-phase2.png" width="140" alt=""></center>
            </div>
            <h4>ESE Phase 2</h4>
            <p>The <i>Eat Stop Eat</i> lifestyle will be your enduring companion throughout your life &mdash; allowing you to easily achieve and maintain a level of leanness, health and energy that will put you set you above nearly everyone around you. However there will be times when you want to go the extra mile and achieve an elite level of low bodyfat for a vacation, photos, or even a sports or fitness competition. Eat Stop Eat Phase 2 is the exact plan you will use to adapt the lifestyle in order to easily attain a physique you never dreamed possible.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-ese-5daydiet.png" width="140" alt=""></center>
            </div>
            <h4>The 5-Day Diet</h4>
            <p>One of the most frequent question Brad gets is: <i>“What should I eat on the 5 or 6 days a week when I’m not using the Eat Stop Eat method.”</i> At first Brad’s answer was always whatever you want. Which in a way is still true. However when pushed for an answer Brad came up with the exact diet plan to absolutely optimize the “other 5 days” of eating if you want ultimate health and a killer physique.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-thinair.png" width="140" alt=""></center>
            </div>
            <h4>Thin Air</h4>
            <p>Have you ever suffered from nagging and unexplained fatigue, headaches, lethargy, mysterious weight problems or headaches and even migraines? If yes, have you tried everything &mdash; including diets, exercise, countless doctors, acupuncture, alternative medicine, and the list goes on &mdash; to get relief? Would you be surprised to discover your nagging challenges may have nothing to do with your diet or your exercise. In fact, it may actually be the air you are breathing right now that’s making you fat, tired and sick! In <i>Thin Air</i> you’ll discover the surprising connection between exhaustion, lethargy, weight gain and the air you breathe.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-gbbb.png" width="140" alt=""></center>
            </div>
            <h4>Good Belly, Bad Belly</h4>
            <p>Have you ever wondered why some people can eat whatever they want and still stay slim? In this shocking book Brad takes a deep dive into the X-Factor that allows 2 people &mdash; even identical twins &mdash; to eat exactly the same foods while one gains weight and the other does not. And it’s something that is completely under your control that you can start doing today!</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-3min.png" width="140" alt=""></center>
            </div>
            <h4>3-Minute Lower Belly Flatteners</h4>
            <p>If you’ve ever lost weight and still can’t tighten up your lower belly &mdash; often referred to as your belly pooch &mdash; then this set of easy and quick exercises is the answer. In only three minutes a day you’ll firm and tighten your abdomen so you finally have the flat belly you desire.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-7min.png" width="140" alt=""></center>
            </div>
            <h4>7-Minute Fat Burning Cardio Solution</h4>
            <p>Brad firmly believes your nutrition is by far the most critical factor to lose all the weight you want. However the progress you enjoy from a great nutritional plan can be accelerated with just a minimum dose of exercise. Brad created the 7-Minute Fat Burning Cardio Solution for exactly that &mdash; to give you the biggest weight loss boost with the smallest time and effort.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-hmp.png" width="140" alt=""></center>
            </div>
            <h4>How Much Protein</h4>
            <p>As a former bodybuilding competitor and supplement industry insider, Brad is uniquely placed to answer this controversial question. Trendy diets and media coverage of dubious scientific studies has created an entire universe of myths around protein. In <i>How Much Protein</i> Brad delves into the actual science and gives you the truth about how much you need, and when, for your personal body-type and lifestyle.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-hm-creatine.png" width="140" alt=""></center>
            </div>
            <h4>How Much Creatine</h4>
            <p>Do you think only bodybuilders and athletes should be taking creatine? Then this eye-opening book will shock you. Science is proving that creatine could be the next wonder supplement, giving you benefits across the entire spectrum of your health. Many experts in the know are comparing it to the Omega-3s found in fish oil. <i>How Much Creatine</i> digs into the science and gives you the recommendations you need to take advantage of this “super-supplement.”</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <br class="mobile-hidden">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-aa.png" width="140" alt=""></center>
            </div>
            <h4>Anabolic Again</h4>
            <p>This is for the folks who’ve been working out for a while, who’ve probably hit their late 30s or beyond, and feel like they are no longer making any progress in the gym. The truth is, there’s a very good reason training results slow down. And it’s something you can easily fix using the step-by-step Anabolic Again method.</p>
        </div>

        <div class="product">
            <div class="insertion right">
                <center><img class="mobile-1of3-width" src="web/i/products/ecover-comp-training.png" width="140" alt=""></center>
            </div>
            <h4>Compound Training</h4>
            <p>Developed specifically as a companion to the Eat Stop Eat lifestyle, this is the perfect workout solution for anyone who’s following a low calorie diet or who uses intermittent fasting.</p>
        </div>

        <p>To get all Brad’s books PLUS hundreds of dollars worth of free extras today just use the button below to secure your access now.</p>

        <br class="mobile-hidden">
        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/products/total-bundle.jpg?v=4" width="800" alt=""></center>
        </div>
        <br class="mobile-hidden">

        <div class="purchase clearfix">
            <p class="price"><b>$99</b> <span class="cl-red">One-Time</span> Payment*</p>
            <p class="note">*Zero Rebills or Extra Fees</p>
            <p class="action">
                <a class="button" href="http://145.eatstopeat.pay.clickbank.net/?cbur=a">Add Lifetime Access To The Premium Treasury<br> To My Order For One Easy Payment</a><br><br>
                <img src="web/i/purchase-accepted-cards.png" width="220" alt="">
            </p>
        </div>

        <p><small>No thanks Brad. I understand that this is my only chance to get your online coaching and to secure access to The Treasury at the steeply discounted price for new customers only. I know that once I leave this page I’ll lose out on this deal. I’m ok with that and I’d rather go it alone. <a href="http://145.eatstopeat.pay.clickbank.net/?cbur=d">I'll pass</a>.</small></p>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.</p>

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
