<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Progressions</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=12">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=12">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=12">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/progressions/web/i/fb-og-progressions.jpg">
	<meta property="og:description" content="Progressions: The Cult-Hit Companion To The Bestselling Book, Eat Stop Eat">
	<meta property="og:title" content="Check Out What I'm Reading...">

<script>

$(function(){

    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });

});

</script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<a name="top"></a>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="tagline">
        <div class="inner">
            The Cult-Hit Companion To The Bestselling Book, Eat Stop Eat
        </div>
    </div>

    <div id="breadcrumbs">
        Books › Weight Loss Books › Best Sellers
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <h1>Eat Stop Eat: Progressions</h1>
                <p class="byline">By Brad Pilon (Author)</p>
                <p class="rating">Digital Edition Pre-Release</p>
            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img class="mobile-half-width" src="web/i/progressions-3d-new.jpg" width="100%" alt="">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png?v=1" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>CLK*Books Exclusive Price</h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$59.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$37*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$22.99</b> (62%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$37</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://148.eatstopeat.pay.clickbank.net/?cbfid=30120&vtid=clksoloprogressions&cbskin=18285"><span class="inner">Buy Now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                    <h1>Eat Stop Eat: Progressions</h1>
                    <p class="byline">By Brad Pilon (Author)</p>
                    <p class="rating">Digital Edition Pre-Release</p>
                </div>

                <h1>
                    What would you say if I told you your workout is insane? Let me explain&hellip;
                </h1>

                <p>Albert Einstein is famous for defining insanity something like this: <i>doing the same thing over and over again and expecting a different result</i>.</p>
                <p>And that’s what most folks are doing when it comes to exercise. They do the same number of sets and reps at the same weight for weeks or even months.</p>
                <p>Listen: it’s not your fault. I know you want to put in the work and get the rewards. Yet if you’re like most folks I know you struggle to increase the weights you’re putting on the bar or the weight stack. The problem is&hellip;</p>
                <p>There’s a fundamental flaw in exercise programs that <b>guarantees we all fail</b>&hellip;</p>
                <p>Unless you’re one of the genetic freaks who just has to look at a dumbbell to get leaner and stronger, there’s no way you can make any significant progress with the common exercise advice like <i>“do 3 sets of 12”</i> that you’re going to get from a trainer or a fitness magazine. Here’s why&hellip;</p>

                <div class="read-more-content collapsed hidden">

                    <p>Traditional programming makes an UNREASONABLE demand on your body. There’s a sneaky flaw built right into traditional exercise programs that requires you to make HUGE leaps in strength from workout to workout.</p>
                    <p>Now you may be thinking, <i>“what’s he talking about, I only need to do one more rep at that weight next session.”</i> But check this out and you’ll see why that’s not as easy as it sounds&hellip;</p>
                    <p>First, by looking at the maximum reps you can do at a given weight on any exercise, you can estimate the maximum weight you could use for <u>one single rep</u>. This is called your Estimated 1-Rep Max (<b>e1RM</b>). For example&hellip;</p>
                    <p>Imagine a 160 lb male who can bench press 205 lbs for 8 reps. Using a special formula we can determine his bench press <b>e1RM</b> as 259.66 lbs today. Now&hellip;</p>
                    <p>A traditional exercise program would require this guy to <u>lift the same 205 lbs for 9 reps</u> on his next session. Sounds easy enough, right? However, you’re about to see <b>how crazy that is</b>&hellip;</p>
                    <p>As we already know, being able to bench press 205 for 8 good reps is the equivalent of having a e1RM of 259.66 pounds. Yet jumping to 205 lbs for 9 good reps is the equivalent of having an e1RM of 266.49 pounds. <b>That’s around a 7 pound JUMP in maximal potential strength in one session!</b></p>

                    <div class="insertion center">
                        <center><img class="mobile-full-width" src="web/i/schema-1.png" width="90%" alt=""></center>
                    </div>

                    <p>Now&hellip; you might be able to do that once or twice… especially IF you are brand new to weight training. However there is no way anyone can sustain that pace. Which is why you plateau on nearly every exercise program!</p>
                    <p><b>Instead</b>, what if we could increase your e1RM by only a pound or even less <u>every single session</u>? Yet you do that again and again every time you exercise! No more stalling at the same weight for weeks at a time! And in the long run, extraordinary progress&hellip;</p>

                    <h3>Why Incremental Progressions Trigger Massive <u>Fat Loss</u> &amp; <u>Strength Gains</u></h3>

                    <p>You see, even if each increase is small, it still <u>triggers powerful <b>cellular signalling</b></u> that forces your body to respond by getting both stronger AND leaner!</p>
                    <p><u>YES</u>, the exact same <b>cellular signalling</b> triggers BOTH your <b>strength</b> gains AND your <b>fat loss</b>. Cool right?</p>
                    <p><u>Each one of these tiny progressions triggers that cellular signalling</u> so that you keep getting a little bit stronger and <b>looking</b> a little bit <b>leaner</b> every single week.</p>
                    <p>Compare that with what you see when you walk into the gym. If you’ve been going to the same gym for a while, pick someone who is there all the time, and that you’ve been seeing there for months. Now answer this&hellip;</p>
                    <p><b>Do they look any different now</b> than they did several months ago? If not, then what is the point of being there? They are spinning their wheels on a meaningless hamster wheel of wasted effort!</p>
                    <p>Here’s what’s happening&hellip; Those poor folks are only experiencing that cellular signaling every once and awhile when they manage to bust a plateau. Sure&hellip; the signal may be stronger on each of those occasions. But it is short lived and too rare to cause noticeable changes in their physique.</p>
                    <p>Now imagine this&hellip; using a carefully crafted and very specific sequence of reps and weights for every single workout, you end up constantly triggering these cellular signals so you get a visible difference every single week that you can actually SEE in the mirror and FEEL in the gym!</p>
                    <p>You get just a little bit stronger and a little bit leaner after <u>every single workout</u> &mdash; yet it accumulates so quickly that it may get you into trouble&hellip;</p>

                    <div class="insertion center">
                        <center><img class="mobile-full-width" src="web/i/schema-2.png?v=2" width="65%" alt=""></center>
                    </div>

                    <p>Look: If someone sees you at the gym today, and then doesn’t see you again until a few weeks from now, that person’s jaw will drop at your visible transformation. And to be honest&hellip;</p>
                    <p>Because we’re conditioned to expect little to no visible improvement from week to week, they may suspect you’re doing something sneaky like taking drugs! Afterall traditional workouts just can’t keep sending the signals that make you leaner and stronger. After a few workouts, you get stalled and can’t make any more progress. And&hellip;</p>
                    <p>You end up on a plateau for weeks and even months. And the worst part is… you are told by all the experts that it’s NORMAL for you to stay stuck like that. Nonsense!&hellip;</p>
                    <p>The problem is, no one has put any thought into how to create constant progress <u><b>for regular folks</b></u> like you and me!&hellip;</p>
                    <p>Sure&hellip; elite athletes who spend hours training every day &mdash; or <b>cheaters</b> who pump themselves full of performance enhancing drugs &mdash; can follow the standard exercise recommendations and in many cases still make continual progress. However&hellip;</p>
                    <p>Regular folks like you and me &mdash; people who can only spend an hour or less in the gym, 3 to 4 times per week &mdash; we need a more systematic approach.</p>

                    <h3>A NATURAL Way To Get The Same Results As Pros (or Cheaters)</h3>

                    <p>Which is why Brad Pilon’s brand new Progressions methodology is so revolutionary. It gives <b>you</b> the <u>same rapid progress</u> as an athlete using performance enhancing drugs or a pro spending hours in the gym.</p>

                    <div class="insertion right" style="width:45%">
                        <center><img class="mobile-full-width" src="web/i/schema-3.jpg?v=2" width="100%" alt=""></center>
                    </div>

                    <p>In fact, when Brad first discovered and started experimenting with this groundbreaking approach to exercise, he was able to switch from his former bodybuilding style of training &mdash; including up to 2 hours a day, 5 to 6 days a week &mdash; and replace it with just <u>3 sessions per week of LESS than one hour</u>&hellip;</p>
                    <p>Plus, he lowered his body fat percentage AND increased his strength numbers while doing less than HALF the exercise volume he was doing before!</p>
                    <p>Not only that, every Progressions workout is super FUN because you <b>ALWAYS make <u>progress</u></b>. You’re always <u>hitting a new high</u>. And&hellip;</p>
                    <p>Since you're working within your body's natural rhythms, you are always signaling your body to increase strength and boost your fat burning metabolism. Each small daily improvement massively compounds over time.</p>
                    <p>Progressions also gives you a huge confidence boost because you never have to guess at what you should be doing. The only time you have to think about your training is your first workout when you figure out your starting point. After that you just <u>follow the formula</u> and everything is laid out for you day-by-day, set-by-set. No confusion, no guessing, just a simple easy approach to follow every week.</p>

                    <h3>Enjoy Early Access To The Next Revolution In Strength &amp; Fat Loss</h3>

                    <p>Truth is, once word gets out about Brad’s methods, there’s no doubt all the copycats are going to come out of the woodwork, and this will be the new standard exercise prescription in the future. Yet lucky for you&hellip;</p>
                    <p>If you’re reading this page you’re getting the first crack at it, and you’ll be able to gloat over your results while everyone else is wondering what the heck you’re doing to make such rapid progress.</p>
                    <p>PLUS, as part of our ongoing partnership with Brad, CLK*Books has negotiated a low retail price of only $49.95 for this groundbreaking system and the very special bonus you’ll discover in just a moment. However&hellip;</p>
                    <p>The best news is that you will NOT be paying the regular retail price today. In fact&hellip;</p>
                    <p>When you take action now as an early adopter of this groundbreaking new system you’ll get everything, including the featured fast-action bonus, for just one payment of $29.</p>

                    <br class="mobile-hidden">
                    <p class="mobile-hidden" align="center"><a class="purchase-button wo-ico" href="#top"><span class="inner">Get Started</span></a></p>
                    <br class="mobile-hidden">

                    <h3>Limited Time Access: Exclusive Email Coaching From Author Brad Pilon</h3>

                    <p>If you’re still on the fence about trying an exercise system that’s so simple, yet so revolutionary, then how would you feel about getting FREE email coaching from Brad to make sure you get 100% benefit from the Progressions methodology?</p>
                    <p><b>YES!</b> Brad has agreed to give you 21 days FREE access to his VIP Newsletter called The Advocate. Even better&hellip;</p>
                    <p>He actually created a separate Advocate group <u>specifically</u> for Progressions customers so that he can deliver the exact information you need to get maximum results with every single workout session.</p>
                    <p>The Advocate is where Brad shares all the insider tips, tricks and insights he gathers through his constant scouring of the scientific research on exercise, nutrition and health. You won’t find this stuff anywhere else, which is why so many thousands of folks around the world eagerly check their inbox every day to see if there’s a new issue of The Advocate waiting for them.</p>
                    <p>And when you act now to secure your discounted price on the Progressions system, you’ll automatically get 21 days of FREE Advocate emails delivered straight to your inbox. These emails are always free of advertising and promotion, so you never get distracted and can always count on getting right down to what’s important to you.</p>

                    <h3>
                        AND&hellip; I need to let you know about a couple MORE <u>FREE</u> gifts that<br>
                        I convinced Brad to include for you&hellip;
                    </h3>

                    <p>However these are <b>only available</b> with this special <u>introductory discounted offer</u>. Once we take this page down and return to the regular retail price these will no longer be available for FREE&hellip;</p>

                    <hr style="width:100%;overflow:hidden;">

                    <div class="insertion right">
                        <center>
                            <img class="mobile-half-width" src="web/i/bonus-1-ecover.jpg" width="200" alt=""><br>
                            <p class="ta-center">[$19.50 Value]</p>
                        </center>
                    </div>

                    <p><big>FREE Gift #1:</big></p>

                    <h3 class="ta-left">The 3-Minute Lower Belly Flatteners</h3>

                    <p>This is a simple 3-Minute sequence of exercises to tighten up the core muscles in your belly and quickly get rid of your lower belly pooch&hellip;</p>
                    <p>This is the exact sequence Brad gave me after I lost all the weight and complained that my lower belly still sagged&hellip;</p>
                    <p>Within days I started to notice my belly looked flatter and tighter in the bathroom mirror&hellip;</p>
                    <p>And you’re getting the 3-Minute Lower Belly Flatteners completely <b>free</b> today&hellip;</p>

                    <hr style="width:100%;overflow:hidden;">

                    <div class="insertion right">
                        <center>
                            <img class="mobile-half-width" src="web/i/bonus-2-ecover.jpg" width="200" alt=""><br>
                            <p class="ta-center">[$19.50 Value]</p>
                        </center>
                    </div>

                    <p><big>FREE Gift #2:</big></p>

                    <h3 class="ta-left">The 7-Minute Fat Burning Cardio Collection</h3>

                    <p>Look, you don’t have to exercise to lose weight with Good Belly, Bad Belly&hellip;</p>
                    <p>However, I was eager to slim down as <b>fast</b> as possible&hellip;</p>
                    <p>And when I asked Brad what I could do to speed up the process&hellip;</p>
                    <p>&hellip;WITHOUT spending hours exercising&hellip;</p>
                    <p>He gave me these simple 7-Minute cardio routines I could do whenever I had the time&hellip;</p>
                    <p>And they worked so well for me I wanted to make sure you get access too&hellip;</p>

                    <hr style="width:100%;overflow:hidden;">

                    <p>To secure your free access now, just tap the button below.</p>

                    <div class="purchase-holder">
                        <div class="insertion center purchase">

                            <img class="ecover mobile-2of3-width" src="web/i/progressions-3d-new.jpg" alt="">

                            <table align="center" class="costs" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>List Price:</td>
                                    <td>$59.99</td>
                                </tr>
                                <tr>
                                    <td>CLK*Books Price:</td>
                                    <td><b>$37*</b></td>
                                </tr>
                                <tr>
                                    <td>You Save</td>
                                    <td>$22.99 (62%)</td>
                                </tr>
                                <tr>
                                    <td colspan="2">*Includes free international wireless delivery</td>
                                </tr>
                                <tr class="total">
                                    <td class="k">Total</td>
                                    <td class="v">$37</td>
                                </tr>
                            </table>

                            <a class="button" href="http://148.eatstopeat.pay.clickbank.net/?cbfid=30120&vtid=clksoloprogressions&cbskin=18285">Buy Now</a>

                        </div>
                    </div>

                    <ul class="checkmarks">
                        <li>Thought-Provoking Books</li>
                        <li>Extreme Weight Loss</li>
                        <li>Healthy Dieting</li>
                    </ul>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>CLK*Books Exclusive Price</h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$59.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$37*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$22.99</b> (62%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$37</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://148.eatstopeat.pay.clickbank.net/?cbfid=30120&vtid=clksoloprogressions&cbskin=18285"><span class="inner">Buy Now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>


</body>
</html>
