<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Eat Stop Eat Affiliate Help &mdash; How To Start?</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

<?php require_once('../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

<? require('menu.php'); ?>

        <h1>Eat Stop Eat<br> Affiliate Help &mdash; How To Start?</h1>

        <p>How to Make Money as an Eat Stop Eat Affiliate<p>

        <p>Thanks to the Internet, there are many, many ways to promote Eat Stop Eat and earn money as an Eat Stop Eat affiliate. Here are some of the best ways to promote Eat Stop Eat as an affiliate&hellip;</p>
        <p>But don&rsquo;t forget, one of the easiest ways to start promoting Eat Stop Eat as an affiliate is to simply replace regular links (i.e. anywhere you have listed www.eatstopeat.com on your site) with your special affiliate link that contains your nickname. (If you don&rsquo;t have an affiliate link, read the instructions in the &ldquo;How to Start&rdquo; column to get going). Make sure that every time you have eatstopeat.com listed on your site that the link is actually your affiliate link &#150; including your links page.</p>

        <p><b>1) Use my Eat Stop Eat articles to pre-sell the readers of your website, e-zine, or blog.</b> You can use as many of the Free Eat Stop Eat articles and newsletters <b>at <a href="http://www.bradpilon.com" target="_blank">www.bradpilon.com</a></b> as you want to pre-sell your audience. Not only can you include the articles in your newsletters, but you are also free to post the content on your website and in your blog. We’ll be adding hundreds of articles and newsletter columns on the benefits of Eat Stop Eat as well as instructional fat loss tips to help educate your readers on better ways to lose fat.</p>
        <p>You can use all of this content for free, and be sure to include your affiliate link at the end of the article and throughout the content. Please sign-up for our affiliate news and you&rsquo;ll be alerted when new articles and newsletters are added to the affiliate section for your use.</p>
        <p>One of the most important places to include your affiliate link is at the end of the article in the byline or resource box. Each article should include this, so that the reader knows who wrote the article (plus it allows you to add a little sales pitch).</p>

        <p><b>Resource Box:</b><br>
        Brad Pilon has his masters degree in human nutrition and is a Certified Strength &amp; Conditioning Specialist. His trademarked Eat Stop Eat program has been featured all over the Internet and been seen on national television, and has helped thousands of men and women around the world lose fat, gain muscle, and heal their relationship with food. For information on Eat Stop Eat that will help you burn fat while literally doing nothing, visit www.eatstopeat.com (use your affiliate link here)</p>

        <p><b>2) Use banner ads and book cover graphics.</b><br>
        Put the Eat Stop Eat banners, book covers and other graphics on your website. A picture of an e-book cover can significantly increase sales and click through rates. There are plenty of covers and banners in the affiliate section, and you can even design your own. Thanks to our graphic designer we are building a consistent brand identity for Eat Stop Eat . And when you use the graphics, make sure that your affiliate code is correctly included in the graphic&rsquo;s HTML code &#150; that way you&rsquo;ll be sure to get credited for the sale. Your click through and conversion rates should jump when you use the graphics, and especially when you combine the graphics with a pre-sell article or review.</p>

        <p><b>3) Write a book review</b><br>
        After you've read the e-book, write your own &quot;book review&quot; (in your own words) explaining what you liked about the book and why everyone should get a copy. A sincere review is most effective.</p>

        <p><b>4) Write about your own Success Story and endorse the book.</b><br>
        Success stories are an incredibly powerful tool to motivate your readers to purchase the same program that worked for you. Put together a testimonial or success story, including the details about your progress (fat lost, etc.) and photos, if possible. You can put this in your blog, on your own website, or in your newsletter (it&rsquo;s best to do all three!). Again, a sincere endorsement and a lot of details in your success story will help you make the most sales.</p>

        <p><b>5) Send a solo promotional email to your email list.</b> The best way to capture your list&rsquo;s attention is to dedicate an entire email to promoting Eat Stop Eat. However, you can only do this if you have an opt-in email list. Send a solo email containing what you think is the best Eat Stop Eat article (or your review &#150; this would work incredibly well) and include the Resource Box (above) at the end of the article. Insert your affiliate link throughout the article where appropriate.</p>
        <p>And one thing to remember; when it comes to Internet Marketing, your list is the key to your success. Build your newsletter list by placing an opt-in form on your website. So even if you don&rsquo;t have a product of your own, you can still make money with your own website and email newsletter list.</p>

        <p><b>6) Blog</b> &#150; Write about Eat Stop Eat in your blog. Include your clickbank hoplink in the promotional link and in your links section. You can also use the Eat Stop Eat affiliate articles, newsletters, and Q&rsquo;n&rsquo;A columns in your blog.</p>

        <p><b>7) Put Eat Stop Eat Ads in your e-zine.</b> You can use the text in the Resource Box to introduce your readers to Eat Stop Eat, or you can include the e-book covers and banner ads, or you might list eatstopeat.com as a sponsor. You can also write your own ads or use any of the ads in the Eat Stop Eat affiliate section. You can place the ads at any place in your newsletter.</p>
        <p>Don&rsquo;t forget, you don&rsquo;t need to have a product of your own to make money on the Internet. Just use our affiliate tools and content and you can start earning money as an Eat Stop Eat affiliate today!</p>
        <p>Contact me if you have any questions about the Eat Stop Eat affiliate marketing system.</p>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="rich-text">

    <p>
        For Questions or Comments Please contact help (at) eatstopeat (dot) com.<br>
        "The Eat Stop Eat method of using flexible, <a href="http://www.eatstopeat.com/">intermittent fasting</a> as a style of <a href="http://www.eatstopeat.com/">fasting for weight loss</a> and <a href="http://www.eatstopeat.com/">fat burning</a>!"<br>
        Eat Stop Eat is &copy; Medwin House, Inc. Ontario, Canada.<br>
    </p>

</div><!-- #footer -->

</body>
</html>
