<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Eat Stop Eat Affiliate Program Home</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

<?php require_once('../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="promo">
    <div class="inner" class="clearfix">
        <img class="logo" src="web/i/head-logo-alt.png" alt="">
        <div class="tag">Profit Center</div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

<? require('menu.php'); ?>

        <h1>Eat Stop Eat<br> Affiliate Program Home</h1>

        <p>Thank you for your interest in the Eat Stop Eat affiliate program. If you just subscribed to the Eat Stop Eat affiliate newsletter, the first thing you should do is to go check your email to confirm your subscription.</p>
        <p>If you haven't subscribed to the Eat Stop Eat affiliate newsletter yet, please take a moment to go to the affiliate newsletter subscription page and subscribe so you don't miss out on important affiliate promotions and tips. Special promotions are ONLY announced by email through the affiliate newsletter and unless you confirm your subscription, you will miss out on the newsletters.</p>
        <p>If you're already an affiliate and subscriber, then welcome back! Use the navigation links on the left hand side of this page to find what you are looking for - book covers, articles, audio interviews, videos, affiliate marketing information and more.</p>
        <p>We are always updating our affiliate area and adding more tools for you so check back regularly.</p>
        <p>Watch your email over the coming weeks for announcements about additional promotional tools we'll be adding to this affiliate area. If you have any questions, please don't hesitate to contact me personally.</p>
        <p>Sincerely,</p>
        <p>Brad Pilon, CSCS, MS<br> Author, Eat Stop Eat</p>

        <form class="signup" action="http://www.aweber.com/scripts/addlead.pl" method="post">
            <input type="hidden" name="meta_web_form_id" value="255542930">
            <input type="hidden" name="meta_split_id" value="">
            <input type="hidden" name="unit" value="eatstopeat_aff">
            <input id="redirect_2e7457dfb43f389aa4e23f11a44e94c9" type="hidden" name="redirect" value="http://www.eatstopeat.com/affiliates/index.shtml">
            <input type="hidden" name="meta_redirect_onlist" value="">
            <input type="hidden" name="meta_adtracking" value="">
            <input type="hidden" name="meta_message" value="1">
            <input type="hidden" name="meta_required" value="from">
            <input type="hidden" name="meta_forward_vars" value="0">

            <h2>Sign Up Here!</h2>
            <p class="hint">Just enter your name and email below to join the Eat Stop Eat Affiliate Newsletter.</p>

            <table>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name" value="" size="20"></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="text" name="from" value="" size="20"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="I want to join!"></td>
                </tr>
            </table>

        </form>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="rich-text">

    <p>
        For Questions or Comments Please contact help (at) eatstopeat (dot) com.<br>
        "The Eat Stop Eat method of using flexible, <a href="http://www.eatstopeat.com/">intermittent fasting</a> as a style of <a href="http://www.eatstopeat.com/">fasting for weight loss</a> and <a href="http://www.eatstopeat.com/">fat burning</a>!"<br>
        Eat Stop Eat is &copy; Medwin House, Inc. Ontario, Canada.<br>
    </p>

</div><!-- #footer -->

</body>
</html>
