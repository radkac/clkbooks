<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Eat Stop Eat Affiliate Agreement, Anti-Spam Policy and Earnings Disclaimer  </title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

<?php require_once('../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

<? require('menu.php'); ?>

        <h1>Eat Stop Eat<br> Affiliate Agreement, Anti-Spam Policy<br> and Earnings Disclaimer  </h1>

        <p>To: All Eat Stop Eat Affiliates</p>
        <p>From: Brad Pilon</p>

        <p>If you're like most affiliates or internet marketers, you may have a habit of skipping over long and boring affiliate agreements and anti spam policy pages. However, DO NOT skip this one! Why? Because WE DO NOT TOLERATE SPAM OR AFFILIATE PROGRAM ABUSE IN ANY WAY, SHAPE, OR FORM! If you break the rules, your clickbank account WILL BE SHUT DOWN AND YOU MAY FORFEIT YOUR EARNINGS! We will have clickbank shut down spammers if the rules are broken. Please read every word of this agreement so you know the rules!</p>

        <h2>EATSTOPEAT.COM AFFILIATE AGREEMENT</h2>

        <p>This agreement describes the terms and conditions for participation in the Eat Stop Eat e-book Affiliate Program. In this agreement, the term &ldquo;Affiliate&rdquo; refers to you (the applicant). In this agreement, Eat Stop Eat and/or Eatstopeat.com refers to the product owner and publisher, Eatstopeat.com, a product of Strength Works Inc.</p>
        <p>The Eat Stop Eat affiliate program (Eatstopeat.com) is administered entirely through a 3rd party affiliate management company called Clickbank (www.clickbank.com).</p>
        <p>To enroll in the affiliate program, if they have not done so already, all affiliates must sign up on the clickbank website. The Eatstopeat.com affiliate program is governed by all rules, regulations, terms and conditions dictated by clickbank.com&rsquo;s Affiliate Site.</p>
        <p>An affiliate account can be created by filling out and submitting the clickbank affiliate sign up form at clickbank.com. On that page, each new affiliate will be asked to choose a clickbank account name (also called a clickbank affiliate &lsquo;nickname&rsquo;), consisting of 5 - 10 letters and digits. This clickbank nickname will be used in the affiliate hoplink. When a prospective customer clicks on an affiliate's hoplink the destination website will be www.eatstopeat.com.</p>

        <h3>Commissions</h3>

        <p>Affiliates will receive 75% of the revenue as a commission from orders placed through properly coded Affiliate links (unless noted otherwise for a particular product), less the charges outlined in the: <u>ClickBank accounting policy page</u>. Commissions may change at the discretion of the product publisher, Eatstopeat.com.</p>
        <p>For a sale to generate a commission to an Affiliate, the customer must complete the order form and remit full payment for the product ordered through the clickbank secure order system. Word of mouth referrals will not result in an affiliate commission being generated. Commissions will only be paid on sales that are made when the customer clicks through qualified, correctly structured Affiliate links. Properly coded links are the sole responsibility of the affiliate.</p>

        <h3>Payment</h3>

        <p>ClickBank will send a check for the applicable commissions within the framework of their payment policies: <u>www.clickbank.com/paychecks.html</u>. If any order that generated an affiliate commission is returned by the customer, or if there are any returned checks or charge backs, the amount will be deducted by ClickBank from the next payment due to the affiliate.</p>

        <h3>Order Fulfillment</h3>

        <p>Eatstopeat.com (via clickbank) will be solely responsible for processing every e-book order placed by a customer on the Affiliate Site. Affiliates are not authorized to collect payments or sell any Eatstopeat.com products from other websites as a &ldquo;reseller&rdquo; and no &ldquo;resale&rdquo; rights are granted in ANY way. Affiliates are not authorized to sell any of these products on ebay or other auction sites Affiliates are not authorized to give away copies of any of these products. Eatstopeat.com will also be solely responsible for all customer service inquires. All affiliates understand and acknowledge that no physical products will be shipped. Eat Stop Eat is a PDF-format e-book, only available through download.</p>

        <h3>Customers</h3>

        <p>Customers who purchase products and services through the Affiliate Program will be deemed to be customers of Eatstopeat.com. Accordingly, all rules, policies, and operating procedures concerning customer orders and service will apply to those customers. We may change our policies and operating procedures at any time. Prices and availability of our products and services may vary from time to time. Eatstopeat.com policies will always determine the price paid by the customer.</p>

        <h3>Qualifying Sites</h3>

        <p>Clickbank.com reserves the right to refuse any site entry into the Affiliate Program based on site content. Sites that do not qualify for the Affiliate Program include sites which:</p>
        <p>Promote sexually explicit materials<br>
        Promote violence<br>
        Promote discrimination based on race, sex, religion, nationality, disability, sexual orientation, or age<br>
        Promote illegal activities<br>
        Infringe or otherwise violate any copyright, trademark, or other intellectual property rights.</p>

        <h2>CLICKBANK.COM AND EATSTOPEAT.COM ANTI-SPAM POLICY</h2>

        <p>Clickbank and Eatstopeat.com strictly prohibit its affiliates from using spam e-mail and other forms of Internet abuse (including forum spam and blog spam) to seek sales. Spam is defined as including, but not limited to, the following:</p>
        <p>Electronic mail messages addressed to a recipient with whom the sender does not have an existing business or personal relationship or is not sent at the request of, or with the express consent of, the recipient through an opt in subscription;</p>
        <p>Messages posted to Usenet, forums and message boards that are off-topic (unrelated to the topic of discussion), cross-posted to unrelated newsgroups, posted in excessive volume, or posted against forum/message board rules. Be conscious of forum rules! If a forum owner or moderator complains that an affiliate has spammed, the affiliate account may be permanently terminated after investigation.</p>
        <p>Content posted on free blog websites for the sole purpose of keyword spamming, or comments posted to legitimate blogs that violate the comment policy of the blog owner.</p>
        <p>Solicitations posted to chat rooms, or to groups or individuals via Internet Relay Chat or &ldquo;Instant Messaging&rdquo; system (such as ICQ);</p>
        <p>Certain off-line activities that, while not considered Spam, are similar in nature, including distributing flyers or leaflets on private property or where prohibited by applicable rules, regulations, or laws.</p>
        <p>Eatstopeat.com, in conjunction with Clickbank.com may undertake, at its sole discretion and with or without prior notice, the following enforcement actions:</p>
        <p>Account Termination: Upon the receipt of a credible complaint, Eatstopeat.com administration may immediately contact clickbank.com to investigate the complaint, and if necessary, will then terminate the affiliate account of the individual implicated in the abuse. Termination results in the immediate closure of the clickbank account, the loss of all referrals, and the forfeiture of any unpaid money on account. At clickbank&rsquo;s discretion, termination may not only result in being banned from the Eat Stop Eat affiliate program, but also being banned from ANY other clickbank affiliate programs.</p>
        <p>If you wish to report a violation of our Anti-Spam Policy, please forward all relevant evidence to our customer service department</p>
        <p>For more information on what spam is, why it is bad, and what can be done about it, visit:</p>

        <p>The Responsible Internet Commerce Site<br>
        <u>www.spam.abuse.net</u></p>

        <p>The Mail Abuse Prevention System, LLC<br>
        <u>www.mail-abuse.org</u></p>

        <p>The Network Abuse Clearinghouse<br>
        <u>www.abuse.net</u></p>

        <p>The Coalition Against Unsolicited Commercial Email<br>
        <u>www.cauce.org</u></p>

        <h3>Relationship of Parties</h3>

        <p>Affiliates are independent contractors, and nothing in this Agreement will create any partnership, joint venture, agency, franchise, sales representative, or employment relationship between the parties. Affiliates have no authority to make or accept any offers or representations on our behalf. Affiliates will not make any statement, whether on their sites or otherwise, that reasonably would contradict this statement.</p>

        <h3>Term and Termination</h3>

        <p>The term of this Agreement will begin when you accept and will end when terminated by either party. Either Eatstopeat.com or the affiliate may terminate this Agreement at any time, with or without cause, by giving the other party written notice of termination. Upon the termination of this Agreement for any reason, all licenses granted hereunder shall immediately terminate and you will immediately cease use of, and remove from Affiliate's Web Site, all links to the Eatstopeat.com websites, and all Eatstopeat.com trademarks and logos, other Eatstopeat.com Marks and all other materials provided in connection with this program.</p>

        <h3>Limitation of Liability</h3>

        <p>Eatstopeat.com will not be liable for indirect, special, or consequential damages (or any loss of revenue, profits, expenditures or data) arising in connection with this Agreement or the Program, even if we have been advised of the possibility of such damages. Further, our aggregate liability arising with respect to this Agreement and the Program will not exceed the total commissions paid or payable to the affiliate under to this Agreement</p>

        <h3>Disclaimers</h3>

        <p>We make no express or implied warranties or representations with respect to the Affiliate Program or an affiliate&rsquo;s potential to earn income from the Affiliate Program. In addition, we make no representation that the operation of the Eatstopeat.com websites or the Affiliate links will be uninterrupted or error-free, and Eatstopeat.com will not be liable for the consequences of any interruptions or errors.</p>

        <h3>Assignment</h3>

        <p>You may not assign this Agreement, by operation of law or otherwise, without our prior written consent. Subject to that restriction, this Agreement will be binding on, inure to the benefit of, and enforceable against the parties and their respective successors and assigns.</p>

        <h3>Miscellaneous</h3>

        <p>Our failure to enforce your strict performance of any provision of this Agreement will not constitute a waiver of our right to subsequently enforce such provision or any other provision of this Agreement.</p>
        <p>If any of the provisions of this Agreement are determined by a court to be unenforceable, they shall be severed from this Agreement, and the remaining provisions shall remain in full force and effect.</p>
        <p>By signing up with ClickBank, you acknowledge that you have read this agreement and agree to all its terms and conditions. You have independently evaluated this program and are not relying on any representation, guarantee or statement other than as set forth in this agreement.</p>

        <h2>EATSTOPEAT.COM AFFILIATE PROGRAM EARNINGS DISCLAIMER</h2>

        <p>Any earnings or income statements, or earnings or income examples mentioned on this website or in emails sent on behalf of Eatstopeat.com, are only estimates of what we think you could earn or based on what other affiliates have actually earned There is no assurance you will do as well. If you rely upon any sales figures we provide, you must accept the risk of not doing as well.</p>
        <p>Where specific income figures are used, and attributed to an individual or business, those persons or business have earned that amount. There is no assurance you will earn the same amount. If you rely upon any figures we provide, you must accept the risk of not doing as well.</p>
        <p>Any and all claims or representations as to the income earnings of affiliates from this website, are not to be considered as average earnings. There can be no assurance that any prior successes, or past results, as to the income earnings, can be used as an indication of your future success or results.</p>
        <p>Monetary and income results are based on many factors. We have no way of knowing how well you will do, as we do not know you, your background, your work ethic, or your business skills or business practices. Therefore, we do not guarantee or imply that you will get rich, that you will do as well, or make any money at all. There is no assurance you will do as well. If you rely upon our figures, you must accept the risk of not doing as well.</p>
        <p>Internet businesses and earnings derived wherefrom, have unknown risks involved and are not suitable for everyone. Making decisions based on any information presented in our products, services, or website, should be done only with the knowledge that you could experience losses, or make no money at all.</p>
        <p>All products, information and services by our company are for educational and informational purposes only. Use caution and seek the advice of qualified professionals. Check with your accountant, lawyer or professional advisor, before acting on this or any information.</p>
        <p>Users of our products, services and website are advised to do their own due diligence when it comes to making business decisions and all information, products and services that have been provided should be independently verified by your own qualified professionals. Our information, products, and services on this website should be carefully considered and evaluated, before reaching a business decision, on whether to rely on them.</p>
        <p>You agree that Eatstopeat.com is not responsible for the success or failure of your business decisions relating to any information presented by our company, or our company products or services.</p>
        <br>
        <p>Sincerely,<br>
        <br>
        Brad Pilon, CSCS, MS<br>
        Author, Eat Stop Eat</p>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="rich-text">

    <p>
        For Questions or Comments Please contact help (at) eatstopeat (dot) com.<br>
        "The Eat Stop Eat method of using flexible, <a href="http://www.eatstopeat.com/">intermittent fasting</a> as a style of <a href="http://www.eatstopeat.com/">fasting for weight loss</a> and <a href="http://www.eatstopeat.com/">fat burning</a>!"<br>
        Eat Stop Eat is &copy; Medwin House, Inc. Ontario, Canada.<br>
    </p>

</div><!-- #footer -->

</body>
</html>
