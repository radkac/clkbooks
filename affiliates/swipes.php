<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Our Books</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

<?php require_once('../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

<? require('menu.php'); ?>

        <h1>Our Books</h1>

        <div class="insertion right">
            <center>
                <a href="https://drive.google.com/open?id=1BcP9iTimwcqvLrmph06M4BoRqE6vpzR2nvaxv1WAy9Q" target="_blank">
                    <img class="mobile-half-width" src="web/i/products/ese-ecover-4mdl-v2.jpg" width="250" alt=""><br><br>
                    Click here to access email creatives
                </a>
            </center>
        </div>
        <p><b>Eat Stop Eat</b> &mdash; The original “2 day diet.” Learn why dieting every day will ruin your fat loss. And how a simple nutritional strategy with only ONE rule is the key to rapid fat loss and lifelong weight management.</p>
        <p align="center"><input type="text" value="http://XXXXX.eatstopeat.hop.clickbank.net" size="50" style="padding: 5px 10px;"></p>
        <p align="center"><small>(XXXX = the affiliate's clickbank id)</small></p>

        <div class="clearer"></div>
        <hr>

        <div class="insertion left">
            <center>
                <a href="https://docs.google.com/document/d/1ugU2aigNwVVzBwH5XvJsPEFcjr-_QKmav7QLIP9mhA4/edit?usp=sharing" target="_blank">
                    <img class="mobile-half-width" src="web/i/products/Fat-Cell-Killer-3D.jpg" width="250" alt=""><br><br>
                    Click here to access email creatives
                </a>
            </center>
        </div>
        <p class="body"><b>Fat Cell Killer</b> &mdash; The ONLY Known Protocol To Permanently KILL And Flush Away Your Fat Cells &mdash; AND Guarantee Permanent &amp; Irreversible Fat Loss.</p>
        <p align="center"><input type="text" value="http://xxxxx.eatstopeat.hop.clickbank.net/?page=clkfcklf" size="50" style="padding: 5px 10px;"></p>
        <p align="center"><small>(XXXX = the affiliate's clickbank id)</small></p>

        <div class="clearer"></div>
        <hr>

        <div class="insertion right">
            <center>
                <a href="https://drive.google.com/open?id=1gRWOD1BiRhu1YWuNYXPScqDc6HNDf86w1FdiE8mB0M8" target="_blank">
                    <img class="mobile-half-width" src="web/i/products/eCover-GBBB.png" width="250" alt=""><br><br>
                    Click here to access email creatives
                </a>
            </center>
        </div>
        <p><b>Good Belly Bad Belly</b> &mdash; How to protect yourself from the risk of obesity-promoting toxins created by the foods you eat. Weight loss meets healthy eating in this brand new book from Brad.</p>
        <p align="center"><input type="text" value="http://XXXX.eatstopeat.hop.clickbank.net/?page=gbbb" size="50" style="padding: 5px 10px;"></p>
        <p align="center"><small>(XXXX = the affiliate's clickbank id)</small></p>

        <div class="clearer"></div>
        <hr>

        <div class="insertion left">
            <center>
                <a href="https://docs.google.com/document/d/1_nBIibnVaix3bnE1D4E4GvJaJAOPJBj67Z89GEl7UIE/edit?usp=sharing" target="_blank">
                    <img class="mobile-half-width" src="web/i/products/progressions3d.jpg" width="250" alt=""><br><br>
                    Click here to access email creatives
                </a>
            </center>
        </div>
        <p class="body"><b>Progressions</b> &mdash; The Official Workout Companion to the Bestselling Book, Eat Stop Eat. Get the Same Results as the Pros (or Cheaters)!</p>
        <p align="center"><input type="text" value="http://xxxxx.eatstopeat.hop.clickbank.net/?page=clkprogressions37" size="58" style="padding: 5px 10px;"></p>
        <p align="center"><small>(XXXX = the affiliate's clickbank id)</small></p>

        <div class="clearer"></div>
        <hr>

        <div class="insertion right">
            <center>
                <a href="https://docs.google.com/document/d/1xk7x1qUeEna-REY9ewvurYoSLccKDWKv1lxW9tp7GjY/edit?usp=sharing" target="_blank">
                    <img class="mobile-half-width" src="web/i/products/eCover-ThinAir.png" width="250" alt=""><br><br>
                    Click here to access email creatives
                </a>
            </center>
        </div>
        <p class="body"><b>Thin Air</b> &mdash; Exposure to high levels of a certain greenhouse gas can manifest itself as chronic fatigue, lethargy, migraines, inexplicable weight gains, irritability, exhaustion, and sleep trouble. The Thin Air, through its thoroughly researched and tested protocol, aims to relieve these symptoms in order to help people maximize their potential by taking advantage of newly found energy.</p>
        <p align="center"><input type="text" value="http://XXXX.eatstopeat.hop.clickbank.net/?page=clkta" size="50" style="padding: 5px 10px;"></p>
        <p align="center"><small>(XXXX = the affiliate's clickbank id)</small></p>

        <div class="clearer"></div>
        <hr>

        <div class="insertion left">
            <center>
                <a href="https://docs.google.com/document/d/1uHlFvrUQdKyng9Pj_aeXQljx1O500bjWtFwWXsExJ3s/edit?usp=sharing" target="_blank">
                    <img class="mobile-half-width" src="web/i/products/eCover-HMP.png" alt=""><br><br>
                    Click here to access email creatives
                </a>
            </center>
        </div>
        <p class="body"><b>How Much Protein</b> &mdash; Learn the ultimate answer to how much protein YOU need to reach your goal. You may be surprised. Yet the research is clear.</p>
        <p align="center"><input type="text" value="http://XXXX.eatstopeat.hop.clickbank.net/?page=clkhmp" size="50" style="padding: 5px 10px;"></p>
        <p align="center"><small>(XXXX = the affiliate's clickbank id)</small></p>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="rich-text">

    <p>
        For Questions or Comments Please contact help (at) eatstopeat (dot) com.<br>
        "The Eat Stop Eat method of using flexible, <a href="http://www.eatstopeat.com/">intermittent fasting</a> as a style of <a href="http://www.eatstopeat.com/">fasting for weight loss</a> and <a href="http://www.eatstopeat.com/">fat burning</a>!"<br>
        Eat Stop Eat is &copy; Medwin House, Inc. Ontario, Canada.<br>
    </p>

</div><!-- #footer -->

</body>
</html>
