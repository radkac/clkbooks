<?php
require_once('../../checkreturning.php');
require_once('../../fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Free Advocate Trial</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web-pa/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web-pa/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web-pa/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="web-pa/s/mobile.css?v=3">

	<?php require_once('../../eztrackingcode.php'); ?>

</head>
<body>

<div id="layout" class="clearfix">

    <div id="content" class="clearfix">
        <h1>Account Setup Step 1:</h1>
        <div id="warning">Warning: Leaving The Page Or Hitting The Back Button Will Cause Your Account Setup To Fail</div>
        <h2 align="center">Please Confirm: <b>Do You Want to Give Up Your VIP Newsletter Access?</b></h2>

        <p>Included with your How Much Protein purchase is 21 days of FREE access to my premium VIP Newsletter service called the Advocate. You have also secured your monthly membership at the discounted price of only $7 a month once your free 21 day trial ends.</p>
        <p>However, this free offer is only available to a limited number of customers. So before we complete your order I need to make sure you want to keep your premium access.</p>
        <p>Your Advocate e-mail subscription gives you an insider’s look at all my latest insights on the research into nutrition, exercise, weight loss and health. It’s where we bust the myths and give you the most up-to-date methods you can apply immediately as they are released.</p>
        <p>So you can see why your free access is so valuable. And why I need to make sure you are interested in keeping your spot.</p>
        <p>To avoid losing your spot and your 21 day free trial, just click the <i>‘I’ll Keep My Access’</i> button below to protect your access and proceed with your order.</p>
        <br>
        <p align="center"><a id="accept" class="button-cancel" href="http://130.eatstopeat.pay.clickbank.net/?cbur=a&cbrblaccpt=true">No, I’ll Keep My Access</a></p>
        <br>
        <hr>

        <p>Yes, I want to give away my free access to the VIP Advocate Newsletter. I understand this is my only chance to get free access to this insider information and the discounted price of only 7 dollars a month. <a href="http://130.eatstopeat.pay.clickbank.net/?cbur=d">Yes, cancel my access.</a></p>

    </div><!-- #content -->

</div><!-- #layout -->

</body>
</html>
