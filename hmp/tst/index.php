<?php require_once('../../prices.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>How Much Protein &mdash; Your Answer To The Most Frustrating Nutrition Myth</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=11">

    <!-- jQuery + UI -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/hmp/web/i/fb-og-hmp.png">
	<meta property="og:description" content="How Much Protein: Revealed: Your Answer To The Most Frustrating Nutrition Myth Of The Past Decade. “How Much And What Kind Of Protein Do You REALLY Need To Eat Every Day?">
	<meta property="og:title" content="Check Out What I'm Reading...">

<script>

$(function(){

    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });

    $(".returns A").click(function(){
        var bodyWidth = $("BODY").width();
        var maxDlgWidth = 640;
        popupWidth = (bodyWidth > maxDlgWidth ? maxDlgWidth : bodyWidth-40);
        $('#guaranteePopup').dialog({
            modal: true,
            width: popupWidth,
            height: 'auto',
            resizable: false,
            draggable: false,
            closeOnEscape: true,
            create: function (event, ui) {
                $("#exit-popup .close").click(function () { $('#guaranteePopup').dialog("close") });
            },
            open: function () {
                $(".ui-widget-overlay").click(function () { $('#guaranteePopup').dialog("close") });
            }
        });
    });

});

</script>

<?php require_once('../../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="breadcrumbs">
        Books › Health &amp; Weight Loss Books › New Releases
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <h1>How Much Protein</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating"><img src="web/i/4nh-of-5-stars-rating.gif" alt=""> <small>(based on 505 reviews from Goodreads)</small></p>
            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img class="main-ecover mobile-half-width" src="web/i/ecover.png" width="100%" alt="">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>Special CLK*Books Price</h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v"><strike>$19.99</strike></td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>$<?php echo $hmp; ?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$9.99</b> (50% OFF)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?php echo $hmp; ?></td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://126.eatstopeat.pay.clickbank.net/?cbfid=27983&cbskin=18285&vtid=clkwg2"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>

                    <p class="returns">
                        <span>Returns are easy</span><br>
                        <a class="guarantee-popup" href="javascript:;">View Our 60-Day Guarantee</a>
                    </p>

                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                <h1>How Much Protein</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating"><img src="web/i/4nh-of-5-stars-rating.gif" alt=""> <small>(based on 505 reviews from Goodreads)</small></p>
                </div>

                <h2><i>Revealed: Your Answer To The Most Frustrating Nutrition Myth Of The Past Decade</i></h2>

                <p class="cl-red"><i>“How Much And What Kind Of Protein Do You REALLY Need To Eat Every Day?”</i></p>
                <p>I felt a flush creep across my cheeks and my ears go red&hellip;</p>
                <p>I was head of research and development for a top supplement company. I thought I was a pretty big deal. And I was 100% bought into what we were selling&hellip;</p>
                <p>That’s when one of the top nutrition scientists in the world took me down a peg in front of everybody, and made me feel like a stupid kid butting in on an adult conversation.</p>
                <p>We were at an important meeting in Europe with some of the smartest folks in the industry. I thought I’d score some big points when I shared my idea to add protein to one of our supplements. This guy just smirked and said&hellip;</p>
                <p><i>“Brad, if you <b>BELIEVE</b> in protein, then we will add it into the formula”.</i></p>
                <p>Everyone chuckled and I just sunk deeper into my chair. I was obviously the butt of an inside joke. I guess I was the only one in the room who actually believed what we were telling folks like you about protein. Which is when I vowed to uncover the truth&hellip;</p>

                <div class="read-more-content collapsed hidden">

                <p>Listen: I was so convinced by my own hype that I believed it all &mdash; hook, line and sinker. And maybe you’ve been brainwashed into some of these myths too. Do you recognize any of these?&hellip;</p>

                <ul class="check green">
                    <li>You catch yourself wringing your hands with worry when you don't have protein with every meal</li>
                    <li>You tug at  your collar when you realize none of the choices at the restaurant has a good sized portion of protein</li>
                    <li>Your stomach flutters after exercise as you rush to gulp down your protein shake at <i>“just the right time”</i></li>
                    <li>You bite your lip in worry over whether you are eating “complete” proteins</li>
                    <li>You wake up and race to eat breakfast for fear of losing muscle mass</li>
                    <li>You blow too much cash at the store because your heart races at the thought of not getting enough protein with every meal</li>
                    <li>And in the extreme perhaps you spend hours each week cooking meat and putting it in plastic containers &mdash; carrying it around with you so you make sure to have protein every 3 hours</li>
                    <li>Maybe you even think cheese and peanut butter are good protein choices&hellip;</li>
                </ul>

                <p>If you recognize any of these symptoms, you're suffering from <b>Protein Guilt</b> just like I was. And giving in to Protein Guilt can cripple your health, your weight, your lifestyle and certainly your bank account.</p>
                <p>Once I discovered the truth about protein and I understood <u>the inside joke</u>, there’s no way I was going to let you be the next victim. That’s why I wrote this short book explaining EXACTLY how much protein YOU personally need to eat. And why&hellip;</p>
                <p>You’ll discover exactly WHEN a high protein diet is a good idea. And when it definitely is NOT. And you’ll see why protein powders are useful at precise times, yet only if you use the right kinds.</p>
                <p>And the best part is&hellip; this is not my opinion. Every recommendation in this short and precise book is based on solid science.</p>
                <p>The book is simply called <u><i>How Much Protein</i></u>. And we’ve sold thousands of copies at the original retail price of $37. However&hellip;</p>
                <p>Because of my exclusive new partnership with CLK*Books.com, you’re getting the complete book today for only $10.</p>

                <div class="purchase-holder">
                    <div class="insertion center purchase">

                        <img class="ecover mobile-2of3-width" src="web/i/ecover.png" alt="">

                        <table align="center" class="costs" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>List Price:</td>
                                <td><strike>$19.99</strike></td>
                            </tr>
                            <tr>
                                <td><b>CLK*Books Price:</b></td>
                                <td><b>$<?php echo $hmp; ?>*</b></td>
                            </tr>
                            <tr>
                                <td>You Save</td>
                                <td>$9.99 (50% OFF)</td>
                            </tr>
                            <tr>
                                <td colspan="2">*Includes free international wireless delivery</td>
                            </tr>
                            <tr class="total">
                                <td class="k">Total</td>
                                <td class="v">$<?php echo $hmp; ?></td>
                            </tr>
                        </table>

                        <a class="button" href="http://126.eatstopeat.pay.clickbank.net/?cbfid=27983&cbskin=18285&vtid=clkwg2">Buy Now</a>

                        <p class="returns">
                            <span>Returns are easy</span><br>
                            <a class="guarantee-popup" href="javascript:;">View Our 60-Day Guarantee</a>
                        </p>

                    </div>
                </div>

                <p>Here’s a quick preview of what you’ll discover&hellip;</p>

                <p align="center"><b>How Much Protein:</b> A Behind-The-Scenes Look At One Of The Fitness Industry’s Most Well-Kept Secrets</p>
                <p align="center"><i>(From an ex-protein supplement developer and research scientist who agreed to 'reveal all')</i></p>
                <p>* The <b>“Got Milk” Effect</b> explains why you could need as little a 1/2 glass of milk to supply your daily protein needs?</p>
                <p>* The <b>Fed Fallacy</b> demonstrates why following the recommended amounts of protein from federal agencies &mdash; like AMDA or RDA &mdash; is completely wrong!</p>
                <p>* Using the <b>Portion Predictor</b> you’ll understand why phony formulas like “2.2 grams of protein per pound of bodyweight” are WRONG &mdash; and how to figure out how much protein is right for you&hellip;</p>
                <p>* The <b>Gas Reducer</b> strategy explains why protein mismanagement gives you an upset stomach and makes you "gassy" — and how to prevent it&hellip;</p>
                <p>* The <b>Fat-Burn Flip Flop</b> explains the sad truth that you can actually burn LESS calories when you increase your daily protein (I PROVE this fact with scientific research on page 29)</p>
                <p><b><i>Warning: This Next Fact WILL Change The Way You Think About Protein Forever!</i></b></p>
                <p>* Discover how the <b>Magic Protein Machine</b> allows you to eat 50 grams of protein and DIGEST 150 grams of protein… in the same day! (this is something the supplement companies will NEVER tell you)</p>
                <p>* And the <b>No Meat Muscle Maker</b> technique demonstrates exactly how vegetarians can easily gain JUST as much lean muscle as people eating meat every day!</p>
                <p>* <b><span class="bg-yellow">On page 54 I reveal the "<u>Magic Number</u>"</span> &mdash; the amount of protein the average person needs to maintain each day to maximize health and fitness</b></p>
                <p>* Discover the 1 <b>Muscle Master Supplement</b> I ALWAYS recommend that can increase your muscle gains by as much as 57% in just 6 weeks!</p>
                <p>* Watch as I show you the <b>Upside Down Protein Pyramid</b> study where people who took in LESS protein gained MORE muscle!</p>
                <p>* Discover the <b>Protein Ceiling</b> calculation that reveals the TRUTH about how much protein your body can actually absorb in one sitting&hellip;</p>
                <p>Remember: <b>Over 100 research studies</b> &mdash; included in the book &mdash; prove what I'm saying is actually true!</p>
                <p>PLUS, I continually update How Much Protein to bring you the latest research and practical tricks you can use daily. As the scientific community learns more, I bring it back to you and tell it like it is.</p>
                <p>And you have <b>LIFETIME free access</b> to all new updated editions of the book.</p>
                <p>Here are some highlights from the NEW UPDATED EDITION&hellip;&hellip;</p>
                <p><span class="bg-yellow">NEW!</span> &mdash; The discovery of the <b>Switchback Effect</b> could mean weight training actually REDUCES your need for protein? (Page 102)</p>
                <p><span class="bg-yellow">NEW!</span> &mdash; See how the media is duping you by reporting research that uses the <b>Patsy Portion</b> for high protein diet studies (Page 78)</p>
                <p><span class="bg-yellow">NEW!</span> &mdash; Find out how a handful of rare muscle building ‘<b>Ultra-Responders</b>’ screw up protein recommendations for folks like you and me</p>
                <p><span class="bg-yellow">NEW!</span> &mdash; And&hellip; is there a right and wrong time for for you to use a high protein diet?</p>

                <div class="purchase-holder">
                    <div class="insertion center purchase">

                        <img class="ecover mobile-2of3-width" src="web/i/ecover.png" alt="">

                        <table align="center" class="costs" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>List Price:</td>
                                <td>$19.99</td>
                            </tr>
                            <tr>
                                <td>CLK*Books Price:</td>
                                <td><b>$<?php echo $hmp; ?>*</b></td>
                            </tr>
                            <tr>
                                <td>You Save</td>
                                <td>$9.99 (50% OFF)</td>
                            </tr>
                            <tr>
                                <td colspan="2">*Includes free international wireless delivery</td>
                            </tr>
                            <tr class="total">
                                <td class="k">Total</td>
                                <td class="v">$<?php echo $hmp; ?></td>
                            </tr>
                        </table>

                        <a class="button" href="http://126.eatstopeat.pay.clickbank.net/?cbfid=27983&cbskin=18285&vtid=clkwg2">Buy Now</a>

                        <p class="returns">
                            <span>Returns are easy</span><br>
                            <a class="guarantee-popup" href="javascript:;">View Our 60-Day Guarantee</a>
                        </p>

                    </div>
                </div>

                <ul class="checkmarks">
                    <li>Thought-Provoking Books</li>
                    <li>Smart Weight Management</li>
                    <li>Energizing Strategies</li>
                </ul>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>Special CLK*Books Price</h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>$<?php echo $hmp; ?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$9.99</b> (50% OFF)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?php echo $hmp; ?></td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://126.eatstopeat.pay.clickbank.net/?cbfid=27983&cbskin=18285&vtid=clkwg2"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>

                    <p class="returns">
                        <span>Returns are easy</span><br>
                        <a class="guarantee-popup" href="javascript:;">View Our 60-Day Guarantee</a>
                    </p>

                </center>
            </div>


        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>



<!-- Guarantee Modal (begin) -->
<div id="guaranteePopup" class="hidden">
    <ul>
        <li>
            <h4><span>1</span> Email Our 24-Hour Customer Service</h4>
            <p align="center"><img src="web/i/guarantee-a.png" alt="CLK"></p>
        </li>
        <li>
            <h4><span>1</span> Receive Quick Confirmation</h4>
            <p align="center"><img src="web/i/guarantee-b.png" alt="CLK"></p>
        </li>
        <li>
            <h4><span>1</span> Your Payment Method Is Directly Refunded</h4>
            <p align="center"><img src="web/i/guarantee-b.png" alt="CLK"></p>
        </li>
    </ul>
</div>



<? /*
<!-- Guarantee Modal (begin) -->
<div id="guaranteePopup" class="hidden">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <span class="guarantee-step">1</span><span class="guarantee-label">Email Our 24-Hour Customer Service</span>
            </div>
            <div class="row">
                <img src="web/i/guarantee-a.png" alt="CLK">
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <span class="guarantee-step">2</span><span class="guarantee-label">Receive Quick Confirmation</span>
            </div>
            <div class="row">
                <img src="web/i/guarantee-b.png" alt="CLK">
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <span class="guarantee-step">3</span><span class="guarantee-label">Your Payment Method Is Directly Refunded</span>
            </div>
            <div class="row">
                <img src="web/i/guarantee-c.png" alt="CLK">
            </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Guarantee Modal (end) -->
*/ ?>


</body>
</html>
