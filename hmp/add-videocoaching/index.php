<?php
require_once('../../checkreturning.php');
require_once('../../fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Enjoy Faster &amp; Easier Health, Wellness &amp; Weight Loss Results With My Online VIDEO Coaching</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

	<script>
	function showHideHelp() {
		e = document.getElementById('help-opts');
		if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
	}

	function resizeVideoPlayer() {
		var new_width = $('#video_player').width();
		var new_height = Math.floor(411*new_width/730);
		$('#video_player').height(new_height);
		//alert(new_width+' '+new_height);
	}

	function showWarning() { $('.warning-message').fadeTo('slow',1) }
	function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
	$(function(){
		// show warning message
		setTimeout('showWarning()', 1500); // 1500
		// show the rest of the content
		setTimeout('showContent()', 3000); // 3000
        // show all hidden items fast in case of "cbur" existence
        <?if(isset($_GET['cbur'])):?>
            $('.hidden').fadeTo('fast',1);
        <?endif;?>
		setTimeout('resizeVideoPlayer()', 3100);
	});

	$(window).resize(function() {
		resizeVideoPlayer();
	});

	</script>

	<?php require_once('../../eztrackingcode.php'); ?>

</head>
<body class="no-deco">

<div id="header" class="clearfix">
    <div class="inner clearfix">

        <img class="head-banner mobile-full-width" src="/web/i/ese-upsell-banner-new.png" alt="">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>
            <b>
            Enjoy Faster &amp; Easier Health, Wellness &amp; Weight Loss<br>
            Results With <u>Online VIDEO Coaching</u><br>
            From Eat Stop Eat Author Brad Pilon
            </b>
        </h1>

        <h2><i>(And Get <u>Clickbooks Total Access</u> <span class="cl-red">FREE</span> for LIFE)</i></h2>

        <hr>

        <p>On this page only you have a one-time opportunity to add exclusive Video Coaching to your order. This offer is only for new ClickBooks customers and it gives you access to exclusive <b>Online Video Coaching</b> for a low one-time payment so you <u><b>never pay</b></u> the regular monthly fee.</p>
        <p>And as an extra bonus you are receiving FREE <u>lifetime</u> enrolment to <b>ClickBooks Total Access</b> where you’ll find all the extra insider content only available through this exclusive service.</p>
        <p>Remember, this is only available right here on this page. Take a second to review the details below and use the button on this page to add Online Video Coaching and FREE Total Access to your order today.</p>

        <hr>

        <div class="insertion right mobile-full-width" style="padding:0px;margin-top:30px;width:35%;background:none;">
            <img class="mobile-full-width" src="web/i/portal-gif-animation-2.gif" width="100%" alt=""><br>
            <p align="center"><i>Searchable Facts In Five Videos Provide Easy Answers In Seconds</i></p>
        </div>

        <h2>Your Online Video Coaching</h2>

        <p>You’ll have access to my <u>Facts In Five videos</u> &mdash; where I bring you into my living room and share my no-holds-barred opinion on anything and everything members are curious about&hellip;</p>
        <p>Enjoy the <b>searchable</b> bank of existing videos. Then&hellip;</p>
        <p><b>Just ask me</b> all your questions. I’ll answer them for you via video and post them to your Online Video Coaching portal. Easy as that.</p>
        <p>And you have <b>LIFETIME access</b> to this service when you enroll today. That’s right, you will <b>never receive a monthly rebill</b>. Ever!</p>

        <hr>

        <h2>
            PLUS, Your FREE <u>ClickBooks Total Access</u><br>
            Also Gives You All This (And More)&hellip;
        </h2>

        <ul class="check green">
            <li>You’ll be automatically enrolled in the <u>university-level <b>nutrition course</b></u> that I previously charged more than $700 for &mdash; absolutely free&hellip;</li>
            <li>You’ll get access to my growing library of <u><b>exercise videos</b></u> in case you want to enhance your results with a workout program&hellip;</li>
            <li>You’ll have all <u>my best podcasts and <b>premium interviews</b></u> collected in one place&hellip;</li>
            <li>And get this &mdash; you’ll also receive a couple of <u><b>FREE books</b></u> that complement your purchase of <i>Good Belly, Bad Belly</i>. This is my gift to you for taking action today and you can see the details below&hellip;</li>
        </ul>
        <p>Remember: you get all this for life and there are <u><b class="bg-yellow">zero monthly dues</b> and no hidden fees</u> &mdash; ever&hellip;</p>

        <hr>

        <h2>Limited Time Bonus: <u>2 FREE Books</u> For YOU Today</h2>

        <p>I now sell both these books separately at a total value of <b>$89</b>. However because I think they are a perfect complement to what you’ll discover in <b>How Much Protein</b>, I want you to have them absolutely FREE today&hellip;</p>

        <br><br class="mobile-hidden">
        <div class="bonusbox clearfix">
            <div class="insertion right" style="margin-top:0;">
                <img class="mobile-half-width" src="web/i/products/eat-stop-eat-3d-new.jpg" width="165" alt="">
            </div>
            <h3>
                <img src="web/i/zhtml/ul-check-green.png" alt="">
                Eat Stop Eat
                <strike class="cl-red">&nbsp;$39.95&nbsp;</strike>
                <span class="cl-green">FREE!</span>
            </h3>
            <p>The original “2 day diet”. Learn why dieting every day will ruin your fat loss. And how a simple nutritional strategy with only ONE rule is the key to rapid fat loss and lifelong weight management.</p>
        </div>
        <div class="bonusbox clearfix">
            <div class="insertion right" style="margin-top:0;">
                <img class="mobile-half-width" src="web/i/products/gbbb-ecover.png" width="160" alt="">
            </div>
            <h3>
                <img src="web/i/zhtml/ul-check-green.png" alt="">
                Good Belly, Bad Belly
                <strike class="cl-red">&nbsp;$49.00&nbsp;</strike>
                <span class="cl-green">FREE!</span>
            </h3>
            <p>How to protect yourself from the risk of obesity-promoting toxins created by the foods you eat. Weight loss meets healthy eating in this brand new book from Brad.</p>
        </div>

        <hr>

        <h1 style="font-size:72px !important;">
            <b>
            ZERO Monthly Dues and<br>
            NO Hidden Fees (ever)&hellip;
            </b>
        </h1>

        <p>You get all 8 weeks of Online Coaching &mdash; regularly $37 per week, a $296 value &mdash; for just <u>a single one time payment of $37</u>.</p>
        <p>And <u><b>you’ll NEVER be charged for your LIFETIME free enrollment to ClickBooks Total Access</b></u>!</p>
        <p>NO monthly fees.</p>
        <p>NO rebills&hellip; EVER.</p>
        <p><b>Remember there’s no risk.</b> As always, your full investment is covered by my 100% iron-clad money-back satisfaction guarantee.</p>
        <p>Just use the button below to secure this once-in-a-lifetime opportunity to guarantee your results&hellip;</p>

        <table class="pricing">
            <tr>
                <td class="reg">
                    At The Regular<br>
                    WEEKLY Price<br>
                    <center><strike>&nbsp;$296&nbsp;</strike></center>
                </td>
                <td>&nbsp;</td>
                <td class="inv">
                    <b class="bg-yellow">One</b> Single <u>LIFETIME</u><br>
                    Investment<br>
                    <center><em>$37</em></center>
                </td>
            </tr>
        </table>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="http://131.eatstopeat.pay.clickbank.net/?cbur=a">Yes! I Want 8 Weeks of Coaching<br> For the Price of Just 1 PLUS Total<br> Access &mdash; Add it to my order!<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="http://131.eatstopeat.pay.clickbank.net/?cbur=a">Yes! I Want 8 Weeks of Coaching For the Price<br> of Just 1 PLUS Total Access &mdash; add it to my order!</a></span>
            </p>

            <hr>

            <p class="discard">No thanks. I understand that this is my only chance to secure access to 8 weeks of Online Coaching for the price of 1 — plus Lifetime enrollment in Total Access — at the steeply discounted price just for new customers. I know that once I leave this page I’ll lose out on this deal. I’m ok with that and I’d rather go it alone. <a href="http://131.eatstopeat.pay.clickbank.net/?cbur=d">I’ll pass.</a>.</p>
        </div>



    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2017 &copy; CLKbooks.com<br>
        For support please contact support [at] clkbooks.com<br>
    </p>

</div><!-- #footer -->

</body>
</html>
