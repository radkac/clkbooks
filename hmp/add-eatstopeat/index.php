<?php
require_once('../../checkreturning.php');
require_once('../../fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Eat STOP Eat: Discover The Future Of (Not) Dieting</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=5">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=5">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=5">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function resizeVideoPlayer() {
	var new_width = $('#video_player').width();
	var new_height = Math.floor(411*new_width/730);
	$('#video_player').height(new_height);
	//alert(new_width+' '+new_height);
}

function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
	setTimeout('resizeVideoPlayer()', 3100);
});

$(window).resize(function() {
	resizeVideoPlayer();
});

</script>

	<?php require_once('../../eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>
            Discover The Future Of (<u class="cl-red">Not</u>) Dieting
        </h1>

        <div class="topbox">
            <p>On this page only you have access to an <b>exclusive new ClickBooks price</b> negotiated directly with the author of <i>How Much Protein</i>. Review the details below before leaving this page.</p>
        </div>

        <p>I want to be the first to congratulate you on choosing to read <i>How Much Protein</i> &mdash; because this book will clear up all the confusion about how to reach your optimal weight and health without spending a fortune on fancy protein combinations&hellip;</p>
        <p>Which is why we’ve got an exciting option to share with you if you’re interested in accelerating your weight loss even more, making weight maintenance easy, and improving all your markers of good health as quickly as possible.</p>

        <h2>The Future Of Weight Loss &amp; Weight Maintenance WITHOUT Dieting</h2>

        <p>Once you start applying the tricks in <i>How Much Protein</i> you’ll see that eating becomes a whole lot simpler.</p>
        <p>Yet wouldn’t it be nice if you could manage your weight WITHOUT dieting&hellip;</p>
        <p>What if you could try a new approach where you only “diet” one or two times a week, and the rest of the time you just eat your normal diet the way you always have&hellip;</p>
        <p>Well you can. And this ground breaking strategy is called <i>Eat Stop Eat</i> and it was created by the author of <i>Thin Air</i>, Brad Pilon&hellip;</p>

        <h2>
            Do THIS Once A Week And Lose Up To 3 Lbs Overnight<br>
            (And Never Give Up Your Favorite Foods)
        </h2>

        <p>Here’s how one reader &mdash; Frank &mdash; describes his experience with Eat Stop Eat:</p>
        <blockquote>
            <p><i>“So I bought Brad’s Eat Stop Eat book. It explained the science so well and made such a great case that I had to try it.</i></p>
            <p><i>I was shocked to see that I already <u><b>dropped 3 pounds by the next day</b></u>. And I felt so much more clear headed and energetic.</i></p>
            <p><i>And the best part is, I just went back to my regular diet and favorite foods right after, without gaining any of the weight back.</i></p>
            <p><i>And now Cathy and I just use Eat Stop Eat once or twice a week to either lose weight or make sure we stay at our ideal weight.”</i></p>
        </blockquote>

        <p>And this is typical. Most folks can easily and consistently lose 3-5 lbs per week simply using the Eat Stop Eat method twice a week. The rest of the time you just eat normally.</p>
        <p>Listen: This is backed by 317+ scientific research papers and has been proven by over 54,000 <i>Eat Stop Eat</i> early adopters. And now it’s your turn to enjoy the same quick and easy results.</p>

        <h2>
            Limited Offer For New ClickBooks Customers<br>
            [On This Page Only]
        </h2>

        <p>Because Brad is one of our brand new authors here at ClickBooks, we’re able to offer you a fantastic deal on Eat Stop Eat today&hellip;</p>
        <p>First, when you purchase Eat Stop Eat through ClickBooks here on this page today you’ll also get Brad’s 5-Day Diet &mdash; that he usually sells separately for $29 &mdash; absolutely FREE.</p>
        <p>And instead of paying the regular retail price on Eat Stop Eat, you get BOTH today for just $9.</p>
        <p>So if you want the fastest results with the LEAST effort, Eat Stop Eat is the ONE nutrition strategy we would recommend to anyone who wants to lose weight or control their weight in the most natural way possible, and without suffering through the discipline and deprivation required by every other diet strategy.</p>
        <p>By the way, Eat Stop Eat also qualifies for our 100% <u class="bg-yellow"><b>60-day money back guarantee</b></u> policy.</p>
        <p>So of course you can go ahead and get your hands on Eat Stop Eat today with no risk to you.</p>
        <p>Just try it out for up to 60 days and see the results for yourself before you decide. You can always get your money back at any time before then if you decide it’s not for you&hellip;</p>
        <p>Use the button below to secure your special offer for <i>How Much Protein</i> customers only&hellip;</p>

        <div class="insertion quote rounded bonusbox">

            <div class="insertion right">
                <center><img class="mobile-3of4-width" src="web/i/products/ese-ecover.png" width="160" alt=""></center>
            </div>

            <h1 class="ta-left"><span class="cl-red">Exclusive Bonus:</span> The 5-Day Diet</h1>
            <p>A Message From Brad Pilon:</p>
            <p>“So many folks ask me for a foolproof formula on exactly how to eat on the days between your Eat Stop Eat protocols to accelerate weight loss even more.</p>
            <p>And this is it. It’s my EXACT blueprint for losing weight and toning your body in HALF the time&hellip;</p>
            <p>This book is easy to read and the diet is even easier to follow. Heck, even if you just use the 5-Day Diet plan half the time, you’ll still get results twice as fast as all the folks around you that are using regular and boring diet methods!&hellip;”</p>
            <p class="ta-right"><b>($29.95 value is yours FREE)</b></p>
        </div>

        <br>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/products/bundle.jpg" alt=""></center>
            <p class="ta-center"><u><b>Save $40.95 On This Page Only</b></u></p>
        </div>

        <div class="pricing">
            <strike>$49.95</strike> $9
        </div>

        <div class="insertion ta-center purchase">
            <a class="button" href="http://155.eatstopeat.pay.clickbank.net/?cbur=a">
                Add Eat Stop Eat To Your Order &mdash; Get<br>
                The 5-Day Diet FREE (Just $9 Today)
            </a>
            <p class="cards"><img src="web/i/purchase-accepted-cards.png" alt=""></p>
            <p class="ta-center"><a class="link" href="http://155.eatstopeat.pay.clickbank.net/?cbur=a">Add Eat Stop Eat To Your Order — Get The 5-Day Diet FREE (Just $9 Today)</a></p>
        </div>

        <hr>
        <p class="discard">I’m ok with dieting and I think I can figure out my nutrition on my own. I understand you negotiated a pretty awesome deal for us here that I can’t find anywhere else. But <a href="http://155.eatstopeat.pay.clickbank.net/?cbur=d">I’ll pass it up</a>.</p>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.</p>

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->

</body>
</html>
