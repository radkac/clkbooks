<?php
if(!isset($_GET['utm_source']) AND !isset($_SERVER['HTTP_REFERER'])){
	header('Location: http://clkbooks.com/free/?utm_source=amazon-book&utm_medium=referral&utm_content=optimized-giveaway-insert');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Free ESE Optimized Book</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex,nofollow">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=3">

    <!-- jQuery -->
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script-->

	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php');?>
</head>
<body>


<div class="section default">
    <div class="content rich-text clearfix">

        <h2>Exclusive Free Gift</h2>
        <h1>Eat Stop Eat Optimized</h1>
        <h2>For Amazon Customers Only</h2>
        <br>
        <p class="subhd">
            The ultimate Eat Stop Eat companion:<br>
            ESE OPTIMIZED is simply the quickest, easiest,<br>
            most done for you day-by-day, real life<br>
            application of the principles in ESE.<br>
        </p>
        <p><b>Today it's FREE to you as an Amazon Customer!</b></p>
        <p>Just tell us where to send it!</p>

        <form id="optInFormID" action="https://www.aweber.com/scripts/addlead.pl" method="post" accept-charset="UTF-8">

            <input type="hidden" name="meta_web_form_id" value="1771778218">
            <input type="hidden" name="meta_split_id" value="">
            <input type="hidden" name="listname" value="awlist4928216">
            <input type="hidden" name="redirect" value="http://clkbooks.com/free/ty/" id="redirect_0860b0a60a4a81ef01b6b5e59fd75ec3">
            <input type="hidden" name="meta_redirect_onlist" value="http://clkbooks.com/free/ty/">
            <input type="hidden" name="meta_adtracking" value="amazon_optimized">
            <input type="hidden" name="meta_message" value="1">
            <input type="hidden" name="meta_required" value="email">
            <input type="hidden" name="meta_tooltip" value="">

            <p class="field text email">
                <label>Enter Your Best Email:</label>
                <input type="text" name="email" value="">
            </p>
            <p class="field button submit">
                <input type="submit" name="Submit" value="Send Me The Book!">
            </p>
            <p style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jOzsjOzsHEyMHA==" alt=""></p>
        </form>

        <p class="footnote">This ebook, valued at $19.99 is 100% FREE. No credit card required. Your information is 100% Secure and Will Never Be Shared With Anyone</p>

    </div><!-- .content -->
</div><!-- .section -->

<div class="section footer">
    <div class="content rich-text">

        <p>Copyright &copy; 2017, clkbooks.com</p>
        <p>
            <a href="/terms/" target="_blank">Terms</a> |
            <a href="/privacy/" target="_blank">Privacy</a> |
            <a href="mailto:help@clkbooks.com">Support</a>
        </p>

    </div><!-- .content -->
</div><!-- .section -->


</body>
</html>
