<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Eat Stop Eat Phase 2</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex,nofollow">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=11">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/ese/web/i/fb-og-ese.png">
	<meta property="og:description" content="Eat Stop Eat: In Just One Day This Simple Strategy Frees You From Complicated Diet Rules — And Eliminates Rebound Weight Gain">
	<meta property="og:title" content="Check Out What I'm Reading...">

<script>
$(function(){
    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });
});
</script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="tagline">
        <div class="inner">
            The Cult-Hit Companion To The Bestselling Book, Eat Stop Eat
        </div>
    </div>

    <div id="breadcrumbs">
        Books › Weight Loss Books › Best Sellers
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <h1>ESE Optimized</h1>
                <p class="byline">By Brad Pilon</p>
                <p class="rating"><i>2017 Revised Edition</i></p>
            </div>

            <div class="columnVisual clearfix mobile-hidden">
                <center>
                    <img class="mobile-half-width" src="web/i/phase2-3d.jpg" width="100%" alt="">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png?v=1" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>CLK*Books Exclusive Price</h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$9*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$10.99</b> (54%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$9</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://71.eatstopeat.pay.clickbank.net/?cbfid=29204&vtid=clkamzoptineseoptmz&cbskin=18285"><span class="inner">Buy Now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

            <div class="columnContent">

                <div class="ty-preface">

                    <div class="insertion left">
                        <center><img class="mobile-half-width" src="web/i/ecover-optimized.png" width="120" alt=""></center>
                    </div>

                    <p><i>Thanks for grabbing a copy of Eat Stop Eat Optimized! We're sending your copy to your email address right now, and it should be there in the next few minutes. In the meantime we're excited to offer you the opportunity to read the entire book &mdash; Eat Stop Eat Phase 2 &mdash; at a generous pre-release discount price of just $9. Just take a look at the page below for details and make sure you take action because once the hard-copy version of the book is released this price will no longer be available.</i></p>

                </div><!-- .ty-preface -->

                <div class="insertion center desktop-hidden">
                    <center>
                        <img class="mobile-half-width" src="web/i/phase2-3d.jpg" width="100%" alt="">
                        <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                        <img class="mobile-1of3-width" src="web/i/read-on-any-device.png?v=1" alt="">
                    </center>
                </div>

                <div class="mobile-hidden">
                    <h1>Eat Stop Eat: Phase 2</h1>
                    <p class="byline">By Brad Pilon (Author)</p>
                    <p class="rating">Digital Edition Pre-Release</p>
                </div>

                <h1>
                    Have you heard about new research proving you need
                    to eat MORE and exercise LESS to lose the last few pounds?
                </h1>

                <p>You are about to discover the true diet secret for achieving the kind of super-lean body usually reserved for fitness cover models and professional physique athletes.</p>
                <p>In fact this plan is helping smart fitness models avoid the serious risk of slowing metabolism, losing lean muscle, and suffering rebound weight gain. And now these groundbreaking methods are available to you.</p>
                <p>This follow-up book to Brad Pilon’s bestselling <i>Eat Stop Eat</i> is only for readers who want to lose the final few pounds and achieve the “perfect physique.”</p>
                <p>The basics and background for <i>ESE Phase 2</i> are all covered in the original Eat Stop Eat. If you haven’t read it yet, we highly recommend you read it before continuing with Phase 2. You can <a href="http://www.eatstopeat.com/" target="_blank">find it here</a>.</p>
                <p>In <i>Phase 2</i>, author Brad Pilon reveals everything he has learned about <u>losing the last few pounds</u> and finally unveiling the exact beach-ready body you desire and deserve.</p>
                <p>This groundbreaking work is formulated directly from his post-graduate research, his years working alongside fitness models and athletes in the supplement industry, and over a decade of self-experimentation and work with clients.</p>

                <div class="read-more-content collapsed hidden">

                    <p>And this <u><b>all-natural plan</b></u> gives you all the same results as those dangerous crash-diets and medications that cover models must resort to &mdash; without any of the risks and side effects.</p>
                    <p>All while you actually <u><b>eat MORE</b></u>&hellip;  and exercise LESS as you get closer to your goal. You’ll discover how in a second&hellip;</p>
                    <p>First, how do you know if you’re ready for <i>Phase 2</i>? Easy. Simply switch to ESE Phase 2 as soon as you reach your recommended level of leanness with your regular Eat Stop Eat program. Nothing could be easier. And&hellip;</p>

                    <h2>During Your <i>ESE Phase 2</i> You’ll Enjoy&hellip;</h2>

                    <ul class="checkmarks checkmarks-full">
                        <li>Eating MORE food as you get closer to your beach body&hellip;</li>
                        <li>Total FREEDOM from suffocating diet rules &mdash; It doesn’t matter if you are a big breakfast eater or a big dinner eater or a grazer &mdash; <i>ESE Phase 2</i> is easily adaptable.</li>
                        <li>A firmer, more toned physique even as you shed more and more fat. Men may even build extra muscle.</li>
                        <li>Confidence that you have <u>eliminated the risk of rebound weight gain</u> permanently.</li>
                    </ul>

                    <h2>
                        <span class="cl-blue">And Yes: This is 100% Safe, Natural and Simple To Follow</span><br>
                        (Plus It Uses One <u>Unique Scientific Principle</u> You Won’t Find Anywhere Else)
                    </h2>

                    <p>Listen, you’ve probably heard that as you get leaner, you need to exercise more, diet harder, be more disciplined, and give up more of your favorite foods. And you may even believe that dieting should get more complicated and confusing as you approach your goal.</p>
                    <p>WRONG&hellip; The truth is, Brad has proven that’s a huge mistake and a recipe for disaster!&hellip;</p>
                    <p>All his research, self-experimentation, and work with thousands of clients has revealed one plain and simple truth &mdash; as you continue to lose weight, you reach a point where <b>you must actually INCREASE the calories you eat and DECREASE your exercise</b>&hellip;</p>
                    <p>It all hinges on a <u>unique scientific principle</u> that Brad helped to develop called the <b><i>Theory of Fat Availability</i></b>&hellip; which he covers in <i>Eat Stop Eat Phase 2</i>&hellip; And it actually means you have to eat more and exercise less in order to reach your perfect beach-ready body!&hellip;</p>
                    <p>Listen, Brad is a science and nutrition author that actually walks the talk. And <i>Phase 2</i> contains the exact secrets he uses to snap pictures like this one &mdash; WITHOUT miserable deprivation dieting, complicated formulas, or running himself down with exercise&hellip;</p>
                    <p>And now these exact strategies have been used by hundreds of Brad’s readers &mdash; all regular folks &mdash; to enter and win prestigious body transformation contests&hellip;</p>
                    <p>Every woman and man needs an edge when it comes to losing the final few pounds. So many seemingly savvy folks are turning to dangerous pills, devastating diets, or even drugs &mdash; desperate to finally achieve their goal.</p>
                    <p>Yet now, thanks to the <i>Theory of Fat Availability</i>, it’s clear that you can and must strategically eat MORE food and exercise less as you get closer to your ultimate beach-ready body. And there’s simply no need to resort to dangerous methods when you take the correct approach to losing the last few pounds.</p>
                    <p>PLUS you will never again worry about Rebound Weight Gain &mdash; the single biggest trap that pulls almost every man or woman down when they try to lose those last few pounds.</p>
                    <p>Finally, this is the plan that will put the finishing touches on your picture perfect body &mdash; the kind of body that will have you looking for excuses to show it off at the beach, the pool, the gym or wherever else you can think of to uncover your creation&hellip;</p>
                    <p>CLK*Books is now offering an exclusive discounted price for Brad’s <i>Eat Stop Eat: Phase 2</i>. To get it now at this reduced price just use the link on this page to secure the special deal for current customers only.</p>

                    <ul class="checkmarks">
                        <li>Thought-Provoking Books</li>
                        <li>Extreme Weight Loss</li>
                        <li>Healthy Dieting</li>
                    </ul>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>CLK*Books Exclusive Price</h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k">CLK*Books Price:</td>
                            <td class="v"><b>$9*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$10.99</b> (54%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$9</td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://71.eatstopeat.pay.clickbank.net/?cbfid=29204&vtid=clkamzoptineseoptmz&cbskin=18285"><span class="inner">Buy Now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>


</body>
</html>
