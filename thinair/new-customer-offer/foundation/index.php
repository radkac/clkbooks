<?php
//LOAD COOKIE SCRIPT
require_once($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');


require_once('../../../lib/Tracking/upsell-analytics-vtid-logic.php');

$vtid = getVtid();
$vtid = resolveVTID($vtid, 'of', false);
// CB upsell buy links
$linkAccept = getClickbankBuyLink('151', null, 'eatstopeat', array(
    'cbur' => 'a',
    'vtid' => $vtid
));
$linkDecline = getClickbankBuyLink('151', null, 'eatstopeat', array(
    'cbur' => 'd',
    'vtid' => $vtid
));

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>The Foundation Package</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=4">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=4">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
});

</script>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>
    <?php include "../../../tracking/ga-tracking.php"; ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1><b>
            Get the foundation<br>
            For lifelong peak health&hellip;
        </b></h1>

        <p>I respect your decision to say no to Treasury. However, it is my life purpose to deliver peak health to each of my clients.</p>
        <p>With that in mind, I want to ensure you have the necessities.</p>
        <p>For a <u>one-time investment</u> of $20, you can get <b>The Foundation Package</b>, which includes:</p>

        <div class="columns floatfix">
            <div class="column cl-1-4 ta-center">
                <img class="mobile-half-width" src="web/i/products/ecover-ese.png?v=2" width="53%" alt="" style="margin-bottom:.5em;"><br>
                <b>Eat Stop Eat</b>
            </div>
            <div class="column cl-1-4 ta-center">
                <img class="mobile-half-width" src="web/i/products/facts-in-five.png" width="100%" alt="" style="margin-bottom:.5em;"><br>
                <b>The Facts in Five</b>
            </div>
            <div class="column cl-1-4 ta-center">
                <img class="mobile-half-width" src="web/i/products/ecover-gbbb.png" width="53%" alt="" style="margin-bottom:.5em;"><br>
                <b>Good Belly Bad Belly</b>
            </div>
            <div class="column cl-1-4 ta-center">
                <img class="mobile-half-width" src="web/i/products/ecover-progressions.jpg" width="53%" alt="" style="margin-bottom:.5em;"><br>
                <b>Progressions</b>
            </div>
        </div>

        <p>That’s only $5 for each product!</p>
        <p>This package is also backed by my iron-clad 60 Day Money-back Guarantee. So, you have nothing to lose.</p>
        <p>Just click the link below to add it to your order.</p>
        <p>YES Add This To My Order for only $20</p>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="<?= $linkAccept ?>b1">Add To My Order!<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="<?= $linkAccept ?>b2">Add To My Order!</a></span>
            </p>

            <hr>

            <p class="discard">I understand that I’ll never see this massive discount again. I’ll take my chances of finding my own path through Thin Air. Thanks for the offer but I don’t want to see how you have experimented and applied Eat Stop Eat for multiple goals and periods in your life. And I don’t need a book on gut health or a done-for-me workout plan for the rest of my life. I know that once I leave this page I’ll lose out on this deal. I’m ok with that and I’d rather take my chances. <a href="<?= $linkDecline ?>">I'll pass</a>.</p>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2017 &copy; Clkbooks.com<br>
        For support please contact support [at] clkbooks.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
