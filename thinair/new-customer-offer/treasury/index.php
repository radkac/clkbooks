<?php
//LOAD COOKIE SCRIPT
require_once($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');

require_once('../../../lib/Tracking/upsell-analytics-vtid-logic.php');

$vtid = getVtid();
$vtid = resolveVTID($vtid, 'ot', false);
// CB upsell buy links
$linkAccept = getClickbankBuyLink('150', null, 'eatstopeat', array(
    'cbur' => 'a',
    'vtid' => $vtid
));
$linkDecline = getClickbankBuyLink('150', null, 'eatstopeat', array(
    'cbur' => 'd',
    'vtid' => $vtid
));

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Treasury</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=4">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=4">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
});

</script>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>
<?php include "../../../tracking/ga-tracking.php"; ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1><b>
            Before your order is complete<br>
            Let me know if you’re interested in<br>
            This exclusive opportunity
        </b></h1>

        <p>Your decision to get Thin Air shows me what I see in my best clients. Which is, you want what works. You want what is best for your body and health.</p>
        <p>So&hellip;</p>

        <h2>Here’s the opportunity:</h2>

        <p>Right now, I’m willing to open the doors to what I only offer to premier clients. It’s called <b><i>Treasury</i></b>.</p>
        <p>Treasury is my entire catalog of products. Inside it features <i>every course</i>, <i>every book</i>, and <i>every new and proven finding</i> I dig up.</p>
        <p>You see&hellip;</p>

        <h2>
            No matter your goals,<br>
            Treasury is your ultimate source to get and keep<br>
            your best body and health
        </h2>

        <p>On one end, it’s like a Wikipedia for your health. It has simple answers to make living healthy a breeze. On the other end, if you’d like, you can use it to transform your body into a head-turning physique.</p>

        <h3 class="ta-center">Here is just a small taste of what’s inside Treasury:</h3>

        <ul class="check green mobile-3of4-width" style="margin:2em auto;width:87%;">
            <li style="width:100%;">
                <div class="insertion right" style="margin:-2em 0 0 2em;">
                    <center><img class="mobile-half-width" src="web/i/products/ecover-ese.png?v=2" width="120" alt=""></center>
                </div>
                <b>Eat Stop Eat:</b> Eat Stop Eat is widely known as one of the best, and most proven diets in the world. Also, it’s more than just diet. It’s a simple eating style which allows you to eat what you want while enjoying superb health and a great body.
            </li>
            <li style="width:100%;overflow:hidden;">
                <div class="insertion right" style="margin:0 0 0 2em;">
                    <center><img class="mobile-half-width" src="web/i/products/ecover-gbbb.png" width="120" alt=""></center>
                </div>
                <b>Good Belly Bad Belly:</b> This simple guide shows you easy steps to follow for good gut health. Aside from the air we breathe, the other big health breakthrough in our century is the discovery of our Microbiome. In short, the critical importance of good gut health. Good gut health is linked to preventing cancer, promoting faster weight loss, turning back the clock on your age, and having better overall health.
            </li>
            <li style="width:100%;overflow:hidden;">
                <div class="insertion right">
                    <center><img class="mobile-half-width" src="web/i/products/facts-in-five.png" width="160" alt=""></center>
                </div>
                <b>The Facts In Five:</b> These short, 5-minute videos answer just about any question you may have on nutrition, diet, exercise, or how to navigate your day to live healthily. The videos dish straight talk and straight facts. My clients call them the best place to get a fast answer or solution for nearly any health goal.
            </li>
        </ul>

        <p>Again, that is just a small taste.</p>
        <p>In Treasury, you not only have access to each of my past products, but you’ll be given access to&hellip;</p>

        <h3 class="ta-center">
            Any FUTURE course, book, or video,<br>
            by myself or top health expert John Barban
        </h3>

        <p>John is known as one of the top weight loss and nutrition experts on the planet. Together, we have and are continuing to create life-changing courses, books, guides, and videos.</p>
        <p>So, what’s it all cost?</p>

        <h3 class="ta-center">
            You can join today, and today only<br>
            for one-time investment of $57
        </h3>

        <p>Here’s the deal, I’m not here to make a fortune. I’m here to change lives and to make attaining your peak health easy. I want the client who prefers intelligent solutions rather than sexy sounding pseudo-science.</p>
        <p>To see if you and I are the right fit&hellip;</p>

        <h3 class="ta-center">
            It’s backed with my iron-clad<br>
            60 Day Money Back Guarantee
        </h3>

        <p>Give Treasury a shot for two months. At the end of the two months you can decide if it’s for you. If not, no worries.</p>
        <p>All you have to do is click the link below. Again, you get two months try it out. Sample it. Test it out. Use it as a resource. If it’s a fit, great. And that’s it. No hidden fees. No future selling, none of that. If you decide not to take advantage of this one-time investment, and wish to get it later, it’s $10 a month for 12 months. It’s up to you. You can have it all right now for only $57.</p>
        <p>Remember&hellip;</p>

        <h2>This opportunity is available TODAY, and TODAY only.</h2>


        <div class="cart">
            <p class="purchase">
                <a class="button" href="<?= $linkAccept ?>b1">Add To My Order!<i></i></a>
                <span class="accepted"><img src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="<?= $linkAccept ?>b2">Add To My Order!</a></span>
            </p>

            <hr>

            <p class="discard">No thanks Brad. I understand that this is my only chance to get your online coaching and to secure access to The Treasury at the steeply discounted price for new customers only. I know that once I leave this page I’ll lose out on this deal. I’m ok with that and I’d rather go it alone. <a href="<?= $linkDecline ?>">I'll pass</a>.</p>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2017 &copy; Clkbooks.com<br>
        For support please contact support [at] clkbooks.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
