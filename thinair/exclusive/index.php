<?php require_once($_SERVER['DOCUMENT_ROOT'].'/prices.php');

include("../../lib/Browser/Browser.php");
$browser = new Browser();
$mobile = $browser->isMobile() || $browser->isTablet();

$vtidBase = "clkjim1ta10c";
$vtid = $mobile ? "mo".$vtidBase : $vtidBase;

$buyLink = "http://156.eatstopeat.pay.clickbank.net/?cbfid=31014&cbskin=18285&vtid=" . $vtid;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Screw Diet And Exercise</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- Google Analytics Content Experiment code -->
    <script>function utmx_section(){}function utmx(){}(function(){var
    k='181700585-2',d=document,l=d.location,c=d.cookie;
    if(l.search.indexOf('utm_expid='+k)>0)return;
    function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
    indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
    length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
    '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
    '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
    '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
    valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
    '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
    </script><script>utmx('url','A/B');</script>
    <!-- End of Google Analytics Content Experiment code -->

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=11">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/thinair/web/i/fb-og-ta.png">
	<meta property="og:description" content="Thin Air: Discover The Surprising Connection Between Exhaustion, Fatigue, Weight Gain and The Air You Breathe.">
	<meta property="og:title" content="Check Out What I'm Reading...">


    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>
    <?php include "../../tracking/ga-tracking.php"; ?>
</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

        <h1>Screw Diet And Exercise</h1>

        <h2 class="ta-center">
            It’s time to advance your health thinking<br>
            beyond the outdated path you’ve been lead down
        </h2>

        <div class="insertion center" style="margin:1.5em 0;">
            <center><img class="mobile-half-width" src="web/i/brad-photo-3.jpg" width="440" alt=""></center>
            <p align="center"><small><i>
                Brad Pilon, Best-Selling Author Eat Stop Eat. Coach. Husband. Father.
            </i></small></p>
        </div>

        <p>I’d like to introduce you to the new normal in health.</p>
        <p>In this letter, you can learn a little bit about my product, and decide if it’s for you.</p>
        <p>What I’m proposing is not some new “mystical discovery” to zap off body fat. Look those ideas make for great emails, internet newsletters, and talk from pseudo-experts, but it’s all basically been debunked.</p>
        <p>I’ve devoted the last 26 years of my life researching nearly everything on living healthy. You name it &mdash; <i>nutrition</i>, <i>exercise</i>, <i>diets</i>, <i>supplements</i>, <i>health claims</i>, <i>etc</i>.</p>
        <p>For years, I worked as a lead researcher for the #1 selling fat loss supplement in the world. I’ve created three top-selling diet and exercise programs for both men and women. Last, I’m widely known as the best-selling author of <i>Eat Stop Eat</i>.</p>
        <p>I say all this because, there is no argument among top scientists, researchers, athletes, nutritionists, or anybody who is active in a serious way about health &mdash; that what you’re about to learn here is considered the new normal for peak health.</p>
        <p>If you’re willing to set aside, for a moment, the single-minded notion that one’s good health derives primarily from diet and exercise&hellip; to entertain the idea that achieving the body and health you desire can come from a far simpler step&hellip; then read below:</p>

        <h3 align="center"><b>Here’s a smarter path to peak health&hellip;</b></h3>

        <p>To get the body and health you desire, you aim for one thing: results.</p>
        <p>Now, you’re already familiar with two steps to try &mdash; <i>eating healthy and exercising</i>.</p>
        <p>What follows is&hellip;</p>
        <p>A ton of thought goes into what and how you eat. You may even try a supplement promising a magic-like outcome. You hope the exercise routine is simple and enjoyable. Then you try to find the time, willpower, and motivation to stick to it.</p>
        <p>As it turns out, getting the results you want is not as easy as you’d like.</p>
        <p>The diet is restrictive, guilt-inducing, and complicated. The exercise routine becomes a tedious chore you put up with and often ignore. After trying these common steps, you can feel hopeless as you’re plagued once again by a bulging waistline. Or, if you find success, it still feels like a daily battle to keep going.</p>
        <p>You may ask, why is it so hard to be healthy?</p>
        <p><u><i><b>In the next 3 minutes</b>, you’ll learn how the air you breathe, holds the key to unlocking your ideal body and health.</i></u></p>
        <p>So, let’s unpack this crazy-sounding idea with straight facts.</p>

        <h3 align="center"><b>
            If your focus on getting or being healthy is based<br>
            around diet and exercise, then you’re stuck with outdated thinking.
        </b></h3>

        <p>Now it goes without saying, a sensible diet and physical activity are vital to good health.</p>
        <p>Here’s the thing, in the last decade, there have been incredible health advances. You can now wire yourself up like a robot to monitor your body. There are Fitbits, SmartWatches, phone apps&hellip; all types of gadgets and tools. We’ve come a long way from the old Thigh-Master.</p>
        <p>In fact, there is more health information in your smartphone than all Western Civilization had in the year 1917.</p>
        <p>A crazy amount of studies pop out monthly, weekly, and daily about food.</p>
        <p>Remember the old saying, <i>“An apple a day keeps the doctor away?”</i></p>
        <p>One of my close friends and his team of nutritionist devoted ten years digging into that saying. Ten years! And they discovered around 10,000 beneficial effects an apple might have on the body.</p>
        <p>We always had a vague idea Apples are healthy. Now evidence shows they may benefit you in thousands of ways. And that’s just an Apple. Similar studies are coming out on Eggs, Bananas, Spinach, and so on.</p>
        <p>How is it then with all these crazy advances, it’s so hard to be healthy?</p>

        <h3 align="center"><b>
            Here’s the <i>MASSIVE PROBLEM</i> you face<br>
            crippling your chances of attaining peak health&hellip;
        </b></h3>

        <p>You are missing out on the biggest health advance: <i>the air you breathe indoors</i>.</p>
        <p><b>No matter the diet, or exercise style you choose, you’ll never get the results you wish for if indoor smog handicaps your body.</b></p>
        <p>Indoor smog?</p>
        <p>Carbon Dioxide &mdash; or CO<sub>2</sub>. It’s a natural, colorless and odorless gas we breathe in daily. However, breathing increased levels of CO<sub>2</sub> robs your chances of ideal health.</p>
        <p>You see, on average, people spend around 90-95% of their time working and living indoors. This is where you’ll find increased levels of CO<sub>2</sub>.</p>
        <p>Below is a handful of known effects you may experience breathing higher levels of CO<sub>2</sub>:</p>

        <div class="columns clearfix">
            <div class="column cl-1-5 mobile-hidden">&nbsp;</div>
            <div class="column cl-1-3">
                <ul class="cross gray" style="margin:0;">
                    <li><i>Weight gain</i></li>
                    <li><i>Hunger Cravings</i></li>
                    <li><i>Type 2 Diabetes</i></li>
                    <li><i>Faster aging</i></li>
                    <li><i>Slower metabolism</i></li>
                    <li><i>Joint Pain</i></li>
                    <li><i>Lack of motivation</i></li>
                </ul>
            </div>
            <div class="column cl-1-3">
                <ul class="cross gray" style="margin:0;">
                    <li><i>Loss of libido</i></li>
                    <li><i>Low willpower</i></li>
                    <li><i>Muscle fatigue</i></li>
                    <li><i>Overeating</i></li>
                    <li><i>Lack of focus &amp; poor memory</i></li>
                    <li><i>Poor sleep quality</i></li>
                </ul>
            </div>
        </div>

        <p>Let’s unpack this with simple science.</p>
        <p>The levels of CO<sub>2</sub> you breathe are measured in <i>parts per million</i> (or ppm), on a scale ranging from 0-8000. Here’s an easier way to understand this:</p>

        <h3 align="center"><b>If you don’t know where your home or office falls in this chart&hellip;</b></h3>

        <table class="sci-data">
            <tr>
                <td>Normal Outdoor Level</td>
                <td>350 &mdash; 450 ppm</td>
            </tr>
            <tr>
                <td><b>Acceptable Levels</b></td>
                <td><b>&lt; 600 ppm</b></td>
            </tr>
            <tr>
                <td>Complaints of stiffness</td>
                <td>600 &mdash; 1000 ppm</td>
            </tr>
            <tr>
                <td>ASHRAE and OSHA standards</td>
                <td>1000 ppm</td>
            </tr>
            <tr>
                <td>Drowsiness, headaches, sleepiness, lethargy</td>
                <td>1000 &mdash; 2500 ppm</td>
            </tr>
            <tr>
                <td>Increased heart rate and nausea</td>
                <td>2500 &mdash; 5000 ppm</td>
            </tr>
            <tr>
                <td>Maximum allowed in 8 hours</td>
                <td>8000 ppm</td>
            </tr>
        </table>

        <h3 align="center"><b>Then it’s unlikely you’ll ever get the body or health you want.</b></h3>

        <p>The way I see it, you’re facing a daily uphill battle for peak health.</p>
        <p>Here’s what I mean: 600 ppm is bolded on the chart above. This ppm level and less is considered <i>ideal</i> for your health.</p>
        <p>Keep that in mind as you read below.</p>
        <p><i>Around 1 in 2 workspaces have levels exceeding 1500ppm.</i></p>
        <p><i>Many homes have levels over 1000 ppm.</i></p>
        <p>Which means it’s likely that&hellip;</p>
        <p><i>If you’re experiencing poor sleep&hellip;</i></p>
        <p><i>Or find sticking to that diet or exercise routine is a battle&hellip;</i></p>
        <p><i>Or getting the results you want is not as fast or as easy you’d like&hellip;</i></p>

        <h3 align="center"><b>IT’S NOT YOU!</b></h3>

        <p>Look, as research shows, breathing in higher levels of CO<sub>2</sub>, is like breathing through a tiny straw with your nose plugged.</p>
        <p>Meaning, <i>your body will never function to its full potential if it’s consuming too much CO<sub>2</sub></i>.</p>
        <p>So, what do we do?</p>
        <p>We now know, you will never get the results you want by just sticking to diet and exercise, without addressing this problem.</p>

        <h3 align="center"><b>
            Some of the world’s healthiest people have paid<br>
            thousands of dollars to find a solution to this problem.
        </b></h3>

        <p>For example, several athletes, celebrities, and the uber-wealthy have tried super expensive Hyperbaric Chambers to sleep in. You can grab a basic one for <i>only</i> $11,900!</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/hyperbaric-chambers.jpg?v=2" width="440" alt=""></center>
        </div>

        <p>Many of the world’s top companies &mdash; <i>Google, Amazon, Facebook, Apple</i> &mdash; pay unbelievable amounts of money to ensure their offices have incredible indoor air quality. They know as studies reveal, your productivity <i>(cognitive power, motivation, creativity, etc.)</i> cranks up when the CO<sub>2</sub> levels are kept low.</p>
        <p>Where does this leave you?</p>

        <h3 align="center"><b>
            If you keep stubbornly sticking to just diet and exercise,<br>
            I say best of luck to you.
        </b></h3>

        <p>Now, this option does work. You know as well as I do, people find success with eating healthy and consistent physical activity. But let’s face it, to find success using this option it requires a strict lifestyle.</p>
        <p>Even if you do stick with it, without addressing indoor smog (CO<sub>2</sub>), you’ll never get the results you want &mdash; until now.</p>

        <h3 align="center"><b>A faster and easier path to better health&hellip;</b></h3>

        <p>Is now unlocked with the formula I created called <b><i>Thin Air</i></b>.</p>

        <div class="insertion center">
            <center><img class="mobile-half-width" src="web/i/ecover-thinair.jpg" width="260" alt=""></center>
        </div>

        <p>Instead of having to change jobs or pay almost $12,000 to sleep in some Star Trek looking device&hellip;</p>

        <h3 align="center"><b>
            For only $10 you can get easy and simple steps<br>
            to beat the indoor smog robbing you<br>
            of your optimal body and health.
        </b></h3>

        <p>Thin Air is a simple e-book. I’ve spent years dissecting complicated science and turned it into a formula and guide even my two young kids can understand.</p>
        <p>Look, if you want to stop obsessing over diet and exercise, or the frustration of not getting the results you want, then Thin Air is your smart path to better health.</p>
        <p>Thin Air delivers simple steps proven to unleash your full health potential.</p>
        <p>Here’s how&hellip;</p>
        <p>First, you’ll learn how to recognize signs if, or how your body has been affected by CO<sub>2</sub>.</p>
        <p>Next, you’ll learn the actionable steps on how you can eliminate the glaring issue of indoor smog. It doesn’t matter where you live, city or country. It doesn’t matter where you work. The steps are easy, fast, and deliver results.</p>
        <p>Last, you’ll get some insider tricks to boost your body’s full health potential. Like which plants to have in your house to reduce hunger cravings. Or a tip you can do during your lunch break to enhance brainpower. And even a few little steps that can ignite your libido. Yes, even your libido.</p>
        <p>It’s important to note&hellip;</p>

        <h3 align="center"><b>
            I only want a client ready to use smart solutions<br>
            to live healthily.
        </b></h3>

        <p>You see, my clients aren’t the type chasing delusional quick health fixes. My clients prefer simple, sound solutions proven to work.</p>
        <p>The fact is: my solutions work.</p>
        <p>I know I am the best at what I do. I love helping and guiding my clients with the straight facts of what works, and what doesn’t. It’s possible, in your lifetime, you’ll never meet anyone as devoted to health as I am.</p>

        <h3 align="center"><b>So, here’s the deal&hellip;</b></h3>

        <p>As I said, Thin Air is only $10. And&hellip;</p>

        <h3 align="center"><b>
            It’s backed with an Iron-Clad 60 Day Money-back Guarantee.<br>
            <a href="<?= $buyLink ?>b1">CLICK HERE</a> To Get Thin Air now.
        </b></h3>

        <div class="insertion center">
            <center><img class="mobile-half-width" src="web/i/ecover-thinair.jpg" width="260" alt=""></center>
        </div>

        <p>Now I know I said I’m picky about who I want as a client.</p>
        <p>So why the low price and a money-back guarantee?</p>
        <p>First and foremost, I know Thin Air can make a positive impact on your health. However, I feel it’s fair we get to know each other.</p>
        <p>Why?</p>
        <p>Thin Air is just the start.</p>
        <p>I have an entire catalog of health information I only make available to premier clients. With Thin Air, I want you to get a feel for my style of teaching. You’ll find there is no fluff or anecdotal nonsense in what I teach. I use straight, simple facts, and easy-to-apply solutions.</p>

        <h3 align="center"><b>
            If you don’t like my style, just let me know,<br>
            and I’ll refund every penny.
        </b></h3>

        <p>I’m not in this business like other experts, chasing after millions upon millions of people.</p>
        <p>By now, you know the decision is in your hands. You know if you want to try it out, or if you’re going to keep gambling with outdated thinking. I mean, it’s $10.</p>
        <p>I know it’s a great product. My clients know it’s a great product. If you don’t want it, then leave the page.</p>
        <p>If you want it, click the link below and you’ll be taken to the 100% secure checkout page.</p>
        <p>After you fill out your information, Thin Air will be sent instantly to your email inbox.</p>

        <h3><b>If you’re ready for solutions that deliver, then click the button below.</b></h3>

        <br>
        <p class="ta-center">
            <a class="purchase-button" href="<?= $buyLink ?>b2">Yes! Get Me Started Today&hellip;</a>
        </p>
        <br>
        <br>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
