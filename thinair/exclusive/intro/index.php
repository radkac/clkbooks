<?php require_once($_SERVER['DOCUMENT_ROOT'].'/prices.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Screw Diet And Exercise</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=11">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/thinair/web/i/fb-og-ta.png">
	<meta property="og:description" content="Thin Air: Discover The Surprising Connection Between Exhaustion, Fatigue, Weight Gain and The Air You Breathe.">
	<meta property="og:title" content="Check Out What I'm Reading...">

	<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>
	
</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

        <h1>Screw Diet And Exercise</h1>

        <h2 class="ta-center">
            It’s time to advance your health thinking<br>
            beyond the outdated path you’ve been lead down
        </h2>

        <div class="insertion center" style="margin:1.5em 0;">
            <center><img class="mobile-half-width" src="web/i/brad-photo-3.jpg" width="440" alt=""></center>
            <p align="center"><small><i>
                Brad Pilon, Best-Selling Author Eat Stop Eat. Coach. Husband. Father.
            </i></small></p>
        </div>

        <p>I’m about to piss a lot of people off in the health and fitness industry, and I don’t care.</p>
        <p>If I didn’t release my new health finding to the public, I wouldn’t be the true teacher and coach my thousands of successful clients depend on for better health.</p>
        <p>I’ve devoted the last 35 years of my life researching nearly everything on living healthy. You name it &mdash; <i>nutrition</i>, <i>exercise</i>, <i>diets</i>, <i>supplements</i>, <i>health claims</i>, <i>etc</i>.</p>
        <p>For years, I worked as a lead researcher for the #1 selling fat loss supplement in the world. I’ve created three top-selling diet and exercise programs for both men and women. Last, I’m widely known as the best-selling author of <i>Eat Stop Eat</i>.</p>
        <p>With all that, I earned a sterling reputation amongst my professional peers. What I’m about to expose, however, may <b>destroy many expert’s careers</b>.</p>
        <p>Some of whom, you may already know.</p>
        <p>If you’re willing to set aside, for a moment, the single-minded notion that one’s good health derives primarily from diet and exercise&hellip; to entertain the idea that achieving the body and health you desire can come from a far simpler step&hellip; then click the link to go to the next page:</p>

        <h4 align="center"><a href="/thinair/exclusive/reveal/"><b>Click here to advance your health thinking</b></a></h4>

        <br>
        <br>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
