<?php require_once('../prices.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Thin Air</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=10">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=10">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=10">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/thinair/web/i/fb-og-ta.png">
	<meta property="og:description" content="Thin Air: Discover The Surprising Connection Between Exhaustion, Fatigue, Weight Gain and The Air You Breathe.">
	<meta property="og:title" content="Check Out What I'm Reading...">

<script>

$(function(){

    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });

});

</script>

<!-- Google Analytics Content Experiment code -->
<script>function utmx_section(){}function utmx(){}(function(){var
k='140758458-86',d=document,l=d.location,c=d.cookie;
if(l.search.indexOf('utm_expid='+k)>0)return;
function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
'" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
</script><script>utmx('url','A/B');</script>
<!-- End of Google Analytics Content Experiment code -->

<?php require_once('../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="breadcrumbs">
        Books › Health &amp; Weight Loss Books › New Releases
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Book</p>
                <h1>Thin Air</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating">Digital Edition Pre-Release</p>
            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img class="main-ecover mobile-half-width" src="web/i/eCover-New.png?v=1" width="100%" alt="">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>
                        Special Pre-Release Price
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>$<?php echo $thinair; ?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$9.99</b> (50%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?php echo $thinair; ?></td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://123.eatstopeat.pay.clickbank.net/?cbfid=27873&cbskin=18285&vtid=clkbooks"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Book</p>
                <h1>Thin Air</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating">Digital Edition Pre-Release</p>
                </div>

                <h2>Discover The Surprising Connection Between Exhaustion, Lethargy, Weight Gain and The Air You Breathe</h2>
                <p>Have you ever suffered from nagging and unexplained fatigue, headaches, lethargy, mysterious weight problems or headaches and even migraines?</p>
                <p>If yes, have you tried everything — including diets, exercise, countless doctors, acupuncture, alternative medicine, and the list goes on &mdash; to get relief?</p>
                <p>Would you be surprised to hear that a recent poll of Americans found that even among folks sleeping 7-8 hours per night, 93% of people wake up feeling fatigued at least 3-4 days a week?</p>
                <p>And as you’re about to discover, your nagging challenges may have <u><b>nothing to do with your</b></u> &hellip;</p>

                <div class="read-more-content collapsed hidden">

                <p>&hellip; <u><b>diet or your exercise</b></u>. And a simple 10 seconds trick along with a few other easy to implement strategies outlined in <i>Thin Air</i> could <b>start reversing ALL your symptoms in less than 20 minutes</b>. Let me explain&hellip;</p>
                <p>All the symptoms described earlier are identical to those outlined in a groundbreaking yet little-known report released by the Air Force. And as strange as this sounds, it turns out there’s a strange link between these mysterious Chronic Fatigue-like symptoms and research into a certain Greenhouse Gas.</p>

                <h2>In other words. The air you are breathing right now could be making you fat, tired and sick!</h2>

                <p>The document reported that the majority of workplace buildings in the US had levels of this Greenhouse Gas high enough to affect up to 80% of workers with these same symptoms.</p>
                <p>Have you guessed the Greenhouse Gas I’ve been talking about? You’ll be shocked&hellip;</p>
                <p>It’s nothing more complicated than everyday Carbon Dioxide or Co2&hellip;</p>
                <p>However when you look into what’s happening it’s terribly frightening. If you consider our human evolution you’ll see we evolved to breathe an atmosphere with Co2 concentrations of about 300 parts per million (or ppm)&hellip;</p>
                <p>Yet thanks to Greenhouse Gas emissions, even average OUTDOOR levels of Co2 are now over 400 ppm. But that’s just OUTDOOR levels. Get this&hellip;</p>
                <p>I discovered “good” office building levels are in the range of 600-800 ppm if you’re lucky. And that it’s not uncommon for many buildings to be twice or three times those levels!&hellip;</p>
                <p>While a recent study by KPMG and Middlesex Universities found that even levels of Co2 in the 600 ppm range could reduce concentration by as much as 30 percent. At levels above 1,500 ppm 79% of people reported feeling lethargic.</p>
                <p>Think about that number for a second&hellip; That’s almost 8 out of every 10 people feeling exhausted because of the air they’re breathing!</p>
                <p>The Air Force report warned that even at 600 ppm many people will suffer sleepiness, fatigue, poor concentration, and a sensation of stuffiness&hellip;</p>
                <p>And we haven’t even started talking about the air you are breathing in modern houses.</p>
                <p><b>Yet as I continued my research I discovered something even more shocking&hellip;</b></p>
                <p>Danish researchers are now discovering how <u><b>rising Co2 concentrations are making humans <span class="bg-yellow">hungrier and fatter</span></b></u>&hellip;</p>
                <p>The researchers were surprised to see that both fat and thin people taking part in their studies over a 22-year period put on weight &mdash; and the increase was proportionately the same. The only consistent factor they could find across the entire group was rising Co2 levels&hellip;</p>
                <p>Looking for a deeper explanation they stumbled on something called Orexins. These are a type of hormone in the brain that affect wakefulness and energy expenditure and they can be disrupted by high Co2 in your blood, and this can cause you to go to bed later, affecting your metabolism so it is easier for you to put on weight&hellip;</p>
                <p>Worse&hellip; Orexins are also DIRECTLY involved in the stimulation of your hunger and food intake. Yep! It looks like science is now proving that Co2 makes you eat more&hellip;</p>
                <p>And it doesn’t stop there. A Swiss study demonstrated signs of increased insulin resistance and waist-circumference in people exposed to poor air quality. Which means these folks had trouble storing nutrients in their muscles and other lean tissue. The science tells us that since the body needs to get rid of the sugar in the blood that results from eating, it quickly stores it as fat!&hellip;</p>
                <p>I explain all this research and much more in a very easy to understand and quick to read book called <b><i>Thin Air</i></b>.</p>
                <p>In the book I also lay out a <b>simple plan</b> you can start implementing <u><b>today</b></u> that will help you <u>reclaim your energy and reboot your fat burning metabolism</u>.</p>
                <p>And as I mentioned, the first tip takes only <u>10 seconds or less and you’ll begin noticing the effects in under 20 minutes</u>!</p>
                <p>Thin Air is currently ONLY available as a pre-release limited-time special offer here on CLK*Books. To get it now at this discounted pre-sale price just use the link on this page to secure the special deal for early readers only.</p>

                <ul class="checkmarks">
                    <li>Thought-Provoking Books</li>
                    <li>Smart Weight Management</li>
                    <li>Energizing Strategies</li>
                </ul>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>
                        Special Pre-Release Price
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$19.99</td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>$<?php echo $thinair; ?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$9.99</b> (50%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?php echo $thinair; ?></td>
                        </tr>
                    </table>

                    <p>
                        <a class="purchase-button" href="http://123.eatstopeat.pay.clickbank.net/?cbfid=27873&cbskin=18285&vtid=clkbooks"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>


        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
