<?php require_once('../prices.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>Fat Cell Killer</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=12">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=12">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=12">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="web/s/month-days-left.js"></script>

	<meta property="og:image" content="http://clkbooks.com/thinair/web/i/fb-og-ta.jpg">
	<meta property="og:description" content="Fat Cell Ciller: The ONLY Known Protocol To Permanently KILL And Flush Away Your Fat Cells &mdash; AND Guarantee Permanent &amp;Irreversible Fat Loss">
	<meta property="og:title" content="Check Out What I'm Reading&hellip;">

<script>

$(function(){

    $(".read-more").click(function(){
        var collapsed = $(".read-more").html() == "Read more" ? true : false;
        var caption = collapsed ? "Read less" : "Read more";
        $(".read-more").html(caption);
        if(collapsed) {
            $(".read-more").addClass("collapsed");
        } else {
            $(".read-more").removeClass("collapsed");
        }
        $(".read-more-content").toggle(400);
    });

});

</script>

<?php require_once('../eztrackingcode.php'); ?>

</head>
<body>

<div id="header">
    <div class="container">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<div id="layout" class="clearfix">

    <div id="breadcrumbs">
        Books › Health &amp; Weight Loss Books › New Releases
    </div>

    <div id="content" class="rich-text clearfix">

        <div class="columns">

            <div class="columnContent desktop-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Book</p>
                <h1>Fat Cell Killer</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating">Digital Edition Pre-Release</p>
            </div>

            <div class="columnVisual clearfix">
                <center>
                    <img class="main-ecover mobile-half-width" src="web/i/ecover.jpg?v=2" width="100%" alt="">
                    <br><br><br class="mobile-hidden"><br class="mobile-hidden">
                    <img class="mobile-1of3-width" src="web/i/read-on-any-device.png" alt="">
                </center>
            </div>

            <div class="columnProduct mobile-hidden clearfix">
                <center>
                    <h4>
                        Special Pre-Release Price
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$69</td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>$<?=$fck?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$40</b> (58%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?=$fck?></td>
                        </tr>
                    </table>
                    <p>
                        <a class="purchase-button" href="http://195.eatstopeat.pay.clickbank.net/?cbfid=34205&cbskin=18285&vtid=clk34205"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>

            <div class="columnContent">
                <div class="mobile-hidden">
                <p class="pre-head">Limited Time Pre-Release Access To Brad Pilon’s Brand New Book</p>
                <h1>Fat Cell Killer</h1>
                <p class="byline">By Brad Pilon <small>(Author)</small></p>
                <p class="rating">Digital Edition Pre-Release</p>
                </div>

                <h2 class="fw-norm">
                    Explained: The <span class="cl-red">Scientific</span> Reason For “Rebound Weight Gain”
                </h2>

                <p>Have you ever wondered why it takes so long to lose weight, and yet <u>you can gain it all back in a single weekend</u>?</p>
                <p>It’s very simple really. And no one else is talking about it because no one has had a solution for it&hellip; until very recently.</p>
                <p>You see&hellip; “losing weight” does not reduce the <b>number of fat cells</b> you have!</p>
                <p>When you lose weight you simply drain those cells, like letting the air out of a balloon. And then all those cells sit there inside your body, <b>starving and just <u>begging for you to fill them up</u> again</b>&hellip;</p>
                <p>Worse&hellip; when you become overweight and fill up your fat cells&hellip; your body actually tells those cells to <u>divide and multiply</u>&hellip; It’s called <b><i>hyperplasia</i></b>.</p>
                <p><b>That means that if you have ever put on excess weight you are almost certain to have <u class="bg-yellow">MORE FAT CELLS</u></b> than people who have never been overweight.</p>
                <p>And to add insult to injury, when you manage to drain your extra fat cells they continually send out signals to your body instructing it to refill them. Which means you’re more likely to be <u>hungry all the time</u> and constantly fight with <u>cravings</u>!</p>
                <p>And that’s why <i>“<b>rebound weight gain</b>”</i> is not difficult to understand. It’s not a myth. And it is very well explained by what we know about human metabolism.</p>
                <p>The only way to reverse this inevitable biological feedback loop is to reduce the number of fat cells in your body. Until very recently this seemed almost impossible. But new discoveries have finally revealed the roadmap to purposefully destroying and removing fat cells from your body.</p>

                <div class="read-more-content collapsed hidden">

                <h2>
                    Apoptosis: The Key To Destroying &amp; <span class="cl-red">Flushing Away Fat Cells</span><br>
                    (Without Diet or Exercise)
                </h2>

                <p>Losing excess weight is just the first step in long term weight management. The second step is to prevent rebound weight gain by triggering something called <u><b>Apoptosis</b></u>.</p>
                <p>Apoptosis is a process of programmed cell death. It occurs naturally in your body all throughout your life as you continually renew every tissue in your body.</p>
                <p>However we now know that you can specifically <u><b>trigger</b></u> this programmed cell death in your fat cells through a series of specific yet simple techniques. And it does NOT require diet or exercise (although in some cases apoptosis can be accelerated by both).</p>
                <p>I was shocked to discover the scientific knowledge to kill fat cells has been around and ignored since the 70’s! It’s based on a shocking discovery made in 1970 by Drs Epstein and Oren while examining a 6 month old child who had a strange reaction to a popsicle&hellip;</p>
                <p>And even though it’s incredibly effective, only a small sliver of the medical community has taken advantage of it since then. One exception is an underground US Patent holder that has been sneakily helping over 5 million people to quietly flush away their excess fat cells since 2008.</p>
                <p>So why haven’t we heard about it? Because it’s something <u>only the wealthy can afford</u>. Starting at almost $1000 and ranging into several thousands&hellip; it just hasn’t been accessible for regular folks like you and me. However&hellip;</p>

                <h2>New Research Reveals Simple Fat Cell Apoptosis Techniques You Can Use Right In Your Own Home For Pennies</h2>

                <p>I discovered how you can get the same results using simple methods that you simply add to your daily routine, that <u>cost you next to nothing</u>! And the simple system I’ve put together will&hellip;</p>

                <ul class="check blue">
                    <li>Cause cell death in approximately 20% of the fat cells you drain through weight loss dieting</li>
                    <li>Reduce hunger and mitigate cravings by destroying excess fat cells</li>
                    <li>Reduce the chance of rebound weight gain by resetting your fat cell count to normal levels</li>
                    <li>Make your body slimmer and more toned by expelling empty fat cells WITHOUT requiring you to continue a weight loss diet</li>
                </ul>

                <p>You can see below the results of a very, very interesting case study published in 2013. The man in the photo had ONE love handle exposed to techniques designed to cause fat cell apoptosis. <b>Two years later</b>, the treated love handle was still visibly smaller than the non-treated love handle!</p>

                <div class="insertion center">
                    <center><img class="mobile-full-width" src="web/i/fig-3.jpg" width="70%" alt=""></center>
                </div>

                <p><u>Now, I confess that it will take up to 12 weeks</u> to complete a cycle of Apoptosis. It takes that long to run through the lifecycle of the fat cells and insure they are killed off.</p>
                <p>Yet in the long run… 12 weeks is a <u>tiny investment</u> to in your long term weight management.</p>
                <p>Yes, I admit the name of this new book &mdash; <i>Fat Cell Killer</i> &mdash; is pretty melodramatic. However it accurately describes the result of following these methods. <i>(and it just sounds so cool too&hellip;)</i></p>
                <p>As usual, CLKbooks has secured a huge discount for you. Simply use the button below to claim your exclusive access and lock in your incredible one-time only discount&hellip;</p>

                <!--p class="price">
                    <strike>&nbsp;$69.99&nbsp;</strike>
                    <b>$29</b>
                </p-->

                <div class="purchase-holder">
                    <div class="insertion center purchase">

                        <img class="ecover mobile-2of3-width" src="web/i/ecover.jpg" width="120" alt="">

                        <table align="center" class="costs" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>List Price:</td>
                                <td>$69</td>
                            </tr>
                            <tr>
                                <td>CLK*Books Price:</td>
                                <td><b>$29*</b></td>
                            </tr>
                            <tr>
                                <td>You Save</td>
                                <td>$40 (58%)</td>
                            </tr>
                            <tr>
                                <td colspan="2">*Includes free international wireless delivery</td>
                            </tr>
                            <tr class="total">
                                <td class="k">Total</td>
                                <td class="v">$29</td>
                            </tr>
                        </table>

                        <a class="button" href="http://195.eatstopeat.pay.clickbank.net/?cbfid=34205&cbskin=18285&vtid=clk34205">Buy Now</a>

                    </div>
                </div>

                </div><!-- .read-more-content -->

                <p><a class="read-more" href="javascript:;">Read more</a></p>

            </div>

            <div class="columnProduct desktop-hidden clearfix">
                <center>
                    <h4>
                        Special Pre-Release Price
                    </h4>
                    <table class="pricing">
                        <tr>
                            <td class="k">List Price:</td>
                            <td class="v">$69</td>
                        </tr>
                        <tr>
                            <td class="k"><b>CLK*Books Price:</b></td>
                            <td class="v"><b>$<?=$fck?>*</b></td>
                        </tr>
                        <tr>
                            <td class="k">You Save:</td>
                            <td class="v red"><b>$40</b> (58%)</td>
                        </tr>
                        <tr>
                            <td class="fee" colspan="2">*Includes free international wireless delivery</td>
                        </tr>
                        <tr class="total">
                            <td class="k">Total</td>
                            <td class="v">$<?=$fck?></td>
                        </tr>
                    </table>
                    <p>
                        <a class="purchase-button" href="http://195.eatstopeat.pay.clickbank.net/?cbfid=34205&cbskin=18285&vtid=clk34205"><span class="inner">Buy now</span></a>
                        <p class="purchase-hint">immediate access</p>
                    </p>
                </center>
            </div>


        </div><!-- .columns -->

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer">
    <div class="container">
        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>
    </div>
</div>

</body>
</html>
