<?php
include("../../lib/Browser/Browser.php");
$browser = new Browser();
$mobile = $browser->isMobile() || $browser->isTablet();

$vtidBase = "clk34205lfcbc";
$vtid = $mobile ? "mo".$vtidBase : $vtidBase;

$buyLink = "http://195.eatstopeat.pay.clickbank.net/?cbfid=34205&cbskin=18285&vtid=" . $vtid;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Fat Cell Killer</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Analytics Content Experiment code -->
    <script>function utmx_section(){}function utmx(){}(function(){var
    k='181700585-3',d=document,l=d.location,c=d.cookie;
    if(l.search.indexOf('utm_expid='+k)>0)return;
    function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
    indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
    length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
    '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
    '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
    '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
    valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
    '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
    </script><script>utmx('url','A/B');</script>
    <!-- End of Google Analytics Content Experiment code -->

    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=8">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=8">

    <link rel="stylesheet" type="text/css" href="web/s/slick/slick.css?v=2">
    <link rel="stylesheet" type="text/css" href="web/s/slick/slick-theme.css?v=2">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="web/s/slick/slick.js"></script>

    <script>
    $(function(){
        $('.before-after').slick({
            accessibility: true,
            adaptiveHeight: true,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: true,
            dots: true,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    });
    </script>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>
    <?php include $_SERVER['DOCUMENT_ROOT'].'/tracking/ga-tracking.php'; ?>

    <!-- Hotjar Tracking Code for ESE -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:923516,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>

<div id="headline">
    <div class="inner">
        <a href="/" class="logo">CLKBooks.com</a>
        <div class="social">
            <a class="icon" href="https://business.facebook.com/clkbks/?business_id=1933102156920314" target="_blank">
                <img src="web/i/social-fb-icon.png" alt="">
            </a>
            <a class="icon" href="https://twitter.com/BradPilon" target="_blank">
                <img src="web/i/social-tw-icon.png" alt="">
            </a>
        </div>
    </div>
</div>

<header>
    <section>

        <h1><b>
            <small>New Discovery</small><br>
            Is Triggering <span class="cl-blue">Permanent</span> <u>Death Of Fat Cells</u><br>
            <small><u>WITHOUT</u> Diet Or Exercise</small>
        </b></h1>
        <div class="inspire insertion center">
            <center>
                <img src="web/i/inspire.jpg" alt=""><br>
                <i>Fig 1: 12 Week Intervention Without Diet Or Exercise</i>
            </center>
        </div>
        <h3><b>
            This Simple At-Home Method Permanently Kills & Flushes Away Ugly and Health-Harming Surplus Fat Cells While You Watch TV
        </b></h3>

    </section>
</header>

<main>
    <section>

        <p>
            <?=date('j F Y')?><br>
            From the Blog of Bestselling Health Author Brad Pilon:
        </p>

        <h2 class="first">
            <b>How To Guarantee <span class="cl-blue">Permanent &amp; Irreversible Fat Reduction</span> WITHOUT Diet or Exercise</b>
        </h2>

        <p>Have you ever wondered why it takes so long to lose weight, and yet you can gain it all back in a single weekend?!?</p>
        <p>It’s very simple really. And no one else is talking about it because they don’t have a solution for it. And neither did I until very recently&hellip;</p>
        <p>You see&hellip; “losing weight” does not reduce the <b>number of fat cells</b> you have! (I’ll explain this in just a second)</p>
        <p>When you lose weight you simply “drain” your fat cells, like letting the air out of a balloon. And then all those cells sit there inside your body like dry sponges, <b>starving and just <u>begging for you to fill them up again</u></b>.</p>
        <p>Worse&hellip; when you become overweight and fill up your fat cells&hellip; they actually <b>hit a size-limit</b> and <u>send signals to your body to produce <b>MORE fat cells</b></u> &mdash; so you end up with more and more fat cells to be filled.</p>

        <hr>

        <h3><b>Your Hungry Fat Cell “Snowball Effect”</b></h3>

        <p>First let’s quickly talk about your fat cells and how they work. The technical term for them is <u>Adipocytes</u>, and you can think of them as containers that you store energy in. </p>
        <p>When you eat more calories than you need, one of the things your body does is store some of that energy in the form of fats that are referred to as “triglycerides.” Those triglycerides get stuffed into your fat cells for later use.</p>
        <p>This is normal. And triglycerides are stored and released from your fat cells constantly throughout the day. <b>Contrary</b> to what you’ve probably been told, it’s not a fixed process, but rather something that <u>happens fluidly all day long</u>.</p>

        <div class="insertion right">
            <center><img class="mobile-full-width visual" src="web/i/visual-adipocyte.jpg" width="400" alt=""></center>
            <label>Fig1. Adipocyte Anatomy</label>
        </div>

        <p>However, if you are consistently consuming more energy in the form of food than you are using during your daily activities, you are also consistently storing more lipids in your fat cells than you are releasing them.</p>
        <p>This <u>makes your fat cells grow</u>. We call that <b class="bg-yellow marker">hypertrophy</b>.</p>
        <p>In the very short term this is totally ok. Yet if it goes on too long you run into the curse of every man or woman who has ever been overweight&hellip;</p>

        <hr>

        <h3 class="clearer"><b>The Curse Of Everyone Who Has Ever Been Overweight</b></h3>

        <p>As we’ve already seen, your fat cells can increase in size (called hypertrophy).</p>
        <p>However what happens next is what makes it seem almost impossible to permanently lose fat from your stomach, thighs, butt or any other “stubborn” bodypart&hellip;</p>
        <p>You see, when your body detects that your existing fat cells are nearly full, it sends out signals for you to <u>create NEW fat cells</u>. This is called <u>fat cell <b class="bg-yellow marker">hyperplasia</b></u>. And it’s truly the curse of any woman or man who has ever gained excess fat mass.</p>
        <p>As a result you <u>always</u> have <b>MORE fat cells</b> than before you gained weight, <u>even if you lose the weight later</u>&hellip;</p>

        <div class="insertion center" style="margin: 2em 0;">
            <center><img class="mobile-full-width" src="web/i/visual-fc-multiplication.png" width="80%" alt=""></center>
            <label>Fig2. Hypertrophy and Hyperplasia In Human Adipose Tissue</label>
        </div>

        <p><big><b>That means that everyone who is now or ever has been overweight, <u>is cursed with <span class="bg-yellow marker">MORE FAT CELLS</span></u></b></big> than people who have never been overweight.</p>
        <p>And to add insult to injury… when you manage to drain your extra fat cells… they continually cry out to your body to be refilled. Which means you’re <u>hungry all the time</u> and constantly fight with <u>cravings</u>!</p>
        <p>Which is why you suffer <b><i>“rebound weight gain”</i></b> and can NEVER seem to keep the weight off easily and permanently. (<b class="cl-red">Until now</b>&hellip; which I’ll explain in a minute. First a warning&hellip;)</p>

        <h3><b><span class="cl-red">Warning</span>: You May Have Suffered Permanent Dieting Damage</b></h3>

        <p>Listen: I need to tell you right now that if you’ve lost weight and regained it all. Or if you’ve struggled to get rid of stubborn fat that never budges. It’s simply <b>not your fault</b>.</p>
        <p>I’ve seen this so many times it makes me want to rip my hair out. Yet the <b>truth is</b> that most popular diet programs are designed to make you feel like they are working by allowing you to lose a lot of weight quickly. What they don’t tell you is that once you stop the diet, <u>9 out of 10 people will suffer a <b>slingshot</b> effect</u>.</p>
        <p>Because the weight came off so quickly certain shifts take place with your metabolism. Long story short&hellip; you’re hungry as heck and your body is PRIMED to overfeed and store those calories as quickly as possible.</p>
        <p>Think of it this way, basically you’ve just simulated a period of famine. As far as your body is concerned you just survived a deadly food shortage and now it’s time to stock up while the going is good in case it happens again.</p>
        <p>Which is why most folks who lose a bunch of weight on a popular diet end up gaining back MORE fat than they lost in the first place. And&hellip;</p>
        <p><u>The end result is fat cell <b>hyperplasia</b></u> &mdash; you end up with more fat cells than you had when you started. Repeat this process enough times and <u>it just gets harder and harder to lose weight</u> and keep it off.</p>

        <div class="insertion center" style="margin: 2em 0;">
            <center><img class="mobile-full-width" src="web/i/visual-body-fat-mass.png" width="80%" alt=""></center>
            <label>Fig3. Correlation Of Adipocyte Addition With Increased Cell Volume</label>
        </div>

        <p><u><b>Until recently</b></u> this would have been hopeless. And I would have told you you’re just cursed to work harder and deprive yourself more and more of the pleasures of good food. However&hellip;</p>
        <p>Recently <b>revealed</b> scientific research going all the way back to the 1970s is allowing folks to actually <u><b>reverse the damage</b></u> by literally KILLING off fat cells naturally and effortlessly&hellip;</p>

        <hr>

        <h3><b>
            Set Your Fat Cells To Self-Destruct:<br>
            Introducing the ONLY Way To Destroy &amp; <span class="cl-red">Flush Away Adipocytes</span> From<br>
            Your Body For Permanent And <u><span class="cl-red">Irreversible</span> Fat Cell Removal</u>
        </b></h3>

        <p>Listen: Making weight loss irreversible is a 2 step process:</p>
        <p>First, you have to <u>empty your existing fat cells</u> and <b>shrink them down</b>. However, that’s not permanent fat loss. For fat loss to be permanent you need step two &mdash; <u><b>kill off those extra fat cells</b></u>.</p>
        <p>That’s why I wrote this letter for you. After years of agonizing over this essential piece of the weight loss puzzle, <u>I finally stumbled on the solution to actually killing fat cells</u> and flushing them from your body&hellip;</p>
        <p>The fancy science name is “<b>Apoptosis</b>”&hellip; It basically means “cell death” and that’s what you’ll be doing &mdash; laser targeting and <u>killing off those unwanted fat cells</u>. And it finally allows you to achieve the irreversible weight loss you desire and deserve.</p>
        <p>I was shocked to discover the scientific knowledge to kill fat cells has been around and <u>ignored</u> since the 70’s! It’s based on a shocking discovery made in 1970 by Drs Epstein and Oren while examining a 6 month old child who had a strange reaction to a popsicle&hellip;</p>
        <p>And even though it’s incredibly effective, only a small sliver of the medical community has taken advantage of it since then. One exception is an underground US Patent holder that has been sneakily helping over 5 million people to quietly flush away their excess fat cells since 2008.</p>
        <p>So why haven’t you heard about it? Because it’s something <u>only the wealthy could afford</u>. Starting at almost $1000 and ranging into several thousands&hellip; it just hasn’t been accessible for regular folks like you and me. However&hellip;</p>

        <hr>

        <h3><b>
            The <u>Permanent Weight Loss</u> Secrets Of The Wealthy Are Now<br>
            Available AND Affordable
        </b></h3>

        <p>I discovered how you can get the same results using simple methods that you just add to your daily routine, that <u>cost you next to nothing</u>! And the simple system I’ve put together will&hellip;</p>

        <ul class="check blue">
            <li><b>Kill at least 20% of the fat cells</b> in your most stubborn areas</li>
            <li>Reduce hunger and eliminate cravings by <b>destroying excess fat cells</b></li>
            <li>Eliminate rebound weight gain by <b>resetting your fat cell count</b> to normal levels</li>
            <li>Make your body slimmer and more toned by <b>expelling empty fat cells</b> WITHOUT requiring you to continue a weight loss diet</li>
        </ul>

        <div class="insertion center" style="margin: 2em 0;">
            <center><img class="mobile-full-width" src="web/i/visual-fc-reduction.jpg" width="100%" alt=""></center>
            <label>Fig 4. Permanent Reduction Of Adipose Tissue Via Apoptosis</label>
        </div>

        <hr>

        <h2 class="revealing"><b>
            <small>Revealing The <u><i>Fat Cell Killer</i></u> System:</small><br class="mobile-show">
            The World's First &amp; Only <span class="cl-red">Science</span>-Based<br>
            Protocols That Target &amp; <u>Kill Fat Cells</u> To<br>
            <span class="cl-red">Smooth</span> Your Trouble Spots,<br>
            Eliminate Food Cravings &amp; Provide<br>
            Truly <span class="cl-red">Permanent</span> <u>Fat Reduction</u>
        </b></h2>

        <h2 class="cl-red"><u><b><span class="bg-yellow">WITHOUT:</span> Diet or Exercise!</b></u></h2>

        <p><u>Now, <b>I confess</b> that it will take up to 12 weeks</u> to make your weight loss irreversible. It takes that long to run through the lifecycle of the fat cells and insure they are killed off.</p>
        <p>Yet in the long run&hellip; 12 weeks is a <u>tiny investment</u> to finally eliminate your most stubborn, hard to lose fat like your love handles.</p>
        <p>In fact, you can see below the results from a very, very interesting case study published in 2013. The man in the photo below had ONE love handle exposed to techniques designed to cause fat cell apoptosis. Two years later, the treated love handle was still visibly smaller than the non-treated love handle!</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/visual-visible-reduction.jpg" width="640" alt=""></center>
            <label>Fig 5. Visible reduction in adipose tissue following site-targeted apoptosis.<br> Image (b) is 2 years post-exposure.</label>
        </div>

        <div class="insertion right">
            <center><img class="mobile-3of4-width" src="web/i/ecover-fck.jpg?v=2" width="400" alt=""></center>
        </div>

        <p>To eliminate the chance of rebound weight gain and double &mdash; or even triple &mdash; the visual results of your weight loss, simply use the button below to get immediate access to my latest book, <u><b><i>Fat Cell Killer</i></b></u>.</p>
        <p>Listen: Like I said earlier, when you lose weight you shrink your fat cells, but you don’t LOSE fat cells.</p>
        <p>The fat cells shrink, but they don’t go away, so your <b>problem areas will ALWAYS be your problem areas</b>.</p>
        <p>Shedding fat cells, especially through <u>TARGETED killing of fat cells</u> has the potential to permanently smooth out stubborn fat areas like your love handles.</p>
        <p>This can double, even triple the <u>visual effects</u> of your weight loss, which is why I’m letting you have <i>Fat Cell Killer</i> for just a fraction of the regular $69.99 price tag for a limited time only.</p>
        <p>Simply use the button below to claim your exclusive access and lock in your incredible one-time only discount (limited to one per customer).</p>


        <p class="pricing">
             <strike>&nbsp;$69.99&nbsp;</strike>
             <strong>$29</strong>
             <span>Limited To One Per Customer</span>
        </p>

        <p class="purchase">
            <a class="button button-buy-link" href="<?= $buyLink; ?>1" data-id="1">ADD TO CART</a><br><br>
            <img src="web/i/payment-methods.jpg" width="260" alt="">
        </p>

        <hr>

        <h3><b>
            What Does It “Look” Like To<br>
            Kill Off Your Fat Cells Permanently?
        </b></h3>

        <p>Are you curious about what it LOOKS LIKE to kill excess fat cells without actually “burning” any fat? Here are some examples of “Apoptosis” in action&hellip;</p>

        <div class="before-after-container">
            <ul class="before-after">
              <li><img src="web/i/before-after/01.jpg" alt=""></li>
              <li><img src="web/i/before-after/02.jpg" alt=""></li>
              <li><img src="web/i/before-after/03.jpg" alt=""></li>
              <li><img src="web/i/before-after/04.jpg" alt=""></li>
              <li><img src="web/i/before-after/05.jpg" alt=""></li>
            </ul>
        </div>

        <p>These are actual examples of the <u><b>visual difference</b></u> it makes when you destroy and flush away unnatural and unhealthy excess fat cells. However&hellip;</p>
        <p>Remember: The faster you empty and shrink your fat cells, the more quickly you’ll be able to trigger the death and removal of your fat cells. Which is why I also want to give you two free gifts today to make your transformation very fast and very simple.</p>

    </section>
</main>

<main class="cl-white bg-blue bonus">
    <section style="overflow:hidden;">

        <h3><span>FREE Bonus Gift #1</span></h3>
        <p>Accelerate Fat Cell Shrinkage By Quickly Emptying Your Fat Reserves Using The</p>
        <h2 class="ta-left"><b>Fat Cell Killer 7-Day Rapid Fat Loss Calculator</b> <small>(<strike>&nbsp;$99&nbsp;</strike> Value)</small></h2>

        <div class="insertion right" style="margin-top: 1em;">
            <center><img class="mobile-half-width" src="web/i/fck-calculator.png" width="260" alt=""></center>
        </div>

        <p>Remember, the more triglycerides you can remove from your fat cells, the faster the process of apoptosis can happen. I created the Fat Cell Killer 7-Day Rapid Fat Loss Calculator to make shrinking your fat cells very easy, very painless and really quick.</p>
        <p>Simply plug in a few pieces of information about your weight, height, activity levels, etc and I’ll tell you exactly how much you should be eating to lose weight as quickly as possible without dipping into the diet danger zone.</p>
        <p>This is the exact formula I use with my consulting clients that they pay $99 a month for. Yet you’re getting it as a free gift because I want Fat Cell Killer to be a great, great success story for you!</p>
        <br>

        <hr>

        <h3><span>FREE Bonus Gift #2</span></h3>
        <p>Trim And Firm Your FORMER Trouble Spots With </p>
        <h2 class="ta-left"><b>The 7-Minute Fat Cell Killer Cardio Collection</b> <small>(<strike>&nbsp;$19.50&nbsp;</strike> Value)</small></h2>

        <div class="insertion right" style="margin-top: 1em;">
            <center><img class="mobile-half-width" src="web/i/ecover-7min.png" width="250" alt=""></center>
        </div>

        <p>Look, you don’t have to exercise to kill off your fat cells. However, if you want to speed up the process as much as possible you should drain your fat cells of triglycerides as quickly as you can.</p>
        <p>One low-stress way to do that is using this specific method of simple 7-Minute cardio routines you can do whenever you have the time. They’re specifically designed for women and men in their 40s, 50s, and 60s to double fat burning without stressing your joints or putting wear and tear on your body.</p>
        <p>These are the exact quick workouts I use for myself when I want to speed up weight loss. They have the added benefit of tightening and toning your whole body. And they work so well for me I wanted to make sure you get access too.</p>
        <p>And there’s no need to go to a gym or own fancy equipment, so you can get started right away and start shrinking your fat cells 7 minutes at a time.</p>
        <br>

    </section>
</main>

<main>
    <section>

        <p>Once your fat cells begin to shrink you’ll be able to accelerate and lock-in the PERMANENT benefits by applying the simple steps outlined in the <i>Fat Cell Killer</i> protocols &mdash; that you have <b>exclusive</b> access to here on this page only.</p>
        <p>Remember: For a one-time investment of just $29 you get everything &mdash; the Fat Cell Killer ebook (a $69 value on it’s own) PLUS the <u><i>Fat Cell Killer 7-Day Rapid Fat Loss Calculator</i></u> PLUS the <u><i>7-Minute Fat Cell Killer Cardio Collection Workouts</i></u>.</p>
        <p>And I didn’t even mention your most valuable free gift yet&hellip;</p>
        <p>You’re also getting 21 days <u>FREE</u> access to my exclusive <u>email coaching service</u> I call The Advocate.</p>
        <p>To guarantee access to everything at today’s low price simply use the button below to add it to your order right now.</p>


        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/bundle-of-everything.jpg" width="480" alt=""></center>
        </div>

        <p align="center"><big><b>Secure Everything &mdash; A $97.50 Value &mdash; For Just $29 Today</b></big></p>

        <p class="purchase">
            <a class="button button-buy-link" href="<?= $buyLink; ?>2" data-id="2">ADD TO CART</a><br><br>
            <a class="plink button-buy-link" href="<?= $buyLink; ?>3" data-id="3">Add To Cart &mdash; Just $29</a><br><br>
            <img src="web/i/payment-methods.jpg" width="260" alt="">
        </p>

        <hr>

        <h3><b>
            <span class="cl-red">Remember</span>:<br>
            Fat Cell Killer Is Guaranteed To Work Or Your Money Back
        </b></h3>

        <p>As with all my books and workout programs, when you invest in Fat Cell Killer today you’re covered by my iron-clad 100% no-questions-asked guarantee.</p>
        <p>In fact, you have 60 days to prove to yourself it is working for you. If at any time before then you decide it’s not for you &mdash; for any reason &mdash; just let me or someone on my team know and you’ll get every penny back within 24 hours.</p>
        <p>You may wonder how I can make such a strong promise and guarantee &mdash; it’s because I’ve seen these protocols work time and again and the evidence is overwhelming that it will work just as well for you too.</p>
        <p>Which means I feel obligated to remove every barrier that might stand in your way of trying this powerful system for yourself. So I’m happy to take all the risk off you and place it on myself.</p>
        <p>Go ahead, try out Fat Cell Killer and enjoy the results first hand without any risk for 60 full days.</p>

        <p class="pricing">
             <strike>&nbsp;$97.50&nbsp;</strike>
             <strong>$29 Today</strong>
        </p>

        <p class="purchase">
            <a class="button button-buy-link" href="<?= $buyLink; ?>4" data-id="4">ADD TO CART</a><br><br>
            <a class="plink button-buy-link" href="<?= $buyLink; ?>5" data-id="5">Add To Cart &mdash; Just $29</a><br><br>
            <img src="web/i/payment-methods.jpg" width="260" alt="">
        </p>

        <hr>

        <h3><b>
            3 Health Risks You’re Exposed To From Adipocyte Proliferation
        </b></h3>

        <p>I admit that my main motivation was just to <b>look good</b> when I started going down the rabbit hole of the science behind the Fat Cell Killer protocols. However I quickly realized that you just can’t afford to ignore the growing health problem associated with surplus fat cell accumulation.</p>
        <p>Listen carefully: if you are, or have ever been more than 15 lbs overweight then the number of fat cells you are carrying around in your body right now is <u><b>not natural</b></u>. It is beyond what your human evolution had in mind for you. And&hellip;</p>
        <p>That causes a ton of health problems that make how you look naked pretty irrelevant. There are actually dozens of direct risks you’re facing but I need to point out the 3 worst offenders so that you are armed with this critical information.</p>

    </section>
</main>

<main class="bg-ligngray risks">
    <section style="overflow:hidden;">

        <h3><span>#1 Health Risk Of Adipocyte Proliferation</span></h3>
        <h2 class="ta-left"><b>Type-2 Diabetes</b></h2>

        <p>In 2009, diabetes was the seventh leading cause of death in the United States. I list it as the #1 health problem because it’s also a major cause of other diseases such as heart disease, kidney disease, stroke, amputation, and blindness. <b>An increase in Adipocyte count</b> causes your cells to change, making them resistant to the hormone insulin. Insulin carries sugar from blood to the cells, where it is used for energy. When a person is insulin resistant, blood sugar cannot be taken up by the cells, resulting in high blood sugar and eventually Type-2 Diabetes.</p>

        <hr>

        <h3><span>#2 Health Risk Of Adipocyte Proliferation</span></h3>
        <h2 class="ta-left"><b>High Blood Pressure</b></h2>

        <p>Every time your heart beats, it pumps blood through your arteries to the rest of your body. Blood pressure is how hard your blood pushes against the walls of your arteries. When that pressure is too high it leads to serious problems, such as heart disease, stroke, and kidney failure to name a few. Having <u><b>a surplus of fat cells</b></u> means your larger body size increases blood pressure because your heart needs to pump harder to supply blood to your whole body. Excess fat mass may also damage your kidneys, which help regulate blood pressure.</p>

        <hr>

        <h3><span>#3 Health Risk Of Adipocyte Proliferation</span></h3>
        <h2 class="ta-left"><b>Depression and Low Self-Esteem</b></h2>

        <p>Folks with obesity have a <u>30% higher risk of mental health issues</u>. Fat tissue is linked to producing higher levels of the stress hormone cortisol. And as you increase the number of fat cells in your body you increase these stress hormones. So you have a chemical introduced by fat tissue that, in high amounts, is linked to depression. And of course being overweight is also associated with a ton of cultural stigmas that can lead to feelings of depression and low self-esteem, so fat cell proliferation is a double whammy when it comes to mental health.</p>
        <br><br>

    </section>
</main>

<main>
    <section>

        <p>And believe me, those three risks are the tip of the iceberg&hellip;</p>
        <p>I don’t know about you, but I’m not willing to risk letting my health fall apart like that. Especially since the solution only takes a little patience, and doesn’t even require a special diet or exercise.</p>
        <p>Do you think the risks are worth ignoring? Look: If you are still not sure whether the Fat Cell Killer system is something you need, just ask yourself this one simple question&hellip;</p>

        <hr>

        <h3><b>Now: Just Ask Yourself This One Simple Question</b></h3>

        <p>There’s a super simple way to know if the <i>Fat Cell Killer</i> system is for you.</p>
        <p>Ask yourself one question: <i>How many times have you lost weight and gained it all back and more?</i></p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/visual-adipocyte-surplus.jpg" width="720" alt=""></center>
        </div>

        <p>If it’s only once or twice, and you really only have around 10 lbs to lose, then you probably have not experienced a lot of hyperplasia and you have a pretty normal number of fat cells. Congrats! You can likely get away with any old conventional weight loss diet.</p>
        <p>However, <b>if you’ve lost and gained back weight several times</b>. And if you have <u>more than 10 lbs to lose</u>. You have probably suffered <u>a lot of hyperplasia</u> of your fat cells and you are now cursed with more than you fair share of them.</p>
        <p>Because you have more fat cells now, losing weight is much, much harder AND keeping it off requires way more work than it used to.</p>
        <p>Which means every time you gain back the weight, next time you will need MORE discipline to lose it again. I hope you can see that it’s a losing battle.</p>
        <p><b>It does not have to be like that</b> though&hellip;</p>

        <hr>

        <h3><b>Your Age Does NOT Make It Harder To Lose Weight</b></h3>

        <p>If you’re like most folks you might think that losing weight just gets harder as you get older.</p>
        <p>However as we’ve seen it’s NOT because you’re getting older. It’s because you have accumulated more and more fat cells over the course of your life.</p>
        <p>The more times you go through the cycle of losing and gaining back weight. And the higher the scale creeps each time. The more difficult it gets to lose weight.</p>
        <p>Which means as time goes by weight loss gets harder — not because you are older but because over the years you have accumulated thousands of extra, unnatural and unhealthy fat cells.</p>
        <p>This may sound depressing, yet it should actually make you really, really HAPPY because&hellip;</p>

        <hr>

        <h3><b>
            Turn Back The Clock: Erase Middle Age POT Belly<br>
            And Make Losing Fat EASY <u>At Any Age</u>
        </b></h3>

        <p>Just think about it. Whether you’re 27 or 77&hellip;</p>
        <p>If you had way less fat cells to fill. Fewer hungry bags of blubber calling out to be fed. Fewer pounds of ugly and unnatural fat tissue weighing down your body&hellip;</p>

        <ul class="check blue">
            <li>How much EASIER would it be for you to stay slim and toned for your entire life?</li>
            <li>How much EASIER would it be to enjoy food again without guilt?</li>
            <li>How much EASIER would it be to not think about food all the time because you’re not really hungry and you no longer suffer from constant cravings!</li>
        </ul>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/visual-happy-couple.jpg" width="260" alt=""></center>
        </div>

        <p>The bottom line is this. Shrinking and then destroying excess fat cells is the ONLY way to lock in permanent fat loss and effortless weight management for life.</p>
        <p><b>No matter how old you are</b>, you can do this. It doesn’t require any special dieting or exercise.</p>
        <p>And <b>if you have a lot of weight to lose</b>, it’s even MORE important for you to take back control of your metabolism so you can manage your weight EASILY again!</p>
        <p>All you have to do is decide to take back control today. If you want to return your body to a natural balance where you have just enough fat cells to maintain a healthy and normal metabolism then the choice is so, so simple.</p>
        <p>Just click on the button below right now to secure the only system that guarantees permanent reduction in your fat cell count so that you enjoy shocking visual changes in your body that will last a lifetime.</p>

        <p class="pricing">
             <strike>&nbsp;$69.99&nbsp;</strike>
             <strong>$29</strong>
             <span>Limited To One Per Customer</span>
        </p>

        <p class="purchase">
            <a class="button button-buy-link" href="<?= $buyLink; ?>6" data-id="6">ADD TO CART</a><br><br>
            <img src="web/i/payment-methods.jpg" width="260" alt="">
        </p>

        <hr>

        <h3 class="ta-left"><b>About The Author</b></h3>

        <p>Hi, my name is Brad Pilon. Thanks so much for reading this letter. In a way everything I’ve done in my life has been leading up to me writing this for you&hellip;</p>
        <p>You could say my claim to fame is my first bestselling book, <i>Eat Stop Eat</i>. Yet my fascination with health and nutrition started many years before.</p>
        <p>When I was just 14 &mdash; when other Canadian kids my age were playing hockey &mdash; I had a subscription to the <i>American Journal of Clinical Nutrition</i>. And by age 16 I was selling supplements to all the local bodybuilders.</p>
        <p>I graduated University with honors in Applied Human Nutrition.</p>
        <p>My first Job out of University was at one of the top supplement companies IN THE WORLD where I worked my way up to <b>Research &amp; Development Manager</b>. I got to travel all over the world meeting with leading nutrition researchers. Not only was I attending major scientific conferences, I was hosting them!</p>
        <p>I sat with top executives from multi-national food ingredient companies. I had an unbelievable amount of access into the scientific world of health and fitness. I had meetings with Mr. Olympia champions, dinner with Arnold Schwarzenegger and workouts with 400 pound strongman competitors.</p>
        <p>It was really terrific. But <b>I knew there was more!</b> I knew there were answers I still needed. Answer we all needed. Because I could see so many people suffering with weight problems.</p>
        <p>So I left a great, great job and went back to school for post-grad studies. Long story short; I discovered that a lot of things I believed to be true were false. AND I discovered that a lot of what I thought to be false was true.</p>
        <p>And ever since then I spent my time searching. My passion is discovering what makes your body and my body tick. What makes you lose weight or gain it back. What makes you strong or weak.</p>
        <p>And I’m very grateful, very thankful for my single-minded need to make sense of how the body works. Because finally in the last 18 months I’ve chased down the trail that has lead me at last to understand and solve the tragedy of Rebound Weight Gain. As you read in this letter the key to this frustrating epidemic is something we’ve known about and ignored for decades. And I’m so excited that I can finally share the science and the solution with you now!</p>

        <div class="insertion left">
            <center><img src="web/i/brad-photo.jpg?v=2" width="100" alt=""></center>
        </div>

        <p>Your friend and advocate,</p>
        <p>Brad Pilon</p>
        <p><img src="web/i/brad-sign.png" alt=""></p>


    </section>
</main>

<footer class="cl-white bg-blue">
    <section>

        <p class="logo"><img src="web/i/head-logo.png" width="140" alt=""></p>
        <p class="support">Contact us: <a href="mailto:help@clkbooks.com">help@clkbooks.com</a></p>
        <p class="cbinfo">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.
        </p>
        <p class="rights">
            <a href="http://clkbooks.com/privacy">Privacy Policy</a> &nbsp;|&nbsp;
            <a href="http://clkbooks.com/terms">Terms</a>
        </p>

    </section>
</footer>

<script>
    $(".button-buy-link").on('click', function () {
        var id = $(this).data('id');

        gtag('event', 'click', {
            'event_category' : '2106c-buy-button-' + id,
            'event_label' : 'control'
        });
    });
</script>
</body>
</html>
