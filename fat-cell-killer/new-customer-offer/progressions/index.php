<?php
//LOAD COOKIE SCRIPT
require_once($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/Tracking/upsell-analytics-vtid-logic.php';

$vtid = getVtid();
$vtid = resolveVTID($vtid, 'op', false);
// CB upsell buy links
$linkAccept = getClickbankBuyLink('149', null, 'eatstopeat', array(
    'cbur' => 'a',
    'vtid' => $vtid,
));
$linkDecline = getClickbankBuyLink('149', null, 'eatstopeat', array(
    'cbur' => 'd',
    'vtid' => $vtid
));

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Add Progressions</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=2">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=2">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=2">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

	<script>
	function showHideHelp() {
		e = document.getElementById('help-opts');
		if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
	}

	function resizeVideoPlayer() {
		var new_width = $('#video_player').width();
		var new_height = Math.floor(411*new_width/730);
		$('#video_player').height(new_height);
		//alert(new_width+' '+new_height);
	}

    function showWarning() { $('.warning-message').fadeTo('slow',1) }
    function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
	$(function(){
		// show warning message
		setTimeout('showWarning()', 1500); // 1500
		// show the rest of the content
		setTimeout('showContent()', 3000); // 3000
        // show all hidden items fast in case of "cbur" existence
        <?if(isset($_GET['cbur'])):?>
            $('.hidden').fadeTo('fast',1);
        <?endif;?>
		setTimeout('resizeVideoPlayer()', 3100);
	});

	$(window).resize(function() {
		resizeVideoPlayer();
	});

	</script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/tracking/ga-tracking.php'; ?>
</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h2><i class="cl-blue">If You Plan On Exercising, Add This To Your Order&hellip;</i></h2>
        <h1><b>
            Now You Too Can Get Triple The Results<br>
            From Exercise &mdash; In Half The Time &mdash; WITHOUT<br>
            Ever Hitting A Frustrating Plateau
        </b></h1>
        <h3 class="ta-center">
            Brad’s Personal Step-By-Step Exercise System &mdash; Designed To Enhance Fat Cell Killer &mdash;<br>
            Is Now Finally Available To YOU As An Exclusive Discounted Upgrade On This Page Only
        </h3>

        <div class="topbox">
            <p>On this page only you have access to an exclusive new ClickBooks price negotiated directly with the author of <i>Fat Cell Killer</i>. Review the details below before leaving this page.</p>
        </div>

        <p>Brad’s <b>Fat Cell Killer</b> is all about terminating and flushing away your body's fat cells. However you must EMPTY them first to make it work.</p>
        <p>The fast way to accelerate the emptying of your fat cells is the right kind of exercise. But who has time for hours in the gym? Which is exactly why Brad has also pioneered a very minimalist approach to exercise.</p>
        <p>So if you want the most results from the least time commitment. And if you never want to worry about hitting a plateau in your workouts. And ESPECIALLY if you never, ever want to have to think about or wonder if you are doing the right thing or progressing as quickly as you can&hellip;</p>
        <p>&hellip;then Brad’s <u><b>done-for-you</b></u> exercise system is everything you’ve been looking for!</p>
        <p>It’s called Progressions. And it <b>fixes</b> the biggest problem with almost every other workout program. Let me explain… pretty much any new program will work for a while. But after 5 to 6 weeks nearly everyone will stop making progress. It’s what’s called a plateau. And even most experienced trainers make the mistake of assuming it is normal&hellip;</p>
        <p>However Brad is never happy with accepting assumptions at face value. And through extensive research and experimentation he has found the solution. Here’s a quick breakdown&hellip;</p>

        <h2>Exposed: Why Most Workout Programs Fail</h2>

        <p>First, by looking at the maximum reps you can do at a given weight on any exercise, you can estimate the maximum weight you could use for <u>one single rep</u>. This is called your Estimated 1-Rep Max (<b>e1RM</b>). For example&hellip;</p>
        <p>Imagine a 160 lb male who can bench press 205 lbs for 8 reps. Using a special formula we can determine his bench press <b>e1RM</b> as 259.66 lbs today. Now&hellip;</p>
        <p>A traditional exercise program would require this guy to <u>lift the same 205 lbs for 9 reps</u> on his next session. Sounds easy enough, right? However, you’re about to see <b>how crazy that is</b>&hellip;</p>
        <p>As we already know, being able to bench press 205 for 8 good reps is the equivalent of having a e1RM of 259.66 pounds. Yet jumping to 205 lbs for 9 good reps is the equivalent of having an e1RM of 266.49 pounds. <b>That’s around <u>a 7 pound JUMP</u> in maximal potential strength in one session!</b></p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/schema-1.png" width="850px" alt=""></center>
        </div>

        <p>Now&hellip; you might be able to do that once or twice&hellip; especially IF you are brand new to weight training. However there is no way anyone can sustain that pace. Which is why you plateau on nearly every exercise program!</p>
        <p><b>Instead</b>, what if we could increase your e1RM by only a pound or even less <u>every single session</u>? Yet you do that again and again every time you exercise! No more stalling at the same weight for weeks at a time! And in the long run, extraordinary progress&hellip;</p>

        <h2 style="font-size:30px;">Solution: Incremental Progressions Trigger Massive <u>Fat Loss</u> &amp; <u>Strength</u> Gains</h2>

        <p>You see, even if each increase is small, it still <u>triggers powerful <b>cellular signalling</b></u> that forces your body to respond by getting both stronger AND leaner!</p>
        <p><u>YES</u>, <span class="bg-yellow">the exact same <b>cellular signalling</b> triggers BOTH your <b>strength</b> gains AND your <b>fat loss</b></span>.</p>
        <p>Cool right?</p>
        <p><u>Each one of these tiny progressions triggers that cellular signalling</u> so that you keep getting a little bit stronger and <b>looking</b> a little bit <b>leaner</b> every single week.</p>
        <p>Compare that with what you see when you walk into the gym. If you’ve been going to the same gym for a while, pick someone who is there all the time, and that you’ve been seeing there for months. Now answer this&hellip;</p>
        <p><b>Do they look any different now</b> than they did several months ago? If not, then what is the point of being there? They are spinning their wheels on a meaningless hamster wheel of wasted effort!</p>
        <p>Here’s what’s happening&hellip; Those poor folks are only experiencing that cellular signaling every once and awhile when they manage to bust a plateau. Sure&hellip; the signal may be stronger on each of those occasions. But it is short lived and too rare to cause noticeable changes in their physique.</p>
        <p>Now imagine this&hellip; using a carefully crafted and very specific sequence of reps and weights for every single workout, you end up constantly triggering these cellular signals so you get a visible difference every single week that you can actually SEE in the mirror and FEEL in the gym!</p>
        <p>You get just a little bit stronger and a little bit leaner after <u>every single workout</u> &mdash; yet it accumulates so quickly that it may get you into trouble&hellip;</p>

        <div class="insertion center">
            <center><img class="mobile-full-width" src="web/i/schema-2.png" width="560px" alt=""></center>
        </div>

        <p>Look: If someone sees you at the gym today, and then doesn’t see you again until a few weeks from now, that person’s jaw will drop at your visible transformation. And to be honest&hellip;</p>
        <p>Because we’re conditioned to expect little to no visible improvement from week to week, they may suspect you’re doing something sneaky like taking drugs! Afterall traditional workouts just can’t keep sending the signals that make you leaner and stronger. After a few workouts, you get stalled and can’t make any more progress. And&hellip;</p>
        <p>You end up on a plateau for weeks and even months. And the worst part is&hellip; you are told by all the experts that it’s NORMAL for you to stay stuck like that. Nonsense!&hellip;</p>

        <h2>A NATURAL Way To Get The Same Results As The Pros (or Cheaters)</h2>

        <p>Which is why Brad Pilon’s brand new Progressions methodology is so revolutionary. It gives <b>you</b> the <u>same rapid progress</u> as an athlete using performance enhancing drugs or a <u><b>pro fitness model</b></u> spending hours in the gym.</p>

        <div class="insertion right">
            <center><img class="mobile-full-width" src="web/i/schema-3.jpg" width="420" alt=""></center>
        </div>

        <p>In fact, when Brad first discovered and started experimenting with this groundbreaking approach to exercise, he was able to switch from his former bodybuilding style of training &mdash; including up to 2 hours a day, 5 to 6 days a week &mdash; and replace it with just <u>3 sessions per week of LESS than one hour</u>&hellip;</p>
        <p>Plus, he lowered his body fat percentage AND increased his strength numbers while doing less than HALF the exercise volume he was doing before!</p>
        <p>Not only that, every Progressions workout is super FUN because you <b>ALWAYS make <u>progress</u></b>. You’re always <u>hitting a new high</u>. And&hellip;</p>

        <h2>
            Today You Can Add Progressions To Your Order &mdash; On This Page Only<br>
            &mdash; At The Exclusive Insider <span class="cl-red">Discount</span> For Fat Cell Killer Customers <span class="cl-red">Only</span>!
        </h2>

        <div class="insertion left">
            <center><img class="mobile-half-width" src="web/i/progressions-3d-new.jpg" width="200" alt=""></center>
        </div>

        <p>Because Progressions is designed specifically to enhance the effects of Fat Cell Killer, Brad insists that we give you the opportunity to add it to your order today at a significant savings.</p>
        <p>Simply use the button below to add Progressions to your order and you’ll secure the one-time discounted price of only $19. However if you leave this page please understand that you’ll pay the regular retail price of $37 to get your hands on the system in the future.</p>
        <p>Act now to lock in your exclusive 49% OFF and we’ll add Progressions to your member dashboard so you get immediate access today for only $19.</p>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="<?= $linkAccept; ?>1">Add To Order!<i></i></a>
                <span class="accepted"><img class="mobile-3of4-width" src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="<?= $linkAccept; ?>2">Add Progressions To My Order!</a></span>
            </p>
        </div>

        <p><b>Remember:</b> All Brad’s programs are covered by the CLK Books 100% money-back guarantee. You have 60 full days to feel and experience the results of the Progressions system for yourself. If for any reason you aren’t thrilled just let us know before the end of 60 days and we’ll make sure you get a full refund the very same day!</p>
        <p>Just use the Add To Order button to try it out for yourself, risk-free&hellip;</p>

        <div class="cart">
            <p class="purchase">
                <a class="button" href="<?= $linkAccept; ?>3">Add To Order!<i></i></a>
                <span class="accepted"><img class="mobile-3of4-width" src="web/i/purchase-accepted-cards.png" alt=""></span>
                <span class="link"><a href="<?= $linkAccept; ?>4">Add Progressions To My Order!</a></span>
            </p>

            <hr>

            <p class="discard">No thanks. I don’t need a simple exercise system that delivers consistent results and enhances Fat Cell Killer. I appreciate the offer, but <a href="<?= $linkDecline; ?>">I’ll pass on having my own done-for-you exercise system.</a>.</p>
        </div>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright 2017 &copy; Clkbooks.com<br>
        For support please contact support [at] clkbooks.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
