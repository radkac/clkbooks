<?php
require($_SERVER['DOCUMENT_ROOT'].'/checkreturning.php');
require($_SERVER['DOCUMENT_ROOT'].'/fetchdata.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>New Health Alert: How To Eliminate The Gut Toxins Formed By The Foods You Eat Every Day</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=1">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=1">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=1">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showHideHelp() {
	e = document.getElementById('help-opts');
	if(e) e.style.display = (e.style.display!='block') ? 'block' : 'none';
}

function resizeVideoPlayer() {
	var new_width = $('#video_player').width();
	var new_height = Math.floor(411*new_width/730);
	$('#video_player').height(new_height);
	//alert(new_width+' '+new_height);
}

function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
	setTimeout('resizeVideoPlayer()', 3100);
});

$(window).resize(function() {
	resizeVideoPlayer();
});

</script>

<?php require($_SERVER['DOCUMENT_ROOT'].'/eztrackingcode.php'); ?>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="/web/i/ese-upsell-banner-new.png">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>
            New Health Alert:<br>
            <i class="cl-red">How To Eliminate The Gut Toxins Formed By<br>
            The <u>Foods You Eat Every Day</u></i>
        </h1>

        <div class="topbox">
            <p>On this page only you have access to an exclusive new ClickBooks price negotiated directly with the author of <i>Fat Cell Killer</i>. Review the details below before leaving this page.</p>
        </div>

        <p>Hey, Brad here. This is an important update based on shocking research I stumbled upon in the past year. Believe me, this will change the way you eat to lose weight and stay healthy&hellip;</p>
        <p>The reason I'm including this is to help you empty even more fat cells that you can then flush out of your body using your brand new Fat Cell Killer program. As you're about to see, a few very subtle shifts to your diet could help you quickly lose weight more easily and enjoyably without extra "dieting."</p>
        <p>Did you know certain foods trigger the production of <b>toxins</b> right inside your digestive system? And that <u>these toxins can actually cause weight gain, diabetes, heart disease</u> and more?</p>
        <p>And did you know some subtle shifts in what you eat, or even how you combine certain foods, <b>can eliminate a huge amount of these “<u>Obesity Toxins</u>”</b> from ever forming or being absorbed in your body?</p>
        <p><b>Obesity Toxins: The REAL Reason Food Makes You Fat &amp; Sick</b></p>
        <p>Common wisdom boils digestion down to a simple equation:</p>
        <p><i>Eat food + digest food + absorb calories and nutrients.</i></p>
        <p>The truth is, the latest science demonstrates that the process is MUCH more complex. And that calories and nutrients are just a small part of what’s happening when you eat.</p>
        <p>You’ve probably heard that your gut is full of bacteria, right? And that “good” bacteria is important for your health. And that’s <u>why probiotics have become so popular</u> all of a sudden.</p>
        <p>However&hellip; Although it’s important to improve the bacterial balance in your gut, there’s a much more immediate danger to your health that you need to take care of right now&hellip;</p>
        <p>It’s the <span class="bg-yellow">TOXINS being formed inside your digestive system as we speak</span>.</p>
        <p>These are referred to as “Endotoxins” because they are created right inside your own body. (Toxins from outside your body are called "Exotoxins".)</p>
        <p>And seemingly harmless foods you swallow right now can quickly be turned into poisonous Obesity Toxins by your own gut, causing everything from obesity to diabetes, cancer, heart disease, autoimmune disease and more&hellip;</p>

        <p><b>Get Instant Relief So You Lose Weight Faster &amp; Rebuild Your Health Right Now</b></p>
        <p>Look: There are hundreds books on the shelves nowadays dealing with “gut health.” Yet the problem with ALL of them is they only focus on the <u><b>long</b></u> term process of improving the balance of your gut bacteria. However&hellip;</p>
        <p>My main goal with <b><i>Good Belly, Bad Belly</i></b> is to provide you with simple <u class="bg-yellow">things you can start doing today</u> to get <u><b>immediate relief</b></u> from the damage Endotoxins are doing to your body and your health right now.</p>
        <p><u><b>Even if you keep eating 90% of the foods you are eating already</b></u>, there are subtle shifts you can make in what you are eating WITH those foods that will make a huge reduction in the formation and absorption of endotoxins in your system.</p>
        <p>Listen: you can read through the entire <i>Good Belly, Bad Belly</i> book in only a couple hours and start applying these life changing tricks right away.</p>
        <p>This isn’t stuff you have to wait months to benefit from. It’s action steps you can take today to prevent endotoxins from chipping away at your health and adding inches to your waistline. And it doesn’t require you to make painful changes to your entire diet.</p>
        <p><i>Good Belly, Bad Belly</i> is hot off the presses and reveals the latest science on how to lose weight and increase your health without “dieting.”</p>
        <p>Now, because you are a brand new customer, I came to an exclusive agreement with ClickBooks to let you have <i>Good Belly, Bad Belly</i> for only $7 today! Just make sure you take advantage of this one-time special offer because it is only available here at ClickBooks.</p>

        <div class="purchase-holder">
            <div class="insertion center purchase">

                <img class="ecover mobile-2of3-width" src="web/i/gbbb-ecover.jpg" alt="">

                <table align="center" class="costs" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>List Price:</td>
                        <td>$ 29.00</td>
                    </tr>
                    <tr>
                        <td><b>CLK*Book Price:</b></td>
                        <td><b>$ 7*</b></td>
                    </tr>
                    <tr>
                        <td>You Save</td>
                        <td>$ 22.00 (76% OFF)</td>
                    </tr>
                    <tr>
                        <td colspan="2">*Includes free international wireless delivery</td>
                    </tr>
                </table>

                <a class="button" href="http://153.eatstopeat.pay.clickbank.net/?cbur=a">Add <em>Good Belly, Bad Belly</em><br> To My Order For Only $7</a>

            </div>
        </div>

        <hr>

        <p class="discard">No thanks. I understand the information in this book is cutting edge stuff that will help me reclaim my lose weight more easily and increase my health without “dieting” &mdash; and that this ClickBooks exclusive opportunity is the only way to get this vital information for only $7 &mdash; however I will try to figure this out on my own. <a href="http://153.eatstopeat.pay.clickbank.net/?cbur=d">I will take a pass on this exclusive one-time-offer</a></p>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.</p>

    <p>
        Copyright 2014 &copy; EatStopEat.com<br>
        For support please contact support [at] eatstopeat.com<br>
    </p>

</div><!-- #footer -->

</body>
</html>
